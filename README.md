# **Council of Four** #

This is the repository of the group ps07, containing the final project for the final assessment of the course *Prova Finale*, part of the course *Software Engineering* at *Politecnico di Milano* for the academic year 2015/16.

## **Repository Folders:** ##
* **src/** : contains the Java source code of the project.
* **docs/** : contains resources unrelated to the source code such as the ```ConfigurationFile.xml``` and the UML diagrams.
* **docs/maps** : contains the ```.txt``` maps supported by this version of the game. 

## **Guide:** ##

### Start the server ###
To play the game, you first need to start the server by running```src/main/java/it/polimi/ingsw/ps07/view/gamemanager/GameManager.java ```,   
the server will start and will be listening for incoming **RMI** or **SOCKET** connections.


### Start the client ###
To play a game on the client side, run ```src/main/java/it/polimi/ingsw/ps07/view/gameplayer/GamePlayer.java```, you will be asked to choose between **RMI** or **SOCKET** connection: 

* Insert ```RMI``` to use the *Remote Method Invocation*.
* Insert ```SOCKET``` to use the *Socket Connection*.

Once the first player connects to the server, the following players will be connected to the same game lobby. When at least two players join the same lobby, a timer of 10 seconds will be started. If the timer reaches 0 before a new player joins the lobby, a game will be created, otherwise the new player will be added to the same lobby. We were not given an upper bound for the maximum number of players, so the only condition to create the game is to let the timer expire, without receiving any new connection request.

In this phase of the game the player who created the lobby, hence the first one to request a connection to the server, has the chance to change the map on which the game will be played, by using the command ```changemap <mapname>```, where ```<mapname>``` can be ```mapA``` or ```mapB```. 

The server supports multiple instances of the game, so, if a new player requests a connection to the server, while the game is already running, a new instance of the lobby will be created with the same rules previously explained.

## **Game Commands: ** ##

Commands are not key-sensitive and parameters marked with ```*``` are optional.

The following command is available only while in the lobby:

* ```changemap <mapname>```: to change the default ```mapA``` to a new one.

The following commands are available before starting and after creating the game:

* ```chat <message>```: to send a broadcast text ```<message>``` to all the players who are joining the same game.

* ```command```: to show the complete list of commands.

The following commands are available only when the game has been created and during the game phase:

* ```map```: to show the map, with cities and their attributes, the king's position marked as ```♛``` and the various bonuses on the map.

* ```data```: to show the private data of the player, such as his politics cards, permit tiles, assistants, progression on various paths and other useful information.

* ```pool```: to show the pool of unused councillors.

* ```balcony```: to show the current status of the four balconies.

* ```tiles```: to show the permit tiles of each region.

* ```usedtiles```: to show the used permit tiles of the player.

* ```market```: to show the market insertions, sorted by type-of-insertion.

The following commands are available only during the game phase, according to the specific [game rules](https://home.deib.polimi.it/pourhashem.kallehbasti/materials/Project/Rules/CoF-en.pdf):

* ```PA```: to pass a quick action.
### Slow Actions: ###
* ```APT <balconyRegion permitTileIndex card1 card2* card3* card4*>```: to 
*Acquire a Business Permit Tile*, where ```balconyRegion``` is the name of the region the chosen balcony belongs to, ```permitTileIndex``` is the index of the permit tile you want to use, ```card1``` is the color of the first politics card you are using, ```card2*``` is the color of the second politics card you are using, ```card3*``` is the color of the third politics card you are using and ```card4*``` is the color of the fourth politics card you are using.

* ```BE <permitTileIndex city*>```: to *Build an Emporium using a Permit Tile*, where ```permitTileIndex``` is the index number (from ```1``` to ```N```) of the permit tile you want to use and ```city*``` is one of the city names displayed on the permit tile you are using (if only one city name is available, do not specify the city).

* ```BEWKH <destinationCity card1 card2* card3* card4*>```: to *Build an Emporium with the help of the King*, where ```destinationCity``` is the city you want to move the King to, ```card1``` is the color of the first politics card you are using, ```card2*``` is the color of the second politics card you are using, ```card3*``` is the color of the third politics card you are using and ```card4*``` is the color of the fourth politics card you are using.

* ```EC <councillorColor balconyRegion>```: to *Elect a Councillor*, where ```councillorColor``` is the color of the chosen Councillor and ```balconyRegion``` is the name of the region the chosen balcony belongs to.

### Quick Actions: ###
* ```CBPT <balconyRegion>```: to *Change building Permit Tiles*, where ```balconyRegion``` is the name of the region the chosen balcony belongs to.

* ```EAA```: to *Engage an Assistant*.

* ```PASA```: to *Perform an additional main(slow) Action*.

* ```SATEC <councillorColor balconyRegion>```: to *Send an Assistant to elect a Councillor*, where ```councillorColor``` is the color of the chosen Councillor and ```balconyRegion``` is the name of the region the chosen balcony belongs to.

### Market Actions: ###
The following commands are available only when the market phase is active:

* ```SELLA <numberOfAssistants price>```: to create a new insertion of assistants, where ```numberOfAssistants``` is the number of assistants to sell and ```price``` is the price, expressed in coins, of the insertion.

* ```SELLPC <politicsCardColor price>```: to create a new insertion of politics cards, where ```politicsCardColor``` is the color of the politics card to sell and ```price``` is the price, expressed in coins, of the insertion.

* ```SELLPT <permitTileIndex price>```: to create an insertion of permit tiles, where ```permitTileIndex``` is the index number (from ```1``` to ```N```) of the permit tile you want to sell and ```price``` is the price, expressed in coins, of the insertion.

* ```PURCHASE <A|PC|PT indexOfInsertion>```: to purchase an insertion, where ```A``` suggests an assistant-type-of insertion, ```PC``` suggests a politics-card-type-of insertion, ```PT``` suggests a permit-tile-type-of insertion and ```indexOfInsertion``` is the index number (from ```1``` to ```N```) of the given type insertion you want to purchase.

* ```DONE```: to interrupt the phase of creation/purchase of the insertions. 

Any other command is not valid.

## **Notes for the evaluation:** ##

We decided to change some of the given specs, to put the examiners in condition to see every phase of the game without having to experience the longevity of the game:

* the initial timeout in the game lobby has been reduced from the original ```20``` seconds to ```10``` seconds.

* the initial amount of Emporia given to each player has been reduced from ```10 ``` to ```3```, so that only ```3``` Emporia have to be built to reach the final phases of the game. This value can easily be changed from the ```docs/ConfigurationFile.xml```.

## **Implementation details** ##

The whole architecture behind the game follows the **MVC** Apple pattern.


![uQNvX.png](https://bitbucket.org/repo/BRokAK/images/2853033388-uQNvX.png)

The Model contains all the information related to the game state, the Controller contains the game initialization and its rules and the View manages the remote view of the game through the game manager and the local view of the game through the game player.

### **Model** ###
![UML_Model.png](https://bitbucket.org/repo/BRokAK/images/683597477-UML_Model.png)
The three region boards are represented by a generic ```RegionBoard``` class extended by the three region classes. Each region reads from ```docs/ConfigurationFile.xml``` the cities to initialize. Finally the three regions are created by the class ```Map```, together with the other needed objects.
Same goes for cards and pawns, which are initialized by a generic ```Deck``` class, where the needed information are read from the configuration file.
The emperor has been implemented with the **Singleton pattern**.

The class appointed to read the needed fields from the ```docs/ConfigurationFile.xml``` is ```ReaderXML```, which implements the **Singleton pattern**. This class has been implemented so that future changes to the game are supported, therefore the scalability of the game is granted. We have singleton collections of colors and cities read from the configuration file.

Since our game comes without a GUI, in order to make the CLI experience enjoyable, we decided to show a ```.txt``` version of the map. This required a ```ConfigMap``` enum class, containing the file path names of the supported maps.

### **Controller** ###
![UML_Controller.png](https://bitbucket.org/repo/BRokAK/images/569849950-UML_Controller.png)

We merged different design patterns to implement the controller: actions are managed by a **FSM** using a **State pattern**, where each state implements the **Singleton pattern**. The execution of each action determines the following state of the FSM and generates a pair of messages: the private response to the requesting client and the broadcast message to notify every other client in the game. Response messages are handled directly by the server, while the broadcast messages are dispatched with the help of the ```PublisherInterface```, implemented by the ```Broker```.

![FSM.png](https://bitbucket.org/repo/BRokAK/images/602345690-FSM.png)
*Yellow-colored states can lead to ```GameFinishedState```, the state in which the game is finished and no further action is allowed.*


Actions are created using a **Visitor pattern**. Specifically, when an ```ActionRequestMessage```, which implements the interface ```ActionCreator```, is received, the class ```ActionFactoryVisitorImplementaion```, implementing the interface ```ActionFactoryVisitor``` and containing all the visit methods for the different types of action request, creates the appropriate action in line with the request. The interface ```ActionCreator``` allows the ```ActionFactoryVisitor``` to visit the correct action without knowing the concrete type of the request.

### **View** ###
![UML_View.png](https://bitbucket.org/repo/BRokAK/images/286005079-UML_View.png)
The communication between the game manager and the game player can happen both with RMI or Socket, based on the choice of the client. The Client-Server communication implements a **Request-Response pattern** and the game updates are managed with a **Publisher-Subscriber pattern**. When a game update needs to be sent to the clients, a publish request to the broker is generated with a specific topic for the game. These publish requests are put in a queue and are dispatched to the clients subscribed to the specific topic.
Each message request, both response messages and broadcast messages, implements a visit method following the **Visitor pattern** to display the correct information based on the client interaction with the game.