package it.polimi.ingsw.ps07.model.bonus;

import it.polimi.ingsw.ps07.model.Game;


/**
 * The value of this bonus represents the number of assistant
 * the player has to be rewarded with.
 */
public class AssistantBonus extends Bonus{
	
	/**
	 * Constructor for AssistantBonus.
	 * 
	 * @param bonusValue, number of assistants given
	 * by this bonus as an int
	 */
	public AssistantBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public String toString() {
		if(bonusValue > 1) {
			
			return "+" + bonusValue + " Assistants";
		} else {
			
			return "+" + bonusValue +" Assistant";
		}
		
	}

	@Override
	public void assignBonus(Game game) {
		
		game.getCurrentPlayer().getPlayersHand().increaseAssistantCounter(bonusValue);
	}
}
