package it.polimi.ingsw.ps07.model;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;
import it.polimi.ingsw.ps07.model.cards.KingsRewardTile;
import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * This class represents the hand of a player
 * 
 */
public class PlayersHand {
	
	public static final int EMPORIUM_INIT = Integer.parseInt(ReaderXML.getInstance().readConstant("EmporiumInit"));
	public static final int ASSISTANTS_INIT = Integer.parseInt(ReaderXML.getInstance().readConstant("AssistantsInit"));
	public static final int COINS_INIT = Integer.parseInt(ReaderXML.getInstance().readConstant("CoinsInit"));
	public static final int STARTING_NUMBER_OF_POLITICS_CARDS = Integer.parseInt(ReaderXML.getInstance().readConstant("StartingNumberOfCards"));
	
	private String playerID;
	private int assistantCounter;
	private int emporiumCounter;
	private int victoryPoints;
	private int coins;
	private int nobilityProgression;
	private List<KingsRewardTile> kingsRewardTiles;
	private List<ColorBonusTile> colorBonusTiles;
	private List<PoliticsCard> politicsCards;
	private List<PermitTile> permitTiles;
	private List<PermitTile> usedPermitTiles;
	
	/**
	 * Constructs a playersHand object initializing the ID of the
	 * player, setting the number of emporia he can build, the starting
	 * victory points, the starting nobility points and creating
	 * the lists of politics cards, color bonus tiles, king's reward
	 * tiles, permit tiles and used permit tiles
	 * @param playerID
	 */
	public PlayersHand(String playerID) { 			
		this.playerID = playerID;
		setEmporiumCounter(EMPORIUM_INIT);
		setVictoryPoints(0);
		setNobilityProgression(0);
		kingsRewardTiles = new ArrayList<>();
		colorBonusTiles = new ArrayList<>();
		politicsCards = new ArrayList<>();
		permitTiles = new ArrayList<>();
		usedPermitTiles = new ArrayList<>();
		
	}
	
	/**
	 * Returns the number of assistants in the
	 * player's hand
	 * @return assistantCounter
	 */
	public int getAssistantCounter() {
		return assistantCounter;
	}

	/**
	 * Sets the number of assistants in the player's hand
	 * @param assistantCounter
	 */
	public void setAssistantCounter(int assistantCounter) {
		this.assistantCounter = assistantCounter;
	}
	
	/**
	 * Increases the number of assistants in the player's hand
	 * @param amount
	 */
	public void increaseAssistantCounter(int amount) {
		assistantCounter = assistantCounter + amount;
	}
	
	/**
	 * Decreases the number of assistants in the player's hand
	 * @param amount
	 */
	public void decreaseAssistantCounter(int amount) {
		assistantCounter = assistantCounter - amount;
	}
	
	/**
	 * @return emporiumCounter, the number of emporia that 
	 * the player still has to build
	 */
	public int getEmporiumCounter() {
		return emporiumCounter;
	}
	
	/**
	 * Decreases the number of emporium in the player's hand
	 * by one
	 */
	public void decreaseEmporiumCounter() {
		
		emporiumCounter--;
	}

	/**
	 * Sets the number of emporia that the player has to build
	 * @param emporiumNumber
	 */
	public void setEmporiumCounter(int emporiumNumber) {
		emporiumCounter = emporiumNumber;
	}

	/**
	 * @return coins, the amount of coins the player has
	 */
	public int getCoins() {
		return coins;
	}

	/**
	 * Sets the number of coins in the player's hand
	 * @param coins
	 */
	public void setCoins(int coins) {
		this.coins = coins;
	}
	
	/**
	 * Decreases the number of coins in the player's hand
	 * by amount
	 * @param amount
	 */
	public void spendCoins(int amount) {
		
		coins -= amount;
		
	}
	
	/**
	 * Increases the number of coins in the player's hand
	 * by amount
	 * @param amount
	 */
	public void acquireCoins(int amount) {
		
		coins += amount;
		
	}

	/**
	 * Returns the number of victory points the player
	 * has earned
	 * @return victoryPoints
	 */
	public int getVictoryPoints() {
		return victoryPoints;
	}

	/**
	 * Sets the number of victory points
	 * @param victoryPoints
	 */
	public void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
	}
	
	/**
	 * Increases the number of victory points
	 * @param victoryPoints
	 */
	public void increaseVictoryPoints(int victoryPoints) {
		
		this.victoryPoints += victoryPoints;
	}

	/**
	 * @return nobilityProgression, the cell of the nobility
	 * path reached by the player
	 */
	public int getNobilityProgression() {
		return nobilityProgression;
	}

	/**
	 * Sets the cell of the nobilty path reached by the player
	 * @param nobilityProgression
	 */
	public void setNobilityProgression(int nobilityProgression) {
		this.nobilityProgression = nobilityProgression;
	}
	
	/**
	 * Increases the index of the cell of the nobility path
	 * reached by the player
	 * @param progression
	 */
	public void progressOntoNobilityPath(int progression) {
		
		nobilityProgression += progression;
	}
	
	/**
	 * @return kingsRewardTiles, the list of king's reward tiles
	 * redeemed by the player
	 */
	public List<KingsRewardTile> getKingsRewardTiles() {
		
		return kingsRewardTiles;
	}
	
	/**
	 * @return colroBonusTiles, the list of color bonus tiles
	 * redeemed by the player
	 */
	public List<ColorBonusTile> getColorBonusTiles() {
		
		return colorBonusTiles;
	}
	
	/**
	 * Adds a politics card to the list of politics card
	 * @param politicsCard
	 */
	public void addPoliticsCard(PoliticsCard politicsCard) {
		this.politicsCards.add(politicsCard);
	}
	
	/**
	 * @return politicsCards, the list of politics cards
	 * owned by the player
	 */
	public List<PoliticsCard> getPoliticsCards() {
		return politicsCards;
	}
	
	/**
	 * Adds a permit tile to the list of permit tiles
	 * owned by the player
	 * @param permitTile
	 */
	public void addPermitTile(PermitTile permitTile) {
		this.permitTiles.add(permitTile);
	}
	
	/**
	 * @return permitTiles, the list of permit tiles
	 * owned by the player
	 */
	public List<PermitTile> getPermitTiles() {
		return permitTiles;
	}
	
	/**
	 * @return usedPermitTiles, the list of permit tiles
	 * previously used by the player
	 */
	public List<PermitTile> getUsedPermitTiles() {
		
		return usedPermitTiles;
	}
	
}
