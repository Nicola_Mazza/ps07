package it.polimi.ingsw.ps07.model.cards;

/**
 * Class for the king's reward tiles 
 *
 */
public class KingsRewardTile{
	
	private int value;
	
	/**
	 * Constructs a new king's reward tile initializing
	 * the value, the number of points given by this tile
	 * @param value
	 */
	public KingsRewardTile(int value) {
		this.value = value;
	}
	
	/**
	 * @return value, the number of points given 
	 * by this tile
	 */
	public int getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		if(value > 1) {
			return "[+" + value + " Victory Points]";
		} else {
			return "[+" + value + " Victory Point]";
		}
	}
}
