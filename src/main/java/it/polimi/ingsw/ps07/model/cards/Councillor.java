package it.polimi.ingsw.ps07.model.cards;

import java.awt.Color;

/**
 * Councillors who populate the balconies on the map.
 */
public class Councillor{
	
	private Color color;
	private String stringColor;

	/**
	 * Constructor to initialize the councillor color. 
	 * The color is read from the XML file.
	 * 
	 * @param hexColor hex String representing the color
	 * @param stringColor String representing the color
	 */
	public Councillor(String hexColor, String stringColor) {
		
		color = Color.decode(hexColor);
		this.stringColor = stringColor;
	}

	/**
	 * Returns the color as a Color object
	 * 
	 * @return color the Color representing the color
	 */
	public Color getColor(){
		return color;
	}
	/**
	 * Returns the color as a String object
	 * 
	 * @return stringColor the String representing the color
	 */
	public String getStringColor() {
		
		return stringColor;
	}

	@Override
	public String toString() {
		return "[" + stringColor + "]";
	}
}
