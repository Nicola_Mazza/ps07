package it.polimi.ingsw.ps07.model.map;

import java.util.ArrayList;

import it.polimi.ingsw.ps07.model.cards.RegionBonusTile;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * Class for the Hills board.
 */
public class Hills extends RegionBoard {
	
	/**
	 * Constructor for Hills.
	 * 
	 * @param numberOfPlayers the number of players in the game.
	 * @param chosenMap the map on which the game is played as a String.
	 */
	public Hills(int numberOfPlayers, String chosenMap) {
		
		regionCities = new ArrayList<>();
		balcony = new ArrayList<>();
		
		regionCities = ReaderXML.getInstance().readCities("Hills", numberOfPlayers,
				chosenMap.charAt(chosenMap.length() - 1));
		regionBonusTile = new RegionBonusTile();
	}
}
