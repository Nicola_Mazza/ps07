package it.polimi.ingsw.ps07.model.map;

/**
 * The Emperor on the map. 
 */
public class Emperor {
	
	private String currentCity;
	private String kingsStartingCity;
	private static Emperor instance = new Emperor();
	
	/**
	 * Constructs an Emperor initializing its current city and
	 * the starting city
	 */
	private Emperor() {
		currentCity = Map.KINGS_STARTING_CITY;
		kingsStartingCity = currentCity;
	}
	
	/**
	 * Gets the Emperor instance.
	 * 
	 * @return instance the instance of the Emperor.
	 */
	public static Emperor getInstance() {
		
		return instance;
	}
	
	/**
	 * @return kingsStartingCity, the king's starting city
	 * as a string
	 */
	public String getKingsStartingCity() {
		
		return kingsStartingCity;
	}
	
	
	/**
	 * Moves the emperor to a new city.
	 * The new city is acquired from the input terminal.
	 * 
	 * @param destination the destination city of the Emperor.
	 */
	public void moveEmperor(String destination) {
		 currentCity = destination;	
	}
	
	/**
	 * Gets the current city of the Emperor.
	 * 
	 * @return currentCity the current city of the Emperor as a String.
	 */
	public String getCurrentCity() {
		
		return currentCity;
		
	}
}
