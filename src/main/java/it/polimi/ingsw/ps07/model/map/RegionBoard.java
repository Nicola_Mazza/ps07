package it.polimi.ingsw.ps07.model.map;

import java.util.List;

import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.cards.RegionBonusTile;

/**
 * Abstract class for the Region board.
 */
public abstract class RegionBoard {
	
	protected RegionBonusTile regionBonusTile;
	public static final int NUMBER_OF_REGION = 3;
	protected List<City> regionCities;
	protected List<Councillor> balcony;
		
	/**
	 * Gets the cities of the region.
	 * 
	 * @return regionCities the cities of the region.
	 */
	public List<City> getRegionCities() {
		return regionCities;
	}

	/**
	 * Gets the balcony of the region.
	 * 
	 * @return balcony a list of councillors, representing the
	 * balcony of the region.
	 */
	public List<Councillor> getRegionBalcony() {
		return balcony;
	}
	
	/**
	 * Gets the region bonus tile.
	 * 
	 * @return regionBonusTile the bonus tile of the region.
	 */
	public RegionBonusTile getRegionBonusTile() {
		
		return regionBonusTile;
	}
}
