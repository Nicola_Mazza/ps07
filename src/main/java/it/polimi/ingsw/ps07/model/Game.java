package it.polimi.ingsw.ps07.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.cards.Deck;
import it.polimi.ingsw.ps07.model.map.Map;
import it.polimi.ingsw.ps07.model.market.Market;
import it.polimi.ingsw.ps07.util.ConfigMap;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * The object that represents the game, composed by most of the components of the Model.
 */
public class Game {
	
	private static int gameID;
	
	private int numberOfPlayers; 
	private final int cardsToDrawAtStart;
	private List<Player> players;
	private Player currentPlayer;
	private final ConfigMap chosenMap;
	
	private Map map;
	private Deck deck;
	private Market market;
	
	private int turnNumber;
	private int roundNumber;
	
	/**
	 * Constructor for the game.
	 * 
	 * @param tokens a list of tokens containing the clients of the server.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public Game(List<Token> tokens, ConfigMap chosenMap) throws ParserConfigurationException, SAXException, IOException {
		
		cardsToDrawAtStart = Integer.parseInt(ReaderXML.getInstance().readNumberOf("CardsToDrawAtStart"));
		
		players = new ArrayList<>();
		createPlayers(tokens);
		currentPlayer = players.get(0);
		
		numberOfPlayers = tokens.size();
		map = new Map(numberOfPlayers, chosenMap);
		deck = new Deck();
		market = new Market();
		
		assignStartCards();
		
		//Assignment of another card for the first player
		currentPlayer.getPlayersHand().addPoliticsCard(deck.getPoliticsDeck().remove(0));
		
		this.chosenMap = chosenMap;
		turnNumber = 1;
		roundNumber = 1;

	}
	/**
	 * Gets GameID.
	 * 
	 * @return gameID the ID of the game.
	 */
	public int getGameID() {
		return Game.gameID;
	}
	
	/**
	 * Sets gameID.
	 * 
	 * @param id an Integer to represent the gameID.
	 */
	public void setGameID(int id) {
		Game.gameID = id;
	}

	/**
	 * Gets market.
	 * 
	 * @return market an object Market.
	 */
	public Market getMarket() {
		return market;
	}

	/**
	 * Gets deck.
	 * 
	 * @return deck an object Deck.
	 */
	public Deck getDeck() {
		return deck;
	}


	/**
	 * Gets the map.
	 * 
	 * @return map as an object Map.
	 */
	public Map getMap() {
		
		return map;
	}


	/**
	 * Gets players.
	 * 
	 * @return players as a List of objects Player.
	 */
	public List<Player> getPlayers() {
		
		return players;
		
	}

	private void assignStartCards() {
		
		for(Player player : players) {
			
			for(int i = 0; i < cardsToDrawAtStart; i++) {
				
				player.getPlayersHand().getPoliticsCards().add(deck.getPoliticsDeck().remove(0));
			}
		}
	}
	
	/**
	 * This method receives a List of tokens and creates the players using the UUID.
	 * Initialization of the assistants and coins fields in PlayersHand.
	 */
	public void createPlayers(List<Token> players2) {
		
		for(int i = 0; i < players2.size(); i++) {
			
			players.add(new Player(players2.get(i).getUuid()));
			players.get(i).setPlayerNumber(i);
			players.get(i).getPlayersHand().setAssistantCounter(PlayersHand.ASSISTANTS_INIT + i + 1);
			players.get(i).getPlayersHand().setCoins(PlayersHand.COINS_INIT + i);
			
		}
		
	}
	
	/**
	 * Assigns a number of politics cards to the currentPlayer.
	 * 
	 * @param numberOfPoliticsCards the number of politicsCards to be assigned.
	 */
	public void assignPoliticsCards(int numberOfPoliticsCards) {
		
		for(int i = 0; i < numberOfPoliticsCards; i++) {
			currentPlayer.getPlayersHand().addPoliticsCard(deck.getPoliticsDeck().remove(0));
		}
		
	}
	
	/**
	 * Gets numberOfPlayers.
	 * 
	 * @return numberOfPlayers the number of players as an Integer.
	 */
	public int getNumberOfPlayers() {
		
		return numberOfPlayers;
	}
	
	/**
	 * Gets currentPlayer.
	 * 
	 * @return currentPlayer the current player as an object Player.
	 */
	public Player getCurrentPlayer() {
		
		return currentPlayer;
		
	}
	/**
	 * Sets currentPlayer.
	 * 
	 * @param nextPlayer the next player as an object Player.
	 */
	public void setCurrentPlayer(Player nextPlayer) {

		currentPlayer = nextPlayer;
	}
	/**
	 * Gets turnNumber.
	 * 
	 * @return turnNumber the number of the current turn.
	 */
	public int getTurnNumber() {
		
		return turnNumber;
		
	}
	
	/**
	 * Sets turnNumber.
	 * 
	 * @param n Integer to update the turnNumber.
	 */
	public void setTurnNumber(int n) {
		
		turnNumber = n;	
	}
	
	
	/**
	 * Gets the number of the currentPlayer.
	 * 
	 * @return currentPlayer number of the currentPlayer.
	 */
	public int getCurrentPlayerNumber() {
		
		return currentPlayer.getPlayerNumber();
	}
	
	/**
	 * Gets the map chosen by the player.
	 * 
	 * @return chosenMap the map chosen by the player.
	 */
	public ConfigMap getChosenMap() {
		
		return chosenMap;
	}
	
	/**
	 * @return roundNumber, the number of the current round
	 */
	public int getRoundNumber() {
		
		return roundNumber;
	}
	
	/**
	 * Increase the round number by 1
	 */
	public void increaseRoundNumber() {
		
		roundNumber++;
	}
}