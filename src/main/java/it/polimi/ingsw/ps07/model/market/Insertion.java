package it.polimi.ingsw.ps07.model.market;

import it.polimi.ingsw.ps07.model.cards.Card;

/**
 * Abstract class for the market insertions.
 */
public abstract class Insertion {
	
	protected int playerID;
	protected int price;
	
	/**
	 * Gets the playerID of the creator of the insertion.
	 * 
	 * @return playerID the playerID.
	 */
	public int getPlayerID() {
		return playerID;
	}
	
	/**
	 * Gets the price of the insertion.
	 * 
	 * @return price the price expressed in coins.
	 */
	public int getPrice() {
		return price;
	}
	
	/**
	 * Gets the insertion object.
	 * 
	 * @return the insertion object.
	 */
	public abstract Card getInsertionObject();

}