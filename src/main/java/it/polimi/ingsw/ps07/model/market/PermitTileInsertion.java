package it.polimi.ingsw.ps07.model.market;
import it.polimi.ingsw.ps07.model.cards.Card;
import it.polimi.ingsw.ps07.model.cards.PermitTile;


/**
 * Class for the permit tile insertion type of insertion
 * Players can sell a permit tile
 *
 */
public class PermitTileInsertion extends Insertion{
	
	private PermitTile permitTile;
	
	/**
	 * Constructs a permit tile insertion initializing the permit
	 * tile that the player wants to sell, his number, and the price
	 * of the tile
	 * @param permitTile
	 * @param playerID
	 * @param price
	 */
	public PermitTileInsertion(PermitTile permitTile, int playerID, int price) {
		this.permitTile = permitTile;
		this.price = price;
		this.playerID = playerID;
	}

	@Override
	public Card getInsertionObject() {
		
		return permitTile;
	}

	@Override
	public String toString() {
		return "[Permit Tile Insertion of player" + playerID + ", "
				+ "Permit Tile :" + permitTile.toString() + ", price: " + price + "]";
	}
	
}