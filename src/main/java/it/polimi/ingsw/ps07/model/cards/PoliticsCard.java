package it.polimi.ingsw.ps07.model.cards;

import java.awt.Color;

/**
 * Class for the politics card
 *
 */
public class PoliticsCard implements Card{
	
	Color color;
	String stringColor;
	boolean jolly;
	
	/**
	 * Constructs a politics card initializing its color as
	 * a color object and as a string. It's not a jolly, so
	 * jolly boolean is set as false
	 * 
	 * @param hexColor
	 * @param stringColor
	 */
	public PoliticsCard(String hexColor, String stringColor) {
		
		color = Color.decode(hexColor);
		this.stringColor = stringColor;
		
		jolly = false;
	}
	
	/**
	 * Constructs a jolly card, so the color is initialized
	 * as JOLLY and the jolly boolean is set as true
	 */
	public PoliticsCard() {
		
		jolly = true;
		stringColor = "JOLLY";
	}
	
	/**
	 * @return true if the card is a jolly
	 */
	public boolean isJolly() {
		
		return jolly;
	}
	
	/**
	 * @return color, the color of the card
	 * as a Color object
	 */
	public Color getColor() {
		
		return color;
	}
	
	/**
	 * @return stringColor, the color of the card
	 * as a string
	 */
	public String getStringColor() {
		
		return stringColor;
	}

	
	@Override
	public String toString() {
		return "[ " + color + " ]";
	}
}