package it.polimi.ingsw.ps07.model.map;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;
import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.cards.KingsRewardTile;
import it.polimi.ingsw.ps07.util.BonusTokensInitializer;
import it.polimi.ingsw.ps07.util.ConfigMap;
import it.polimi.ingsw.ps07.util.CouncillorsInitializer;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * A class representing the Map on which the game will be played.
 */
public class Map {
	
	public static final int NUMBER_OF_BONUS_TOKENS = Integer.parseInt(ReaderXML.getInstance().readNumberOf("BonusCards"));

	public static String KINGS_STARTING_CITY;
	
	private List<ColorBonusTile>  colorBonusTiles;
	private List<KingsRewardTile> kingsRewardBonusTiles;
	private List<Councillor> kingsBalcony;
	private List<Councillor> councillorsPool;
	private Route route;
	private RegionBoard coast;
	private RegionBoard hills;
	private RegionBoard mountains;
	private static CitiesGraph citiesGraph;
	private final ConfigMap chosenMap;
	
	/**
	 * Creates a new map.
	 * 
	 * @param numberOfPlayers the number of players in the game.
	 * @param chosenMap the filename of the .txt map.
	 */
	public Map(int numberOfPlayers, ConfigMap chosenMap) {
		
		this.chosenMap = chosenMap;
		Map.KINGS_STARTING_CITY = ReaderXML.getInstance().readKingsStartingCity(chosenMap.toString().charAt(chosenMap.toString().length() - 1));
		
		colorBonusTiles = new ArrayList<>();
		kingsRewardBonusTiles = new ArrayList<>();
		kingsBalcony = new ArrayList<>();
		councillorsPool = new ArrayList<>();
		route = new Route();
		
		//creates the 3 region boards with their cities
		coast = new Coast(numberOfPlayers, chosenMap.toString());
		hills = new Hills(numberOfPlayers, chosenMap.toString());
		mountains = new Mountains(numberOfPlayers, chosenMap.toString());
		
		//creates and assign bonus tokens to the cities
		BonusTokensInitializer bonusTokensInitializer = new BonusTokensInitializer();
		bonusTokensInitializer.assignTokens(coast, hills, mountains);
		
		//reads all councillors colors on the configuration file, creates and shuffle them
		CouncillorsInitializer councillorsInitializer = new CouncillorsInitializer();
		
		//assignment of the councillors, returns the remaining councillors
		councillorsPool = councillorsInitializer.assignCouncillors(coast, hills, mountains, kingsBalcony);
		
		//creates a simple graph with cities as vertices and couple of cities as edges
		citiesGraph = new CitiesGraph(coast.getRegionCities(), hills.getRegionCities(),
				mountains.getRegionCities(), chosenMap);
		
		//initialization of all the color bonus tiles 
		for(int i = 1; i <= Integer.parseInt(ReaderXML.getInstance().readNumberOf("ColorBonusTiles")); i++) {
			colorBonusTiles.add(new ColorBonusTile(ReaderXML.getInstance().readColorBonusTileColor(i), ReaderXML.getInstance().readColorBonusTileStringColor(i),
								Integer.parseInt(ReaderXML.getInstance().readColorBonusTileValue(i))));
		}
		
		//initialization of the 5 king's reward bonus tiles
		for(int i = 1; i <= Integer.parseInt(ReaderXML.getInstance().readNumberOf("KingsRewardBonusTiles")); i++) {
			kingsRewardBonusTiles.add(new KingsRewardTile(Integer.parseInt(ReaderXML.getInstance().readKingsRewardTile(i))));
		}	 
	}
	
	/**
	 * Gets the cities graph.
	 * 
	 * @return citiesGraph a graph of cities.
	 */
	public CitiesGraph getCitiesGraph() {
		return citiesGraph;
	}
	
	/**
	 * Gets the councillors forming the King's balcony.
	 * 
	 * @return kingsBalcony a list of councillors.
	 */
	public List<Councillor> getKingsBalcony() {
		return kingsBalcony;
	}
	
	/**
	 * Gets the pool of unused councillors.
	 * 
	 * @return councillorsPool the pool of unused councillors.
	 */
	public List<Councillor> getCouncillorsPool() {
		return councillorsPool;
	}
	
	/**
	 * Gets the Coast region board.
	 * 
	 * @return coast the Coast region board.
	 */
	public RegionBoard getCoast() {
		return coast;
	}
	
	/**
	 * Gets the Hills region board.
	 * 
	 * @return hills the Hills region board.
	 */
	public RegionBoard getHills() {
		return hills;
	}
	
	/**
	 * Gets the Mountains region board.
	 * 
	 * @return mountains the Mountains region board.
	 */
	public RegionBoard getMountains() {
		return mountains;
	}
	
	/**
	 * Gets all the cities across the three region boards.
	 * 
	 * @return allCities all the cities across the three region boards.
	 */
	public List<City> getAllCities() {
		
		List<City> coastCities = getCoast().getRegionCities();
		List<City> hillsCities = getHills().getRegionCities();
		List<City> mountainsCities = getMountains().getRegionCities();
		List<City> allCities = new ArrayList<>();
		
		allCities.addAll(coastCities);
		allCities.addAll(hillsCities);
		allCities.addAll(mountainsCities);
		
		return allCities;
	}
	
	/**
	 * Gets the route.
	 * 
	 * @return route the route.
	 */
	public Route getRoute() {
		
		return route;
	}
	
	/**
	 * Gets the color bonus tiles.
	 * 
	 * @return colorBonusTiles, the list of the color
	 * bonus tiles
	 */
	public List<ColorBonusTile> getColorBonusTiles() {
		
		return colorBonusTiles;
	}
	
	/**
	 * Gets the King's Reward tiles.
	 * 
	 * @return kingsRewardBonusTiles, the list of king's 
	 * reward bonus tiles
	 */
	public List<KingsRewardTile> getKingsRewardTiles() {
		
		return kingsRewardBonusTiles;
	}
}
