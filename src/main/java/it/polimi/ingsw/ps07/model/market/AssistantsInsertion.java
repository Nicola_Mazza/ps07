package it.polimi.ingsw.ps07.model.market;

import it.polimi.ingsw.ps07.model.cards.Assistant;
import it.polimi.ingsw.ps07.model.cards.Card;

/**
 * Class for assistants-type-of insertions.
 */
public class AssistantsInsertion extends Insertion{
	
	private final Assistant assistants;
	
	/**
	 * Constructs a new AssistantInsertion object initializing
	 * the number of assistants that the player wants to sell, 
	 * the ID of the player, and the price.
	 *  
	 * @param assistants the amount of assistants of the insertion.
	 * @param playerID the playerID of the player who created the insertion.
	 * @param price the price of the insertion expressed in coins.
	 */
	public AssistantsInsertion(int assistants, int playerID, int price) {
		
		this.assistants = new Assistant(assistants);
		this.playerID = playerID;
		this.price = price;
	}

	@Override
	public Card getInsertionObject() {
		
		return assistants;
	}

	@Override
	public String toString() {
		return "[Assistants Insertion of Player" + playerID + ", "
				+ "assistants :" + assistants.toString() + ", price: " + price + "]";
	}

	
}
