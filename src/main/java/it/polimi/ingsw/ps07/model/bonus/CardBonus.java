package it.polimi.ingsw.ps07.model.bonus;

import it.polimi.ingsw.ps07.model.Game;


/**
 * The value of this bonus represents the number of politics cards
 * the player has to be rewarded with.
 */
public class CardBonus extends Bonus{

	/**
	 * Constructs a new politics card bonus initializing
	 * the number of cards it gives
	 * @param bonusValue, the value of the bonus.
	 */
	public CardBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public void assignBonus(Game game) {
		
		for(int i = 0; i < bonusValue; i++) {
			
			//the card is drawn and then assigned to a player's hand 
			game.getCurrentPlayer().getPlayersHand().addPoliticsCard(game.getDeck().drawAPoliticsCard());
		}
	}
	
	@Override
	public String toString() {
		if(bonusValue > 1) {
			
			return "+" + bonusValue + " Politics Cards";
		} else {
			
			return "+" + bonusValue + " Politics Card";
		}
		
	}
}
