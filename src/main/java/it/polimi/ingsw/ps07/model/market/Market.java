package it.polimi.ingsw.ps07.model.market;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;

/**
 * A class to represent the market.
 */
public class Market {
	
	private List<Insertion> permitTileInsertions;
	private List<Insertion> politicsCardInsertions;
	private List<Insertion> assistantsInsertions;
	
	public Market() {
		
		permitTileInsertions = new ArrayList<>();
		politicsCardInsertions = new ArrayList<>();
		assistantsInsertions = new ArrayList<>();
	}
	
	/**
	 * Adds a permit tile insertion in the market.
	 * 
	 * @param permitTile the Permit Tile to sell.
	 * @param playerID the id of the player who is creating the insertion.
	 * @param price the price of the insertion.
	 */
	public void addPermitTileInsertion(PermitTile permitTile, int playerID, int price) {
		permitTileInsertions.add(new PermitTileInsertion(permitTile, playerID, price));
	}
	
	/**
	 * Adds a politics card insertion in the market.
	 * 
	 * @param politicsCard the Politics Card to sell.
	 * @param playerID the id of the player who is creating the insertion.
	 * @param price the price of the insertion.
	 */
	public void addPoliticsCardInsertion(PoliticsCard politicsCard, int playerID, int price) {
		politicsCardInsertions.add(new PoliticsCardInsertion(politicsCard, playerID, price));
	}
	
	/**
	 * Adds an assistants insertion in the market.
	 * 
	 * @param assistants the amount of assistants to sell.
	 * @param playerID the id of the player who is creating the insertion.
	 * @param price the price of the insertion.
	 */
	public void addAssistantsInsertion(int assistants, int playerID, int price) {
		assistantsInsertions.add(new AssistantsInsertion(assistants, playerID, price));
	}
	
	/**
	 * Gets an array of nsertions of the given type of insertion.
	 * 
	 * @param typeOfInsertion the type of insertion as a String.
	 * 
	 * @return the array of insertions of the given type.
	 */
	public List<Insertion> getInsertionsArray(String typeOfInsertion) {
		
		switch (typeOfInsertion.toUpperCase()) {
		
		case "A" :
			return assistantsInsertions;
			
		case "PC" :
			return politicsCardInsertions;
			
		case "PT" :
			return permitTileInsertions;
			
		default :
			return null;
		}
	}
	
}
