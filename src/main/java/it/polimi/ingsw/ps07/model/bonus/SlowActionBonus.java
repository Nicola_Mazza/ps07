package it.polimi.ingsw.ps07.model.bonus;

import it.polimi.ingsw.ps07.controller.turn.TurnState;
import it.polimi.ingsw.ps07.model.Game;

/**
 * Class for the slow action bonus
 * The player can perform another slow action.
 */
public class SlowActionBonus extends Bonus{

	/**
	 * Constructs a new slow action bonus initializing
	 * the number of addictional slow action that the player can perform
	 * @param bonusValue
	 */
	public SlowActionBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public void assignBonus(Game game) {
		
		TurnState.incrementSlowActionCounter();
	}

	@Override
	public String toString() {
		return "+SlowAction";
	}
	
	
}

