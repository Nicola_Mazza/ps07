package it.polimi.ingsw.ps07.model.market;

import it.polimi.ingsw.ps07.model.cards.Card;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;

/**
 * Class for the politics card type of insertion
 *
 */
public class PoliticsCardInsertion extends Insertion{
	
	private PoliticsCard politicsCard;
	
	/**
	 * Constructs a Politics card insertion initializing
	 * the card that the player wants to sell, the number of the 
	 * player that created the insertion, and its price
	 * @param politicsCard
	 * @param playerID
	 * @param price
	 */
	public PoliticsCardInsertion(PoliticsCard politicsCard, int playerID, int price) {
		this.politicsCard = politicsCard;
		this.playerID = playerID;
		this.price = price;
	}

	@Override
	public Card getInsertionObject() {
		
		return politicsCard;
	}

	@Override
	public String toString() {
		return "[Politics Card Insertion of Player" + playerID + ", "
				+ "Politics Card :" + politicsCard.getStringColor() + "," + " price: " + price + "]";
	}
}
