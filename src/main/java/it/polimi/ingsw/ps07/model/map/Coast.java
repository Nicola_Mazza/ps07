package it.polimi.ingsw.ps07.model.map;

import java.util.ArrayList;

import it.polimi.ingsw.ps07.model.cards.RegionBonusTile;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * Class for the Coast board.
 */
public class Coast extends RegionBoard {
	
	/**
	 * Constructor for Coast.
	 * 
	 * @param numberOfPlayers the number of players in the game.
	 * @param chosenMap the map on which the game is played as a String.
	 */
	public Coast(int numberOfPlayers, String chosenMap) {
		
		regionCities = new ArrayList<>();
		balcony = new ArrayList<>();
		
		regionCities = ReaderXML.getInstance().readCities("Coast", numberOfPlayers,
				chosenMap.charAt(chosenMap.length() - 1));
		
		regionBonusTile = new RegionBonusTile();
	}
	
}
