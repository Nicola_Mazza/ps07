package it.polimi.ingsw.ps07.model.bonus;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the bonus token of the cities.
 * Its attribute is indeed a List of bonuses
 *
 */
public class BonusToken {
	
	private List<Bonus> bonuses = new ArrayList<>();
	
	/**
	 * Constructs a new Bonus Token initializing the
	 * bonuses it gives
	 * @param bonuses
	 */
	public BonusToken(List<Bonus> bonuses) {
		this.bonuses = bonuses;
	}
	
	/**
	 * @return the bonuses given by this token
	 */
	public List<Bonus> getBonuses() {
		return bonuses;
	}
	
	
	/**
	 * @param bonuses
	 * @return a string modified to print the tokens
	 * of the cities in the right way
	 */
	private String bonusesToString(List<Bonus> bonuses) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(" [");
		for(Bonus bonus : bonuses) {

			sb.append(bonus.toString());
			sb.append(" ");
		}
		sb.replace(sb.toString().length() - 1, sb.toString().length(), "]");
		
		return sb.toString();
	}
	
	@Override
	public String toString() {

		return bonusesToString(bonuses);
	}
}
