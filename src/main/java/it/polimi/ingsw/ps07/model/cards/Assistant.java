package it.polimi.ingsw.ps07.model.cards;

public class Assistant implements Card{

	private final int numberOfAssistants;
	
	/**
	 * Constructs a new Assistant object initializing the number
	 *  of assistants
	 * @param numberOfAssistants
	 */
	public Assistant(int numberOfAssistants) {
		
		this.numberOfAssistants = numberOfAssistants;
	}
	
	/**
	 * @return numberOfAssistants, the number of assistants
	 */
	public int getNumberOfAssistants() {
		
		return numberOfAssistants;
	}
	

	@Override
	public String toString() {
		
		return Integer.toString(numberOfAssistants);
	}

	
}
