package it.polimi.ingsw.ps07.model.bonus;

import it.polimi.ingsw.ps07.model.Game;

/**
 * The value of this bonus represents the number of coins
 * the player has to be rewarded with.
 */
public class CoinBonus extends Bonus{
	
	/**
	 * Constructs a coins bonus initializing
	 * the number of coins it gives
	 * @param bonusValue the value of the coins
	 */
	public CoinBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public void assignBonus(Game game) {
		
		game.getCurrentPlayer().getPlayersHand().acquireCoins(bonusValue);
	}
	
	@Override
	public String toString() {
		
		if(bonusValue > 1) {
			
			return "+" + bonusValue + " Coins";
		} else {
			
			return "+" + bonusValue + " Coin";
		}
		
	}
}
