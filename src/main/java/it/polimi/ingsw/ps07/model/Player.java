package it.polimi.ingsw.ps07.model;

/**
 * Class for the player object
 * This class represents a player in the game
 * 
 */
public class Player {
	
	private String playerID;
	private PlayersHand playersHand;
	private int playerNumber;
	
	/**
	 * Constructs a new player initializing his ID
	 * and constructing his hand
	 * @param playerID
	 */
	public Player(String playerID) {
		
		this.playerID = playerID;
		playersHand = new PlayersHand(playerID);
	}
	
	/**
	 * @return playerID, the ID of the player
	 */
	public String getPlayerID(){
		
		return playerID;
	}
	
	/**
	 * @return playersHand. the hand of this player
	 */
	public PlayersHand getPlayersHand() {
		
		return playersHand;
	}
	
	/**
	 * @return playerNumber, the number of the player
	 */
	public int getPlayerNumber() {
		
		return playerNumber;
	}

	/**
	 * Sets the number of this player
	 * @param playerNumber
	 */
	public void setPlayerNumber(int playerNumber) {

		this.playerNumber = playerNumber;
	}
}
