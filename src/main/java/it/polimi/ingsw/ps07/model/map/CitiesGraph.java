package it.polimi.ingsw.ps07.model.map;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import it.polimi.ingsw.ps07.util.ConfigMap;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * Class for the cities graph
 * The cities graph is a SimpleGraph object 
 *
 */
public class CitiesGraph {
	
	private static SimpleGraph<City, DefaultEdge> graph;
	private final ConfigMap chosenMap;
	List<City> allCities = new ArrayList<>();
	
	/**
	 * Constructs a new cities graph object initializing the
	 * SimpleGraph with all the cities of the three regions
	 * @param list, list of Coast cities
	 * @param list2, list of Hills cities
	 * @param list3, list of Mountains cities
	 * @param chosenMap, the map chosen by the first player
	 */
	public CitiesGraph(List<City> list, List<City> list2, List<City> list3, ConfigMap chosenMap) {
		
		//creates a simple graph
		graph = new SimpleGraph<>(DefaultEdge.class);
		
		allCities.addAll(list);
		allCities.addAll(list2);
		allCities.addAll(list3);
		
		this.chosenMap = chosenMap;
		
		//now allCities array list contains all the cities
		
		for(int i = 1; i <= Integer.parseInt(ReaderXML.getInstance().
				readConstant("Links" + chosenMap.toString().charAt(chosenMap.toString().length() - 1))); i++){
			
			City city1 = null;
			City city2 = null;
			
			for(City city : allCities){
				
				graph.addVertex(city);
				
				if(city.getCityName().equals(ReaderXML.getInstance().readLinkVertex(i, 1, chosenMap.toString().charAt(chosenMap.toString().length() - 1)))) {
					
					city1 = city;
				}
				if(city.getCityName().equals(ReaderXML.getInstance().readLinkVertex(i, 2, chosenMap.toString().charAt(chosenMap.toString().length() - 1)))) {
					
					city2 = city;
				}
				if(city1 != null && city2 != null) {
					
					graph.addEdge(city1, city2);
				}
			}
		}
	}
	
	
	/**
	 * Returns the cities adjacent to a city in which a player has just built an emporium
	 * and in which he has already built an emporium
	 * 
	 * @param city, the city in which a player has just built an emporium
	 * @param cities, list of cities adjacent to the city in which the
	 * player has just built an emporium
	 * @param playerID, the number of the player that built the emporium
	 * @return cities, the list of cities adjacent to the one in which a
	 * player has just built an emporium
	 */
	public List<City> navigateAdiacentCities(City city, List<City> cities, int playerID) {
		
		int size = cities.size();
		
		if(cities.contains(city)){
			
			return cities;
		}
		
		for(Emporium emporium : city.getEmporia()) {
			
			if(emporium.isBuilt() && emporium.getPlayerID() == playerID){
				
				cities.add(city);
			}
		}
		
		if(cities.size() == size) {
			
			return cities;
		}
		
		Set<DefaultEdge> edges = graph.edgesOf(city);
		
		for(DefaultEdge edge : edges) {
			City vertex1 = graph.getEdgeSource(edge);
			City vertex2 = graph.getEdgeTarget(edge);
			
			if(vertex1.getCityName().equals(city.getCityName())) {
				
				cities = navigateAdiacentCities(vertex2, cities, playerID);
			} else {
				
				cities = navigateAdiacentCities(vertex1, cities, playerID);
			}
		}
		
		return cities;
	}
	
	/**
	 * @return the SimpleGraph of this class.
	 * Its vertices represent the the cities and
	 * the edges the links between those cities
	 */
	public SimpleGraph<City, DefaultEdge> getGraph() {
		
		return graph;
	}

}
