package it.polimi.ingsw.ps07.model.bonus;

import it.polimi.ingsw.ps07.model.Game;

/**
 * Class for the victory points bonus
 * The value of this bonus represents the number of victory points
 * the player has to be rewarded with.
 */
public class VictoryBonus extends Bonus{

	/**
	 * Constructs a victory points bonus initializing
	 * the number of the victory points it gives
	 * 
	 * @param bonusValue Integer bonus value.
	 */
	public VictoryBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public void assignBonus(Game game) {
		
		game.getCurrentPlayer().getPlayersHand().increaseVictoryPoints(bonusValue);
	}
	
	@Override
	public String toString() {
		
		if(bonusValue > 1) {
			
			return "+" + bonusValue + " Victory Points";
		} else {
			
			return "+" + bonusValue + " Victory Point";
		}
	}
}
