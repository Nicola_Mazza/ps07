package it.polimi.ingsw.ps07.model.cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * Class that contains the most important decks of the game:
 * The politics card deck, and the three permit tiles decks
 * (one per region)
 */
public class Deck {
	public static final int NUMBER_OF_PERMIT_TILES_PER_REGION = Integer.parseInt(ReaderXML.getInstance().readNumberOf("PermitTiles"));
	public static final int REVEALED_PERMIT_TILES = Integer.parseInt(ReaderXML.getInstance().readNumberOf("RevealedPermitTilesPerRegion"));

	private List<PoliticsCard> politicsDeck;
	private List<PermitTile> permitTileDeckCoast;
	private List<PermitTile> permitTileDeckHills;
	private List<PermitTile> permitTileDeckMountains;
	
	/**
	 * Constructs a Deck object initializing the 4
	 * decks 
	 */
	public Deck() {
		
		List<String> politicsHexColors = new ArrayList<>();
		List<String> politicsStringColors = new ArrayList<>();
		
		politicsDeck = new ArrayList<>();
		permitTileDeckCoast = new ArrayList<>();
		permitTileDeckHills = new ArrayList<>();
		permitTileDeckMountains = new ArrayList<>();
		
		politicsHexColors = ReaderXML.getInstance().readPoliticsColors();
		politicsStringColors = ReaderXML.getInstance().readPoliticsColorsString();
		
		//The number of cards per color is a string
		for(int i = 0; i < Integer.parseInt(ReaderXML.getInstance().readNumberOf("PoliticsCardsPerColor")); i++) {
			
			// Iterates a number of times equal to the number of cards per color
			for(int j = 0; j < politicsHexColors.size(); j++) {
				PoliticsCard politicsCard = new PoliticsCard(politicsHexColors.get(j), politicsStringColors.get(j));
				politicsDeck.add(politicsCard);
			}
			
		}
		
		//creates jolly Cards (Multicolor)
		for(int i = 0; i < Integer.parseInt(ReaderXML.getInstance().readNumberOf("MulticolorCards")); i++) {
			
			politicsDeck.add(new PoliticsCard());
		}
		
		Collections.shuffle(politicsDeck);
		
		for(int i = 1; i <= NUMBER_OF_PERMIT_TILES_PER_REGION; i++) {
			permitTileDeckCoast.add(new PermitTile("Coast", i));//initialization of coast permit tiles
		}
		Collections.shuffle(permitTileDeckCoast);
		
		for(int i = 1; i <= NUMBER_OF_PERMIT_TILES_PER_REGION; i++){
			permitTileDeckHills.add(new PermitTile("Hills", i));//initialization of hills permit tiles
		}
		Collections.shuffle(permitTileDeckHills);
		
		for(int i = 1; i <= NUMBER_OF_PERMIT_TILES_PER_REGION; i++ ){
			permitTileDeckMountains.add(new PermitTile("Mountains", i));//initialization of mountains permit tiles
		}
		Collections.shuffle(permitTileDeckMountains);
	}
	
	/**
	 * @return the politics deck, a list of politics cards
	 */
	public List<PoliticsCard> getPoliticsDeck() {
		
		return politicsDeck;
	}
	
	/**
	 * @return the first politics card of the politics cards
	 * deck that has been removed
	 */
	public PoliticsCard drawAPoliticsCard() {
		
		return politicsDeck.remove(0);
	}
	
	/**
	 * @return the permit tile deck of the coast region, a list
	 * of permit tiles
	 */
	public List<PermitTile> getPermitTileDeckCoast() {
		
		return permitTileDeckCoast;
		
	}
	
	/**
	 * @return the permit tile deck of the hills region, a list
	 * of permit tiles
	 */
	public List<PermitTile> getPermitTileDeckHills() {
		
		return permitTileDeckHills;
		
	}

	/**
	 * @return the permit tile deck of the mountains region, a list
	 * of permit tiles
	 */
	public List<PermitTile> getPermitTileDeckMountains() {
		
		return permitTileDeckMountains;
		
	}
}