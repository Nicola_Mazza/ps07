package it.polimi.ingsw.ps07.model.cards;

import java.awt.Color;

/**
 * This is a class for the color bonus tiles
 * This tiles are redeemed by the players when they
 * build an emporium in all the cities that are of the
 * same color
 *
 */
public class ColorBonusTile{
	
	private final Color color;
	private final String stringColor;
	private final int bonusPoints;
	
	/**
	 * Constructs a new ColorBonusTile object initializing
	 * the color as a Color object and as a String, and the number
	 * of bonus points of the tile.
	 * 
	 * @param hexColor
	 * @param stringColor
	 * @param bonusPoints
	 */
	public ColorBonusTile(String hexColor, String stringColor, int bonusPoints) {
		
		color = Color.decode(hexColor);
		this.bonusPoints = bonusPoints;
		this.stringColor = stringColor;
	}
	
	/**
	 * Gets stringColor.
	 * 
	 * @return stringColor, the color of the tile as a String.
	 */
	public String getStringColor() {
		
		return stringColor;
	}
	
	/**
	 * Gets color.
	 * 
	 * @return color, the color of the tile as a Color object.
	 */
	public Color getColor() {
		
		return color;
	}
	
	/**
	 * @return bonusPoints, the number of victory points 
	 * of the bonus tile
	 */
	public int getBonusValue() {
		
		return bonusPoints;
	}

	@Override
	public String toString() {
		
		if(bonusPoints > 1) {
			
			return "[" + stringColor.substring(1, stringColor.length() - 1) + " +" + bonusPoints + " Victory Points]";
		} else{
			
			return "[" + stringColor.substring(1, stringColor.length() - 1) + " +" + bonusPoints + " Victory Point]";
		}
			
		
	}
	
	
}
