package it.polimi.ingsw.ps07.model.map;

/**
 * Class for the Emporia
 *
 */
public class Emporium {
	
	private boolean built;
	private int playerID;
	
	/**
	 * Constructs an Emporium object 
	 * setting the built boolean as false and
	 * the playerID at a negative value
	 */
	public Emporium() {
		built = false;
		playerID = -1;
	}

	/**
	 * @return built, true if the the emporium is built
	 */
	public boolean isBuilt() {
		
		return built;
	}

	/**
	 * Set the Emporium as built
	 * Built is set as true;
	 */
	public void setBuilt() {
		built = true;
	}
	
	/**
	 * @return the number of the player that built this emporium
	 */
	public int getPlayerID() {
		return playerID;
	}

	/**
	 * Sets the number of the player that built this emporium
	 * @param playerID
	 */
	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}
	
	@Override
	public String toString() {
		if(playerID != -1) {
			return "Player" + playerID + "";
		} else {
			return "";
		}
		
	}
}
