package it.polimi.ingsw.ps07.model.map;

import java.util.List;

import it.polimi.ingsw.ps07.model.bonus.Bonus;

/**
 * Class for a Nobility Bonus cell of the Nobility path.
 */
public class NobilityBonusCell {
	
	private List<Bonus> nobilityBonuses;
	private int nobilityIndex;
	
	/**
	 * Constructor for NobilityBonusCell.
	 * 
	 * @param nobilityBonus list of bonuses read from .xml file.
	 * @param index index of the cell on the Nobility path.
	 */
	public NobilityBonusCell(List<Bonus> nobilityBonus, int index) {
		this.nobilityBonuses = nobilityBonus;
		this.nobilityIndex = index;
	}
	
	/**
	 * Gets a list of bonuses.
	 * 
	 * @return nobilityBonuses the list of bonuses.
	 */
	public List<Bonus> getNobilityBonus(){
		return nobilityBonuses;
	}
	
	/**
	 * Gets the index of the nobility bonus cell on the Nobility path.
	 * 
	 * @return nobilityIndex the index of the nobility bonus cell.
	 */
	public int getNobilityIndex(){
		return nobilityIndex;
	}
}