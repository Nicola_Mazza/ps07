package it.polimi.ingsw.ps07.model.bonus;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.map.NobilityBonusCell;

/**
 * The value of this bonus represents the number of steps to make 
 * onto the nobility path.
 */
public class NobilityBonus extends Bonus{

	/**
	 * Constructs a nobility bonus initializing
	 * the number of nobility points (steps) it gives
	 * @param bonusValue
	 */
	public NobilityBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}
	
	@Override
	public boolean isNobilityBonus() {
		
		return true;
	}

	@Override
	public void assignBonus(Game game) {
		
		game.getCurrentPlayer().getPlayersHand().progressOntoNobilityPath(bonusValue);
		
		NobilityBonusCell cell = game.getMap().getRoute().getNobilityCell(game.getCurrentPlayer().getPlayersHand().getNobilityProgression());
		
		//the new position of the player on the nobility path grants him a bonus
		if(cell != null) {
			
			for(Bonus bonus : cell.getNobilityBonus()) {
				
				bonus.assignBonus(game);
			}
		}
	}
	
	@Override
	public String toString() {
		
		if(bonusValue > 1) {
			
			return "+" + bonusValue + " Nobility Points";
		} else {
			
			return "+" + bonusValue + " Nobility Point";
		}
		
	}
}