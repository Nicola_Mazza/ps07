package it.polimi.ingsw.ps07.model.map;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.bonus.BonusToken;


/**
 * Class for the cities
 * A City on the map.
 */
public class City {
	
	private String cityName;
	private Color cityColor;
	private String cityColorString;
	private BonusToken bonusToken;
	private List<Emporium> emporia;
	
	/**
	 * Constructor for City.
	 * 
	 * @param cityName name of the city as a String
	 * @param cityColor hexadecimal color of the city as a String
	 * @param cityColorString the name of the color of the city as a String
	 * @param numberOfPlayers number of players of the game
	 */
	public City(String cityName, String cityColor, String cityColorString, int numberOfPlayers) {
		
		this.cityName = cityName; 
		this.cityColor = Color.decode(cityColor);
		this.cityColorString = cityColorString;
		emporia = new ArrayList<>();
		for(int i = 0; i < numberOfPlayers; i++) {
			emporia.add(new Emporium());
		}
	}
	
	/**
	 * Sets the bonusToken in this city.
	 * 
	 * @param bonusToken the bonus token of the city.
	 */
	public void setBonusTile(BonusToken bonusToken){
		this.bonusToken = bonusToken;
	}
	
	/**
	 * Gets the cityName.
	 * 
	 * @return cityName the name of the city as a String.
	 */
	public String getCityName() {
		return cityName;
	}
	
	/**
	 * Gets the color of this city.
	 * 
	 * @return cityColor the color of the city as a String.
	 */
	public String getColor() {
		
		return cityColorString;
	}

	/**
	 * Gets the emporia built in this city.
	 * 
	 * @return emporia a list of emporium built in this city.
	 */
	public List<Emporium> getEmporia() {
		return emporia;
	}
	
	
	/**
	 * Checks whether or not the given player can build an emporium
	 * in this city.
	 * 
	 * @param playerID the player ID identifying the player
	 * @return a boolean value
	 */
	public boolean canBuildEmporium(int playerID){
		for(Emporium emporium : emporia){
			if(emporium.isBuilt() && emporium.getPlayerID() == playerID) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Returns the number of emporia built in this city, representing
	 * the number of assistants to spend to build a new emporium.
	 * This method must be invoked only if the player can build an emporium,
	 * so if he hasn't already built in this city.
	 * @return counter
	 */
	public int assistantsCostToBuild() {
		
		int counter = 0;
		
		for(Emporium emporium : emporia) {
			
			if(emporium.isBuilt()) {
				
				counter++;
			}
		}
		
		return counter;
	}
	
	/**
	 * Builds an emporium in this city.
	 * 
	 * @param playerID the player ID identifying the player
	 */
	public void buildEmporium(int playerID){
		for( int i = 0; i < emporia.size(); i++) {
			if(!emporia.get(i).isBuilt()) {
				emporia.get(i).setBuilt();
				emporia.get(i).setPlayerID(playerID);
				return;
			}
		}
	}
	
	/**
	 * Gets the bonus token of this city.
	 * 
	 * @return bonusToken the bonus of this city.
	 */
	public BonusToken getBonusToken() {
		return bonusToken;
	}
	
	/**
	 * Checks whether the City has an emporium or not.
	 * 
	 * @return bool value, true if there are no emporium in the city.
	 */
	private boolean noEmporia() {
		int cont = 0;
		for(Emporium emporium : getEmporia()) {
			if(!emporium.isBuilt()) {
				cont++;
			}
		}
		if(cont == getEmporia().size()) {
			return true;
		}
		return false;
	}
	
	private String getEmporiaToString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("[");
		for(Emporium emporium : emporia) {

			sb.append(emporium.toString());
			sb.append(" ");
		}
		
		sb.replace(sb.toString().length() - 1, sb.toString().length(), "]");
		
		return sb.toString();
	}
	
	
	@Override
	public String toString() {
		if(bonusToken != null) {
			if(!noEmporia()) {
				if(Emperor.getInstance().getCurrentCity().equals(cityName)) {
					return "[" + cityName + " "+ cityColorString + bonusToken.toString()
					+" " + getEmporiaToString() + "] ♛";
				} else {
					return "[" + cityName + " "+ cityColorString + bonusToken.toString()
					+" " + getEmporiaToString() + "]";
				}
				
			} else {
				if(Emperor.getInstance().getCurrentCity().equals(cityName)) {
					return "[" + cityName + " "+ cityColorString + bonusToken.toString() + " [<<none>>]] ♛";
				} else {
					return "[" + cityName + " "+ cityColorString + bonusToken.toString() + " [<<none>>]]";
				}
				
			}
			
		} else {
			if(!noEmporia()) {
				if(Emperor.getInstance().getCurrentCity().equals(cityName)) {
					return "[" + cityName + " "+ cityColorString + " " + getEmporiaToString() +"] ♛";
				} else {
					return "[" + cityName + " "+ cityColorString + " " + getEmporiaToString() +"]";
				}
				
			} else {
				if(Emperor.getInstance().getCurrentCity().equals(cityName)) {
					return "[" + cityName + " "+ cityColorString + " [<<none>>]] ♛";
				} else {
					return "[" + cityName + " "+ cityColorString + " [<<none>>]]";
				}
					
				
			}
			
		}
		
	}
	
}
