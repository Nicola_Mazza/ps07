package it.polimi.ingsw.ps07.model.bonus;

import it.polimi.ingsw.ps07.model.Game;


/**
 * Abstract class for all the bonuses
 */
public abstract class Bonus {

	protected int bonusValue;
	
	/**
	 * Returns the value of the bonus as an int.
	 * @return bonusValue the value of the bonus.
	 */
	public int getBonusValue(){
		return bonusValue;
	}
	
	/**
	 * Method overridden in every bonus class, used to assigned
	 * a specific bonus to a player.
	 * 
	 * @param game, the instance of this game.
	 */
	public abstract void assignBonus(Game game);
	
	/**
	 * Method overridden in nobilityBonus, class in which
	 * it returns true.
	 * 
	 * @return false, always.
	 */
	public boolean isNobilityBonus() {
		
		return false;
	}
}
