package it.polimi.ingsw.ps07.model.bonus;

import java.util.Random;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.cards.PermitTile;

/**
 * The player receives one of the face up permit tiles
 * chosen randomly
 */
public class PermitTileBonus extends Bonus{

	private final int REVEALED_PERMIT_TILES = 2;
	private final int N_OF_REGIONS = 3;
	private PermitTile chosenTile;
	
	/**
	 * Constructs a permit tile bonus initializing
	 * the number of free permit tiles it gives
	 * @param bonusValue
	 */
	public PermitTileBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public void assignBonus(Game game) {
		
		Random rand = new Random();
		int randomRegion;
		int randomTile;
		
		randomRegion = rand.nextInt(N_OF_REGIONS);
		randomTile = rand.nextInt(REVEALED_PERMIT_TILES);
		
		switch(randomRegion) {
		
		case 0 :
			chosenTile = game.getDeck().getPermitTileDeckCoast().remove(randomTile);
			break;
		case 1 :
			chosenTile = game.getDeck().getPermitTileDeckHills().remove(randomTile);
			break;
		case 2 :
			chosenTile = game.getDeck().getPermitTileDeckMountains().remove(randomTile);
			break;
		default:
			break;
			
		}
		
		//add the chosen permit tile to the player's hand
		game.getCurrentPlayer().getPlayersHand().addPermitTile(chosenTile);
	}
	
	@Override
	public String toString() {
		return "free " + chosenTile.toString();
	}
}
