package it.polimi.ingsw.ps07.model.cards;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.bonus.AssistantBonus;
import it.polimi.ingsw.ps07.model.bonus.Bonus;
import it.polimi.ingsw.ps07.model.bonus.CardBonus;
import it.polimi.ingsw.ps07.model.bonus.CoinBonus;
import it.polimi.ingsw.ps07.model.bonus.NobilityBonus;
import it.polimi.ingsw.ps07.model.bonus.SlowActionBonus;
import it.polimi.ingsw.ps07.model.bonus.VictoryBonus;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * Class for the permit tile
 *
 */
public class PermitTile implements Card {
	
	private static final int MAX_CITIES_AND_BONUSES_PER_PERMIT_TILE = Integer.parseInt(ReaderXML.getInstance().readNumberOf("MaxCitiesAndBonusesPerPermitTile"));
	private List<String> cities = new ArrayList<>();
	private List<Bonus> bonuses = new ArrayList<>();
	private String region;
	private int index;
	
	/**
	 * Constructs a permit tile initializing its region, the index
	 * of the tile, the list of cities in which the player will be able 
	 * to build in and the bonuses granted by this tile
	 * @param region
	 * @param index
	 */
	public PermitTile(String region, int index) {
		
		this.region = region;
		this.index = index;
		
		for (int i = 1; i <= MAX_CITIES_AND_BONUSES_PER_PERMIT_TILE; i++) {
			String city = ReaderXML.getInstance().readPermitCity(region , index, i);
			String bonus = ReaderXML.getInstance().readPermitBonus(region, index, i);
			
			if (!("null".equals(city))) {
				cities.add(city);
			}
			
			if (!("null".equals(bonus))) {
				int bonusValue = Integer.parseInt(ReaderXML.getInstance().readPermitBonusValue(region, index, i));
				switch (bonus) {
					case "NumberOfCoins" :
						bonuses.add(new CoinBonus(bonusValue));
						break;
					case "NumberOfVictoryPoints" :
						bonuses.add(new VictoryBonus(bonusValue));
						break;
					case "NumberOfCards" :
						bonuses.add(new CardBonus(bonusValue));
						break;
					case "NumberOfAssistants" :
						bonuses.add(new AssistantBonus(bonusValue));
						break;
					case "NobilitySteps" :
						bonuses.add(new NobilityBonus(bonusValue));
						break;
					case "SlowAction":
						bonuses.add(new SlowActionBonus(bonusValue));
						break;
					default :
						break;
				}
			}
		}
	}

	/**
	 * @return index, the index of the tile
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * @return region, the region of this tile
	 */
	public String getRegionName() {
		return region;
	}
	
	/**
	 * @return cities, list of the cities of this tile
	 * these are the cities in which the player that redeems
	 * this tile wile be able to build in
	 */
	public List<String> getTileCities() {
		return cities;
	}

	/**
	 * @return bonuses, a list of the bonuses granted by this
	 * tile when it's redeemed
	 */
	public List<Bonus> getTileBonuses() {
		
		return bonuses;
	}

	@Override
	public String toString() {
		
		return "[" + cities + ", " + bonuses + "] ";
	}
}
