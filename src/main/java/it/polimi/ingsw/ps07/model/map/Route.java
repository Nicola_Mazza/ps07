package it.polimi.ingsw.ps07.model.map;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.util.NobilityBonusCellInitializer;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * A class to represent the Nobility path.
 */
public class Route {
	public static final int VICTORY_PATH = Integer.parseInt(ReaderXML.getInstance().readConstant("VictoryPath"));
	public static final int COINS_PATH = Integer.parseInt(ReaderXML.getInstance().readConstant("CoinsPath"));
	public static final int NOBILITY_PATH = Integer.parseInt(ReaderXML.getInstance().readConstant("NobilityPath"));
	public static final int NUMBER_OF_NOBILITY_BONUS = Integer.parseInt(ReaderXML.getInstance().readConstant("NumberOfNobilityBonus"));
	private List<NobilityBonusCell> nobilityBoard; 

	/**
	 * Constructs Route initializing the nobilityBoard
	 * that represents the nobility path on the map
	 */
	public Route() {
		
		//creation of the Nobility Path
		nobilityBoard = new ArrayList<>();
		
		NobilityBonusCellInitializer nobilityBonusCellInitializer = new NobilityBonusCellInitializer();
		NobilityBonusCell nobilityBonusCell = null;
		
		//iterates for a number of time equal to the number of nobility cells in the nobility board
		for (int nobilityCellNumber = 1; nobilityCellNumber <= NOBILITY_PATH; nobilityCellNumber++) {
			
			nobilityBonusCell = nobilityBonusCellInitializer.createNobilityBonusCell(nobilityCellNumber);
			
			//if the nobility cell with at least one bonus has been created, it is then added to the nobility board
			if(nobilityBonusCell != null) {
				
				nobilityBoard.add(nobilityBonusCell);
			}
		}
	}
	
	/**
	 * Gets the nobility cell at the given index.
	 * 
	 * @param index index number of the cell.
	 * @return the nobility cell at the index position.
	 */
	public NobilityBonusCell getNobilityCell(int index){
		
		for(NobilityBonusCell nobilityBonusCell : nobilityBoard) {
			
			if(nobilityBonusCell.getNobilityIndex() == index) {
				
				return nobilityBonusCell;
			}
		}
		
		return null;
	}
	
	/**
	 * @return nobilityBoard, the nobility path on the map
	 */
	public List<NobilityBonusCell> getNobilityBoard() {
		return nobilityBoard;
	}
	
}	
