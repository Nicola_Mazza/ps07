package it.polimi.ingsw.ps07.model.cards;

/**
 * Interface for a card, we need this mostly for the market.
 */
public interface Card {
	
	// no methods.
}
