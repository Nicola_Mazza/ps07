package it.polimi.ingsw.ps07.model.bonus;

import java.util.List;
import java.util.Random;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.map.City;
import it.polimi.ingsw.ps07.util.ReaderXML;

/*
 * The player obtains the bonuses of a bonus token of bonusValue cities
 * in which he has built an emporium.
 * These bonuses can't make the player move forward on the nobility path.
 */
public class BonusTokenBonus extends Bonus{

	private final int N_OF_REGIONS = 3;
	
	/**
	 * Constructs a bonus token bonus initializing
	 * the number of bonus token it gives
	 * 
	 * @param bonusValue Integer bonus value.
	 */
	public BonusTokenBonus(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public void assignBonus(Game game) {
		
		List<City> cities = game.getMap().getAllCities();
		
		int randomCity;
		Random rand;
		
		for(int i = 0; i <= bonusValue; i++) {
		
			rand = new Random();
			
			randomCity = rand.nextInt(Integer.parseInt(ReaderXML.getInstance().readConstant("CitiesPerRegion")) * N_OF_REGIONS);
				
			for(Bonus bonus : cities.get(randomCity).getBonusToken().getBonuses()) {
				
				if(!bonus.isNobilityBonus()) {
					bonus.assignBonus(game);
				}
				
			}
		}
	}
	
}