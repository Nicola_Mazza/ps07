package it.polimi.ingsw.ps07.model.bonus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.cards.PermitTile;


/**
 * The player obtains the bonuses of a Permit Tile
 * he previously bought or already used. The tile
 * is chosen randomly
 */
public class BonusPermitTile extends Bonus{

	/**
	 * Constructs a bonus permit tile bonus
	 * initializing the number of permit tiles
	 * of which the bonuses are going to be redeemed
	 * by the player
	 * 
	 * @param bonusValue the value of the bonus.
	 */
	public BonusPermitTile(int bonusValue) {
		
		this.bonusValue = bonusValue;
	}

	@Override
	public void assignBonus(Game game) {
		
		List<PermitTile> playersTiles = new ArrayList<>();
		playersTiles.addAll(game.getCurrentPlayer().getPlayersHand().getPermitTiles());
		playersTiles.addAll(game.getCurrentPlayer().getPlayersHand().getUsedPermitTiles());
		Collections.shuffle(playersTiles);
		
		PermitTile chosenTile;
		int randomTile;
		Random rand = new Random();
		
		if(!playersTiles.isEmpty()) {
			
			randomTile = rand.nextInt(playersTiles.size());
			chosenTile = playersTiles.get(randomTile);
			
			for(Bonus bonus : chosenTile.getTileBonuses()) {
				
				bonus.assignBonus(game);
			}
		}
		
	}
	
	@Override
	public String toString() {
		return "Obtain the bonuses of an owned permit tile";
	}
}
