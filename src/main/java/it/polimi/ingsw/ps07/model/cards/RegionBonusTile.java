package it.polimi.ingsw.ps07.model.cards;

/**
 * Class for the region bonus tiles
 * The bonus shown at the bottom of each region.
 */
public class RegionBonusTile{

	private int bonus;
	
	// true when a player gets the tile. 
	private boolean redeemed; 
	
	/**
	 * Constructor for RegionBonusTile.
	 */
	public RegionBonusTile(){
		bonus = 5;
		redeemed = false;
	}
	
	/**
	 * Assigns the tile, sets redeemed true.
	 */
	public void setAssigned(){
		
		redeemed = true;
	}
	
	/**
	 * @return true if the tile has been assigned
	 */
	public boolean isAssigned() {
		
		return redeemed;
	}
	
	/**
	 * @return bonus, the number of victory points
	 * given by the tile if redeemed
	 */
	public int getBonusValue() {
		
		return bonus;
	}
	
	@Override
	public String toString() {
		if(redeemed) {
			
			if(bonus > 1) {
				return "✘ +" + bonus + " Victory Points";
			} else {
				return "✘ +" + bonus + " Victory Point";
			}
		} else {
			if(bonus > 1) {
				return "✔ +" + bonus + " Victory Points";
			} else {
				return "✔ +" + bonus + " Victory Point";
			}
			
		}		
	}
}
