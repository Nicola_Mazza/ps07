package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.controller.turn.InsertionMadeState;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.PermitTile;

/**
 * Class to perform a permit tile insertion
 * This class creates a permit tile insertion
 */
public class PermitTileInsertion extends InsertionAction{

	private final int chosenPermitTile;
	private PermitTile permitTile;
	
	/**
	 * Constructs a PermitTileInsertion initilizing the game, the player
	 * that performed the action, the index of the permit tile he wants to sell
	 * and its price
	 * @param game
	 * @param player
	 * @param chosenPermitTile
	 * @param price
	 */
	public PermitTileInsertion(Game game, Player player, int chosenPermitTile, int price) {
		
		this.chosenPermitTile = chosenPermitTile;
		this.price = price;
		this.player = player;
		this.game = game;
	}
	
	@Override
	public boolean isValid() {
		
		try {
			
			permitTile = game.getCurrentPlayer().getPlayersHand().getPermitTiles().remove(chosenPermitTile - 1);
			
			return true;
			
		} catch (IndexOutOfBoundsException e) {
			
			return false;
		}
	}

	@Override
	public Object execute() {
		
		game.getMarket().addPermitTileInsertion(permitTile, game.getPlayers().indexOf(player), price);
		
		setMessages(ResponseFactory.sellptResponse(game, player, chosenPermitTile, price));
		
		return InsertionMadeState.getInstance();
	}
}
