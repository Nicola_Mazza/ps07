package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.SlowAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the turn machine
 * this state is reached when the player performs the quick
 * action that allows him to perform another slow action
 */
public class QuickPerformAnotherSlowState extends TurnState{

	private static final QuickPerformAnotherSlowState INSTANCE = new QuickPerformAnotherSlowState();
	
	/**
	 * Returns the instance of this singleton.
	 * 
	 * @return INSTANCE
	 */
	public static QuickPerformAnotherSlowState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {
		
		return action instanceof SlowAction && action.isValid();
	}

}
