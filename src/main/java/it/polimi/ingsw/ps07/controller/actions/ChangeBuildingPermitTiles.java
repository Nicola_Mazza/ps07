package it.polimi.ingsw.ps07.controller.actions;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.Deck;
import it.polimi.ingsw.ps07.model.cards.PermitTile;

/**
 * A class for the action "Change building permit tiles".
 */
public class ChangeBuildingPermitTiles extends QuickAction{
	
	private static final int ACTION_ASSISTANTS_PRICE = 1;
	private String chosenRegion;

	/**
	 * Construct a new CBPT action initializing the
	 * region whose permit tiles will be changed.
	 * 
	 * @param game the game where to execute this action.
	 * @param player the player executing the CBPT action.
	 * @param chosenRegion the region chosen by the player.
	 */
	public ChangeBuildingPermitTiles(Game game, Player player, String chosenRegion) {
		
		this.game = game;
		this.player = player;
		this.chosenRegion = chosenRegion;
	}
	
	@Override
	public boolean isValid() {
		
		/**
		 * The only condition for this action to be 
		 * performed, is the player having enough assistants
		 * to change the tiles.
		 */
		if(game.getCurrentPlayer().getPlayersHand().getAssistantCounter() >= ACTION_ASSISTANTS_PRICE) {
			
			return true;
		}
		
		return false;
	}

	@Override
	public Object execute() {
			
		List<PermitTile> newRevealedPermitTiles = new ArrayList<>();
		
		game.getCurrentPlayer().getPlayersHand().decreaseAssistantCounter(ACTION_ASSISTANTS_PRICE);
		
		switch(chosenRegion) {
			
		case "COAST" :
				
			changeCoastTiles();
			newRevealedPermitTiles.add(game.getDeck().getPermitTileDeckCoast().get(0));
			newRevealedPermitTiles.add(game.getDeck().getPermitTileDeckCoast().get(1));
			
			break;
			
		case "HILLS" :
				
			changeHillsTiles();
			newRevealedPermitTiles.add(game.getDeck().getPermitTileDeckHills().get(0));
			newRevealedPermitTiles.add(game.getDeck().getPermitTileDeckHills().get(1));
			
			break;
			
		case "MOUNTAINS" :
				
			changeMountainsTiles();
			newRevealedPermitTiles.add(game.getDeck().getPermitTileDeckMountains().get(0));
			newRevealedPermitTiles.add(game.getDeck().getPermitTileDeckMountains().get(1));
			
			break;
			
		default:
			break;
		}
		
		//Sets the response and broadcast messages
		setMessages(ResponseFactory.cbptResponse(game, player, chosenRegion, newRevealedPermitTiles));
		
		return updateTurnMachineState();
	}
	
	/**
	 * Removes the revealed permit tiles on top of the deck and then appends them,
	 * so that they are placed at the bottom of the deck.
	 */
	private void changeCoastTiles() {
		
		for(int i = 0; i < Deck.REVEALED_PERMIT_TILES; i++) {
			
			game.getDeck().getPermitTileDeckCoast().add(game.getDeck().getPermitTileDeckCoast().remove(0));
			
		}
	}
	
	/**
	 * Removes the revealed permit tiles on top of the deck and then appends them,
	 * so that they are placed at the bottom of the deck.
	 */
	private void changeHillsTiles() {
		
		for(int i = 0; i < Deck.REVEALED_PERMIT_TILES; i++) {
			
			game.getDeck().getPermitTileDeckHills().add(game.getDeck().getPermitTileDeckHills().remove(0));
			
		}
	}
	
	/**
	 * Removes the revealed permit tiles on top of the deck and then appends them,
	 * so that they are placed at the bottom of the deck.
	 */
	private void changeMountainsTiles() {
		
		for(int i = 0; i < Deck.REVEALED_PERMIT_TILES; i++) {
			
			game.getDeck().getPermitTileDeckMountains().add(game.getDeck().getPermitTileDeckMountains().remove(0));
			
		}
	}
	
}
