package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.controller.turn.InsertionMadeState;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;

/**
 * This is an action of the market phase.
 * This action creates a new assistants insertion
 */
public class AssistantsInsertion extends InsertionAction{

	private final int nOfAssistants;
	
	/**
	 * Constructs a new AssistantsInsertion object
	 * initializing the number of assistants and the price
	 * @param nOfAssistants
	 * @param price
	 */
	public AssistantsInsertion(Game game, Player player, int nOfAssistants, int price) {
		
		this.nOfAssistants = nOfAssistants;
		this.price = price;
		this.player = player;
		this.game = game;
	}
	
	@Override
	public boolean isValid() {
		
		if(nOfAssistants <= game.getCurrentPlayer().getPlayersHand().getAssistantCounter()) {
			
			return true;
		}
		
		return false;
	}

	@Override
	public Object execute() {
		
		game.getMarket().addAssistantsInsertion(nOfAssistants, game.getPlayers().indexOf(player), price);
		player.getPlayersHand().decreaseAssistantCounter(nOfAssistants);
		
		setMessages(ResponseFactory.sellaResponse(game, player, nOfAssistants, price));
		
		return InsertionMadeState.getInstance();
	}

}
