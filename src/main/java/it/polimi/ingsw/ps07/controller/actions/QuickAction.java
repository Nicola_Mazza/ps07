package it.polimi.ingsw.ps07.controller.actions;

import java.util.logging.Logger;

import it.polimi.ingsw.ps07.controller.turn.GameFinishedState;
import it.polimi.ingsw.ps07.controller.turn.MarketPhaseStartedState;
import it.polimi.ingsw.ps07.controller.turn.QuickActionPerformedFirstState;
import it.polimi.ingsw.ps07.controller.turn.TurnStartedState;
import it.polimi.ingsw.ps07.controller.turn.TurnState;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.PlayersHand;

/**
 * An abstract class for the quick actions.
 */
public abstract class QuickAction extends Action {
	
	protected static final Logger LOG = Logger.getLogger(QuickAction.class.getName());
	
	protected PlayersHand playersHand;
	protected Player player;
	
	/**
	 * Returns the next state of the machine that can be 
	 * a TurnStartedState if the player had already performed a 
	 * slow action, a QuickActionPerformedFirstState if the player
	 * hasn't performed a slow action yet, a marketPhaseStartedState
	 * if the player had already performed a slow action and is the 
	 * last of the round
	 * 
	 * @return the next state of the machine
	 */
	protected TurnState updateTurnMachineState() {
		
		TurnState.setQuickActionPerformed();
		
		if(TurnState.isSlowActionPerformed()) {
				
			Player nextPlayer = pass(player);
			
			game.setCurrentPlayer(nextPlayer);
			
			//A player has finished his turn, so the round counter must be incremented
			TurnState.incrementRoundCounter();
			
			if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && !TurnState.isFinishing()) {
				
				return MarketPhaseStartedState.getInstance();
			}
			
			if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && TurnState.isFinishing()) {
				
				TurnState.setGameFinished();
				
				return GameFinishedState.getInstance();
			}
			
			nextPlayer.getPlayersHand().addPoliticsCard(game.getDeck().getPoliticsDeck().remove(0));
			return TurnStartedState.getInstance();
			
		} else {
			
			return QuickActionPerformedFirstState.getInstance();
		}
	}
	
}
