package it.polimi.ingsw.ps07.controller.actions;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.messages.requests.actions.APTRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BERequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BEWKHRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.CBPTRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.DoneRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.EAARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.ECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PASARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PurchaseInsertionRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SATECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellAssistantsRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPermitTileRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPoliticsCardRequestMessage;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.util.exception.InvalidRequestMessageException;

/**
 * This class implements the Visitor pattern to create actions from an ActionRequestMessage
 * without "knowing" the concrete type of the message.
 */
public class ActionFactoryVisitorImplementation implements ActionFactoryVisitor {
	
	private final Game game;
	private List<Token> clients;
	
	/**
	 * Constructor for ActionFactoryVisitorImplementation.
	 * 
	 * @param game the game where we execute the actions.
	 * @param clients list of clients with the same order of the game players.
	 * @param turnMachine the turn machine that manages the turns of the game.
	 */
	public ActionFactoryVisitorImplementation (Game game, List<Token> clients) {
		
		this.game = game;
		this.clients = new ArrayList<>(clients);
		
	}
	
	/**
	 * Returns the player with the given token.
	 * 
	 * @param token the player's token.
	 * @return the player's object from the given token.
	 */
	private Player getPlayerByToken(Token token) {
		
		if(!clients.contains(token)) {
			
			throw new InvalidRequestMessageException("Token not found.");
		}
		
		int playerNumber = clients.indexOf(token);
		return game.getPlayers().get(playerNumber);
			
	}

	@Override
	public Action visit(PARequestMessage requestMessage) {
		
		return new PassAction(game, getPlayerByToken(requestMessage.getToken()));
	}

	@Override
	public Action visit(BERequestMessage requestMessage) {
		
		return new BuildEmporium(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getChosenPermitTile(), requestMessage.getChosenCityCapitalLetter());
	}

	@Override
	public Action visit(BEWKHRequestMessage requestMessage) {
		
		return new BuildEmporiumWithKingsHelp(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getDestinationCity(),
				requestMessage.getCard1(), requestMessage.getCard2(), requestMessage.getCard3(), requestMessage.getCard4());
	}

	@Override
	public Action visit(CBPTRequestMessage requestMessage) {
		
		return new ChangeBuildingPermitTiles(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getRegion());
	}

	@Override
	public Action visit(ECRequestMessage requestMessage) {
		
		//the fourth parameter of this action is set to true if this action is a slow one, as EC is used in SATEC (quick action) too
		//the action is visited only as a slow one, when it's called from SATEC's execute method it's not visited
		return new ElectCouncillor(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getColor(), requestMessage.getBalcony(), true);
	}

	@Override
	public Action visit(EAARequestMessage requestMessage) {
		
		//this action doesn't need further choices from the player 
		return new EngageAnAssistant(game, getPlayerByToken(requestMessage.getToken()));
	}

	@Override
	public Action visit(PASARequestMessage requestMessage) {
		
		return new PerformAnotherSlowAction(game, getPlayerByToken(requestMessage.getToken()));
	}

	@Override
	public Action visit(SATECRequestMessage requestMessage) {
		
		return new SendAssistantToElectCouncillor(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getColor(), requestMessage.getBalcony());
	}

	@Override
	public Action visit(APTRequestMessage requestMessage) {
		
		return new AcquirePermitTile(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getChosenBalconyString(),
				requestMessage.getChosenpermitTileString(), requestMessage.getCard1(), requestMessage.getCard2(),
				requestMessage.getCard3(), requestMessage.getCard4());
	}

	@Override
	public Action visit(SellAssistantsRequestMessage requestMessage) {

		return new AssistantsInsertion(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getNumberOfAssistants(),
				requestMessage.getPrice());
	}

	@Override
	public Action visit(SellPermitTileRequestMessage requestMessage) {
		
		return new PermitTileInsertion(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getIndexOfPermitTile(),
				requestMessage.getPrice());
	}

	@Override
	public Action visit(SellPoliticsCardRequestMessage requestMessage) {
		
		return new PoliticsCardInsertion(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getPoliticsCard(),
				requestMessage.getPrice());
	}

	@Override
	public Action visit(PurchaseInsertionRequestMessage requestMessage) {
		
		return new PurchaseAction(game, getPlayerByToken(requestMessage.getToken()), requestMessage.getTypeOfInsertion(), 
				requestMessage.getIndexOfInsertion());
	}

	@Override
	public Action visit(DoneRequestMessage donePurchasingRequestMessage) {
		
		return new DoneAction(game);
	}
}
