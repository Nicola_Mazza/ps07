package it.polimi.ingsw.ps07.controller.turn;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.messages.ResultMessages;
import it.polimi.ingsw.ps07.messages.requests.InvalidRequestMessage;
import it.polimi.ingsw.ps07.model.Game;

/**
 * We modeled this class with a FSM to manage the actions.
 * Actions act like transitions to new states of the FSM.
 * The validity of the actions is checked before they are performed.
 */
public class TurnMachine {
	private static final Logger LOG = Logger.getLogger(TurnMachine.class.getName());
	
	private TurnState state;
	private final Game game;
	
	
	/**
	 * Creates a TurnMachine to manage the game.
	 * 
	 * @param game the game managed by the turn machine.
	 */
	public TurnMachine(Game game) {
		
		this.game = game;
		state = TurnStartedState.getInstance();
		
	}
	
	/**
	 * Attempts to perform the action in the current state of FSM.
	 * 
	 * @param action the action to be performed.
	 * @return a message pair, if the action is invalid, the broadcast message is not generated.
	 */
	public ResultMessages performAction(Action action) {
		if(state.isActionValid(action, game)) {
			
			// this sets the next state of the FSM. If the nextState
			// is null the state does not change.
			TurnState nextState = state.performAction(action, game);
			
			if(nextState != null) {
				
				setState(nextState);
				
			}
			return action.getResult();
		}
		
		else
			
			// The action which is attempted to be performed is not legal.
			// The method informs the player that the action violate the game rules and
			// it does not send a broadcast message.
			return new ResultMessages(new InvalidRequestMessage("This action is illegal."), null);
		
	}
	
	/**
	 * Gets the current state of the FSM.
	 * 
	 * @return state the state of the turn machine.
	 */
	public TurnState getState() {
		
		return state;
		
	}
	
	/**
	 * Changes the current state of the FSM.
	 * 
	 * @param state the state to be set in the turn machine.
	 */
	public void setState(TurnState state) {

		LOG.log(Level.INFO, "State changed to " + state.getClass().getSimpleName());
		this.state = state;
		
	}
	
	
	
	
}
