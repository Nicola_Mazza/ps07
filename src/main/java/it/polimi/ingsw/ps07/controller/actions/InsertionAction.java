package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.model.Player;

/**
 * Abstract class for Insertion action
 * the price attribute is the price of the insertion
 * the player attribute is the player object of the player
 * that performed the insertion action
 *
 */
public abstract class InsertionAction extends Action{
	
	protected int price;
	protected Player player;
}
