package it.polimi.ingsw.ps07.controller.actions;

import java.util.List;

import it.polimi.ingsw.ps07.messages.ResultMessages;
import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.APTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.BEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.BEWKHBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.CBPTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.DONEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.EAABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.ECBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PASABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PurchaseBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SATECBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLPCBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLPTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.APTResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.BEResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.BEWKHResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.CBPTResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.DONEResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.EAAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.ECResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.PAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.PASAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.PurchaseResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SATECResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLPCResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLPTResponseMessage;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.cards.PermitTile;

/**
 * Factory to create a pair of messages (a private response one and
 * a public broadcast one) after the execution of an action.
 */
public class ResponseFactory {

	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player who is executing actions.
	 * @param turnNumber the number of the turn.
	 * @param nextPlayer the next player to play.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages passResponse(Game game, Player player, int turnNumber, Player nextPlayer) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		int nextPlayerInt;
		
		playerInt = game.getPlayers().indexOf(player);
		nextPlayerInt = game.getPlayers().indexOf(nextPlayer);
		
		responseMessage = new PAResponseMessage(true, turnNumber, nextPlayerInt);
		broadcastMessage = new PABroadcastMessage(playerInt, turnNumber, nextPlayerInt);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player who is executing the EC action.
	 * @param balconyName the region name the balcony belongs to.
	 * @param updatedBalcony the updated balcony after the successful execution of the action.
	 * @param councillorColor the color of the chosen Councillor.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages ecResponse(Game game, Player player, String balconyName,
			List<Councillor> updatedBalcony, String councillorColor) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new ECResponseMessage(true, balconyName, updatedBalcony.toString(), councillorColor);
		broadcastMessage = new ECBroadcastMessage(playerInt, balconyName, updatedBalcony.toString(), councillorColor);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player who is executing a BE action.
	 * @param chosenCity the city chosen by the player as a String.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages beResponse(Game game, Player player, String chosenCity) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new BEResponseMessage(true, chosenCity);
		broadcastMessage = new BEBroadcastMessage(playerInt, chosenCity);
		
		return new ResultMessages(responseMessage, broadcastMessage);
		
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player executing an APT action.
	 * @param chosenPermitTileNumber the index of the Permit Tile chosen by the player.
	 * @param regionName the name of the region chosen by the player.
	 * @param numberOfCardsUsed the number of cards used by the player in the execution of this APT action.
	 * @param numberOfJolliesUsed the number of jollies used by the player in the execution of this APT action.
	 * @param spentCoins the number of coins spent by the player.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages aptResponse(Game game, Player player, int chosenPermitTileNumber, String regionName, int numberOfCardsUsed,
			int numberOfJolliesUsed, int spentCoins) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new APTResponseMessage(true, chosenPermitTileNumber, regionName, numberOfCardsUsed, numberOfJolliesUsed, spentCoins);
		broadcastMessage = new APTBroadcastMessage(playerInt, chosenPermitTileNumber, regionName, numberOfCardsUsed, numberOfJolliesUsed, spentCoins);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player executing a BEWKH action.
	 * @param chosenCity the city chosen by the player.
	 * @param numberOfCardsUsed the number of Politics Cards used by the player.
	 * @param numberOfJolliesUsed the number of Jollies used by the player.
	 * @param spentCoins the number of coins spent by the player.
	 * @param usedAssistants the number of used assistants used by the player.
	 * @return a pair of messages.
	 */
	protected static ResultMessages bewkhResponse(Game game, Player player, String chosenCity, int numberOfCardsUsed, int numberOfJolliesUsed,
			int spentCoins, int usedAssistants) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new BEWKHResponseMessage(true, chosenCity, numberOfCardsUsed, numberOfJolliesUsed, spentCoins, usedAssistants);
		broadcastMessage = new BEWKHBroadcastMessage(playerInt, chosenCity, numberOfCardsUsed, numberOfJolliesUsed, spentCoins, usedAssistants);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player executing a SATEC action.
	 * @param balconyName the region name the balcony belongs to.
	 * @param councillorColor the color of the Councillor chosen by the player.
	 * @param updatedBalcony the balcony updated after the execution of this SATEC action.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages satecResponse(Game game, Player player, String balconyName,
			String councillorColor, List<Councillor> updatedBalcony) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new SATECResponseMessage(true, councillorColor, balconyName, updatedBalcony.toString());
		broadcastMessage = new SATECBroadcastMessage(playerInt, councillorColor, balconyName, updatedBalcony.toString());
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player executing an EAA action.
	 * @param numberOfSpentCoins the number of coins spent by the player.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages eaaResponse(Game game, Player player, int numberOfSpentCoins) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new EAAResponseMessage(true, numberOfSpentCoins);
		broadcastMessage = new EAABroadcastMessage(playerInt, numberOfSpentCoins);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player executing a CBPT action.
	 * @param regionName the name of the region chosen by the player.
	 * @param newRevealedPermitTiles the new reveled Permit Tiles after the execution of this CBPT action.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages cbptResponse(Game game, Player player, String regionName, List<PermitTile> newRevealedPermitTiles) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new CBPTResponseMessage(true, regionName, newRevealedPermitTiles.toString());
		broadcastMessage = new CBPTBroadcastMessage(playerInt, regionName, newRevealedPermitTiles.toString());
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be 
	 * visited and displayed
	 * @param game the game currently played.
	 * @param player the player executing a PASA action.
	 * @param numberOfUsedAssistants the number of used Assistants by the player.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages pasaResponse(Game game, Player player, int numberOfUsedAssistants) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new PASAResponseMessage(true, numberOfUsedAssistants);
		broadcastMessage = new PASABroadcastMessage(playerInt, numberOfUsedAssistants);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player executing a SELLA action.
	 * @param nOfAssistants the number of assistants the player is willing to sell.
	 * @param price the price the player is setting the insertion to.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages sellaResponse(Game game, Player player, int nOfAssistants, int price) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new SELLAResponseMessage(true, nOfAssistants, price);
		broadcastMessage = new SELLABroadcastMessage(playerInt);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be
	 * visited and displayed.
	 * 
	 * @param game the game currently played.
	 * @param player the player executing a SELLPT action.
	 * @param chosenPermitTile the index of the Permit Tile the player is willing to sell.
	 * @param price the price the player is setting the insertion to.
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage) a pair of messages.
	 */
	protected static ResultMessages sellptResponse(Game game, Player player, int chosenPermitTile, int price) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new SELLPTResponseMessage(true, chosenPermitTile, price);
		broadcastMessage = new SELLPTBroadcastMessage(playerInt);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be
	 * visited and displayed
	 * 
	 * @param game
	 * @param player
	 * @param chosenPoliticsCard
	 * @param price
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage)
	 */
	protected static ResultMessages sellpcResponseMessage(Game game, Player player, String chosenPoliticsCard, int price) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new SELLPCResponseMessage(true, chosenPoliticsCard, price);
		broadcastMessage = new SELLPCBroadcastMessage(playerInt);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be
	 * visited and displayed
	 * 
	 * @param game
	 * @param playerThatPerformed
	 * @param nextPlayer
	 * @return new ResultMessages(ResponseMessage, BroadcastMessage)
	 */
	protected static ResultMessages doneResponseMessage(Game game, int playerThatPerformed, int nextPlayer) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		responseMessage = new DONEResponseMessage(true);
		broadcastMessage = new DONEBroadcastMessage(playerThatPerformed, nextPlayer, game.getTurnNumber());
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}
	
	/**
	 * Returns both the response and broadcast messages that will be
	 * visited and displayed
	 * 
	 * @param game
	 * @param player
	 * @param typeOfInsertion
	 * @param nOfInsertion
	 * @param playerID
	 * @param price
	 * @return
	 */
	protected static ResultMessages purchaseResponseMessage(Game game, Player player, String typeOfInsertion, int nOfInsertion,
			int playerID, int price) {
		
		ResponseMessage responseMessage;
		BroadcastMessage broadcastMessage;
		
		int playerInt;
		playerInt = game.getPlayers().indexOf(player);
		
		responseMessage = new PurchaseResponseMessage(true, typeOfInsertion, nOfInsertion, playerID, price);
		broadcastMessage = new PurchaseBroadcastMessage(playerInt, typeOfInsertion, nOfInsertion, playerID, price);
		
		return new ResultMessages(responseMessage, broadcastMessage);
	}

}
