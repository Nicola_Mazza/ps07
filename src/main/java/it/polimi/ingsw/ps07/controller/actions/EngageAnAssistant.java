package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;

/**
 * A class for the action "Engage an assistant".
 */
public class EngageAnAssistant extends QuickAction{
	
	//coins required to buy an assistant
	private static final int ACTION_COINS_PRICE = 3;
	
	/**
	 * Constructor to create a new EAA action.
	 * 
	 * @param game the game where to execute this action.
	 * @param player the player that executes this action.
	 */
	public EngageAnAssistant(Game game, Player player) {
		
		this.game = game;
		this.player = player;
		playersHand = game.getCurrentPlayer().getPlayersHand();
	}

	@Override
	public boolean isValid() {
		
		/**
		 * the only condition for this action to be
		 * performed, is the player having enough coins
		 * to buy the assistant
		 */
		if(playersHand.getCoins() >= ACTION_COINS_PRICE) {
			
			return true;
		}
		
		return false;
	}

	@Override
	public Object execute() {

		//decreases the amount of coins in the player's hand
		playersHand.spendCoins(ACTION_COINS_PRICE);
			
		//increases the amount of assistants by 1 in the player's hand
		playersHand.increaseAssistantCounter(1);
		
		//sets the response and broadcast messages
		setMessages(ResponseFactory.eaaResponse(game, player, ACTION_COINS_PRICE));
		
		return updateTurnMachineState();
	}

}
