package it.polimi.ingsw.ps07.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.messages.requests.ConnectionRequest;
import it.polimi.ingsw.ps07.messages.requests.InvalidRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;
import it.polimi.ingsw.ps07.messages.responses.ConnectionResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.view.RequestHandler;
import it.polimi.ingsw.ps07.view.gamemanager.PublisherInterface;

/**
 * we want the player to look for a game, notify the GamesController which will be able to decide whether to add the player
 * to an already-existing game or create a new game.
 */
public class GamesController implements RequestHandler {
    private static final Logger LOG = Logger.getLogger(GamesController.class.getName());
    
    // COUNTDOWN should be 20
    private static final int COUNTDOWN = 10;
	
	Timer timer;
	TimerTask timerTask;
	
	private final Map<Token, GameController> clients;
	private final List<Token> pendingPlayers;
	private GameController pendingGame;
	private final PublisherInterface publisherInterface;
	
	public GamesController(PublisherInterface publisherInterface) {
		timer = new Timer();
		pendingPlayers = new ArrayList<>();
		this.publisherInterface = publisherInterface;
		clients = new HashMap<>();	
		pendingGame = new GameController(publisherInterface);
		
	}
	/**
	 * This is how we handle a new client joining the game. When there are two players, the countdown starts
	 * to create a new game. Meanwhile, if someone else tries to connect to a game, the timer resets and starts over.
	 * There is no upper bound for the number of players, so, when the timer expires, the game starts.
	 * 
	 * @param token new player's token.
	 */
	 public void newPlayer(Token token) {
	        pendingPlayers.add(token);
	        pendingGame.addPlayer(token);
	        clients.put(token, pendingGame);
	        LOG.log(Level.INFO, "New player added to waiting room.");
	        
	        // we don't have an upper bound!!!
	        if (pendingPlayers.size() >= 2) {
	            timer.cancel();
	            timerTask = new TimerTask() {
	                @Override
	                public void run() {
	                    try {
	                    	
	                    	pendingGame.initializeGame();
	                        pendingGame = new GameController(publisherInterface);
	                        pendingPlayers.clear();
	                        
	                        LOG.log(Level.INFO, "New game created.");
	                    } catch (Exception e) {
	                        LOG.log(Level.SEVERE, "Unexpected exception while creating a new game.", e);
	                    }
	                }
	            };
	            
	            timer = new Timer();
	            timer.schedule(timerTask, (long) COUNTDOWN * 1000);
	       
	        } 
	    }
	 
	 @Override
	 public Token connect() throws RemoteException {
		 
		 Token token = new Token(UUID.randomUUID().toString());
		 newPlayer(token);
		 return token;
		 
	 }
	 
	@Override
	public ResponseMessage handleRequest(RequestMessage requestMessage) throws RemoteException {
		if(requestMessage instanceof ConnectionRequest) {
			
			ConnectionRequest connectionRequest = (ConnectionRequest) requestMessage;
			
			if(connectionRequest.getToken() == null) {
				
				return new ConnectionResponseMessage(connect());
				
			} else {
				
				return new InvalidRequestMessage("Reconnection is not supported.");
				
			}
		} else {
			Token tokenRequest = requestMessage.getToken();
			
			if(!clients.containsKey(tokenRequest)) {
				return new InvalidRequestMessage("Invalid token.");
				
			} else {
				
				GameController game = clients.get(tokenRequest);
				return game.handleRequest(requestMessage);
			}
		}
				
	}
	
}
