package it.polimi.ingsw.ps07.controller.turn;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.model.Game;

/**
 * Abstract class for the states of the FSM
 *slowActionToBePerformed counts the number of slow action
 *that the player has yet to perform
 *quickActionPerformed is set as true if the player already performed 
 *a quick action
 *isFinishig is set as true if a player built his last emporium
 *isFinished is set as true if the game is finished
 *roundCounter counts the number of players that already performed their 
 *turn in a round
 *playersAlreadyPurchased contains the numbers of the players that already 
 *performed a purchase action or skipped the buying phase of the market phase
 */
public abstract class TurnState {

	protected static int slowActionToBePerformed = 1;
	private static boolean quickActionPerformed = false;
	private static boolean isFinishing = false;
	private static boolean isFinished = false;
	private static int roundCounter = 0;
	private static List<Integer> playersAlreadyPurchased = new ArrayList<>();
	
	/**
	 * @param action
	 * @param game
	 * @return
	 */
	public abstract boolean isActionValid(Action action, Game game);
		
	/**
	 * Executes the action returning the next state of the machine
	 * @param action
	 * @param game
	 * @return TurnState, the next state of the FSM
	 */
	public TurnState performAction(Action action, Game game) {
		
		return (TurnState)action.execute();
		
	}
	
	/**
	 * @return true if the the player can't perform
	 * another slow action
	 */
	public static boolean isSlowActionPerformed() {
		
		if(slowActionToBePerformed > 0) {
			return false;
		}
		
		return true;
	}


	/**
	 * Decrement the slow action counter.
	 * If the slow action counter is set to 2, it is then set to 1, and a slow action
	 * has to be performed another time.
	 * If the slow action counter is set to 1, no slow action has to be performed.
	 */
	public static void setSlowActionPerformed() {
		
		slowActionToBePerformed -- ;
	}
	
	/**
	 * Increments the slow action counter
	 */
	public static void incrementSlowActionCounter() {
		
		slowActionToBePerformed ++;
	}


	/**
	 * @return true if the current player
	 * has already performed a quick action
	 */
	public static boolean isQuickActionPerformed() {
		return quickActionPerformed;
	}


	/**
	 * Sets the quick action as performed by setting
	 * the boolean parameter as true
	 */
	public static void setQuickActionPerformed() {
		quickActionPerformed = true;
	}
	
	/**
	 * Sets the quick action as not performed by setting
	 * the boolean parameter as false
	 */
	public static void setQuickActionNotPerformed() {
		
		quickActionPerformed = false;
	}
	
	/**
	 * Returns the number of slow actions that have to be performed
	 * @return slowActionToBePerformed
	 */
	public static int getSlowActionToBePerformed() {
		return slowActionToBePerformed;
	}
	
	/**
	 * Returns the number of players that performed
	 * their turn in a round
	 * @return roundCounter
	 */
	public static int getRoundCounter() {
		
		return roundCounter;
	}
	
	/**
	 *  Reset to 0 the roundCounter
	 */
	public static void resetRoundCounter() {
		
		roundCounter = 0;
	}
	
	/**
	 *  Increments the round counter
	 */
	public static void incrementRoundCounter() {
		
		roundCounter++;
	}

	/**
	 * @return playersAlreadyPurchased, a list containing the numbers
	 * of the players that already entered their buy phase 
	 */
	public static List<Integer> getPlayersAlreadyPurchased() {
		
		return playersAlreadyPurchased;
	}
	
	/**
	 * If the game IsFinishing the players can perform their last turn
	 * @return true if a player has built all his emporia
	 */
	public static boolean isFinishing() {
		
		return isFinishing;
	}
	
	/**
	 * Sets the isFinishing parameter at true, so that the 
	 * players can perform their last turn
	 */
	public static void setGameFinishing() {
		
		isFinishing = true;
	}
	
	/**
	 * @return isFinished, true if the game is finished and
	 * the gameController has to calculate the winner of the game
	 */
	public static boolean isFinished() {
		
		return isFinished;
	}
	
	/**
	 * Sets the isFinished parameter at true, so that the 
	 * GameController can calculate the winner of the game
	 */
	public static void setGameFinished() {
		
		isFinished = true;
	}

}

