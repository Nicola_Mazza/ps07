package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.PassAction;
import it.polimi.ingsw.ps07.controller.actions.SlowAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the state machine that manages the game.
 * This state is reached when the player performs a quick action first.
 */
public class QuickActionPerformedFirstState extends TurnState {

	private static final QuickActionPerformedFirstState INSTANCE = new QuickActionPerformedFirstState();
	
	
	/**
	 * Gets INSTANCE.
	 * 
	 * @return INSTANCE the instance of this Singleton object.
	 */
	public static QuickActionPerformedFirstState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {

		return action instanceof SlowAction && action.isValid();
	}

}
