package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.controller.turn.InsertionMadeState;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;

/**
 * Class for the market phase
 * This action creates a politics card insertion
 *
 */
public class PoliticsCardInsertion extends InsertionAction{

	private final String cardColorString;
	private PoliticsCard chosenPoliticsCard;
	
	/**
	 * Constructs a new PoliticsCardInsertion initializing
	 * the color of the chosen card and the price
	 * @param game
	 * @param player
	 * @param cardColorString
	 * @param price
	 */
	public PoliticsCardInsertion(Game game, Player player, String cardColorString, int price) {
		
		this.cardColorString = cardColorString;
		this.price = price;
		this.player = player;
		this.game = game;
	}
	
	@Override
	public boolean isValid() {
		
		if(player.getPlayersHand().getPoliticsCards().isEmpty()) {
			
			return false;
		}
		
		chosenPoliticsCard = null;
		boolean found = false;
		
		for(int i = 0; i < player.getPlayersHand().getPoliticsCards().size(); i++) {
			
			if(cardColorString.equals(player.getPlayersHand().getPoliticsCards().get(i).getStringColor()) &&
					!found) {
				
				chosenPoliticsCard = player.getPlayersHand().getPoliticsCards().remove(i);
				found = true;
			}
		}
		
		if(!found) {
			
			return false;
		}
		
		return true;
	}

	@Override
	public Object execute() {
		
		game.getMarket().addPoliticsCardInsertion(chosenPoliticsCard, game.getPlayers().indexOf(player), price);
		
		setMessages(ResponseFactory.sellpcResponseMessage(game, player, cardColorString, price));
		
		return InsertionMadeState.getInstance();
	}
}
