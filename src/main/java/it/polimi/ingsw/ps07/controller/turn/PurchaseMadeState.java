package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.DoneAction;
import it.polimi.ingsw.ps07.controller.actions.PurchaseAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the machine that manages the game
 * This state is reached when the player performs his first
 * PurchaseAction and potentially any further PurchaseAction
 */
public class PurchaseMadeState extends TurnState{

	private static final PurchaseMadeState INSTANCE = new PurchaseMadeState();
	
	/**
	 * Returns the instance of this singleton object
	 * @return instance
	 */
	public static PurchaseMadeState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {
		
		return (action instanceof PurchaseAction || action instanceof DoneAction) && action.isValid();
	}

}
