package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.DoneAction;
import it.polimi.ingsw.ps07.controller.actions.PurchaseAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the machine that manages the game
 * This state is reached when the last player of the round
 * performs his last InsertionAction, so that the players can
 * perform the PurchaseAction they want to
 */
public class BuyingPhaseStartedState extends TurnState{

	private static final BuyingPhaseStartedState INSTANCE = new BuyingPhaseStartedState();
	
	/**
	 * Returns the instance of this singleton object.
	 * @return instance
	 */
	public static BuyingPhaseStartedState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {
		
		return (action instanceof PurchaseAction || action instanceof DoneAction) && action.isValid();
	}

}
