package it.polimi.ingsw.ps07.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitorImplementation;
import it.polimi.ingsw.ps07.controller.turn.TurnMachine;
import it.polimi.ingsw.ps07.controller.turn.TurnState;
import it.polimi.ingsw.ps07.messages.ResultMessages;
import it.polimi.ingsw.ps07.messages.broadcast.BPSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.ChatBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.GameFinishedBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.GameStartedBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.LTSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.NRSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.REBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.TEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.requests.ChangeMapRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.InvalidRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.PlayerDataRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;
import it.polimi.ingsw.ps07.messages.requests.SendChatRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowBalconyRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowCommandRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowMapRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowMarketRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowPoolRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowTilesRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowUsedTilesRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.APTRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.ActionRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BERequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BEWKHRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.ECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PurchaseInsertionRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SATECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellAssistantsRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPermitTileRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPoliticsCardRequestMessage;
import it.polimi.ingsw.ps07.messages.responses.AckResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.PlayerDataResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowBalconyResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowCommandResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowMapResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowMarketResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowPoolResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowTilesResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowUsedTilesResponseMessage;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;
import it.polimi.ingsw.ps07.model.cards.Deck;
import it.polimi.ingsw.ps07.model.cards.KingsRewardTile;
import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;
import it.polimi.ingsw.ps07.util.CitiesCollection;
import it.polimi.ingsw.ps07.util.ColorsCollection;
import it.polimi.ingsw.ps07.util.ConfigMap;
import it.polimi.ingsw.ps07.util.exception.InvalidRequestMessageException;
import it.polimi.ingsw.ps07.view.gamemanager.PublisherInterface;

/**
 * GameController is the controller of a single game. It dispatches messages
 * and manages the actions of every player through a TurnMachine.
 */
public class GameController {
	private static final Logger LOG = Logger.getLogger(GameController.class.getName());
	private static int numberOfGames = 1;
	
	private Game game;
	private TurnMachine turnMachine;
	private ActionFactoryVisitorImplementation actionFactory;
	
	private final List<Token> players; 
	
	private final PublisherInterface publisherInterface;
	
	private final int gameID;
	private ConfigMap map;
	
	private boolean bPhaseStarted = false;
	private int winnerPoints;
	private int winnerNumber;
	private final int NOBILITY_REWARD = 5;
	private final int PERMIT_TILES_REWARD = 3;
	
	/**
	 * Constructor for GameController.
	 * 
	 * @param publisherInterface the publisher interface to notify the subscribed clients.
	 */
	public GameController(PublisherInterface publisherInterface) {
		
		players = new ArrayList<>();
		
		this.publisherInterface = publisherInterface;
		
		// This is just the default map, it can be changed
        map = ConfigMap.MAPA;
        
		gameID = numberOfGames;
		numberOfGames++;
		
		publisherInterface.addTopic(getTopic());
	}

	/**
	 * Initializes the model and the TurnMachine.
	 */
	public void initializeGame() throws ParserConfigurationException, SAXException, IOException{
		
		game = new Game(players, map);
		
		turnMachine = new TurnMachine(game);
		actionFactory = new ActionFactoryVisitorImplementation(game, players);
		
		publisherInterface.publish(new GameStartedBroadcastMessage(getGameID(), game), getTopic());
		
	}

	/**
	 * Adds the player with this token to the game
	 * 
	 * @param token, the token of the player that's
	 * trying to connect
	 */
	public void addPlayer(Token token) {
		
		// the game hasn't started yet
		if(game == null) {
			
			players.add(token);
			publisherInterface.subscribeClientToTopic(getTopic(), token);
			
		}
	}
	
	/**
	 * @return the topic as a string 
	 */
	public String getTopic() {
		
		return "GAME" + Integer.toString(getGameID());
		
	}

	/**
	 * @return the gameID as an int
	 */
	private int getGameID() {
		
		return gameID;
		
	}
	
	/**
	 * @param token the token of the player
	 * @return true if the index of the token in its arrayList
	 * equals the number of the current player
	 */
	private boolean isThisPlayerTurn(Token token) {
		
		 return players.indexOf(token) == game.getCurrentPlayerNumber();	
	}
	
	/**
	 * @param request, message containing the message that the player wrote
	 * @return an ack response for the player that wrote the message as a message
	 */
	private ResponseMessage handleChatMessage(SendChatRequestMessage request) {

        int clientNumber = players.indexOf(request.getToken());
        BroadcastMessage broadcast = new ChatBroadcastMessage(clientNumber, request.getMessage());
        publisherInterface.publish(broadcast, getTopic());
        return new AckResponseMessage("Chat message delivered.");

    }

	/**
	 * This method handles all the requests of the players
	 * returning the right response messages. 
	 * If the request is an action request message the action is performed,
	 * and if it's valid it'll set both the response and broadcast message.
	 * If the messages are set, so if the broadcast isn't null, a broadcast message
	 * is published. A response message is always returned.
	 * When the round ends, a market phase starts, a buying phase starts, a turn ends
	 * or a market phase finishes, a broadcast message is publish to notify all the players
	 * @param requestMessage
	 * @return a response message in line with the request
	 */
	public ResponseMessage handleRequest(RequestMessage requestMessage) {


		// these requests do not require the game to be initialized to be shown.
		
		if(requestMessage instanceof ShowCommandRequestMessage) {					
			
			return new ShowCommandResponseMessage();			
		}
		
		if(requestMessage instanceof SendChatRequestMessage) {
			
			return handleChatMessage((SendChatRequestMessage) requestMessage);
		}
		
		if (requestMessage instanceof ChangeMapRequestMessage) {
            return changeMap((ChangeMapRequestMessage) requestMessage);
        }
		
		if(game == null) {
			
			return new InvalidRequestMessage("Wait for the game to start.");
		}
		
		if(requestMessage instanceof PlayerDataRequestMessage) {
			
			return getPlayerData((PlayerDataRequestMessage) requestMessage);
		}
		
		if(requestMessage instanceof ShowMapRequestMessage) {
			
			return new ShowMapResponseMessage(game);
		}
		
		if(requestMessage instanceof ShowBalconyRequestMessage) {
			
			return new ShowBalconyResponseMessage(game.getMap().getCoast().getRegionBalcony().toString(), 
					game.getMap().getHills().getRegionBalcony().toString(), game.getMap().getMountains()
					.getRegionBalcony().toString(), game.getMap().getKingsBalcony().toString());
		}
		
		if(requestMessage instanceof ShowTilesRequestMessage) {
			
			return getTiles((ShowTilesRequestMessage) requestMessage);
		}
		
		if(requestMessage instanceof ShowPoolRequestMessage) {
			
			return new ShowPoolResponseMessage(game.getMap().getCouncillorsPool().toString());
		}
		
		if(requestMessage instanceof ShowUsedTilesRequestMessage) {
			
			return new ShowUsedTilesResponseMessage(game.getCurrentPlayer().getPlayersHand()
					.getUsedPermitTiles().toString());
		}
		
		if(requestMessage instanceof ShowMarketRequestMessage) {
			
			return new ShowMarketResponseMessage(game.getMarket().getInsertionsArray("PT").toString(),
					game.getMarket().getInsertionsArray("PC").toString(),
					game.getMarket().getInsertionsArray("A").toString());
		}
		
		if(!isThisPlayerTurn(requestMessage.getToken())){
			
			return new InvalidRequestMessage("Wait for your turn to play.");
		}
		
		if(requestMessage instanceof ActionRequestMessage) {
			
			if(requestMessage instanceof ECRequestMessage && (!ColorsCollection.getInstance().getColors()
						.contains(((ECRequestMessage) requestMessage).getColor()))) {
			
				return new InvalidRequestMessage("The color <" + ((ECRequestMessage) requestMessage).getColor() +
							"> is not supported.");
			}
			if(requestMessage instanceof APTRequestMessage && (!areCardsValid((APTRequestMessage) requestMessage))) {
				
		
				return new InvalidRequestMessage("One or more of the chosen Politics Cards are not valid.");
					
			}
			

			if(requestMessage instanceof BEWKHRequestMessage && (!areCardsValid((BEWKHRequestMessage) requestMessage)
						|| !isCityValid((BEWKHRequestMessage) requestMessage))) {
					
					return new InvalidRequestMessage("One or more parameters of the BEWKH Action are not valid.");				
			}
			
			if(requestMessage instanceof BERequestMessage) {
				
				int chosenPermitTile = ((BERequestMessage) requestMessage).getChosenPermitTile();
				if(chosenPermitTile < 0 || chosenPermitTile > Deck.NUMBER_OF_PERMIT_TILES_PER_REGION * 3) {
					
					return new InvalidRequestMessage("<"+ chosenPermitTile + ">" + " is an invalid Permit Tile.");
				}
				
			if(requestMessage instanceof SellAssistantsRequestMessage) {
				
				int numberOfAssistants = ((SellAssistantsRequestMessage) requestMessage).getNumberOfAssistants();
				if(numberOfAssistants < 0) {
					
					return new InvalidRequestMessage("<" + numberOfAssistants + "> is an invalid number of Assistants.");
				}
			}
			
			if(requestMessage instanceof SellPermitTileRequestMessage) {
				
				int chosenPermitTile2 = ((SellPermitTileRequestMessage) requestMessage).getIndexOfPermitTile();
				if(chosenPermitTile2 < 0 || chosenPermitTile2 > Deck.NUMBER_OF_PERMIT_TILES_PER_REGION * 3) {
					
					return new InvalidRequestMessage("<"+ chosenPermitTile2 + ">" + " is an invalid Permit Tile.");
				}
			}
			
			if(requestMessage instanceof SellPoliticsCardRequestMessage &&
					(!ColorsCollection.getInstance().getColors()
							.contains(((SellPoliticsCardRequestMessage) requestMessage).getPoliticsCard()))) {
				
				return new InvalidRequestMessage("The color <" + ((SellPoliticsCardRequestMessage) requestMessage)
						.getPoliticsCard() + "> is not supported.");
			}
			if(requestMessage instanceof PurchaseInsertionRequestMessage &&
					((PurchaseInsertionRequestMessage) requestMessage).getIndexOfInsertion() < 0) {
				
				return new InvalidRequestMessage("Invalid index for the Insertion.");
			}
			if(requestMessage instanceof SATECRequestMessage &&
					(!ColorsCollection.getInstance().getColors()
					.contains(((SATECRequestMessage) requestMessage).getColor()))) {
				
				return new InvalidRequestMessage("The color <"+ ((SATECRequestMessage) requestMessage).getColor()
						+ "> is not supported.");
			}
			
			}
			
			ResultMessages result = performAction((ActionRequestMessage) requestMessage);
			
			if(result.broadcast != null) {
				
				publisherInterface.publish(result.broadcast, getTopic());
			}
			
			if(TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() == 0 &&
					TurnState.isFinishing() && TurnState.getRoundCounter() == 1) {
				
				publisherInterface.publish(lastTurnStartedBroadcastMessage(), getTopic());
			}
			
			if( TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() == 0 &&
					TurnState.getRoundCounter() != game.getNumberOfPlayers() && !TurnState.isFinished()) {
				
				publisherInterface.publish(turnEndedBroadcastMessage(), getTopic());
				TurnState.setQuickActionNotPerformed();
				TurnState.incrementSlowActionCounter();
			}
			
			if(TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() == 0 &&
					TurnState.getRoundCounter() == game.getNumberOfPlayers() && !TurnState.isFinishing()) {
				
				TurnState.resetRoundCounter();
				publisherInterface.publish(roundEndedBroadcastMessage(), getTopic());
				TurnState.setQuickActionNotPerformed();
				TurnState.incrementSlowActionCounter();
			}
			
			if(!TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() != 0 &&
					TurnState.getRoundCounter() == game.getNumberOfPlayers() && !bPhaseStarted) {
				
				publisherInterface.publish(buyingPhaseStartedBroadcastMessage(), getTopic());
				bPhaseStarted = true;
			}
			
			if(!TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() != 0 &&
					TurnState.getRoundCounter() == 0 && !TurnState.getPlayersAlreadyPurchased().isEmpty()) {
				
				game.increaseRoundNumber();
				publisherInterface.publish(newRoundStartedBroadcastMessage(), getTopic());
				TurnState.getPlayersAlreadyPurchased().clear();
				bPhaseStarted = false;
			}
			
			if(TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() == 0 &&
					TurnState.isFinishing() && TurnState.getRoundCounter() == 1) {
				
				publisherInterface.publish(lastTurnStartedBroadcastMessage(), getTopic());
			}
			
			if(TurnState.isFinished()) {
				
				finishGame();
			}
			
			return result.response;
		}
		
		return null;
	}

	 /**
     * Changes the map where the game will be played, can be done only before
     * starting the game.
     *
     * @param requestMessage the map change request message
     * @return the appropriate response message
     */
	private ResponseMessage changeMap(ChangeMapRequestMessage requestMessage) {
		
		// this request can be handled only if the game has not started yet.
		if(game == null) {
			
			if (!(players.indexOf(requestMessage.getToken()) == 0)) {
				
                return new InvalidRequestMessage("Only the first player can change map.");
            } 
            	
                try {
                	
					map = ConfigMap.valueOf(requestMessage.getMap());
					return new AckResponseMessage("Map changed to " + requestMessage.getMap() +".");
				} catch (Exception e) {
					
					LOG.log(Level.WARNING, "There was a problem changing the map.");
				}
        }

        return new InvalidRequestMessage("Cannot change map now.");
	}
		

	/**
	 * This method tries to create the action in line with the action request
	 * message and then perform it only if it's valid e and the state of the FSM
	 * allows the player to perform it. 
	 * 
	 * @param requestMessage
	 * @return ResultMessages (Response and Broadcast)
	 */
	private ResultMessages performAction(ActionRequestMessage requestMessage) {

		Action action;
		
		try {
			
			action = requestMessage.createAction(actionFactory);
			
			return turnMachine.performAction(action);
			
		} catch(InvalidRequestMessageException e) {
			
			LOG.log(Level.WARNING, "Handled an invalid request.", e);
			return new ResultMessages(new InvalidRequestMessage("Invalid request."), null);
		}
	}
	
	/**
	 * @return a broadcast message for the end of the turn, when the
	 * market phase is started
	 */
	private BroadcastMessage turnEndedBroadcastMessage() {
		
		int currentPlayer = game.getCurrentPlayerNumber();
		int nextPlayer = currentPlayer;
		
		return new TEBroadcastMessage(currentPlayer, game.getTurnNumber(), nextPlayer);
	}
	
	/**
	 * @return a broadcast message for the end of the round, when the 
	 * market phase starts with the insertion phase
	 */
	private BroadcastMessage roundEndedBroadcastMessage() {
		
		int currentPlayer = game.getCurrentPlayerNumber();
		int nextPlayer = currentPlayer;
		
		return new REBroadcastMessage(currentPlayer);
	}
	
	/**
	 * @return a broadcast message for the start of the buying phase
	 * during the market phase
	 */
	private BroadcastMessage buyingPhaseStartedBroadcastMessage() {
		
		int currentPlayer = game.getCurrentPlayerNumber();
		
		return new BPSBroadcastMessage(currentPlayer);
	}
	
	/**
	 * @return a broadcast message for the start of a new round, when the
	 * market phase is finished
	 */
	private BroadcastMessage newRoundStartedBroadcastMessage() {
		
		int currentPlayer = game.getCurrentPlayerNumber();
		
		return new NRSBroadcastMessage(currentPlayer, game.getRoundNumber());
	}
	
	/**
	 * @return a broadcast message when a player builds his last emporium
	 * and all the the other players can perform their last turn
	 */
	private BroadcastMessage lastTurnStartedBroadcastMessage() {
		//-1 because the turn has already passed to the next player
		int currentPlayer = game.getCurrentPlayerNumber() - 1;
		
		//if the current player is 0, the player that built all his emporia
		//is the last one of the players
		if(currentPlayer == -1) {
			currentPlayer = game.getNumberOfPlayers() - 1;
		}
		
		return new LTSBroadcastMessage(currentPlayer);
	}

	/**
	 * Returns a message containing the number of coins, victory points,
	 * nobility points, number of assistants, number of remaining emporia,
	 * the politics cards and the permit tiles of the player that sent 
	 * the request
	 * 
	 * @param request
	 * @return a player data response message 
	 */
	private ResponseMessage getPlayerData(PlayerDataRequestMessage request) {
	      
		if (game == null) {
	     
			return new InvalidRequestMessage("The game is not started yet.");	     
		}

		int playerNumber = players.indexOf(request.getToken());
		Player gamePlayer = game.getPlayers().get(playerNumber);
		
		int playerCoins = gamePlayer.getPlayersHand().getCoins();
		int playerVictoryPoints = gamePlayer.getPlayersHand().getVictoryPoints();
		int playerNobility = gamePlayer.getPlayersHand().getNobilityProgression();
		int playerAssistants = gamePlayer.getPlayersHand().getAssistantCounter();
		int playerEmporia = gamePlayer.getPlayersHand().getEmporiumCounter();

		List<String> politicsCards = new ArrayList<>();
		List<String> permitTiles = new ArrayList<>();
	 
		for(PoliticsCard pCard : gamePlayer.getPlayersHand().getPoliticsCards()) {
	        	
			politicsCards.add(pCard.getStringColor());
	    }
		
		for(PermitTile pTile : gamePlayer.getPlayersHand().getPermitTiles()) {
			
			permitTiles.add(pTile.toString());
		}
	   
		return new PlayerDataResponseMessage(playerNumber, game.getCurrentPlayerNumber(), politicsCards, permitTiles,
				playerCoins, playerVictoryPoints, playerNobility, playerAssistants, playerEmporia);

	    }
	
	/**
	 * Returns a message containing the face up permit tiles
	 * of all the three regions
	 * 
	 * @param requestMessage
	 * @return a show tiles response message
	 */
	private ResponseMessage getTiles(ShowTilesRequestMessage requestMessage) {
		
		if (game == null) {
		     
			return new InvalidRequestMessage("The game is not started yet.");	     
		}
		
		String coastPermitTiles = game.getDeck().getPermitTileDeckCoast().get(0).toString() + 
				game.getDeck().getPermitTileDeckCoast().get(1).toString();
		String hillsPermitTiles = game.getDeck().getPermitTileDeckHills().get(0).toString() + 
				game.getDeck().getPermitTileDeckHills().get(1).toString();
		String mountainsPermitTiles = game.getDeck().getPermitTileDeckMountains().get(0).toString() + 
				game.getDeck().getPermitTileDeckMountains().get(1).toString();
		
		return new ShowTilesResponseMessage(coastPermitTiles, hillsPermitTiles, mountainsPermitTiles);
	}
	
	/**
	 * @param requestMessage
	 * @return true if the cards chosen by the player are supported,
	 * jollies included (APT action)
	 */
	private boolean areCardsValid(APTRequestMessage requestMessage) {
		String color1 = requestMessage.getCard1();
		String color2 = requestMessage.getCard2();
		String color3 = requestMessage.getCard3();
		String color4 = requestMessage.getCard4();
		String jolly = "JOLLY";
		
		int unusedCard = 0;
		List<String> colors = ColorsCollection.getInstance().getColors();
		
		if("empty".equals(color1)) {
			unusedCard++;
		}
		
		if("empty".equals(color2)) {
			unusedCard++;
		}
		
		if("empty".equals(color3)) {
			unusedCard++;
		}
		
		if("empty".equals(color4)) {
			unusedCard++;
		}
		
		switch(unusedCard) {
		
		// controls the first 4 cards.
		case 0:
			if((!colors.contains(color1) &&	!color1.equals(jolly)) || 
					(!colors.contains(color2) && !color2.equals(jolly) ) ||
					(!colors.contains(color3) && !color3.equals(jolly)) ||
					(!colors.contains(color4) && !color4.equals(jolly))) {
				return false;
			}
			return true;
			
			
		// controls the first 3 cards
		case 1:
			if((!colors.contains(color1) && !color1.equals(jolly)) ||
					(!colors.contains(color2) && !color2.equals(jolly)) ||
					(!colors.contains(color3) && !color3.equals(jolly))) {

				return false;
			}
			return true;
			
		// controls the first 2 cards
		case 2:
			if((!colors.contains(color1) && !color1.equals(jolly)) ||
					(!color2.equals(jolly) && !colors.contains(color2))) {
				
				return false;
			}
			return true;
			
		// controls the first card	
		case 3:
			if(!colors.contains(color1) && !color1.equals(jolly)) {
				
				return false;
			}
			return true;
		default:
		
			return true;
		}
	}
	
	/**
	 * @param requestMessage
	 * @return true if the cards chosen by the player are supported,
	 * jollies included (APT action)
	 */
	private boolean areCardsValid(BEWKHRequestMessage requestMessage){
		String color1 = requestMessage.getCard1();
		String color2 = requestMessage.getCard2();
		String color3 = requestMessage.getCard3();
		String color4 = requestMessage.getCard4();
		String jolly = "JOLLY";
		
		int unusedCard = 0;
		List<String> colors = ColorsCollection.getInstance().getColors();
		
		if("empty".equals(color1)) {
			unusedCard++;
		}
		
		if("empty".equals(color2)) {
			unusedCard++;
		}
		
		if("empty".equals(color3)) {
			unusedCard++;
		}
		
		if("empty".equals(color4)) {
			unusedCard++;
		}
		
		switch(unusedCard) {
		
		// controls the first 4 cards.
		case 0:
			if((!colors.contains(color1) &&	!color1.equals(jolly)) || (!colors.contains(color2) && !color2.equals(jolly))
					|| (!colors.contains(color3) && !color3.equals(jolly)) ||
					(!colors.contains(color4) && !color4.equals(jolly))) {
				
				return false;
			}
			return true;
					
					
		// controls the first 3 cards
		case 1:
			if((!colors.contains(color1) && !color1.equals(jolly)) || (!colors.contains(color2) && !color2.equals(jolly))
					|| (!colors.contains(color3) && !color3.equals(jolly))) {
				
				return false;
			}
			return true;
			
		// controls the first 2 cards
		case 2:
			if((!colors.contains(color1) && !color1.equals(jolly)) || 		
					(!colors.contains(color2) && !color2.equals(jolly))) {
				
				return false;

			}
			return true;
					
		// controls the first card	
		case 3:
			if(!colors.contains(color1) && !color1.equals(jolly)) {
						
				return false;
			}
			return true;
			
		default:
				
			return true;
		}
	}

	
	/**
	 * @param message
	 * @return true if the city in which the player wants to build
	 * exists
	 */
	private boolean isCityValid(BEWKHRequestMessage message){
		
		String city = message.getDestinationCity();
		List<String> cities = CitiesCollection.getInstance().getCitiesCapitalLetters();
		if(!cities.contains(city)) {
			return false;
		} 
		return true;
	}
	
	/**
	 * This method calculate the final value of the victory points
	 * counter for each player in the game, finds the winner and
	 * publishes a broadcast message to let the players know who
	 * is the winner
	 */
	private void finishGame() {
		
		calculateVictoryPoints();
		
		findWinner();
		
		publisherInterface.publish(new GameFinishedBroadcastMessage(winnerNumber, winnerPoints), getTopic());
	}
	
	/**
	 * Calculate the victory points that have to be given to all 
	 * the players in the game in line with their color bonus tiles, 
	 * their king's reward tiles and number of face up and face 
	 * down tiles and then gives them to the players
	 * 
	 */
	private void calculateVictoryPoints() {
		
		int points = 0;
		int nobilityProgression = 0;
		List<Integer> supportArray = new ArrayList<>();
		List<Integer> playersToReward = new ArrayList<>();
		
		for(Player player : game.getPlayers()) {
			
			//Calculate points from color bonus tiles
			if(!player.getPlayersHand().getColorBonusTiles().isEmpty()) {
				
				for(ColorBonusTile colorTile : player.getPlayersHand().getColorBonusTiles()) {
					
					points += colorTile.getBonusValue();
				}
			}
			
			//Calculate points from king's reward bonus tiles
			if(!player.getPlayersHand().getKingsRewardTiles().isEmpty()) {
				
				for(KingsRewardTile kingsTile : player.getPlayersHand().getKingsRewardTiles()) {
					
					points += kingsTile.getValue();
				}
			}
			
			player.getPlayersHand().increaseVictoryPoints(points);
			points = 0;
		}
		
		for(Player player : game.getPlayers()) {
			
			supportArray.add(player.getPlayersHand().getNobilityProgression());
		}
		
		int max = Collections.max(supportArray);
		
		for(int i = 0; i < supportArray.size(); i++) {
			
			if(supportArray.get(i) == max) {
				playersToReward.add(i);
			}
		}
		
		for(int playerToReward : playersToReward) {
			
			game.getPlayers().get(playerToReward).getPlayersHand().increaseVictoryPoints(NOBILITY_REWARD);
		}
		
		playersToReward.clear();
		supportArray.clear();
		
		for(Player player : game.getPlayers()) {
			
			supportArray.add(player.getPlayersHand().getPermitTiles().size() + 
					player.getPlayersHand().getUsedPermitTiles().size());
		}
		
		max = Collections.max(supportArray);
		
		for(int i = 0; i < supportArray.size(); i++) {
			
			if(supportArray.get(i) == max) {
				playersToReward.add(i);
			}
		}
		
		for(int playerToReward : playersToReward) {
			
			game.getPlayers().get(playerToReward).getPlayersHand().increaseVictoryPoints(PERMIT_TILES_REWARD);
		}
	}
	
	/**
	 * Sets the winnerNumber parameter with the number of the
	 * players that has the highest number of victory points, 
	 * comparing all the points of the players of the game
	 */
	private void findWinner() {
		
		List<Integer> winners = new ArrayList<>();
		List<Integer> allVictoryPoints = new ArrayList<>();
		int max = 0;
		
		for(Player player : game.getPlayers()) {
			
			allVictoryPoints.add(player.getPlayersHand().getVictoryPoints());
		}
		
		max = Collections.max(allVictoryPoints);
		
		for(int i = 0; i < allVictoryPoints.size(); i++) {
			
			if(allVictoryPoints.get(i) == max) {
				
				winners.add(i);
			}
		}
		
		winnerNumber = winners.get(0);
		winnerPoints = max;
	}
}
