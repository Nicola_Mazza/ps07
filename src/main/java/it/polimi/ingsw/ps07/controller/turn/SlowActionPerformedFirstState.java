package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.PassAction;
import it.polimi.ingsw.ps07.controller.actions.QuickAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the state machine that manages the game.
 * This state is reached when the player performs a slow action first.
 */
public class SlowActionPerformedFirstState extends TurnState {

	private static final SlowActionPerformedFirstState INSTANCE = new SlowActionPerformedFirstState();
	
	@Override
	public boolean isActionValid(Action action, Game game) {
		
		return (action instanceof QuickAction || action instanceof PassAction)
				&& action.isValid();
	}
	
	/**
	 * Returns the instance of this singleton
	 * @return INSTANCE
	 */
	public static SlowActionPerformedFirstState getInstance() {
		
		return INSTANCE;
	}

}
