package it.polimi.ingsw.ps07.controller.actions;

import java.util.Random;

import it.polimi.ingsw.ps07.controller.turn.BuyingPhaseStartedState;
import it.polimi.ingsw.ps07.controller.turn.MarketPhaseStartedState;
import it.polimi.ingsw.ps07.controller.turn.TurnStartedState;
import it.polimi.ingsw.ps07.controller.turn.TurnState;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;

/**
 * The players can perform this action only in market phase, so
 * if the turn machine is in MarketPhaseStartedState, 
 * InsertionMadeState, BuyingPhaseStartedState, or
 * PurchaseMadeState.
 * Performing this action, the player passes the turn to the next
 * player and won't be able to perform another insertion action or
 * a purchase action
 *
 */
public class DoneAction extends Action{

	private int currentPlayerInt;
	
	public DoneAction(Game game) {
		
		this.game = game;
	}
	
	@Override
	public boolean isValid() {
		
		//This action is always valid
		return true;
	}

	@Override
	public Object execute() {
		
		if(TurnState.getRoundCounter() != game.getNumberOfPlayers()) {
			
			TurnState.incrementRoundCounter();
		}
		
		if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && TurnState.getPlayersAlreadyPurchased().size() != game.getNumberOfPlayers()) {
			
			boolean found = false;
			Random rand = new Random();
			
			while(!found) {
				
				int nextPlayer = rand.nextInt(game.getNumberOfPlayers());
				if(TurnState.getPlayersAlreadyPurchased().isEmpty()) {
					
					setNextPlayer(nextPlayer);
					found = true;
					
				}else if(!TurnState.getPlayersAlreadyPurchased().contains(nextPlayer)){
					
					setNextPlayer(nextPlayer);
					found = true;
				}
			}
			
			setMessages(ResponseFactory.doneResponseMessage(game, currentPlayerInt, game.getCurrentPlayerNumber()));
			
			return BuyingPhaseStartedState.getInstance();
		}
		
		if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && TurnState.getPlayersAlreadyPurchased().size() == game.getNumberOfPlayers()) {
			
			game.setTurnNumber(game.getTurnNumber() + 1);
			game.setCurrentPlayer(game.getPlayers().get(0));
			TurnState.resetRoundCounter();
			
			setMessages(ResponseFactory.doneResponseMessage(game, currentPlayerInt, game.getCurrentPlayerNumber()));
			TurnState.resetRoundCounter();
			
			game.getCurrentPlayer().getPlayersHand().addPoliticsCard(game.getDeck().getPoliticsDeck().remove(0));
			return TurnStartedState.getInstance();
		}
		
		currentPlayerInt = game.getCurrentPlayerNumber();
		Player nextPlayer = pass(game.getCurrentPlayer());
		game.setCurrentPlayer(nextPlayer);
		
		setMessages(ResponseFactory.doneResponseMessage(game, currentPlayerInt, game.getCurrentPlayerNumber()));
		
		return MarketPhaseStartedState.getInstance();
	}
	
	/**
	 * Sets the next player number and player object in game, 
	 * increment the turn number and set the player as one of the
	 * players that already purchased or passed the buying phase
	 * by adding him to the playersALreadyPurchased list in TurnState
	 * @param nextPlayer
	 */
	private void setNextPlayer(int nextPlayer) {
		
		// Increases the turn number
		game.setTurnNumber(game.getTurnNumber() + 1);
		currentPlayerInt = game.getCurrentPlayerNumber();
		game.setCurrentPlayer(game.getPlayers().get(nextPlayer));
		TurnState.getPlayersAlreadyPurchased().add(nextPlayer);
	}
}
