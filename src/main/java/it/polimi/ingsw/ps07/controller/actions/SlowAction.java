package it.polimi.ingsw.ps07.controller.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.controller.turn.FirstSlowActionPerformedState;
import it.polimi.ingsw.ps07.controller.turn.GameFinishedState;
import it.polimi.ingsw.ps07.controller.turn.MarketPhaseStartedState;
import it.polimi.ingsw.ps07.controller.turn.SlowActionPerformedFirstState;
import it.polimi.ingsw.ps07.controller.turn.TurnStartedState;
import it.polimi.ingsw.ps07.controller.turn.TurnState;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.bonus.Bonus;
import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;
import it.polimi.ingsw.ps07.model.map.City;
import it.polimi.ingsw.ps07.model.map.Emperor;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * An abstract class for the slow actions.
 */
public abstract class SlowAction extends Action {
	
	protected static final Logger LOG = Logger.getLogger(SlowAction.class.getName());
	private final int LAST_EMPORIUM_REWARD = 3;
	
	protected Player player;
	
	/**
	 * Returns the next state of the machine that
	 * can be a firstSlowActionPerformed if the player can perform
	 * another slow action, a TurnStarted if the player performed
	 * all the actions he was allowed to, a MarketPhaseStarted if
	 * the player is the last of the round and performed all the
	 * actions he was allowed to, a slowActionPerformedFirst if the
	 * player can perform another quick action.
	 * 
	 * @return the next state of the machine.
	 */
	protected TurnState updateTurnMachineState() {
		
		//decrement slow action counter
		TurnState.setSlowActionPerformed();
		
		//if the players built all his emporia, the game is set as "Finishing"
		if(player.getPlayersHand().getEmporiumCounter() == 0 && !TurnState.isFinishing()) {
			
			TurnState.setGameFinishing();
			player.getPlayersHand().increaseVictoryPoints(LAST_EMPORIUM_REWARD);
			//Reset the counter, so that if the return is a TurnStartedState
			//the roundCounter is incremented to 1 before the return statement and the
			//player that finished the emporium won't be able to perform another turn
			TurnState.resetRoundCounter();
		}
		
		//The player performed a PASA action so that he could perform 2 slow actions in a row
		if(TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() == 1) {
					
			return FirstSlowActionPerformedState.getInstance();
							
		} else if(TurnState.isQuickActionPerformed() && TurnState.getSlowActionToBePerformed() == 0){
					
			Player nextPlayer = pass(player);
			
			game.setCurrentPlayer(nextPlayer);
			
			//A player has finished his turn, so the round counter must be incremented
			TurnState.incrementRoundCounter();
			
			//All the players performed their turn, market phase should starts
			if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && !TurnState.isFinishing()) {
				
				return MarketPhaseStartedState.getInstance();
			}
			
			//The game is finished, return a state in which no further actions can be performed
			if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && TurnState.isFinishing()) {
				
				TurnState.setGameFinished();
				
				return GameFinishedState.getInstance();
			}
			
			//Not all the players have performed their turn in this round
			nextPlayer.getPlayersHand().addPoliticsCard(game.getDeck().getPoliticsDeck().remove(0));
			return TurnStartedState.getInstance();
					
		} else {
			
			//The player hasn't performed a quick action yet
			return SlowActionPerformedFirstState.getInstance();
		}
	}
	
	/**
	 * Starts the redeeming of the bonuses of the cities
	 * linked to the city in which the player has built
	 * and emporium.
	 * 
	 * @param city the city in which the player
	 * has built an emporium.
	 */
	protected void redeemBonuses(City city) {
		
		List<City> citiesWithBonus = new ArrayList<>();
		
		citiesWithBonus = game.getMap().getCitiesGraph().navigateAdiacentCities(city, citiesWithBonus, game.getCurrentPlayerNumber());
		
		if(citiesWithBonus != null) {
			
			if(Emperor.getInstance().getKingsStartingCity().equals(city.getCityName())) {
				
				for(int i = 0; i < citiesWithBonus.size(); i++) {
					
					if(Emperor.getInstance().getKingsStartingCity().equals(citiesWithBonus.get(i).getCityName())) {
						citiesWithBonus.remove(i);
					}
				}
			}
			
			for(int i = 0; i < citiesWithBonus.size(); i++) {
				
				for(Bonus bonus : citiesWithBonus.get(i).getBonusToken().getBonuses()) {
					
					bonus.assignBonus(game);
				}
			}
		}
	}
	
	/**
	 * Assign a color bonus tile to the current player if he
	 * has built an emporium in all the cities that have the same
	 * color of the city in which he has just built in 
	 * @param targetCity, the city in which the current player
	 * has just built an emporium
	 */
	protected void checkColorBonusTiles(City targetCity) {
		
		int counter = 0;
		String color = targetCity.getColor();
		
		List<City> allCities = game.getMap().getAllCities();
		
		for(City city : allCities) {
			
			if(city.getColor().equals(color) && !city.canBuildEmporium(game.getCurrentPlayerNumber())) {
				
				counter++;
			}
		}
		
		if(!Emperor.getInstance().getKingsStartingCity().equals(targetCity.getCityName())) {
			
			if(counter == Integer.parseInt(ReaderXML.getInstance().readNumberOfCitiesOfAColor(color,
					game.getChosenMap().toString().charAt(game.getChosenMap().toString().length() - 1)))) {
				
				List<ColorBonusTile> bonusTiles = game.getMap().getColorBonusTiles();
				
				for(int i = 0; i < bonusTiles.size(); i++) {
					
					if(bonusTiles.get(i).getStringColor().equals(color)) {
						
						player.getPlayersHand().getColorBonusTiles().add(bonusTiles.remove(i));
						tryToGetKingsRewardBonusTile();
					}
					
				}
			}
		}
		
		
	}
	
	/**
	 * Assign a region bonus tile to the current player if he has built 
	 * an emporium in all the cities of a region
	 * @param targetCity, the city in which the current player
	 * has just built an emporium
	 */
	protected void checkRegionBonusTiles(City targetCity) {
		
		int counter = 0;
		int citiesPerRegion = Integer.parseInt(ReaderXML.getInstance().readConstant("CitiesPerRegion"));
		
		for(City city : game.getMap().getCoast().getRegionCities()) {
			
			if(!city.canBuildEmporium(game.getCurrentPlayerNumber())) {
				counter++;
			}
		}
		
		if(counter == citiesPerRegion && !game.getMap().getCoast().getRegionBonusTile().isAssigned()) {
			
			player.getPlayersHand().increaseVictoryPoints(game.getMap().getCoast().getRegionBonusTile().getBonusValue());
			tryToGetKingsRewardBonusTile();
		}
		
		counter = 0;
		
		for(City city : game.getMap().getHills().getRegionCities()) {
			
			if(!city.canBuildEmporium(game.getCurrentPlayerNumber())) {
				counter++;
			}
		}
		
		if(counter == citiesPerRegion && !game.getMap().getHills().getRegionBonusTile().isAssigned()) {
			
			player.getPlayersHand().increaseVictoryPoints(game.getMap().getHills().getRegionBonusTile().getBonusValue());
			tryToGetKingsRewardBonusTile();
		}
		
		counter = 0;
		
		for(City city : game.getMap().getMountains().getRegionCities()) {
			
			if(!city.canBuildEmporium(game.getCurrentPlayerNumber())) {
				counter++;
			}
		}
		
		if(counter == citiesPerRegion && !game.getMap().getMountains().getRegionBonusTile().isAssigned()) {
			
			player.getPlayersHand().increaseVictoryPoints(game.getMap().getMountains().getRegionBonusTile().getBonusValue());
			tryToGetKingsRewardBonusTile();
		}
	}
	
	/**
	 *  Assign the face up king's reward bonus tile to the
	 *  current player if there actually is a face up king's
	 *  reward bonus tile
	 */
	protected void tryToGetKingsRewardBonusTile() {
		
		if(!game.getMap().getKingsRewardTiles().isEmpty()) {
			player.getPlayersHand().getKingsRewardTiles().add(game.getMap().getKingsRewardTiles().remove(0));
		}
	}
}
