package it.polimi.ingsw.ps07.controller;

import java.io.Serializable;

import com.google.common.base.Objects;

/**
 *
 *
 */
public class Token implements Serializable {

	//universally unique identifier for the client
	private final String uuid; 
	private int playerNumber;
	
	/**
	 * Constructor for Token.
	 * 
	 * @param uuid uuid to identify the client.
	 */
	public Token(String uuid) {
		this.uuid = uuid;
	}
	
	/**
	 * Gets uuid.
	 * 
	 * @return uuid the identifier for the client.
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * Gets playerNumber.
	 * 
	 * @return playerNumber the number of the player to be identified in the game.
	 */
	public int getPlayerNumber() {

		return playerNumber;
	}
	
	/**
	 * Sets playerNumber.
	 * 
	 * @param playerNumber the player number calculated by the game.
	 */
	public void setPlayerNumber(int playerNumber) {
		
		this.playerNumber = playerNumber;
	}
	
	@Override
	public boolean equals(Object o) {
	 
	    // NOTE: Only uuid is used!
		if (this == o){
	    	
			return true;	        
	    }
	    if (o == null || getClass() != o.getClass()){
	    	
	    	return false;	       
	    }
	    
	    Token token = (Token) o;	        
	    return Objects.equal(uuid, token.uuid);
    }
	
	@Override    
	public int hashCode() {
	   
		// NOTE: Only uuid is used!
		return Objects.hashCode(uuid);
	}

}
