package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the turn machine
 * This state is reached when the game is finished, no further action
 * is allowed and the game controller will calculate the winner of the game
 */
public class GameFinishedState extends TurnState{

	private static final GameFinishedState INSTANCE = new GameFinishedState();
	
	/**
	 * @return instance, the instance of this singleton object
	 */
	public static GameFinishedState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {
		
		//no further action is allowed, the game is finished
		return false;
	}
	
}
