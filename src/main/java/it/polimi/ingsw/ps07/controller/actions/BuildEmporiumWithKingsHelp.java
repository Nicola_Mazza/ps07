package it.polimi.ingsw.ps07.controller.actions;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.PlayersHand;
import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;
import it.polimi.ingsw.ps07.model.map.City;
import it.polimi.ingsw.ps07.model.map.Emperor;

/*
 * The player can build an emporium satisfying the king's balcony and moving the king to a city 
 */
public class BuildEmporiumWithKingsHelp extends SlowAction{
	
	private final PlayersHand playersHand;
	private String chosenCity;
	private City sourceCity;
	private City targetCity;
	private double distance;
	private int nOfCardsToUse;
	private int nOfJolliesToUse;
	
	private List<String> chosenCardsStrings;
	private List<PoliticsCard> chosenCards;
	private List<Councillor> satisfiedBalconyCouncillors;
	
	private int satisfiedCouncillors;
	
	/**
	 * Creates a BuildEmporiumWithKingsHelp action.
	 *
	 * @param game the game where to execute this action.
	 * @param player the player executing the BEWKH action.
	 * @param chosenCity the city chosen by the player as a String.
	 * @param card1 the first card played by the player.
	 * @param card2 the second card played by the player.
	 * @param card3 the third card played by the player.
	 * @param card4 the fourth card played by the player.
	 */
	public BuildEmporiumWithKingsHelp(Game game, Player player, String chosenCity, String card1, String card2, String card3, String card4) {
		
		this.game = game;
		this.player = player;
		this.chosenCity = chosenCity;
		this.playersHand = game.getCurrentPlayer().getPlayersHand();
		chosenCardsStrings = new ArrayList<>();
		chosenCardsStrings.add(card1);
		chosenCardsStrings.add(card2);
		chosenCardsStrings.add(card3);
		chosenCardsStrings.add(card4);
		
		for(int i = 0; i < chosenCardsStrings.size(); i++) {
			
			if("empty".equals(chosenCardsStrings.get(i))) {
				chosenCardsStrings.remove(i);
				i--;
			}
		}
		
		chosenCards = new ArrayList<>();
		satisfiedBalconyCouncillors = new ArrayList<>();
		
		nOfJolliesToUse = 0;
		nOfCardsToUse = 0;
		satisfiedCouncillors = 0;
		
		for(String card : chosenCardsStrings) {
			
			if("JOLLY".equals(card)) {
				
				nOfJolliesToUse++;
				
			} else {
				
				nOfCardsToUse++;
			}
		}
		
		
	}
	
	@Override
	public boolean isValid() {
		
		boolean cardFound;
		
		if(playersHand.getPoliticsCards().isEmpty()) {
			
			return false;
		}
		
		//Cycle the player's politics cards for every chosen card string to check if they're actually
		// in the player's hand
		for(String card : chosenCardsStrings) {
			
			cardFound = false;
			
			for(int i = 0; i < playersHand.getPoliticsCards().size(); i++) {
				
				if(card.equals(playersHand.getPoliticsCards().get(i).getStringColor()) && 
						!cardFound && !chosenCards.contains(playersHand.getPoliticsCards().get(i))) {
					
					chosenCards.add(playersHand.getPoliticsCards().remove(i));
					cardFound = true;
				}
			}
			
			if(!cardFound) {
				
				//If the action isn't valid, returns the removed politics cards to the player's hand
				playersHand.getPoliticsCards().addAll(chosenCards);
				return false;
			}
		}
		
		sourceCity = getCityByName(Emperor.getInstance().getCurrentCity().substring(0, 1));
		targetCity = getCityByName(chosenCity);
		DijkstraShortestPath shortestPath = 
				new DijkstraShortestPath((Graph) game.getMap().getCitiesGraph().getGraph(), sourceCity, targetCity);
		distance = shortestPath.getPathLength();
		
		if(cardsMatchKingsBalcony() && hasEnoughCoinsToMoveAndBuild()) {
			
			return true;
		}
		
		playersHand.getPoliticsCards().addAll(chosenCards);
		return false;
	}

	@Override
	public Object execute() {
		
		//move the emperor to the target city in which the new emporium will be built
		Emperor.getInstance().moveEmperor(targetCity.getCityName());
		
		//remove the coins cost of the action from the player's hand
		playersHand.spendCoins((int) ((distance * 2) + calculateCoinsCost()));
		
		//remove the assistants cost of the action from the player's hand
		playersHand.decreaseAssistantCounter(targetCity.assistantsCostToBuild());
		
		//remove an emporium from the player's hand
		playersHand.decreaseEmporiumCounter();
		
		// build the emporium in the target city
		targetCity.buildEmporium(game.getCurrentPlayerNumber());
		
		//the cards that have been used by the player are now put back in the deck
		game.getDeck().getPoliticsDeck().addAll(chosenCards);
		
		//method implemented in SlowAction super class, assigns the bonuses to the player
		redeemBonuses(targetCity);
		
		checkColorBonusTiles(targetCity);
		checkRegionBonusTiles(targetCity);
		
		//Sets both the response and broadcast messages
		setMessages(ResponseFactory.bewkhResponse(game, player, targetCity.getCityName(), nOfCardsToUse,
				 nOfJolliesToUse, (int) ((distance * 2) + calculateCoinsCost()), targetCity.assistantsCostToBuild() - 1));
		
		return updateTurnMachineState();
	}
	
	/**
	 * Checks whether the cards played by the player match the King's
	 * balcony or not.
	 * 
	 * @return true if every card chosen by the player matches
	 * a councillor in the king's balcony or is a JOLLY card.
	 */
	private boolean cardsMatchKingsBalcony() {
		
		boolean cardMatches;
		
		for(String card : chosenCardsStrings) {
			
			cardMatches = false;
			
			for(Councillor councillor : game.getMap().getKingsBalcony()) {
				
				if(card.equals(councillor.getStringColor()) && !cardMatches && !satisfiedBalconyCouncillors.contains(councillor)) {
					
					satisfiedBalconyCouncillors.add(councillor);
					satisfiedCouncillors++;
					cardMatches = true;
				}
				
				if("JOLLY".equals(card) && !cardMatches) {
					
					satisfiedCouncillors++;
					cardMatches = true;
				}
			}
			
			if(!cardMatches) {
				
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * @return the number of coins the player has to spend
	 * to satisfy the king's balcony
	 */
	private int calculateCoinsCost() {
		
		switch(satisfiedCouncillors) {
		
		case 1 :
			return 10 + nOfJolliesToUse;
		
		case 2 :
			return 7 + nOfJolliesToUse;
			
		case 3 :
			return 4 + nOfJolliesToUse;
			
		default :
			return nOfJolliesToUse;
		}
	}
	
	/**
	 * Get the city object with the cityName matching
	 * the city chosen by the player as a string.
	 * 
	 * @param chosenCity the city chosen by the player as a String.
	 * @return City the corresponding City object.
	 */
	private City getCityByName(String chosenCity) {
		
		List<City> allCities = new ArrayList<>();
		allCities.addAll(game.getMap().getCoast().getRegionCities());
		allCities.addAll(game.getMap().getHills().getRegionCities());
		allCities.addAll(game.getMap().getMountains().getRegionCities());
		
		for(City city : allCities) {
			
			if(chosenCity.equals(city.getCityName().substring(0, 1))) {
				
				return city;
				
			} 	
		}
		
		return null;
			
	}
	
	/**
	 * Checks whether the player has enough coins to move the emperor and build
	 * an Emporium in the chosen city or not.
	 * 
	 * @return true if the player has enough coins to satisfy
	 * the king's balcony, to move the emperor where he wants to
	 * and enough assistants to build an Emporium there.
	 */
	private boolean hasEnoughCoinsToMoveAndBuild() {
		
		if(!targetCity.canBuildEmporium(game.getCurrentPlayerNumber())) {
			return false;
		}
		
		if((int) ((distance * 2) + calculateCoinsCost()) > playersHand.getCoins() || targetCity.assistantsCostToBuild() > playersHand.getAssistantCounter()) {
			
			return false;	
		}
		
		return true;
	}

}
