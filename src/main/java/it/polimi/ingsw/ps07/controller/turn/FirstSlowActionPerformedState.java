package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.SlowAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the turn machine
 * This state is reached when the player performs the quick
 * action that allows him to perform another slow action as his first action,
 * and then performs the first of the two slow actions he can perform
 */
public class FirstSlowActionPerformedState extends TurnState{

	private static final FirstSlowActionPerformedState INSTANCE = new FirstSlowActionPerformedState();
	
	/**
	 * Returns the instance of this singleton
	 * 
	 * @return INSTANCE
	 */
	public static FirstSlowActionPerformedState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {
		
		return action instanceof SlowAction && action.isValid();
	}

}
