package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the state machine that manages the game.
 * This state is reached when the player has no action to execute.
 */
public class FinishedState extends TurnState {

	private static final FinishedState INSTANCE = new FinishedState();
	
	/**
	 * Gets INSTANCE.
	 * 
	 * @return INSTANCE the instance of this Singleton object.
	 */
	public static FinishedState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {

		// no further actions are allowed.
		return false;
	}

}
