package it.polimi.ingsw.ps07.controller.actions;


import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * A class for the action "Elect councillor".
 */
public class ElectCouncillor extends SlowAction{
	
	private static final int ACTION_REWARD = 4;
	
	private String chosenCouncillorColor;
	private String chosenBalcony;
	private boolean isThisSlowAction;
	
	private int balconySize;
	
	/**
	 * Construct a new ElectCouncillor action initializing the color of the councillor
	 * and the balcony to change chosen by the player.
	 * 
	 * @param game the game where to execute this action.
	 * @param player the player that executes this action.
	 * @param chosenCouncillorColor the councillor color chosen by the player.
	 * @param chosenBalcony the region of the balcony chosen by the player.
	 * @param isThisSlowAction boolean value that checks if this is the first Slow Action request or not.
	 */
	public ElectCouncillor(Game game, Player player, String chosenCouncillorColor, String chosenBalcony, boolean isThisSlowAction) {
		
		this.game = game;
		this.player = player;
		this.chosenCouncillorColor = chosenCouncillorColor;
		this.chosenBalcony = chosenBalcony;
		this.isThisSlowAction = isThisSlowAction;
		
		balconySize = Integer.parseInt(ReaderXML.getInstance().readConstant("BalconyLength"));
	}
	

	@Override
	public boolean isValid() {
		
		List<String> councillorsPoolColors = new ArrayList<>();
		
		for(Councillor councillor : game.getMap().getCouncillorsPool()) {
			
			councillorsPoolColors.add(councillor.getStringColor());
		}
		
		for(String color : councillorsPoolColors) {
			
			if(chosenCouncillorColor.equals(color)) {
				
				return true;
			}
		}
		
		return false;
	}

	@Override
	public Object execute() {
		
		switch (chosenBalcony) {
		
		case "COAST" : 
			changeCoastBalcony(getCouncillor(chosenCouncillorColor));
			setMessages(ResponseFactory.ecResponse(game, player, chosenBalcony,
					game.getMap().getCoast().getRegionBalcony(), chosenCouncillorColor));
			break;
		
		case "HILLS" :
			changeHillsBalcony(getCouncillor(chosenCouncillorColor));
			setMessages(ResponseFactory.ecResponse(game, player, chosenBalcony,
					game.getMap().getHills().getRegionBalcony(), chosenCouncillorColor));
			break;
			
		case "MOUNTAINS" :
			changeMountainsBalcony(getCouncillor(chosenCouncillorColor));
			setMessages(ResponseFactory.ecResponse(game, player, chosenBalcony,
					game.getMap().getMountains().getRegionBalcony(), chosenCouncillorColor));
			break;
			
		case "KING" :
			changeKingsBalcony(getCouncillor(chosenCouncillorColor));
			setMessages(ResponseFactory.ecResponse(game, player, chosenBalcony,
					game.getMap().getKingsBalcony(), chosenCouncillorColor));
			break;

		default :
			break;
			
		}
		
		if(!isThisSlowAction) {
			
			return null;
		}
		
		//gives the player some coins as a reward for the action just performed only if this is a slow action
		game.getCurrentPlayer().getPlayersHand().acquireCoins(ACTION_REWARD);
		
		return updateTurnMachineState();
	}
	
	/**
	 * Seeks and remove the councillor that matches the chosen color 
	 * from the councillors pool.
	 * 
	 * @param chosenCouncillor the councillor chosen by the player.
	 * @return the Councillor object corresponding the choice of the player.
	 */
	private Councillor getCouncillor(String chosenCouncillor) {
		
		List<Councillor> councillors;
		councillors = game.getMap().getCouncillorsPool();
		
		for(int i = 0; i < councillors.size(); i++) {
			
			if(chosenCouncillor.equals(councillors.get(i).getStringColor())) {
				
				return councillors.remove(i);
			}
		}
		
		//this line of code shouldn't be reached, as there has been previous input controls
		return null;
	}
	
	/**
	 * Pops the last councillor of the coast balcony and add at the first position the new chosen councillor.
	 * The councillor that's removed goes back to the councillors pool.
	 * 
	 * @param chosenCouncillor the Councillor object corresponding the councillor chosen by the player.
	 */
	private void changeCoastBalcony(Councillor chosenCouncillor) {
		game.getMap().getCouncillorsPool().add(game.getMap().getCoast().getRegionBalcony().remove(balconySize - 1));
		game.getMap().getCoast().getRegionBalcony().add(0, chosenCouncillor);
	}
	
	/**
	 * Pops the last councillor of the hills balcony and add at the first position the new chosen councillor
	 * The councillor that's removed goes back to the councillors pool.
	 * 
	 * @param chosenCouncillor the Councillor object corresponding the councillor chosen by the player.
	 */
	private void changeHillsBalcony(Councillor chosenCouncillor) {
		game.getMap().getCouncillorsPool().add(game.getMap().getHills().getRegionBalcony().remove(balconySize - 1));
		game.getMap().getHills().getRegionBalcony().add(0, chosenCouncillor);
	}
	
	/**
	 * Pops the last councillor of the mountains balcony and add at the first position the new chosen councillor
	 * The councillor that's removed goes back to the councillors pool.
	 * 
	 * @param chosenCouncillor the Councillor object corresponding the councillor chosen by the player.
	 */
	private void changeMountainsBalcony(Councillor chosenCouncillor) {
		game.getMap().getCouncillorsPool().add(game.getMap().getMountains().getRegionBalcony().remove(balconySize - 1));
		game.getMap().getMountains().getRegionBalcony().add(0, chosenCouncillor);
	}
	
	/**
	 * Pop the last councillor of the king's balcony and add at the first position the new chosen councillor
	 * The councillor that's removed goes back to the councillors pool.
	 * 
	 * @param chosenCouncillor the Councillor object corresponding the councillor chosen by the player.
	 */
	private void changeKingsBalcony(Councillor chosenCouncillor) {
		game.getMap().getCouncillorsPool().add(game.getMap().getKingsBalcony().remove(balconySize - 1));
		game.getMap().getKingsBalcony().add(0, chosenCouncillor);
	}
}
