package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.messages.requests.actions.APTRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BERequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BEWKHRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.CBPTRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.DoneRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.EAARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.ECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PASARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PurchaseInsertionRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SATECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellAssistantsRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPermitTileRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPoliticsCardRequestMessage;

/**
 * The factory uses the Visitor patter to create the correct Action from
 * an ActionRequestMessage without knowing the concrete type of the
 * message object.
 */
public interface ActionFactoryVisitor {

	/**
	 * Visit a PARequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the PARequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(PARequestMessage requestMessage);
	
	/**
	 * Visit a BERequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the BERequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(BERequestMessage requestMessage);
	
	/**
	 * Visit a BEWKHRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the BEWKHRequestMessage
	 * @return the corresponding action.
	 */
	Action visit(BEWKHRequestMessage requestMessage);
	
	/**
	 * Visit a CBPTRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the CBPTRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(CBPTRequestMessage requestMessage);
	
	/**
	 * Visit a ECRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the ECRequestMessage
	 * @return the corresponding action.
	 */
	Action visit(ECRequestMessage requestMessage);
	
	/**
	 * Visit a EAARequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the EAARequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(EAARequestMessage requestMessage);
	
	/**
	 * Visit a PASARequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the PASARequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(PASARequestMessage requestMessage);
	
	/**
	 * Visit a SATERequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the SATECRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(SATECRequestMessage requestMessage);
	
	/**
	 * Visit a APTRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the APTRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(APTRequestMessage requestMessage);

	/**
	 * Visit a SellAssistantsRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the SellAssistantsRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(SellAssistantsRequestMessage requestMessage);

	/**
	 * Visit a SellPermitTileRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the SellPermitTileRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(SellPermitTileRequestMessage requestMessage);

	/**
	 * Visit a sellPoliticsCardRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the sellPoliticsCardRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(SellPoliticsCardRequestMessage requestMessage);

	/**
	 * Visit a PurchaseInsertionRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the PurchaseInsertionRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(PurchaseInsertionRequestMessage requestMessage);

	/**
	 * Visit a DoneRequestMessage to create an appropriate action.
	 * 
	 * @param requestMessage the DoneRequestMessage.
	 * @return the corresponding action.
	 */
	Action visit(DoneRequestMessage requestMessage);
}
