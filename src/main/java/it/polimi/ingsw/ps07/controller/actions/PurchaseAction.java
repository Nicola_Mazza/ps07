package it.polimi.ingsw.ps07.controller.actions;

import java.util.List;

import it.polimi.ingsw.ps07.controller.turn.PurchaseMadeState;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.Assistant;
import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;
import it.polimi.ingsw.ps07.model.market.Insertion;

/**
 * Class for the three purchase action.
 * the players can purchase an assistant, a permit tile,
 * or a politics card
 *
 */
public class PurchaseAction extends Action{

	private final Player player;
	private final String typeOfInsertion;
	private final int nOfInsertion;
	private List<Insertion> insertionsList;
	private Insertion chosenInsertion;
	
	/**
	 * Constructs a Purchase Action initializing the game, the player that 
	 * performed the action, the type of the insertion he wants to buy and
	 * its number
	 * @param game
	 * @param player
	 * @param typeOfInsertion
	 * @param nOfInsertion
	 */
	public PurchaseAction(Game game, Player player, String typeOfInsertion, int nOfInsertion) {
		
		this.typeOfInsertion = typeOfInsertion;
		this.nOfInsertion = nOfInsertion;
		this.game = game;
		this.player = player;
	}
	
	@Override
	public boolean isValid() {
		
		insertionsList = game.getMarket().getInsertionsArray(typeOfInsertion);
		
		try{
			
			chosenInsertion = insertionsList.get(nOfInsertion - 1);
			
			if(player.getPlayersHand().getCoins() >= chosenInsertion.getPrice()) {
				
				insertionsList.remove(nOfInsertion - 1);
				return true;
			}
			
			return false;
			
		} catch(IndexOutOfBoundsException e) {
			
			return false;
		}
	}

	@Override
	public Object execute() {
		
		switch(typeOfInsertion.toUpperCase()) {
		
		case "A" :
			Assistant assistant = (Assistant) chosenInsertion.getInsertionObject();
			player.getPlayersHand().increaseAssistantCounter(assistant.getNumberOfAssistants());
			break;
			
		case "PT" :
			player.getPlayersHand().addPermitTile((PermitTile) chosenInsertion.getInsertionObject());
			break;
			
		case "PC" :
			player.getPlayersHand().addPoliticsCard((PoliticsCard) chosenInsertion.getInsertionObject());
			break;
			
		default :
			break;
		}
		
		player.getPlayersHand().spendCoins(chosenInsertion.getPrice());
		game.getPlayers().get(chosenInsertion.getPlayerID()).getPlayersHand().acquireCoins(chosenInsertion.getPrice());
		
		setMessages(ResponseFactory.purchaseResponseMessage(game, player, typeOfInsertion, nOfInsertion,
				chosenInsertion.getPlayerID(), chosenInsertion.getPrice()));
		
		return PurchaseMadeState.getInstance();
	}

}
