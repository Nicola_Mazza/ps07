package it.polimi.ingsw.ps07.controller.actions;

import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.Councillor;

/**
 * A class for the action "Send an assistant to elect a councillor".
 */
public class SendAssistantToElectCouncillor extends QuickAction{
	
	private static final int ACTION_ASSISTANTS_PRICE = 1;
	
	private String councillorColor;
	private String balcony;

	/**
	 * Constructor for the SendAssistantToElectCouncillor.
	 * 
	 * @param game the game where to execute this action.
	 * @param player the player that executes this action.
	 * @param councillorColor the councillor color chosen by the player.
	 * @param balcony the region of the balcony chosen by the player.
	 */
	public SendAssistantToElectCouncillor(Game game, Player player, String councillorColor, String balcony) {
		
		this.game = game;
		this.player = player;
		this.councillorColor = councillorColor;
		this.balcony = balcony;
		playersHand = game.getCurrentPlayer().getPlayersHand();
	}
	
	@Override
	public boolean isValid() {
		
		if(playersHand.getAssistantCounter() >= ACTION_ASSISTANTS_PRICE) {
			
			List<Councillor> councillorsPool = game.getMap().getCouncillorsPool();
			
			for(int i = 0; i < councillorsPool.size(); i++) {
				
				if(councillorColor.equals(councillorsPool.get(i).getStringColor())) {
					
					return true;
				}
				
			}
		}
		
		return false;
	}

	@Override
	public Object execute() {
		
		//Decreases the amount of assistants in the player's hand
		playersHand.decreaseAssistantCounter(ACTION_ASSISTANTS_PRICE);
		
		//Creates an EC action passing false, as it's not a slow action
		ElectCouncillor electCouncillor = new ElectCouncillor(game, player, councillorColor, balcony, false);
			
		//the return of the execute() is lost, and that's what we want (to avoid the update of the FSM)
		electCouncillor.execute();
		
		//sets response and broadcast messages
		setMessages(ResponseFactory.satecResponse(game, player, balcony, councillorColor, getChosenBalcony()));
		
		return updateTurnMachineState();
	}
	
	/**
	 * Returns the balcony chosen by the player, getting it from the right region
	 *
	 * @return List of Councillor objects basing on the player choice
	 */
	private List<Councillor> getChosenBalcony() {
		
		switch(balcony) {
		
		case "COAST" :
			
			return game.getMap().getCoast().getRegionBalcony();
			
		case "HILLS" :
			
			return game.getMap().getHills().getRegionBalcony();
			
		case "MOUNTAINS" :
			
			return game.getMap().getMountains().getRegionBalcony();
			
		case "KING" :
			
			return game.getMap().getKingsBalcony();
			
		default :
			
			return null;
		}
	}

}
