package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.PassAction;
import it.polimi.ingsw.ps07.controller.actions.QuickAction;
import it.polimi.ingsw.ps07.controller.actions.SlowAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is the initial state of the FSM managing the game. This is 
 * the beginning of the turn of the player. We decided to include the
 * drawing of the politics cards here, since the drawing action is mandatory
 * for each player.
 */
public class TurnStartedState extends TurnState {
	
	private static final TurnStartedState INSTANCE = new TurnStartedState();

	/**
	 * @return INSTANCE, the instance of this
	 * singleton object
	 */
	public static TurnStartedState getInstance() {
		
		return INSTANCE;
	}

	@Override
	public boolean isActionValid(Action action, Game game) {
		
		return (action instanceof SlowAction || action instanceof QuickAction ||
				action instanceof PassAction) && (action.isValid());
	}
}
