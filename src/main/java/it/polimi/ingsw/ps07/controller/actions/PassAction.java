package it.polimi.ingsw.ps07.controller.actions;


import it.polimi.ingsw.ps07.controller.turn.GameFinishedState;
import it.polimi.ingsw.ps07.controller.turn.MarketPhaseStartedState;
import it.polimi.ingsw.ps07.controller.turn.TurnStartedState;
import it.polimi.ingsw.ps07.controller.turn.TurnState;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;


/**
 * A class to pass the action.
 */
public class PassAction extends Action {
	
	private final Player player;
	
	public PassAction(Game game, Player player) {
		
		this.game = game;
		this.player = player;
		
	}
	
	@Override
	public boolean isValid() {
		
		// this action is always legal
		// not true, you can't, for instance, pass a SlowAction
		return true;
		
	}
	
	@Override
	public Object execute() {
		
		// sets the action as executed
		super.setExecuted();
		
		Player nextPlayer = pass(player);
		
		game.setCurrentPlayer(nextPlayer);
		
		// creates a response message to the pass action
		setMessages(ResponseFactory.passResponse(game, player, game.getTurnNumber(), nextPlayer));
		
		TurnState.setQuickActionPerformed();
		
		TurnState.incrementRoundCounter();
		
		if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && !TurnState.isFinishing()) {
			
			return MarketPhaseStartedState.getInstance();
		}
		
		if(TurnState.getRoundCounter() == game.getNumberOfPlayers() && TurnState.isFinishing()) {
			
			TurnState.setGameFinished();
			
			return GameFinishedState.getInstance();
		}
		
		nextPlayer.getPlayersHand().addPoliticsCard(game.getDeck().getPoliticsDeck().remove(0));
		return TurnStartedState.getInstance();
		
	}
}
