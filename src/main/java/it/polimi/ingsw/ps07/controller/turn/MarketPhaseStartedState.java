package it.polimi.ingsw.ps07.controller.turn;

import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.DoneAction;
import it.polimi.ingsw.ps07.controller.actions.InsertionAction;
import it.polimi.ingsw.ps07.model.Game;

/**
 * This is a state of the machine that manages the game
 * This state is reached when a round is completed, so that 
 * the market phase can start
 */
public class MarketPhaseStartedState extends TurnState{

	private static final MarketPhaseStartedState INSTANCE = new MarketPhaseStartedState();
	
	/**
	 * Returns the instance of this singleton object
	 * @return instance
	 */
	public static MarketPhaseStartedState getInstance() {
		
		return INSTANCE;
	}
	
	@Override
	public boolean isActionValid(Action action, Game game) {
		
		return (action instanceof InsertionAction || action instanceof DoneAction) && action.isValid();
	}

}
