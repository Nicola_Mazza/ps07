package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.controller.turn.QuickPerformAnotherSlowState;
import it.polimi.ingsw.ps07.controller.turn.TurnState;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;

/**
 * A class for the action "Perform another slow action".
 */
public class PerformAnotherSlowAction extends QuickAction{

	private static final int ACTION_ASSISTANTS_PRICE = 3;
	
	/**
	 * Constructor for PerformAnotherSlowAction.
	 * 
	 * @param game the game where to execute this action.
	 * @param player the player that executes this action.
	 */
	public PerformAnotherSlowAction(Game game, Player player) {
		
		this.game = game;
		this.player = player;
	}
	
	@Override
	public boolean isValid() {
		
		/**
		 * the only condition for this action to be
		 * performed, is the player having enough assistants
		 * to perform another slow action
		 */
		if(game.getCurrentPlayer().getPlayersHand().getAssistantCounter() >= ACTION_ASSISTANTS_PRICE) {
			
			return true;
		}
		
		return false;
	}

	@Override
	public Object execute() {
		
		player.getPlayersHand().decreaseAssistantCounter(ACTION_ASSISTANTS_PRICE);
		
		setMessages(ResponseFactory.pasaResponse(game, player, ACTION_ASSISTANTS_PRICE));
		
		//allow the player to perform another slow action 
		TurnState.incrementSlowActionCounter();
		TurnState.setQuickActionPerformed();
		
		return QuickPerformAnotherSlowState.getInstance();
	}

}
