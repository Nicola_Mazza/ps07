package it.polimi.ingsw.ps07.controller.actions;


/**
 * This interface will be implemented by every request message that involves 
 * creating a new action performed on the game on the server side.
 * This is how we allow the action factory to create the appropriate action from a
 * message without knowing the exact type of the message by using a visitor
 * pattern.
 */
public interface ActionCreator {

	/**
	 * This is needed by the action factory to create the appropriate action to
	 * a certain message with a Visitor Pattern. It is implemented so that every subclass
	 * must return themselves through this method.
	 * 
	 * @param visitor visitor for the visitor class, the factory of actions.
	 * @return the server action corresponding the message.
	 */
	Action createAction(ActionFactoryVisitor visitor);

}
