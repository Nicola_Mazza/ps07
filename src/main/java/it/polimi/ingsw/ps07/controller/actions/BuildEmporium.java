package it.polimi.ingsw.ps07.controller.actions;

import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.map.City;
import it.polimi.ingsw.ps07.util.ReaderXML;



/**
 * The player can build an emporium using one of the Permit Tiles in his hand
 */
public class BuildEmporium extends SlowAction{
	
	private PermitTile chosenPermitTile;
	private int chosenNumberOfPermitTile;
	private String chosenCityCapitalLetter;
	
	/**
	 * Constructor for BuildEmporium action (BE)
	 * initialization of game, player (current), the index of the chosen permit tile in player's hand
	 * and the capital letter of the chosen city.
	 * 
	 * @param game the game where to execute the action.
	 * @param player the player that builds an emporium.
	 * @param chosenNumberOfPermitTile the index number of the chosen Permit Tile.
	 * @param chosenCityCapitalLetter the initial capital letter of the chosen city.
	 */
	public BuildEmporium(Game game, Player player, int chosenNumberOfPermitTile, String chosenCityCapitalLetter) {
		
		this.game = game;
		this.player = player;
		this.chosenNumberOfPermitTile = chosenNumberOfPermitTile;
		this.chosenCityCapitalLetter = chosenCityCapitalLetter;
	}
	
	@Override
	public boolean isValid() {
		
		try {
			
			chosenPermitTile = game.getCurrentPlayer().getPlayersHand().getPermitTiles().get(chosenNumberOfPermitTile - 1);
			
			//returns false if the player didn't choose a city to build in and the tile allows him to build in more than a single city
			if("cityIsUnique".equals(chosenCityCapitalLetter) && chosenPermitTile.getTileCities().size() > 1) {
				
				return false;
				
			} else if("cityIsUnique".equals(chosenCityCapitalLetter)) {
				
				//if cityIsUnique, get the capital letter of the unique city of the permit tile and check it
				chosenCityCapitalLetter = chosenPermitTile.getTileCities().get(0);
				if(canBuildInThisCity()) {
					
					return true;
				}
				
			}
			
			for(String cityName : chosenPermitTile.getTileCities()) {
					
				if(cityName.startsWith(chosenCityCapitalLetter) && canBuildInThisCity()) {
						
					return true;
				}
			}
			
		} catch(IndexOutOfBoundsException e) {
			
			return false;
		}
		
		return false;
	}

	@Override
	public Object execute() {

		City city = getChosenCity(chosenCityCapitalLetter, chosenPermitTile);
		Player player = game.getCurrentPlayer();
				
		//builds the emporium and make the player pay the cost of the action, if any
		city.buildEmporium(game.getCurrentPlayerNumber());
		player.getPlayersHand().decreaseAssistantCounter(city.assistantsCostToBuild());
		player.getPlayersHand().decreaseEmporiumCounter();
				
		//method implemented in SlowAction super class, assigns the bonuses to the player
		redeemBonuses(city);
		
		//sets the permit tile chosen by the player as used, adding it to the usedPermitTiles list in his hand
		game.getCurrentPlayer().getPlayersHand().getUsedPermitTiles().add(game.getCurrentPlayer().getPlayersHand().
				getPermitTiles().remove(chosenNumberOfPermitTile - 1));
		
		checkColorBonusTiles(city);
		checkRegionBonusTiles(city);
		
		//sets response and broadcast messages
		setMessages(ResponseFactory.beResponse(game, player, city.getCityName()));
		
		return updateTurnMachineState();
	}
	
	/**
	 * Get the city object which city name parameter starts with the letter
	 * chosen by the player.
	 * 
	 * @param chosenCity the city chosen by the player.
	 * @param permitTile 
	 * @return the chosen city as a City object.
	 */
	private City getChosenCity(String chosenCity, PermitTile permitTile) {
		
		String regionName = permitTile.getRegionName();
		List<City> cities = null;
		
		//series of if statement to get all the cities of the right region
		// the right region is the one reported on the permit tile 
		if("Coast".equals(regionName)) {
			cities = game.getMap().getCoast().getRegionCities();
		}
		
		if("Hills".equals(regionName)) {
			cities = game.getMap().getHills().getRegionCities();
		}
		
		if("Mountains".equals(regionName)) {
			cities = game.getMap().getMountains().getRegionCities();
		}
		
		if(cities == null) {
			
			throw new IllegalArgumentException();
		}
		
		//instructions to find and return the chosen city
		for(int i = 0; i < Integer.parseInt(ReaderXML.getInstance().readConstant("CitiesPerRegion")); i++) {
			
			if(cities.get(i).getCityName().startsWith(chosenCity)) {
				return cities.get(i);
			} 
		}
		
		// the code doesn't handle this case
		// this line of code should not be reached
		// as there have been previous choices controls, so that
		// there should not be problems in finding the city.
		 
		return null;
	}
	
	/**
	 * 
	 * Returns true if the player hasn't already built an emporium in the
	 * chosen city and if he has enough assistants to build there. The
	 * assistants cost is based on how many players have already built in 
	 * the chosen city.
	 * 
	 * @return bool value, true if the player can build an emporium in
	 * this city.
	 */
	private boolean canBuildInThisCity() {
		
		for(String cityName : chosenPermitTile.getTileCities()) {
			
			if(cityName.startsWith(chosenCityCapitalLetter)) {
				
				City city = getChosenCity(chosenCityCapitalLetter, chosenPermitTile);
				Player player = game.getCurrentPlayer();
				
				if(city.canBuildEmporium(game.getCurrentPlayerNumber()) &&
						player.getPlayersHand().getAssistantCounter() >= city.assistantsCostToBuild()) {
					
					return true;
				}
			}
		}
		
		return false;
	}
	
}
