package it.polimi.ingsw.ps07.controller.actions;

import it.polimi.ingsw.ps07.messages.ResultMessages;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;


/**
 * Abstract class for the actions.
 */
public abstract class Action {
	
	protected boolean executed;
	protected Game game;
	
	private ResultMessages resultMessages;
	
	/**
	 * Constructor for a generic Action.
	 * It sets executed to false.
	 */
	protected Action() {
		
		this.executed = false;
		
	}
	
	/**
	 * Checks whether the action invoking this method
	 * is legal or not, according to the game rules.
	 * 
	 * @return bool value, ture value if the action is valid.
	 */
	public abstract boolean isValid();

	/**
	 * Returns both the private response message and the
	 * public broadcast message after the execution of the action.
	 * 
	 * @return resultMessages a pair of messages related to the execution of the action.
	 */
	public ResultMessages getResult() {
		
		return resultMessages;
		
	}

	/**
	 * Executes the action. After the execution, it returns the
	 * state of the TurnMachine. Null is returned if the state
	 * does not have to change.
	 * 
	 * @return the next TurnState of the state machine controlling the sequence
     * of actions.
	 */
	public abstract Object execute();


	/**
	 * Sets the action to executed.
	 */
	public void setExecuted() {

		executed = true;
		
	}
	
	/**
	 * Sets the two result messages of the action
	 * after its execution.
	 */
	protected void setMessages(ResultMessages resultMessages) {
		
		this.resultMessages = resultMessages;
		
	}
	
	/**
	 * Checks whether the action invoking this method
	 * has been executed or not.
	 * 
	 * @return bool value, true if the action has been executed.
	 */
	protected boolean hasBeenExecuted() {
		
		return executed;
		
	}
	
	/**
	 * The method implements the pass action.
	 * It returns the next player.
	 * 
	 * @param player2 the player that passes.
	 * @return the object Player of the next player.
	 */
	protected Player pass(Player player2) {

		int currentPlayerIndex = game.getPlayers().indexOf(player2);
		Player nextPlayer;
		
		// it checks if the current player is the last player of the round
		// it probably needs more controls for the non-connected players
		if(currentPlayerIndex == game.getNumberOfPlayers() - 1) {
			
			nextPlayer = game.getPlayers().get(0);
			
		} else {
			
			nextPlayer = game.getPlayers().get(currentPlayerIndex + 1);
			
		}
		
		// Increases the turn number
		game.setTurnNumber(game.getTurnNumber() + 1);
		
		return nextPlayer;
		
	}
	
}
