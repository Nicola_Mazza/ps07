package it.polimi.ingsw.ps07.controller.actions;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.PlayersHand;
import it.polimi.ingsw.ps07.model.bonus.Bonus;
import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;
import it.polimi.ingsw.ps07.util.ReaderXML;

/**
 * A class for the action "Acquire a permit tile".
 */
public class AcquirePermitTile extends SlowAction{
	
	private final int REVEALED_PERMIT_TILES = Integer.parseInt(ReaderXML.getInstance().readNumberOf("RevealedPermitTilesPerRegion"));
	
	private final String chosenBalconyString;
	private final int chosenPermitTile;
	private final List<String> chosenCardsStrings;
	
	private List<PoliticsCard> chosenCards;
	private List<Councillor> chosenBalcony;
	private List<Councillor> satisfiedBalconyCouncillors;
	private int nOfCardsToUse;
	private int nOfJolliesToUse;
	private PlayersHand playersHand;	
	private int satisfiedCouncillors;

	
	
	/**
	 * Constructs a new permit tile action initializing all its parameters.
	 * empty cards strings are discarded and the numbers of chosen normal cards 
	 * and jollies are counted.
	 * 
	 * @param game the game where to execute this action.
	 * @param player the player that executes this action.
	 * @param chosenBalconyString the region of the balcony chosen by the player as a String.
	 * @param chosenPermitTileString the permit tile chosen by the player as a String.
	 * @param card1 the first card used by the player as a String.
	 * @param card2 the second card used by the player as a String.
	 * @param card3 the third card used by the player as a String.
	 * @param card4 the fourth card used by the player as a String.
	 */
	public AcquirePermitTile(Game game, Player player, String chosenBalconyString, String chosenPermitTileString, 
			String card1, String card2, String card3, String card4) {
		
		this.game = game;
		this.playersHand = game.getCurrentPlayer().getPlayersHand();
		this.player = player;
		this.chosenBalconyString = chosenBalconyString;
		chosenPermitTile = Integer.parseInt(chosenPermitTileString);
		
		chosenCardsStrings = new ArrayList<>();
		chosenCardsStrings.add(card1);
		chosenCardsStrings.add(card2);
		chosenCardsStrings.add(card3);
		chosenCardsStrings.add(card4);
		
		//for cycle to remove the empty strings and keep only the valid cards
		for(int i = 0; i < chosenCardsStrings.size(); i++) {
			
			if("empty".equals(chosenCardsStrings.get(i))) {
				chosenCardsStrings.remove(i);
				i--;
			}
		}
		
		chosenCards = new ArrayList<>();
		satisfiedBalconyCouncillors = new ArrayList<>();
		
		nOfJolliesToUse = 0;
		nOfCardsToUse = 0;
		satisfiedCouncillors = 0;
		
		for(String card : chosenCardsStrings) {
			
			if("JOLLY".equals(card)) {
				
				nOfJolliesToUse++;
				
			} else {
				
				nOfCardsToUse++;
			}
		}
	}
	
	@Override
	public boolean isValid() {
		
		boolean cardFound;
		
		if(chosenPermitTile < 0 || chosenPermitTile > REVEALED_PERMIT_TILES) {
			
			return false;
		}
		
		if(playersHand.getPoliticsCards().isEmpty()) {
			
			return false;
		}
		
		//Cycle the player's politics cards for every chosen card string to check if they're actually
		// in the player's hand
		for(String card : chosenCardsStrings) {
			
			cardFound = false;
			
			for(int i = 0; i < playersHand.getPoliticsCards().size(); i++) {
				
				if(card.equals(playersHand.getPoliticsCards().get(i).getStringColor()) && 
						!cardFound && !chosenCards.contains(playersHand.getPoliticsCards().get(i))) {
					
					chosenCards.add(playersHand.getPoliticsCards().remove(i));
					cardFound = true;
				}
			}
			
			if(!cardFound) {
				
				//If the action isn't valid, returns the removed politics cards to the player's hand
				playersHand.getPoliticsCards().addAll(chosenCards);
				return false;
			}
		}
		
		chosenBalcony = getChosenBalcony();
		
		if(cardsMatch() && coinsCost() <= playersHand.getCoins()) {
			
			return true;
		}
		
		playersHand.getPoliticsCards().addAll(chosenCards);
		return false;
	}

	@Override
	public Object execute() {
		
		//returns the used cards to the bottom of the deck
		game.getDeck().getPoliticsDeck().addAll(chosenCards);
		
		//remove the chosen permit tile from its deck and assign it to the player's hand
		//permit tile's bonuses are assigned to the player with this method
		assignChosenPermitTile();
		
		//remove the amount of coins needed to perform the action
		playersHand.spendCoins(coinsCost());
		
		//Set both response and broadcast messages for this action
		setMessages(ResponseFactory.aptResponse(game, player, chosenPermitTile, chosenBalconyString, 
				nOfCardsToUse, nOfJolliesToUse, coinsCost()));
		
		return updateTurnMachineState();
	}
	
	/**
	 * Returns the balcony chosen by the player.
	 * 
	 * @return a list of councillors representing the balcony.
     */
	private List<Councillor> getChosenBalcony() {
		
		switch(chosenBalconyString) {
		
		case "COAST" :
			return game.getMap().getCoast().getRegionBalcony();
		
		case "HILLS" :
			return game.getMap().getHills().getRegionBalcony();
			
		case "MOUNTAINS" :
			return game.getMap().getMountains().getRegionBalcony();
			
		default :
			return null;
		}
	}
	
	/**
	 * Checks whether the cards match or not.
	 * 
	 * @return true if every card chosen by the player
	 * matches a councillor in the chosen balcony or is
	 * a JOLLY card.
	 */
	private boolean cardsMatch() {
		
		boolean cardMatches;
		
		for(String card : chosenCardsStrings) {
			
			cardMatches = false;
			
			for(Councillor councillor : chosenBalcony) {
				
				if(card.equals(councillor.getStringColor()) && !cardMatches && !satisfiedBalconyCouncillors.contains(councillor)) {
					
					satisfiedBalconyCouncillors.add(councillor);
					satisfiedCouncillors++;
					cardMatches = true;
				}
				
				if("JOLLY".equals(card) && !cardMatches) {
					
					satisfiedCouncillors++;
					cardMatches = true;
				}
			}
			
			if(!cardMatches) {
				
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Returns the number of coins the player has to spend to
	 * perform the action.
	 * 
	 * @return int amount representing the amount of coins.
     */
	private int coinsCost() {
		
		switch(satisfiedCouncillors) {
		
		case 1 :
			return 10 + nOfJolliesToUse;
		
		case 2 :
			return 7 + nOfJolliesToUse;
			
		case 3 :
			return 4 + nOfJolliesToUse;
			
		default :
			return nOfJolliesToUse;
		}
	}
	
	/**
	 * Assigns the permit tile the player has chosen to his hand
	 * and the bonuses on it.
	 */
	private void assignChosenPermitTile() {
		
		PermitTile permitTile;
		
		switch(chosenBalconyString) {
		
		case "COAST" :
			permitTile = game.getDeck().getPermitTileDeckCoast().remove(chosenPermitTile - 1);
			assignPermitTileBonuses(permitTile);
			playersHand.getPermitTiles().add(permitTile);
			return;
			
		case "HILLS" :
			permitTile = game.getDeck().getPermitTileDeckHills().remove(chosenPermitTile - 1);
			assignPermitTileBonuses(permitTile);
			playersHand.getPermitTiles().add(permitTile);
			return;
			
		case "MOUNTAINS" :
			permitTile = game.getDeck().getPermitTileDeckMountains().remove(chosenPermitTile - 1);
			assignPermitTileBonuses(permitTile);
			playersHand.getPermitTiles().add(permitTile);
			return;
			
		default:
			return;
		}
	}
	
	/**
	 * Assigns every bonus of the permit tile to the player performing the action.
	 * 
	 * @param permitTile the object Permit Tile.
	 */
	private void assignPermitTileBonuses(PermitTile permitTile) {
		
		for(Bonus bonus : permitTile.getTileBonuses()) {
			
			bonus.assignBonus(game);
		}
	}
	
}
