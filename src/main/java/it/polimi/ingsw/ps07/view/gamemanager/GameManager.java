package it.polimi.ingsw.ps07.view.gamemanager;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.controller.GamesController;

/**
 * Entry point to start a GameManager.
 */
public class GameManager {

	private static final Logger LOG = Logger.getLogger(GameManager.class.getName());
	public static void main(String[] args) {
		
		ServerConnectionFactory serverInitializer = new ServerConnectionFactory();
		PublisherInterface publisherInterface = serverInitializer.getPublisherInterface();
		
		GamesController mainController = new GamesController(publisherInterface);
		
		serverInitializer.setRequestHandler(mainController);
		serverInitializer.startServers();
		
		LOG.log(Level.INFO, "Game manager started, now listening for RMI/SOCKET incoming connections.");
	}
}
