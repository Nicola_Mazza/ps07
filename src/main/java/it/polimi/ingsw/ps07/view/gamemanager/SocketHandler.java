package it.polimi.ingsw.ps07.view.gamemanager;

import java.net.Socket;

/**
 * This abstract class contain a basic structure to handle a socket connection. 
 * Implements Runnable for multi-threading.
 */
public abstract class SocketHandler implements Runnable {

	private final Socket socket;

    /**
     * Abstract constructor for the SocketHandler.
     * 
     * @param socket the socket connection.
     */
    public SocketHandler(Socket socket) {

        this.socket = socket;
    }

    /**
     * Gets the Socket connection.
     * 
     * @return socket the socket connection.
     */
    protected Socket getSocket() {

        return socket;
    }
}
