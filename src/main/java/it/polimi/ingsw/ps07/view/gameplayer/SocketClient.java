package it.polimi.ingsw.ps07.view.gameplayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.messages.requests.ConnectionRequest;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;
import it.polimi.ingsw.ps07.messages.requests.SubscribeRequestMessage;
import it.polimi.ingsw.ps07.messages.responses.ConnectionResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.SubscribeResponseMessage;
import it.polimi.ingsw.ps07.view.BrokerInterface;
import it.polimi.ingsw.ps07.view.RequestHandler;
import it.polimi.ingsw.ps07.view.SubscriberInterface;

/**
 * Handles the client for the request-response interaction and the subscriber for the game player.
 * Implements BrokerInterface and RequestHandler, the interfaces that are also remotely
 * used by the RMI implementation.
 * After a successful subscription messages are dispatched to the SubscriberInterface
 * (used also by the RMI connection implementation).
 */
public class SocketClient extends Thread implements RequestHandler, BrokerInterface {

	private static final Logger LOG = Logger.getLogger(SocketClient.class.getName());
	
	// TODO : chieck if the timeout value is ok
	// maximum timeout for a client response.
	private static final int TIMEOUT = 10000;
	
	private final ViewUpdater viewUpdater;
	
	private final SubscriberInterface subscriberInterface;
	
	// Socket configuration.
	private final String host;
	private final int serverPort;
	private final int publisherPort;
	
	private Socket publisherSocket;
	private ObjectInputStream publisherInputStream;
	
	// must be true after a successful subscription.
	private AtomicBoolean subscribed;
	
	
	/**
	 * Creates a new SocketClient to manage the client and the subscriber for the game player.
	 * 
	 * @param host the hostname of the game manager.
	 * @param serverPort the port of the game manager server.
	 * @param publisherPort the port of the game manager publisher.
	 * @param viewUpdater the view updater to display server responses.
	 * @param subscriberInterface the subscriber where to dispatch.
	 * @throws IOException occurrence of an error while connecting to the publisher.
	 */
	public SocketClient(String host, int serverPort, int publisherPort, ViewUpdater viewUpdater,
			SubscriberInterface subscriberInterface) throws IOException {
		
		if(host == null) {
			
			throw new IllegalArgumentException("Host cannot be null.");
		} else if(viewUpdater == null) {
			
			throw new IllegalArgumentException("View updater cannot be null.");
		} else if(subscriberInterface == null) {
			
			throw new IllegalArgumentException("Subscriber interface cannot be null.");
		}
		
		this.host = host;
		this.serverPort = serverPort;
		this.publisherPort = publisherPort;
		this.viewUpdater = viewUpdater;
		this.subscriberInterface = subscriberInterface;
		
		// sets the connection to the publisher.
		publisherSocket = new Socket(host, publisherPort);
		subscribed = new AtomicBoolean(false);
		
	}

	@Override
	public void subscribe(SubscriberInterface subscriber, Token token) throws RemoteException {
       
		Object response = null;

        try {

            try {

                synchronized (publisherSocket) {
                    ObjectOutputStream publisherOutputStream = new ObjectOutputStream(publisherSocket.getOutputStream());

                    publisherOutputStream.writeObject(new SubscribeRequestMessage(token));
                    publisherOutputStream.flush();
                }

            } catch (IOException e) {
                throw new RemoteException("Error while writing to the socket.", e);
            }

            try {

                synchronized (publisherSocket) {
                    publisherSocket.setSoTimeout(TIMEOUT);
                    publisherInputStream = new ObjectInputStream(publisherSocket.getInputStream());
                    response = publisherInputStream.readObject();
                    
                    // Puts the timeout to unlimited, since the subscription is successful and we 
                    // do not need to check for the timeout to expire.
                    publisherSocket.setSoTimeout(0);
                }

            } catch (IOException e) {
                throw new RemoteException("Error while reading from the socket.", e);
            } catch (ClassNotFoundException e) {
                throw new RemoteException("An invalid message has been returned from the server.", e);
            }

            try {
                publisherSocket.shutdownOutput();
            } catch (IOException e) {
                throw new RemoteException("Unable to shutdown output.", e);
            }

        } catch (RemoteException remoteException) {

            try {
                synchronized (publisherSocket) {
                    publisherSocket.close();
                }
            } catch (IOException e) {
                LOG.log(Level.SEVERE, "An error occurred while closing the socket.", e);
            }

            throw remoteException;

        }

        if (!(response instanceof SubscribeResponseMessage)) {
            throw new IllegalArgumentException("IllegalArgumentException has occured.");
        }

        setSubscribed(true);

		
	}

	@Override
	public void unsubscribe(SubscriberInterface subscriber) throws RemoteException {
		try {

            synchronized (publisherSocket) {
                // Close the connection to unsubscribe.
                publisherSocket.close();
            }

        } catch (IOException e) {
            throw new RemoteException("An error occurred while closing the connection.", e);
        }

        setSubscribed(false);
		
	}

	@Override
	public Token connect() throws RemoteException {
		
		ResponseMessage response = handleRequest(new ConnectionRequest(null));
		
		if(response instanceof ConnectionResponseMessage) {
			
			return ((ConnectionResponseMessage) response).getToken();
		}
		
		LOG.log(Level.SEVERE, "A problem has occurred while establishing a new connection to the server.");
		throw new RemoteException("An error has occured while establishing a new connection to the server.");	
	}
		
	

	@Override
	public ResponseMessage handleRequest(RequestMessage requestMessage) throws RemoteException {
		
		try {
			Object response = sendServerRequest(requestMessage);
			
			if(response instanceof ResponseMessage) {
				
				return (ResponseMessage) response;
			}
			
		} catch(IOException e) {
			
			LOG.log(Level.SEVERE, "A problem has occured with the server request.", e);
		} catch(ClassNotFoundException e) {
			
			LOG.log(Level.SEVERE, "An invalid message has been returned from the server", e);
		}
		
		throw new RemoteException("An error has occured while handling a request.");
	}

	
	/**
	 * Creates a new connection to the server, sends a request, wait for the response and closes the connection.
	 * 
	 * @param requestMessage the request to send to the server.
	 * @return the response from the server.
	 * @throws IOException occurrence of an error while connecting or occurrence of a TIMEOUT.
	 * @throws ClassNotFoundException error while reading the response object.
	 */
	private Object sendServerRequest(RequestMessage requestMessage) throws IOException, ClassNotFoundException {
		
		Socket server = new Socket(host, serverPort);
		server.setSoTimeout(TIMEOUT);
		
		ObjectOutputStream serverOutputStream = new ObjectOutputStream(server.getOutputStream()); 
		serverOutputStream.writeObject(requestMessage);
		
		serverOutputStream.flush();
		
		ObjectInputStream serverInputStream = new ObjectInputStream(server.getInputStream());
		
		Object responseMessage = serverInputStream.readObject();
		
		server.close();
		return responseMessage;
	}

	/**
	 * Sets the subscription status.
	 * 
	 * @param subscribed the subscription.
	 */
	private void setSubscribed(boolean subscribed) {

		synchronized(this.subscribed) {
			
			this.subscribed.set(subscribed);
			this.subscribed.notifyAll();
		}
	}
	
	/**
	 * Wait for a successful subscription.
	 */
	private void waitForSubscription() {
		
		synchronized(this.subscribed) {
			while(!subscribed.get()) {
				try{
					subscribed.wait();
				} catch(InterruptedException e) {
					LOG.log(Level.SEVERE, "InterruptedException occured.", e);
				}
			}
		}
	}

    @Override
    public void run() {

        try {
            while (true) {

                waitForSubscription();
                Object received = null;

                synchronized (publisherSocket) {

                    // Set the timeout to unlimited and wait for a message to be received
                    publisherSocket.setSoTimeout(0);
                    received = publisherInputStream.readObject();

                }

                if (received instanceof BroadcastMessage) {
                    BroadcastMessage responseMessage = (BroadcastMessage) received;
                    subscriberInterface.dispatchMessage(responseMessage);
                }

            }
        } catch (ClassNotFoundException e) {
            LOG.log(Level.SEVERE, "Error while receiving a subscribe message.", e);
        } catch (RemoteException e) {
            LOG.log(Level.SEVERE, "Error in the subscribe connection.", e);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Error occurred while reading from socket.", e);
        }

    }
}
