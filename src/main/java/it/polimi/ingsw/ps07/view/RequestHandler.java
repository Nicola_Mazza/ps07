package it.polimi.ingsw.ps07.view;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;

/**
 * This remote interface handles the server by receiving messages from the client
 * and answering with appropriate messages.
 */

public interface RequestHandler extends Remote {

	/**
	 * Ask the server for a new connection by sending a ConnectionRequestMessage. 
	 * The method returns a new token, which will be used by the client to identify himself.
	 * 
	 * @return a new token to identify the client.
	 * @throws RemoteException occurrence of an error while connecting or TIMEOUT.
	 */
	Token connect() throws RemoteException;
	
	/**
	 *  Ask the server to handle a specific request and wait for its processing.
	 *  
	 *  @param requestMessage the request to be processed by the server.
	 *  @return the response obtained by the server.
	 *  @throws RemoteException occurrence of an error while connecting or TIMEOUT.
	 */
	ResponseMessage handleRequest(RequestMessage requestMessage) throws RemoteException;

}
