package it.polimi.ingsw.ps07.view.gamemanager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.view.RequestHandler;

/**
 * This is the server for the request-response communications, it listens to a port
 * for incoming connections and initializes a SocketHandler when a new connection happens.
 */
public class SocketServer extends Thread {

	private static final Logger LOG = Logger.getLogger(SocketServer.class.getName());
	
	private final int port;
	private final ExecutorService executorService;
	private boolean isStopped;
	
	private RequestHandler requestHandler;

	/**
	 * Creates a new SocketServer.
	 * 
	 * @param port the port where to listen.
	 * @param executorService an executor service to handle the threads.
	 * @param requestHandler the interface the handle the client requests.
	 */
	public SocketServer(int port, ExecutorService executorService, RequestHandler requestHandler) {
		
		this(port, executorService);
		this.requestHandler = requestHandler;
	}
	
	/**
	 * Creates a new SocketServer.
	 * 
	 * @param port the port where to listen.
	 * @param executorService an executor service to handle the threads.
	 */
	protected SocketServer(int port, ExecutorService executorService) {
		
		this.port = port;
		this.executorService = executorService;
		this.isStopped = false;
		
	}

	/**
	 * Stops the server component.
	 */
	public synchronized void stopServer() {

		executorService.shutdownNow();
		this.isStopped = true;
	}


	/**
	 * Gets a new client handler for a client-server request.
	 * 
	 * @param socket the socket used for the connection.
	 * @return the new client handler.
	 */
	protected SocketHandler newHandler(Socket socket) {

		return new ClientHandler(socket, requestHandler);
	}
	
	@Override
	public void run() {
		
		ServerSocket serverSocket;
		
		try {
			
			serverSocket = new ServerSocket(getPort());
		} catch(IOException e) {
			
			LOG.log(Level.WARNING, "An error occurred while creating a socket.", e);
			return;
		}
		
		while(!isStopped()) {
			
			Socket socket;
			
			try {
				
				socket = serverSocket.accept();
				SocketHandler socketHandler = newHandler(socket);
				getExecutorService().execute(socketHandler);
				
			} catch(IOException e) {
				
			}
		}
			
			
		try {
			
			serverSocket.close();			
		} catch(IOException e) {
				
				LOG.log(Level.WARNING, "Cannot close the Socket server.", e);
		}
	}
	
	/**
	 * Gets port.
	 * 
	 * @return port the socket port.
	 */
	protected int getPort() {
		
		return port;
	}
	
	/**
	 * Gets executorService.
	 * 
	 * @return executorService the executor service.
	 */
	protected ExecutorService getExecutorService() {
		
		return executorService;
	}
	
	/**
	 * Gets requestHandler.
	 * 
	 * @return requestHandler the request handler.
	 */
	protected RequestHandler getRequestHandler() {
		
		return requestHandler;
	}
	
	/**
	 * Checks whether the server has stopped or not.
	 * 
	 * @return bool value, true if the server has stopped.
	 */
	protected boolean isStopped() {
		
		return isStopped;
	}

	/**
	 * Sets the server stopped.
	 * 
	 * @param isStopped bool value, true to stop the server.
	 */
	protected void setStopped(boolean isStopped) {
		
		this.isStopped = isStopped;
	}
}
