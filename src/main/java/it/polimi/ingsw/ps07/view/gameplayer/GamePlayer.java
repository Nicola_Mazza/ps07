package it.polimi.ingsw.ps07.view.gameplayer;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.messages.requests.RequestMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.view.RequestHandler;
import it.polimi.ingsw.ps07.view.cli.CliInterpreter;
import it.polimi.ingsw.ps07.view.cli.CliUpdater;

/**
 * Entry point for a game player.
 */
public class GamePlayer {

	private static final Logger LOG = Logger.getLogger(GamePlayer.class.getName());
	
	public static void main(String[] args) {
		
		ViewUpdater view = new CliUpdater();
		Scanner scanner = new Scanner(System.in);
		
		String connectionType;
		do {
			System.out.println("Enter RMI or SOCKET to choose the connection type:");
			connectionType = scanner.nextLine().toUpperCase();
		} while (!connectionType.matches("^(RMI|SOCKET)$"));
		try {
			PlayerConnectionFactory playerConnectionFactory;
			
			if(connectionType.matches("^(RMI)$")) {
				playerConnectionFactory = new RMIFactory("localhost", view);
			} else {
				playerConnectionFactory = new SocketFactory("localhost", view);
			}
			
			RequestHandler requestHandler = playerConnectionFactory.getRequestHandler();
			
			while(true) {
				
				String cmd = scanner.nextLine().toUpperCase();
				
				ResponseMessage response;
				RequestMessage request = CliInterpreter.parseString(playerConnectionFactory.getToken(), cmd);

				if(request == null) {
					
					LOG.log(Level.SEVERE, "ERROR : invalid command.");
					continue;
				}
				
				response = requestHandler.handleRequest(request);
				view.update(response);
				
			}
			
		
		} catch (IOException e) {
			
			LOG.log(Level.SEVERE, "Cannot establish a SOCKET connection.", e);
		} catch (NotBoundException e) {
			
			LOG.log(Level.SEVERE, "Cannot establish a RMI connection.", e);
		}
	}
	
}
