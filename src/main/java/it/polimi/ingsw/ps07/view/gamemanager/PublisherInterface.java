package it.polimi.ingsw.ps07.view.gamemanager;

import java.util.Set;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;

/**
 * Interface for a publisher component.
 */
public interface PublisherInterface {
	 
	/**
     * Publishes a message to the clients subscribed to a topic.
     * 
     * @param message the message to publish.
     * @param topic the topic of the message.
     */
    void publish(BroadcastMessage message, String topic);
    
    /**
     * Adds a new topic to the publisher.
     * 
     * @param topic the name of the new topic.
     */
    void addTopic(String topic);

    /**
     * Adds a new topic to the publisher and subscribes clients to it.
     * 
     * @param topic the name of the new topic.
     * @param clients the clients to subscribe to the topic.
     */
    void addTopic(String topic, Set<Token> clients);
    
    /**
     * Subscribes a client to a topic.
     * 
     * @param topic the name of the topic.
     * @param client the client to subscribe to the topic.
     */
    void subscribeClientToTopic(String topic, Token client);
    
    /**
     * Subscribes clients to a topic.
     * 
     * @param topic the name of the topic.
     * @param clients the clients to subscribe to the topic.
     */
    void subscribeClientsToTopic(String topic, Set<Token> clients);
    
    /**
     * Unsubscribes a client from a topic.
     * 
     * @param topic the name of the topic.
     * @param client the client to unsubscribe from the topic.
     */
    void unsubscribeClientFromTopic(String topic, Token client);
    
    /**
     * Removes a client from the publisher and unsubscribes him from all the
     * topics.
     * 
     * @param client the token of the client.
     */
    void removeClient(Token client);
    
    /**
     * Removes clients to the publisher and unsubscribe them from all
     * the topics.
     * 
     * @param clients the tokens of the clients.
     */
    void removeClients(Set<Token> clients);
    
    /**
     * Removes a topic from the publisher.
     * 
     * @topic the name of the topic.
     */
    void removeTopic(String topic);

}
