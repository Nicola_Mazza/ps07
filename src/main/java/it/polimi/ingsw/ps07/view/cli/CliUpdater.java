package it.polimi.ingsw.ps07.view.cli;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;
import it.polimi.ingsw.ps07.view.gameplayer.ViewUpdater;

/**
 * A class to update the CLI.
 */
public class CliUpdater implements ViewUpdater {

	MessageVisitor messageVisitor;
	
	/**
	 * Constructor for CliUpdater.
	 */
	public CliUpdater() {
		
		messageVisitor = new CliMessageVisitor();
	}
	
	@Override
	public void update(ResponseMessage message) {
		
		message.display(messageVisitor);
		
	}

	@Override
	public void update(BroadcastMessage message) {
		
		message.display(messageVisitor);
	}

	@Override
	public void setPlayerNum(int playerNumber) {

		messageVisitor.setPlayerNumber(playerNumber);
	}

	@Override
	public int getPlayerNumber() {

		return messageVisitor.getPlayerNumber();
	}

}
