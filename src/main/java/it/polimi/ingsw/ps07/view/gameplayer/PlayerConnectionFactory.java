package it.polimi.ingsw.ps07.view.gameplayer;

import java.rmi.RemoteException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.view.BrokerInterface;
import it.polimi.ingsw.ps07.view.RequestHandler;
import it.polimi.ingsw.ps07.view.SubscriberInterface;

/**
 * Abstract class for a PlayerConnectionFactory.
 */
public abstract class PlayerConnectionFactory {
	
	private final ViewUpdater viewUpdater;
	private final Subscriber subscriber;
	
	private Token token;
	
	/**
	 * Constructor for the abstract class PlayerConnectionFactory.
	 * 
	 * @param viewUpdater the view updater to show the updates coming from the server.
	 */
	public PlayerConnectionFactory(ViewUpdater viewUpdater) {
		
		if(viewUpdater == null) {
			
			throw new IllegalArgumentException("ViewUpdater must not be null.");
		}
		
		this.viewUpdater = viewUpdater;
		subscriber = new Subscriber(viewUpdater);
		
		token = null;
	}
	
	/**
	 * Setups a new connection to the game manager as a new client.
	 */
	public void setupConnection() throws RemoteException {
		
		token = getRequestHandler().connect();
		
		if(viewUpdater.getPlayerNumber() == -1) {
			
			viewUpdater.setPlayerNum(token.getPlayerNumber());
		}
		
		getBrokerInterface().subscribe(getSubscriberInterface(), token);
	}

	/**
	 * Gets the remote request handler.
	 * 
	 * @return the remote request handler.
	 */
	public abstract RequestHandler getRequestHandler();
	
	/**
	 * Gets the remote broker interface.
	 * 
	 * @return the remote broker interface.
	 */
	public abstract BrokerInterface getBrokerInterface();
	
	/**
	 * Sets the initial token for the connection.
	 * [Not used]
	 * 
	 * @param an old token to reconnect a known client.
	 */
	public void setToken(Token token) {
		
		this.token = token;
	}
	
	/**
	 * Gets the token that identifies the client.
	 * 
	 * @return the client's token.
	 */
	public Token getToken() {
		
		return token;
	}
	
	/**
	 * Gets the view updater.
	 * 
	 * @return viewUpdater the view updater.
	 */
	public ViewUpdater getViewUpdater() {
		
		return viewUpdater;
	}
	
	/**
	 * Get the subscriber.
	 * 
	 * @return subscriber the subscriber.
	 */
	public Subscriber getSubscriber() {
		
		return subscriber;
	}
	
	/**
	 * Gets the subscriber interface.
	 * 
	 * @return subscriber the subscriber interface.
	 */
	public SubscriberInterface getSubscriberInterface() {
		
		return subscriber;
	}
	
	

}
