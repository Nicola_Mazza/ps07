package it.polimi.ingsw.ps07.view.gamemanager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.messages.requests.SubscribeRequestMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.SubscribeResponseMessage;
import it.polimi.ingsw.ps07.view.BrokerInterface;
import it.polimi.ingsw.ps07.view.SubscriberInterface;

/**
 * Handles the subscriber in the publisher-subscriber component. It receives subscription requests
 * and after a successful subscription all the messages received from the
 * dispatchMessage method are processed from the queue and sent to the subscriber.
 */
public class SubscriberHandler extends SocketHandler implements SubscriberInterface {
	
	private static final Logger LOG = Logger.getLogger(SubscriberHandler.class.getName());
	
	// TODO : check the timeout from the game requirements.
	private static final int TIMEOUT = 10000;

	
	private final BrokerInterface brokerInterface;
	
	private final Queue<BroadcastMessage> broadcastBuffer;
	private ObjectOutputStream outputStream;
	
	boolean isSubscribed;

	/**
	 * Creates a new SubscriberHandler.
	 * 
	 * @param socket the socket connection.
	 * @param broker the broker interface used to set up new subscriptions.
	 */
	public SubscriberHandler(Socket socket, BrokerInterface broker) {
		
		super(socket);
		brokerInterface = broker;
		
		broadcastBuffer = new ConcurrentLinkedQueue<>();
		
		isSubscribed = false;
		
	}


	/**
	 * Manages a subscription request by sending it to the broker interface.
	 * 
	 * @param subscribeRequestMessage the subscription request.
	 * @throws IOException occurrence of an error while subscribing.
	 */
	private void manageSubscription(SubscribeRequestMessage subscribeRequestMessage) throws IOException {
		
		try {
			
			brokerInterface.subscribe(this, subscribeRequestMessage.getToken());
			isSubscribed = true;
			
		} catch(RemoteException e) {
			LOG.log(Level.SEVERE, "Remote exception occured during subscription.", e);
		}
		
		ResponseMessage responseSubscription = new SubscribeResponseMessage(isSubscribed);
		sendMessage(responseSubscription);
	}
	
	/**
	 * Listens for a subscription request.
	 * 
	 * @return the subscription request message.
	 */
	private SubscribeRequestMessage receiveSubscriptionRequestMessage() {

        // null is returned if there is a problem.
        SubscribeRequestMessage subscribeMessage = null;

        try {

            getSocket().setSoTimeout(TIMEOUT);
            ObjectInputStream inputStream = new ObjectInputStream(getSocket().getInputStream());
            Object object = inputStream.readObject();
            
            if (!(object instanceof SubscribeRequestMessage))
                
            	throw new ClassCastException();
            subscribeMessage = (SubscribeRequestMessage) object;

            // 
            try {
                
            	getSocket().shutdownInput();
            } catch (IOException e) {
                
            	LOG.log(Level.WARNING, "Error while shutting down input.", e);
            }

        } catch (SocketException e) {
           
        	LOG.log(Level.WARNING, "Unhandled socket exception.", e);
        } catch (IOException e) {
            
        	LOG.log(Level.WARNING, "Error while reading from input.", e);
        } catch (ClassNotFoundException e) {
           
        	LOG.log(Level.WARNING, "Received invalid message from client.", e);
        }

        return subscribeMessage;

    }


    /**
     * Manages the unsubscription of this client from the broker.
     */
    private void unsubscribe() {

        if (isSubscribed) {
            try {
               
            	brokerInterface.unsubscribe(this);
                isSubscribed = false;
            } catch (RemoteException e) {
                
            	LOG.log(Level.WARNING, "Unhandled remote exception.", e);
            } catch (IllegalArgumentException e) {
                
            	LOG.log(Level.WARNING, "Error while unsubscribing.", e);
            }
        }

    }
    
    /**
     * Sends a message to the subscriber.
     * 
     * @param message the message to send.
     * @throws IOException occurrence of an error while sending the message.
     */
    private void sendMessage(Object message) throws IOException {

        try {
            if (outputStream == null) {
                
            	outputStream = new ObjectOutputStream(getSocket().getOutputStream());
            }

            outputStream.writeObject(message);
            outputStream.flush();
        } catch (IOException e) {
            
        	LOG.log(Level.WARNING, "IOException occured while writing to the output stream.", e);
            unsubscribe();
        }

    }

	@Override
	public void dispatchMessage(BroadcastMessage broadcastMessage) throws RemoteException {

		broadcastBuffer.add(broadcastMessage);
		synchronized(broadcastBuffer) {
			
			broadcastBuffer.notify();
		}
	}


	@Override
	public void run() {

		try {
			
			SubscribeRequestMessage subscribeRequestMessage = receiveSubscriptionRequestMessage();
			manageSubscription(subscribeRequestMessage);
			
			while(true) {
				
				broadcastBuffer();
			}
			
		} catch(IOException e) {
			
			unsubscribe();
		} catch(InterruptedException e) {
			
			LOG.log(Level.WARNING, "InterruptedException occurred.", e);
		} finally {
			try {
				
				getSocket().close();
			} catch(IOException e) {
				
				LOG.log(Level.SEVERE, "Cannot close the socket.", e);
			}
		}
	}


	/**
	 * Sends to the subscriber the contend of the broadcast buffer.
	 * 
	 * @throws IOException occurrence of an error while sending the message.
	 * @throws InterruptedException occurrence of an interrupted exception.
	 */
	private void broadcastBuffer() throws IOException, InterruptedException {

		synchronized(broadcastBuffer) {
			
			while(broadcastBuffer.isEmpty()) {
				
				try {
					
					broadcastBuffer.wait();
				} catch(InterruptedException e) {
					throw e;
				}
			}
			
			BroadcastMessage broadcastMessage;
			
			do {
				
				broadcastMessage = broadcastBuffer.poll();
				if(broadcastMessage != null) {
					
					sendMessage(broadcastMessage);
				}
			} while(broadcastMessage != null);
		}
	}

}
