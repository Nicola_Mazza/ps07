package it.polimi.ingsw.ps07.view.gameplayer;

import java.rmi.RemoteException;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.SubscriberInterface;

/**
 * Implementation of the SubscriberInterface that will be "seen" by the broker
 * to publish messages for the clients.
 */
public class Subscriber implements SubscriberInterface {

	protected ViewUpdater viewUpdater;
	
	/**
	 * Constructs a new Subscriber.
	 * 
	 * @param viewUpdater the view updater to display the received broadcast messages.
	 */
	public Subscriber(ViewUpdater viewUpdater) {
		
		this.viewUpdater = viewUpdater;
	}

	@Override
	public void dispatchMessage(BroadcastMessage broadcastMessage) throws RemoteException {
		
		viewUpdater.update(broadcastMessage);
	}

}
