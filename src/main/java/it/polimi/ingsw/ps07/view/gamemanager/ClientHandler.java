package it.polimi.ingsw.ps07.view.gamemanager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.messages.requests.InvalidRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.view.RequestHandler;

/**
 * Handles a single request-response connection from a client. 
 */
public class ClientHandler extends SocketHandler {

	private static final Logger LOG = Logger.getLogger(ClientHandler.class.getName());
	
	private static final int TIMEOUT = 10000;
	private final RequestHandler requestHandler;
	
	/**
	 * Creates a new ClientHandler.
	 * 
	 * @param socket the socket used by the client.
	 * @param requestHandler the interface to handle requests.
	 */
	public ClientHandler(Socket socket, RequestHandler requestHandler) {

		super(socket);
		this.requestHandler = requestHandler;
	}
	
	/**
	 * Receives a request message object from a client.
	 * 
	 * @return the object received from the client.
	 * @throws ClassNotFoundException occurrence of an error while reading the input stream.
	 * @throws IOException occurrence of an error while receiving the message from the client.
	 */
	protected Object receiveObject() throws ClassNotFoundException, IOException {

        Object receivedObject;

        try {

            getSocket().setSoTimeout(TIMEOUT);

            ObjectInputStream inputStream = new ObjectInputStream(getSocket().getInputStream());

            receivedObject = inputStream.readObject();

        } catch (SocketException e) {
            LOG.log(Level.WARNING, "SocketException occured while setting socket timeout.", e);
            throw e;
        } catch (IOException e) {
            LOG.log(Level.WARNING, "IOException occurred while receiving message from client.", e);
            throw e;
        } catch (ClassNotFoundException e) {
            LOG.log(Level.WARNING, "Cannot read from input stream.", e);
            throw e;
        }

        return receivedObject;

    }

	@Override
	public void run() {

        ResponseMessage response;

        try {

            Object request = receiveObject();

            if (request instanceof RequestMessage) {
                response = requestHandler.handleRequest((RequestMessage) request);
            } else {
                response = new InvalidRequestMessage("Unknown request.");
            }

            sendResponse(response);


        } catch (IOException | ClassNotFoundException e) {
            LOG.log(Level.WARNING, "Unhandled errors during client-server communication.", e);
        }
	}

	/**
	 * Sends the response to the client after receiving the request. It also
	 * closes the connection.
	 * 
	 * @param response the response to send to the client.
	 */
	protected void sendResponse(ResponseMessage response) {

		try {
			
			ObjectOutputStream outputStream = new ObjectOutputStream(getSocket().getOutputStream());
			if(response != null) {
				
				outputStream.writeObject(response);
			} else {
				
				outputStream.writeObject(new InvalidRequestMessage("A problem has occurred while handling this request."));
			}
			
			outputStream.flush();
			
		} catch(IOException e) {
			
			LOG.log(Level.WARNING, "Cannot reply to the client.", e);
		} finally {
			try {
				getSocket().close();
			} catch(IOException e) {
			
				LOG.log(Level.WARNING, "IOException occured while trying to close the socket.", e);
		    }
	    }  
    }
}
