package it.polimi.ingsw.ps07.view.gameplayer;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;

/**
 * Interface to update the game view.
 */
public interface ViewUpdater {

	/**
	 * Updates the view with a received response message.
	 * 
	 * @param message the received ResponseMessage.
	 */
	public void update(ResponseMessage message);
	
	/**
	 * Updates the view with a received broadcast message.
	 * 
	 * @message the received BroadcastMessage.
	 */
	public void update(BroadcastMessage message);

	/**
	 * Sets the player number of the client in the game.
	 * 
	 * @param playerNum the player number of the client in the game.
	 */
	void setPlayerNum(int playerNum);
	
	
	/**
	 * Gets the player number of the client in the game.
	 * 
	 * @return the player number of the client in the game..
	 */
	int getPlayerNumber();
}
