package it.polimi.ingsw.ps07.view.cli;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import it.polimi.ingsw.ps07.messages.broadcast.BPSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.ChatBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.GameFinishedBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.GameStartedBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.LTSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.NRSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.REBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.TEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.APTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.BEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.BEWKHBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.CBPTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.DONEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.EAABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.ECBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PASABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PurchaseBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SATECBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLPCBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLPTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.requests.InvalidRequestMessage;
import it.polimi.ingsw.ps07.messages.responses.AckResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ConnectionResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.PlayerDataResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowBalconyResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowCommandResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowMapResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowMarketResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowPoolResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowTilesResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowUsedTilesResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.SubscribeResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.APTResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.BEResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.BEWKHResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.CBPTResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.DONEResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.EAAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.ECResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.PASAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.PurchaseResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SATECResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLPCResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLPTResponseMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;


/**
 * A visitor class for the Visitor Pattern to visit incoming messages and display them on
 * a Command Line Interface.
 */
public class CliMessageVisitor implements MessageVisitor {

	private int playerNumber;
	
	/**
	 * Constructor for CliMessageVisitor.
	 */
	public CliMessageVisitor() {
	
		playerNumber = -1;
	}
	
	public int getPlayerNumber() {
		
		return playerNumber;
	}
	
	public void setPlayerNumber(int playerNumber) {
		
		this.playerNumber = playerNumber;
	}
	
	/**
	 * Prints the built string.
	 * 
	 * @param string the string to print.
	 */
	private void cli(String string) {

        System.out.println(string);

    }
	
	/**
	 * Prints the map from the .txt file.
	 * 
	 * @param fileName the filename of the .txt file.
	 * @throws IOException
	 */
	private void printMap(String fileName) throws IOException {
		
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		String line;
		while((line = in.readLine()) != null)
		{
		   cli(line);
		}
		in.close();
	}
	
	@Override
	public void display(APTBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed an APT action.\n");
		sb.append("He acquired the Permit Tile number ");
		sb.append(message.getChosenPermitTileNumber());
		sb.append(" of the ");
		sb.append(message.getRegionName());
		sb.append(".\n");
		sb.append("He used ");
		sb.append(message.getNumberOfCardsUsed());
		sb.append(" Politics card");
		if(message.getNumberOfCardsUsed() > 1) {
			sb.append("s");
		}
		sb.append(", ");
		sb.append(message.getNumberOfJolliesUsed());
		sb.append(" Jolly card");
		if(message.getNumberOfJolliesUsed() > 1) {
			sb.append("s");
		}
		sb.append(" and spent ");
		sb.append(message.getSpentCoins());
		sb.append(" coin");
		if(message.getSpentCoins() > 1) {
			sb.append("s");
		}
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(BEBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a BE action using one of his permit tiles. New emporium built in ");
		sb.append(message.getChosenCity());
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(BEWKHBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a BEWKH action.\n");
		sb.append("He built an emporium in ");
		sb.append(message.getChosenCity());
		sb.append(" which is the city where the King currently is.\nPlayer");
		sb.append(message.getPlayerInt());
		sb.append(" used ");
		sb.append(message.getNumberOfCardsUsed());
		sb.append(" Politics card");
		if(message.getNumberOfCardsUsed() > 1) {
			sb.append("s");
		}
		sb.append(", ");
		sb.append(message.getNumberOfJolliesUsed());
		sb.append(" Jolly card");
		if(message.getNumberOfJolliesUsed() > 1) {
			sb.append("s");
		}
		sb.append(", spent ");
		sb.append(message.getSpentCoins());
		sb.append(" coin");
		if(message.getSpentCoins() > 1) {
			sb.append("s");
		}
		sb.append(" and ");
		sb.append(message.getUsedAssistants());
		sb.append(" assistant");
		if(message.getUsedAssistants() > 1) {
			sb.append("s");
		}
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(CBPTBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a CBPT<");
		sb.append(message.getRegionName());
		sb.append("> action.\n");
		sb.append("The new face up permit tiles are : ");
		sb.append(message.getNewRevealedPermitTiles());
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(EAABroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed an EAA action.\n");
		sb.append("He spent ");
		sb.append(message.getNumberOfSpentCoins());
		sb.append(" coin");
		if(message.getNumberOfSpentCoins() > 1) {
			sb.append("s");
		}
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(ECBroadcastMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed an EC<");
		sb.append(message.getColorString());
		sb.append(" ");
		sb.append(message.getBalconyName());
		sb.append("> action.\n");
		sb.append(message.getBalconyName());
		sb.append(" balcony has been updated: ");
		sb.append(message.getUpdatedBalcony());
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(PABroadcastMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("Player");
		sb.append(message.getPlayer());
		sb.append(" performed a PA action.");
		
		cli(sb.toString());
	}

	@Override
	public void display(PASABroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a PASA action returning ");
		sb.append(message.getNumberOfUsedAssistants());
		sb.append(" Assistants to the pool.\n");
		sb.append("He can now perform another slow action.");
		
		cli(sb.toString());
	}

	@Override
	public void display(SATECBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a SATEC<");
		sb.append(message.getCouncillorColor());
		sb.append(" ");
		sb.append(message.getBalconyName());
		sb.append("> action\n");
		sb.append(message.getBalconyName());
		sb.append(" balcony has been updated: ");
		sb.append(message.getUpdatedBalcony());
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(SELLABroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a SELLA action. ");
		sb.append("A new assistants insertion has been created!");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(SELLPTBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a SELLPT action. ");
		sb.append("A new permit tile insertion has been created!");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(SELLPCBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a SELLPC action. ");
		sb.append("A new politics card insertion has been created!");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(DONEBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" performed a DONE action. ");
		sb.append("Turn passed to player");
		sb.append(message.getNextPlayer());
		sb.append(", new turn number is ");
		sb.append(message.getTurnNumber());
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(PurchaseBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerInt());
		sb.append(" purchased the ");
		sb.append(message.getTypeOfInsertion());
		sb.append(" insertion number ");
		sb.append(message.getNOfInsertion());
		sb.append(" of the player");
		sb.append(message.getPlayerID());
		sb.append(". He spent ");
		sb.append(message.getPrice());
		sb.append(" coins that have been given to player");
		sb.append(message.getPlayerID());
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(GameStartedBroadcastMessage message) {

		StringBuilder sb = new StringBuilder();
		
		cli("   ____                       __           __  _____ ");               
		cli(" / ___|___  _   _ _ __   ___(_) |   ___  / _| |  ___|__  _   _ _ __ ");
		cli("| |   / _ \\| | | | '_ \\ / __| | |  / _ \\| |_  | |_ / _ \\| | | | '__|");
        cli("| |__| (_) | |_| | | | | (__| | | | (_) |  _| |  _| (_) | |_| | |   ");
        cli(" \\____\\___/ \\__,_|_| |_|\\___|_|_|  \\___/|_|   |_|  \\___/ \\__,_|_|   ");
        cli("                                                                    ");
         
        String[] cities;
		
		try {
			printMap(message.getChosenMap());
		} catch (IOException e) {
			
		}
		sb.append("\n");
		
		sb.append("Coast Region Bonus Tile: [");
		sb.append(message.getCoastBonus());
		sb.append("].\nHills Region Bonus Tile: [");
		sb.append(message.getHillsBonus());
		sb.append("].\nMountains Region Bonus Tile: [");
		sb.append(message.getMountainsBonus());
		sb.append("].\n\nBonus Tiles: ");
		
		String[] colorBonuses = message.getColorBonuses().substring(1,message.getColorBonuses().length() - 1).split(",");
		
		for(int i = 0; i < colorBonuses.length; i++) {
			
			sb.append(colorBonuses[i]);
		}
		
		sb.append(".\n\nKing's Reward Tile: ");
		sb.append(message.getKingsRewardTile());
	
		sb.append(".\n\n");
		
		cities = message.getCities().split(",");
		
		for(int j = 0; j < cities.length; j++) {
			
			if(j == 0) {
				
				sb.append(cities[j]);
			} else {
				
				sb.append(cities[j].substring(1, cities[j].length()));
			}
			
			sb.append("\n");
		}	
        
        sb.append("\nGame");
		sb.append(message.getGameNumber());
		sb.append(": ");
		sb.append(message.getNumberOfPlayers());
		sb.append(" players, played on map");
		sb.append(message.getChosenMap().charAt(message.getChosenMap().length() - 1));
		
		sb.append(".\nThe turn starts from player");
		sb.append(message.getPlayerTurn());
		sb.append(".\n\n");
		
		sb.append("»COMMANDS for Slow Actions:");
		sb.append("\n›APT<balconyRegion permitTileIndex card1 card2* card3* card4*> to acquire a Permit Tile.");
		sb.append("\n›BE<permitTileIndex city*> to build an Emporium.");
		sb.append("\n›BEWKH<destinationCity card1 card2* card3* card4*> to build an Emporium with the King's help.");
		sb.append("\n›EC<councillorColor balconyRegion> to elect a Councillor.");
		
		sb.append("\n\n»COMMANDS for Quick Actions:");
		sb.append("\n›CBPT<balconyRegion> to change the building Permit Tiles.");
		sb.append("\n›EAA to engage an Assistant.");
		sb.append("\n›PASA to perform an additional Slow Action.");
		sb.append("\n›SATEC<councillorColor balconyRegion> to send an Assistant to elect a Councillor.");
		
		sb.append("\n\n»COMMANDS for utility Actions:");
		sb.append("\n›DATA to show your private information.");
		sb.append("\n›PA to pass your current Quick Action.");
		sb.append("\n›MAP to show the map.");
		sb.append("\n›BALCONY to show the current status of the balconies.");
		sb.append("\n›TILES to show the Permit Tiles on the map.");
		sb.append("\n›USEDTILES to show the Permit Tiles you have already used.");
		sb.append("\n›POOL to show the current status of the councillors pool.");
		sb.append("\n›MARKET to show the Insertions of the Market.");
		sb.append("\n›SELLA<numberOfAssistants price> to create an Insertion of Assistants.");
		sb.append("\n›SELLPC<politicsCardColor price> to create an Insertion of Politics Cards.");
		sb.append("\n›SELLPT<permitTileIndex price> to create an Insertion of Permit Tiles.");
		sb.append("\n›PURCHASE<A|PC|PT> to purchase an Insertion.");
		sb.append("\n›COMMAND to show this list of commands again.");
		
		sb.append("\n\n");
		
		sb.append("Coast balcony: ");
		sb.append(message.getCoastBalcony());
		sb.append(".\nHills balcony: ");
		sb.append(message.getHillsBalcony());
		sb.append(".\nMountains balcony: ");
		sb.append(message.getMountainsBalcony());
		sb.append(".\nKing's balcony: ");
		sb.append(message.getKingsBalcony());
		sb.append(".");
		
		
		cli(sb.toString());
	}


	@Override
	public void display(GameFinishedBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getPlayerNumber());
		sb.append(" wins with ");
		sb.append(message.getWinnerPoints());
		sb.append(" points.");
		
		cli(sb.toString());
	}

	@Override
	public void display(SubscribeResponseMessage message) {

		//Does nothing.
	}

	@Override
	public void display(ConnectionResponseMessage message) {

		//Does nothing.
	}

	@Override
	public void display(InvalidRequestMessage message) {

		StringBuilder sb = new StringBuilder();
		sb.append("ERROR: ");
		sb.append(message.getReason());
		
		cli(sb.toString());
	}

	@Override
	public void display(AckResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		if(!message.getMessage().isEmpty()) {
			
			sb.append(message.getMessage());
			cli(sb.toString());
		}
	}

	@Override
	public void display(PlayerDataResponseMessage message) {
		
		if(playerNumber == -1) {
			playerNumber = message.getPlayerNumber();
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("\nYou are Player");
		sb.append(message.getPlayerNumber());
		sb.append(".\nCurrent Turn: Player");
		sb.append(message.getTurnPlayer());
		sb.append(".\nYou have the following Politics Cards:");
		if(!message.getPoliticsCard().isEmpty()) {
		
			for(String card : message.getPoliticsCard()) {
						
				sb.append(" [");
				sb.append(card);
				sb.append("]");
			}
			
			sb.append(".");
		} else {
			sb.append(" <<none>>.");
		}
		sb.append("\nYou have the following Permit Tiles:");
		if(!message.getPermitTile().isEmpty()) {
			
			for(String tile : message.getPermitTile()) {
				
				sb.append(" ");
				sb.append(tile);
				sb.append(",");
			}
			sb.replace(sb.length() - 2, sb.length(), ".");
		} else {
			sb.append(" <<none>>.");
		}
		
		sb.append("\nYour current progression on the Victory path: ");
		sb.append(message.getVictoryProgression());
		sb.append(".");
		sb.append("\nYour current progression on the Coins path: ");
		sb.append(message.getCoinsProgression());
		sb.append(".");
		sb.append("\nYour current progression on the Nobility path: ");
		sb.append(message.getNobilityProgression());
		sb.append(".");
		sb.append("\nYou have ");
		sb.append(message.getAssistants());
		sb.append(" Assistant");
		if(message.getAssistants() > 1) {
			sb.append("s");
		}
		sb.append(" and ");
		sb.append(message.getRemainingEmporia());
		if(message.getRemainingEmporia() > 1) {
			sb.append(" Emporia");
		} else {
			sb.append(" Emporium");
		}
		sb.append(".");
		
		cli(sb.toString());
	
	}

	@Override
	public void display(ShowMapResponseMessage message) {

		StringBuilder sb = new StringBuilder();
		String[] cities;

		try {
			printMap(message.getChosenMap());
		} catch (IOException e) {

		}
		sb.append("\n");
		
		sb.append("Coast Region Bonus Tile: [");
		sb.append(message.getCoastBonus());
		sb.append("].\nHills Region Bonus Tile: [");
		sb.append(message.getHillsBonus());
		sb.append("].\nMountains Region Bonus Tile: [");
		sb.append(message.getMountainsBonus());
		sb.append("].\n\nBonus Tiles: ");
		
		String[] colorBonuses = message.getColorBonuses().substring(1,message.getColorBonuses().length() - 1).split(",");
		
		for(int i = 0; i < colorBonuses.length; i++) {
			
			sb.append(colorBonuses[i]);
		}
		
		sb.append(".\n\nKing's Reward Tile: ");
		sb.append(message.getKingsRewardTile());
	
		sb.append(".\n\n");
		
		cities = message.getCities().split(",");

		for(int i = 0; i < cities.length; i++) {

			if(i == 0) {

				sb.append(cities[i]);
			} else {

				sb.append(cities[i].substring(1, cities[i].length()));
			}

			sb.append("\n");
		}	
		
		cli(sb.toString());
	}

	@Override
	public void display(ShowCommandResponseMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("»COMMANDS for Slow Actions:");
		sb.append("\n›APT<balconyRegion permitTileIndex card1 card2* card3* card4*> to acquire a Permit Tile.");
		sb.append("\n›BE<permitTileIndex city*> to build an Emporium.");
		sb.append("\n›BEWKH<destinationCity card1 card2* card3* card4*> to build an Emporium with the King's help.");
		sb.append("\n›EC<councillorColor balconyRegion> to elect a Councillor.");
		
		sb.append("\n\n»COMMANDS for Quick Actions:");
		sb.append("\n›CBPT<balconyRegion> to change the building Permit Tiles.");
		sb.append("\n›EAA to engage an Assistant.");
		sb.append("\n›PASA to perform an additional Slow Action.");
		sb.append("\n›SATEC<councillorColor balconyRegion> to send an Assistant to elect a Councillor.");
		
		sb.append("\n\n»COMMANDS for utility Actions:");
		sb.append("\n›DATA to show your private information.");
		sb.append("\n›PA to pass your current Quick Action.");
		sb.append("\n›MAP to show the map.");
		sb.append("\n›BALCONY to show the current status of the balconies.");
		sb.append("\n›TILES to show the Permit Tiles on the map.");
		sb.append("\n›USEDTILES to show the Permit Tiles you have already used.");
		sb.append("\n›POOL to show the current status of the councillors pool.");
		sb.append("\n›MARKET to show the Insertions of the Market.");
		sb.append("\n›SELLA<numberOfAssistants price> to create an Insertion of Assistants.");
		sb.append("\n›SELLPC<politicsCardColor price> to create an Insertion of Politics Cards.");
		sb.append("\n›SELLPT<permitTileIndex price> to create an Insertion of Permit Tiles.");
		sb.append("\n›PURCHASE<A|PC|PT> to purchase an Insertion.");
		sb.append("\n›DONE to stop creating/purchasing a Insertions.");
		sb.append("\n›COMMAND to show this list of commands again.");
		
		cli(sb.toString());
	}

	@Override
	public void display(ShowBalconyResponseMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("\nCoast balcony: ");
		sb.append(message.getCoastBalcony());
		sb.append(".\nHills balcony: ");
		sb.append(message.getHillsBalcony());
		sb.append("\nMountains balcony: ");
		sb.append(message.getMountainsBalcony());
		sb.append(".\nKing's balcony: ");
		sb.append(message.getKingsBalcony());
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(ECResponseMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully elected the ");
		sb.append(message.getColorString());
		sb.append(" Councillor in the ");
		sb.append(message.getBalconyName());
		sb.append(" Balcony and gained 4 Coins!");
		
		cli(sb.toString());
	}
	

	@Override
	public void display(BEResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully built and emporium in ");
		sb.append(message.getChosenCity());
		sb.append(". \nYou obtained the bonuses of the city you built in and all the ones of the cities linked to it.");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(APTResponseMessage message) {
	
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have succesfully acquired the Permit Tile number ");
		sb.append(message.getChosenPermitTileNumber());
		sb.append(" of the ");
		sb.append(message.getRegionName());
		sb.append(".\n");
		sb.append("You used ");
		sb.append(message.getNumberOfCardsUsed());
		sb.append(" politics cards, ");
		sb.append(message.getNumberOfJolliesUsed());
		sb.append(" Jolly card");
		if(message.getNumberOfJolliesUsed() > 1) {
			sb.append("s");
		}
	    sb.append(" and you spent ");
		sb.append(message.getSpentCoins());
		sb.append(" coins.\n");
		sb.append("The bonuses of the Permit Tile you just acquired have been assigned to you.");
		
		cli(sb.toString());
	}
	

	@Override
	public void display(BEWKHResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have succesfully built an Emporium in ");
		sb.append(message.getChosenCity());
		sb.append(".\n");
		sb.append("You used ");
		sb.append(message.getNumberOfCardsUsed());
		sb.append(" Politics Card");
		if(message.getNumberOfCardsUsed() > 1) {
			sb.append("s");
		}
		sb.append(" and ");
		sb.append(message.getNumberOfJolliesUsed());
		sb.append(" Jolly Card");
		if(message.getNumberOfJolliesUsed() > 1) {
			sb.append("s");
		}
		sb.append(" and you spent ");
		sb.append(message.getSpentCoins());
		sb.append(" Coin");
		if(message.getSpentCoins() > 1) {
			sb.append("s");
		}
		sb.append(" and ");
		sb.append(message.getUsedAssistants());
		sb.append(" Assistant");
		if(message.getUsedAssistants() > 1) {
			sb.append("s");
		}
		sb.append(".\nYou obtained the bonuses of the city you built in and the ones of the cities linked to it.");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(SATECResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully elected the ");
		sb.append(message.getCouncillorColor());
		sb.append(" Councillor in the ");
		sb.append(message.getBalconyName());
		sb.append(" Balcony.");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(EAAResponseMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully engaged an Assistant.\n");
		sb.append("You spent ");
		sb.append(message.getNumberOfSpentCoins());
		sb.append(" Coin");
		if(message.getNumberOfSpentCoins() > 1) {
			sb.append("s");
		}
		sb.append(".");
				
		cli(sb.toString());
	}

	@Override
	public void display(CBPTResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully changed the ");
		sb.append(message.getBalconyName());
		sb.append(" Permit Tiles.\n");
		sb.append("The new face up Permit Tiles are: ");
		sb.append(message.getNewRevealedPermitTiles());
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(PASAResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully returned ");
		sb.append(message.getNumberOfUsedAssistants());
		sb.append(" Assistant");
		if(message.getNumberOfUsedAssistants() > 1) {
			sb.append("s");
		}
		sb.append(" to the pool. You can now perform an additional slow action.");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(SELLAResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully created an assistants insertion consisted of ");
		sb.append(message.getNumberOfAssistants());
		sb.append(" assistants worth ");
		sb.append(message.getPrice());
		sb.append(" coin");
		if(message.getPrice() > 1) {
			sb.append("s");
		}
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(SELLPTResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully created a permit tile insertion consisted of your permit tile number ");
		sb.append(message.getChosenpermitTile());
		sb.append(" worth ");
		sb.append(message.getPrice());
		sb.append(" coin");
		if(message.getPrice() > 1) {
			sb.append("s");
		}
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(SELLPCResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have successfully created a politics card insertion consisted of your ");
		sb.append(message.getChosenPoliticsCard());
		sb.append(" Politics card worth ");
		sb.append(message.getPrice());
		sb.append(" coin");
		if(message.getPrice() > 1) {
			sb.append("s");
		}
		sb.append(".");
		
		cli(sb.toString());
	}	
	
	@Override
	public void display(DONEResponseMessage doneResponseMessage) {
		
		//Displays nothing
	}
	
	@Override
	public void display(PurchaseResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nYou have succesfully purchased the ");
		sb.append(message.getTypeOfInsertion());
		sb.append(" insertion number ");
		sb.append(message.getNOfInsertion());
		sb.append(" of the player");
		sb.append(message.getPlayerID());
		sb.append(" spending ");
		sb.append(message.getPrice());
		sb.append(" coin");
		if(message.getPrice() > 1) {
			sb.append("s");
		}
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(ChatBroadcastMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("\nPlayer");
		sb.append(message.getClientNumber());
		sb.append(": ");
		sb.append(message.getMessage());
		cli(sb.toString());
	}

	@Override
	public void display(ShowTilesResponseMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("\nCoast Permit Tiles: ");
		sb.append(message.getCoastPermitTiles());
		sb.append(".\nHills Permit Tiles: ");
		sb.append(message.getHillsPermitTiles());
		sb.append(".\nMountains Permit Tiles: ");
		sb.append(message.getMountainsPermitTiles());
		sb.append(".");
		
		cli(sb.toString());
	}

	@Override
	public void display(TEBroadcastMessage message) {

		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Turn passed to player ");
		sb.append(message.getNextPlayerInt());
		sb.append(", new turn number is ");
		sb.append(message.getTurnNumber());
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(REBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Market phase started! ");
		sb.append("Turn passed to player ");
		sb.append(message.getCurrentPlayer());
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(BPSBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Buying phase started! ");
		sb.append("Turn passed to player");
		sb.append(message.getCurrentPlayer());
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(NRSBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Market phase ended! Baaaaack to the game, new round number is ");
		sb.append(message.getRoundNumber());
		sb.append(".\nTurn passed to player");
		sb.append(message.getCurrentPlayerNumber());
		sb.append(".");
		
		cli(sb.toString());
	}
	
	@Override
	public void display(LTSBroadcastMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nBROADCAST: Player");
		sb.append(message.getCurrentPlayer());
		sb.append(" built his last emporium! The rest of the players can now perform their last turn!");
		
		cli(sb.toString());
	}

	@Override
	public void display(ShowPoolResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nThe Councillors pool: ");
		if(message.getPool().isEmpty()) {
			sb.append("<<none>>.");
		} else {
			sb.append(message.getPool());
		}
		
		cli(sb.toString());	
	}

	@Override
	public void display(ShowUsedTilesResponseMessage message) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nUsed Permit Tiles: ");
		if(message.getUsedTiles().substring(1, message.getUsedTiles().length() - 1).isEmpty()) {
			sb.append("<<none>>.");
		} else {
			sb.append(message.getUsedTiles());
		}
		
		cli(sb.toString());
	}

	@Override
	public void display(ShowMarketResponseMessage message) {

		StringBuilder sb = new StringBuilder();
		sb.append("\nPT Insertions: ");
		if(message.getPTInsertions().substring(1, message.getPTInsertions().length() - 1).isEmpty()) {
			sb.append("<<none>>.");
		} else {
			sb.append(message.getPTInsertions());
		}
		sb.append("\nPC Insertions: ");
		if(message.getPCInsertions().substring(1, message.getPCInsertions().length() - 1).isEmpty()) {
			sb.append("<<none>>.");
		} else {
			sb.append(message.getPCInsertions());
		}	
		sb.append("\nA Insertions: ");
		if(message.getAInsertions().substring(1, message.getAInsertions().length() - 1).isEmpty()) {
			sb.append("<<none>>.");
		} else {
			sb.append(message.getAInsertions());
		}	
		
		cli(sb.toString());
	}
}