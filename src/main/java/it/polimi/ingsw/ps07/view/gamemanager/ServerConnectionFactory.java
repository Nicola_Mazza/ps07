package it.polimi.ingsw.ps07.view.gamemanager;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.view.BrokerInterface;
import it.polimi.ingsw.ps07.view.RequestHandler;

/**
 * Creates both RMI and Socket connections for request-response Server and
 * publisher-subscriber Broker.
 */
public class ServerConnectionFactory {
	
	private static final Logger LOG = Logger.getLogger(ServerConnectionFactory.class.getName());
	
	private static final int RMI_PORT = 7777;
	private static final int SOCKET_SERVER_PORT = 1337;
	private static final int SOCKET_PUBLISHER_PORT = 1338;
	private static final int MAX_SUB_THREADS = 24;
	
	private RequestHandler requestHandler;
	private final Broker broker;
	private Registry registry;
	private SocketServer server;
	private SocketServer publisher;
	
	/**
	 * Constructs a ServerConnectionFactory.
	 * A new broker is initialized.
	 */
	public ServerConnectionFactory() {
		
		broker = new Broker();
	}

	/**
	 * Gets the publisher interface, which is the broker previously initialized.
	 * 
	 * @return broker the publisher interface.
	 */
	public PublisherInterface getPublisherInterface() {
		
		return broker;
	}

	/**
	 * Sets the request handler for the server to process requests.
	 * 
	 * @param requestHandler the request handler for the server.
	 */
	public void setRequestHandler(RequestHandler requestHandler) {
		
		this.requestHandler = requestHandler;
	}

	/**
	 * Starts both the RMI and Socket connections.
	 */
	public void startServers() {
		if(requestHandler == null) {
			throw new IllegalArgumentException("RequestHandler has not been set.");
		}
		
		initRMI();
		initSocket();
		
	}
	
	/**
	 * Stops both RMI and SOCKET connections.
	 */
	public void stopServers() {
		
		releaseRMI();
		releaseSocket();
	}

	/**
	 * Releases Socket resources.
	 */
	private void releaseSocket() {
		
		publisher.stopServer();
		server.stopServer();
	}

	/**
	 * Releases RMI resources.
	 */
	private void releaseRMI() {
		
		try {
			//
			registry.unbind("RequestHandler");
			//
			UnicastRemoteObject.unexportObject(requestHandler, true);
			//
			registry.unbind("Broker");
			//
			UnicastRemoteObject.unexportObject(broker, true);
		} catch(RemoteException | NotBoundException e) {
			LOG.log(Level.SEVERE, "Exception thrown while releasing RMI.", e);
		}
		
	}

	/**
	 * Initializes the Socket communication.
	 */
	private void initSocket() {
		try {
			server = new SocketServer(SOCKET_SERVER_PORT, Executors.newCachedThreadPool(), requestHandler);
			publisher = new SocketPublisherServer(SOCKET_PUBLISHER_PORT, Executors.newFixedThreadPool(MAX_SUB_THREADS), broker);
			
			server.start();
			publisher.start();
			
		} catch(Exception e) {
			LOG.log(Level.SEVERE, "Cannot initialize Socket server.", e);
		}
		
	}

	/**
	 * Initializes the RMI communication.
	 */
	private void initRMI() {
		
		try {
			//
			registry = LocateRegistry.createRegistry(RMI_PORT);
			
			//
			BrokerInterface brokerStub = (BrokerInterface) UnicastRemoteObject.exportObject(broker, 0);
			
			//
			registry.rebind("Broker", brokerStub);
			
			RequestHandler requestHandlerStub = (RequestHandler) UnicastRemoteObject.exportObject(requestHandler, 0); 
			
			//
			registry.rebind("RequestHandler", requestHandlerStub);
			
		} catch(RemoteException e) {
			LOG.log(Level.SEVERE, "Cannot initialize the RMI registry.", e);
		}
	}

}
