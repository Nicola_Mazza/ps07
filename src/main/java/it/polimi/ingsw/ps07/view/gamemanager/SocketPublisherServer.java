package it.polimi.ingsw.ps07.view.gamemanager;

import java.net.Socket;
import java.util.concurrent.ExecutorService;

import it.polimi.ingsw.ps07.view.BrokerInterface;

/**
 * SocketPublisherServer extends a SocketServer to manage the connection requests for
 * the publisher-subscriber component. A Broker is used instead of a RequestHandler
 * and a new SubscriberHandler is used instead of a ClientHandler.
 *
 */
public class SocketPublisherServer extends SocketServer {

	private final BrokerInterface broker;
	
	/**
	 * Creates a new SocketPublisherServer.
	 * 
	 * @param port the port where to listen for new connections.
	 * @param executorService the executor to manage additional threads.
	 * @param broker the broker interface for the publisher-subscriber component.
	 */
	public SocketPublisherServer(int port, ExecutorService executorService, BrokerInterface broker) {
		
		super(port, executorService);
		this.broker = broker;
	}

	@Override
	protected SocketHandler newHandler(Socket socket) {
		
		return new SubscriberHandler(socket, broker);
	}
}
