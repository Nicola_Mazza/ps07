package it.polimi.ingsw.ps07.view.gameplayer;

import java.io.IOException;

import it.polimi.ingsw.ps07.view.BrokerInterface;
import it.polimi.ingsw.ps07.view.RequestHandler;

/**
 * Socket implementation of the PlayerConnectionFactory.
 * It implements both the RequestHandler and the BrokerInterface.
 */
public class SocketFactory extends PlayerConnectionFactory {
	
	private static final int SOCKET_SERVER_PORT = 1337;
    private static final int SOCKET_PUBLISHER_PORT = 1338;
    private final SocketClient socketClient;

    /**
     * Constructs a new SocketFactory.
     * 
     * @param host the hostname of the game manager.
     * @param viewUpdater the view updater.
     * @throws IOException occurrence of an error while setting up the connection.
     */
    public SocketFactory(String host, ViewUpdater viewUpdater) throws IOException {
		super(viewUpdater);
		
		if(host == null) {
			
			throw new IllegalArgumentException("The host can't be null.");
		}
		
		socketClient = new SocketClient(host, SOCKET_SERVER_PORT, SOCKET_PUBLISHER_PORT, viewUpdater, getSubscriber());
		socketClient.start();
		setupConnection();
	}

	@Override
	public RequestHandler getRequestHandler() {
		
		return socketClient;
	}

	@Override
	public BrokerInterface getBrokerInterface() {
		
		return socketClient;
	}

}
