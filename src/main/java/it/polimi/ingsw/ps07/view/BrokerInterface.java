package it.polimi.ingsw.ps07.view;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * This remote interface handles the interaction publisher-subscriber.
 * It allows a client to subscribe or unsubscribe from a broker.
 */
public interface BrokerInterface extends Remote {

	/**
	 * Subscribes a client to a broker.
	 * 
	 * @param subscriber the subscription handler.
	 * @param token the token of the client.
	 * @throws RemoteException occurrence of an error while connecting.
	 */
	void subscribe(SubscriberInterface subscriber, Token token) throws RemoteException;
	
	/**
	 * Unsubscribe a client from a broker.
	 * 
	 * @param subscriber subscriber.
	 * @throws RemoteException occurrence of an error while connecting.
	 * @throws IllegalArgumentException if the subscriber does not exist.
	 */
	void unsubscribe(SubscriberInterface subscriber) throws RemoteException, IllegalArgumentException;
	
}
