package it.polimi.ingsw.ps07.view.gameplayer;

import it.polimi.ingsw.ps07.messages.broadcast.BPSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.ChatBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.GameFinishedBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.GameStartedBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.LTSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.NRSBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.REBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.TEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.APTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.BEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.BEWKHBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.CBPTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.DONEBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.EAABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.ECBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PASABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.PurchaseBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SATECBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLABroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLPCBroadcastMessage;
import it.polimi.ingsw.ps07.messages.broadcast.actions.SELLPTBroadcastMessage;
import it.polimi.ingsw.ps07.messages.requests.InvalidRequestMessage;
import it.polimi.ingsw.ps07.messages.responses.AckResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ConnectionResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.PlayerDataResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowBalconyResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowCommandResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowMapResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowMarketResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowPoolResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowTilesResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.ShowUsedTilesResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.SubscribeResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.APTResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.BEResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.BEWKHResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.CBPTResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.DONEResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.EAAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.ECResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.PASAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.PurchaseResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SATECResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLAResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLPCResponseMessage;
import it.polimi.ingsw.ps07.messages.responses.actions.SELLPTResponseMessage;

/**
 * A Visitor class to display the messages on the Command Line Interface.
 */
public interface MessageVisitor {

	/////////////////////// BROADCAST \\\\\\\\\\\\\\\\\\\\\\\
	
	// Action-related broadcast messages.
	
	/**
	 * Displays the content of the AquirePermitTileBroadcast.
	 * 
	 * @param message the received message.
	 */
	void display(APTBroadcastMessage message);
	
	/**
	 * Displays the content of the BuildEmporiumBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(BEBroadcastMessage message);
	
	/**
	 * Displays the content of the BuildEmporiumWithKingsHelpBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(BEWKHBroadcastMessage message);
	
	/**
	 * Displays the content of the ChangeBuildingPermitTilesBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(CBPTBroadcastMessage message);
	
	/**
	 * Displays the content of the EngageAnAssistantBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(EAABroadcastMessage message);
	
	/**
	 * Displays the content of the ElectCouncillorBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(ECBroadcastMessage message);
	
	/**
	 * Displays the content of the PassActionBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(PABroadcastMessage message);
	
	/**
	 * Displays the content of the PerformAnotherSlowActionBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(PASABroadcastMessage message);
	
	/**
	 * Displays the content of the SendAssistantToElectCouncillorBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SATECBroadcastMessage message);
	
	/**
	 * Displays the content of the sellAssistantsBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SELLABroadcastMessage message);
	
	/**
	 * Displays the content of the SELLPTBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SELLPTBroadcastMessage message);
	
	/**
	 * Displays the content of the SELLPCBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SELLPCBroadcastMessage message);
	
	/**
	 * Displays the content of the DONEBroadcastMessage
	 * @param doneBroadcastMessage
	 */
	void display(DONEBroadcastMessage message);
	
	/**
	 * Displays the content of the PurchaseBroadcastMessage
	 * @param purchaseBroadcastMessage
	 */
	void display(PurchaseBroadcastMessage message);
	
	
	// Non-Action-related broadcast messages.
	
	/**
	 * Displays the content of the GameStartedBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(GameStartedBroadcastMessage message);
	
	/**
	 * Displays the content of the GameFinishedBroadcastMessage.
	 * 
	 * @param message the received message.
	 */
	void display(GameFinishedBroadcastMessage message);
	
	/**
	 * Displays the content of the ChatBroadcastMessage.
	 * 
	 * @param chatBroadcastMessage the content of the ChatBroadcastMessage.
	 */
	void display(ChatBroadcastMessage message);
	
	/**
	 * Displays the content of the TEBroadcastMessage.
	 * 
	 * @param teBroadcastMessage the content of the TEBroadcastMessage.
	 */
	void display(TEBroadcastMessage message);
	
	/**
	 * Displays the content of the REBroadcastMessage
	 * 
	 * @param message the received message.
	 */
	void display(REBroadcastMessage message);
	
	/**
	 * Displays the content of the BPSBroadcastMessage
	 * 
	 * @param message the received message.
	 */
	void display(BPSBroadcastMessage message);

	/**
	 * Displays the content of the NRSBroadcastMessage
	 * 
	 * @param message, the received message
	 */
	void display(NRSBroadcastMessage message);
	
	/**
	 * Displays the content of the LTSBroadcastMessage
	 * 
	 * @param message, the received message
	 */
	void display(LTSBroadcastMessage message);
	

	/////////////////////// RESPONSES \\\\\\\\\\\\\\\\\\\\\\\
	
	// Action-related response messages.
	
	/**
	 * Displays the content of the ECResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(ECResponseMessage message);
	
	/*
	 * Displays the content of the BEResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(BEResponseMessage message);
	
	/*
	 * Displays the content of the APTResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(APTResponseMessage message);
	
	/*
	 * Displays the content of the BEWKHResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(BEWKHResponseMessage bewkhResponseMessage);
	
	/**
	 * Displays the content of the SATECResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SATECResponseMessage message);
	
	/*
	 * Displays the content of the EEAResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(EAAResponseMessage message);
	
	/*
	 * Displays the content of the CBPTResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(CBPTResponseMessage message);
	
	/*
	 * Displays the content of the PASAResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(PASAResponseMessage message);
	
	/**
	 * Displays the content of the SELLAResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SELLAResponseMessage message);
	
	/**
	 * Displays the content of the SELLPTResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SELLPTResponseMessage message);
	
	/**
	 * Displays the content of the SELLPCResponseMessage
	 * 
	 * @param message the received message.
	 */
	void display(SELLPCResponseMessage message);
	
	
	/**
	 * Displays nothing.
	 * @param doneResponseMessage
	 */
	void display(DONEResponseMessage message);
	
	/**
	 * Displays the content of the PurchaseResponseMessage
	 * @param purchaseResponseMessage
	 */
	void display(PurchaseResponseMessage message);
	
	// Non-Action-related response messages.
	
	/**
	 * Displays the content of the ConnectionResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(ConnectionResponseMessage message);

	/**
	 * Displays the content of the ConnectionResponseMessage.
	 * 
	 * @param message the received message.
	 */
    void display(InvalidRequestMessage message);

	/**
	 * Displays the content of the SubscribeResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(SubscribeResponseMessage message);
	
	/**
	 * Displays the content of the AckResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(AckResponseMessage message);

	/**
	 * Displays the content of the PlayerDataResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(PlayerDataResponseMessage message);

	
	/**
	 * Displays the content of the ShowMapResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(ShowMapResponseMessage message);
	
	/**
	 * Displays the content of the ShowCommandResponseMessage.
	 * 
	 * @param message the received message.
	 */
	void display(ShowCommandResponseMessage message);
	
	
	/**
	 * Displays the content of the ShowBalconyResponseMessage.
	 * 
	 * @param showBalconyResponseMessage request to show the balcony.
	 */
	void display(ShowBalconyResponseMessage message);
	
	/**
	 * Displays the content of the ShowTilesResponseMessage.
	 * 
	 * @param showTilesResponseMessage request to show the tiles on the map.
	 */
	void display(ShowTilesResponseMessage message);
	
	/**
	 * Displays the content of the ShowUsedTilesResponseMessage.
	 * 
	 * @param showUsedTilesResponseMessage request to show the Permit Tiles used by the player.
	 */
	void display(ShowUsedTilesResponseMessage message);
	
	/**
	 * Displays the content of the ShowTilesResponseMessage.
	 * 
	 * @param showPoolResponseMessage request to show the councillor pool.
	 */
	void display(ShowPoolResponseMessage message);
	
	/**
	 * Displays the content of the ShowMarketResponseMessage.
	 * 
	 * @param message request to show the Market Insertions.
	 */
	void display(ShowMarketResponseMessage message);

	/**
	 * Gets the player number.
	 * 
	 * @return the game player number.
	 */
	int getPlayerNumber();

	/**
	 * Sets the game player number.
	 */
	void setPlayerNumber(int playerNumber);
	
}
