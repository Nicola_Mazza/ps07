package it.polimi.ingsw.ps07.view.gameplayer;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.ps07.view.BrokerInterface;
import it.polimi.ingsw.ps07.view.RequestHandler;
import it.polimi.ingsw.ps07.view.SubscriberInterface;

/**
 * PlayerConnectionFactory implementation using RMI.
 */
public class RMIFactory extends PlayerConnectionFactory {

	private static final Logger LOG = Logger.getLogger(RMIFactory.class.getName());
	
	public static final int RMI_PORT = 7777;
	
	private final BrokerInterface broker;
	private final RequestHandler requestHandler;
	private SubscriberInterface subscriberInterface;
	
	/**
	 * Creates a new RMI player connection factory.
	 * 
	 * @param host the hostname of the server.
	 * @param viewUpdater the view updater to show the server updates on the view.
	 * @throws RemoteException occurrence of an error while starting RMI.
	 * @throws NotBoundException if the remote interface is not found.
	 */
	public RMIFactory(String host, ViewUpdater viewUpdater) throws RemoteException, NotBoundException {
		
		super(viewUpdater);
		
		try {
			// Gets the reference to the remote Registry object on the given host and port.
			// If the host is null, the local host is used.
			Registry registry = LocateRegistry.getRegistry(host, RMI_PORT);
			
            // Gets the remote reference associated to the specified name in this registry.
			requestHandler = (RequestHandler) registry.lookup("RequestHandler");
			broker = (BrokerInterface) registry.lookup("Broker");
			
			// Exports the remote object to receive incoming calls using the given port. 
			// By using 0, the RMI implementation chooses the port.
			subscriberInterface = (SubscriberInterface) UnicastRemoteObject.exportObject(getSubscriber(), 0);
			
			setupConnection();
			
		} catch(RemoteException e) {
			LOG.log(Level.SEVERE, "A problem has occured while establishing a RMI connection to the game manager.", e);
			throw e;
		} catch(NotBoundException | ClassCastException e) {
			LOG.log(Level.SEVERE, "Error with the remote interface.", e);
			throw e;
		}
		
	}

	@Override
	public RequestHandler getRequestHandler() {
		
		return requestHandler;
	}

	@Override
	public BrokerInterface getBrokerInterface() {
		
		return broker;
	}

	@Override
	public SubscriberInterface getSubscriberInterface() {
		
		return subscriberInterface;
	}
}
