package it.polimi.ingsw.ps07.view;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;

/**
 * This interface is used by the publisher to dispatch messages
 * to subscribers.
 */
public interface SubscriberInterface extends Remote {

	/**
	 * Dispatch the message to the subscriber.
	 * 
	 * @param message the message to dispatch.
	 * @throws RemoteException occurrence of an error while transferring data.
	 */
	void dispatchMessage(BroadcastMessage message) throws RemoteException;

}
