package it.polimi.ingsw.ps07.view.cli;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.messages.requests.ChangeMapRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.PlayerDataRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;
import it.polimi.ingsw.ps07.messages.requests.SendChatRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowBalconyRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowCommandRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowMapRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowMarketRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowPoolRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowTilesRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.ShowUsedTilesRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.APTRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BERequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.BEWKHRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.CBPTRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.DoneRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.EAARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.ECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PASARequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.PurchaseInsertionRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SATECRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellAssistantsRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPermitTileRequestMessage;
import it.polimi.ingsw.ps07.messages.requests.actions.SellPoliticsCardRequestMessage;

/**
 * This class interprets the commands coming from the CLI and
 * converts them into request messages.
 */
public class CliInterpreter {

	private CliInterpreter() {

		throw new AssertionError();
	}

	/**
	 * Parses the cli input and generates the appropriate request message.
	 * 
	 * @param token player's token
	 * @param cmd the input string.
	 * @return a request message.
	 */
	public static RequestMessage parseString(Token token, String cmd) {

		if(cmd.matches("^(CHANGEMAP\\s(MAPA|MAPB))$")) {

			String map = cmd.replaceFirst("CHANGEMAP ", "");
			return new ChangeMapRequestMessage(token, map);
		} else if(cmd.matches("^(MAP)$")) {

			return new ShowMapRequestMessage(token);	
		} else if(cmd.matches("^(DATA)$")) {

			return new PlayerDataRequestMessage(token);	
		} else if(cmd.matches("^(COMMAND)$")) {

			return new ShowCommandRequestMessage(token);

		} else if(cmd.matches("^(APT\\s(COAST|HILLS|MOUNTAINS)\\s(\\d|\\d{2})\\s*(.*) *\\s*(.*) *\\s*(.*) *\\s"
				+ "*(.*) *)$")) {

			String[] parsedInput = cmd.split(" ");
			int size = parsedInput.length;
			String empty = "empty";

			switch(size) {

			case 4:
				return new APTRequestMessage(token, parsedInput[1], parsedInput[2],
						parsedInput[3], empty, empty, empty);
			case 5:	
				return new APTRequestMessage(token, parsedInput[1], parsedInput[2],
						parsedInput[3], parsedInput[4], empty, empty);
			case 6:
				return new APTRequestMessage(token, parsedInput[1], parsedInput[2],
						parsedInput[3], parsedInput[4], parsedInput[5], empty);
			case 7:
				return new APTRequestMessage(token, parsedInput[1], parsedInput[2],
						parsedInput[3], parsedInput[4], parsedInput[5], parsedInput[6]);
			default:
				return null;
			}

		} else if(cmd.matches("^(BE\\s(\\d|\\d{2})\\s(A|B|C|D|E|F|G|H|I|J|K|L|M|N|O))$") ||
				cmd.matches("^(BE\\s(\\d|\\d{2})\\s)$") || cmd.matches("^(BE\\s(\\d|\\d{2}))$")) {

			String[] parsedInput = cmd.split(" ");

			try {

				return new BERequestMessage(token, Integer.parseInt(parsedInput[1]), parsedInput[2]); 

			} catch(IndexOutOfBoundsException e) {

				return new BERequestMessage(token, Integer.parseInt(parsedInput[1]), "cityIsUnique");

			}		

		} else if(cmd.matches("^(BEWKH\\s*(.*) *\\s*(.*) *\\s*(.*) *\\s*(.*) *" +
				"\\s*(.*) *)$")) {


			String[] parsedInput = cmd.split(" ");
			int size = parsedInput.length;
			String empty = "empty";

			switch(size) {
			case 3:
				return new BEWKHRequestMessage(token, parsedInput[1], parsedInput[2], empty,
						empty, empty);
			case 4:
				return new BEWKHRequestMessage(token, parsedInput[1], parsedInput[2], parsedInput[3],
						empty, empty);
			case 5:
				return new BEWKHRequestMessage(token, parsedInput[1], parsedInput[2], parsedInput[3],
						parsedInput[4], empty);
			case 6:
				return new BEWKHRequestMessage(token, parsedInput[1], parsedInput[2], parsedInput[3],
						parsedInput[4], parsedInput[5]);
			default:
				return null;
			}	


		} else if(cmd.matches("^(CBPT\\s(COAST|HILLS|MOUNTAINS))$")) {

			String[] parsedInput = cmd.split(" ");

			return new CBPTRequestMessage(token, parsedInput[1]);

		} else if(cmd.matches("^(EAA)$")) {

			return new EAARequestMessage(token);

		} else if(cmd.matches("^(EC\\s*(.*) *\\s(COAST|HILLS|MOUNTAINS|KING))$")) {
			
			String[] parsedInput = cmd.split(" ");

			return new ECRequestMessage(token, parsedInput[1], parsedInput[2]);

		} else if(cmd.matches("^(PA)$")) {

			return new PARequestMessage(token);

		} else if(cmd.matches("^(PASA)$")) {

			return new PASARequestMessage(token);

		} else if(cmd.matches("^(SATEC\\s*(.*) *\\s(COAST|HILLS|MOUNTAINS|KING))$")) {
			
			String[] parsedInput = cmd.split(" ");

			return new SATECRequestMessage(token, parsedInput[1], parsedInput[2]);

		} else if(cmd.matches("^(BALCONY)$")) {

			return new ShowBalconyRequestMessage(token);

		} else if (cmd.matches("^(POOL)$")) {

			return new ShowPoolRequestMessage(token);
		} else if(cmd.matches("^CHAT\\s(.+)$")) {

			String parsedInput = cmd.replaceFirst("CHAT ","");
			return new SendChatRequestMessage(token, parsedInput);

		} else if(cmd.matches("^(TILES)$")) {

			return new ShowTilesRequestMessage(token);
		} else if(cmd.matches("^(MARKET)$")) {
			
			return new ShowMarketRequestMessage(token);
		}
		else if(cmd.matches("^(USEDTILES)$")) {
			
			return new ShowUsedTilesRequestMessage(token);
		} else if(cmd.matches("^(SELLPC\\s*(.*) *\\s(\\d|\\d{2}))$")) {
			
			String[] parsedInput = cmd.split(" ");
			return new SellPoliticsCardRequestMessage(token, parsedInput[1], Integer.parseInt(parsedInput[2]));
		} else if(cmd.matches("^(SELLPT\\s(\\d|\\d{2})\\s(\\d|\\d{2}))$")) {
			
			String[] parsedInput = cmd.split(" ");
			return new SellPermitTileRequestMessage(token, Integer.parseInt(parsedInput[1]),
					Integer.parseInt(parsedInput[2]));
		} else if(cmd.matches("^(SELLA\\s(\\d|\\d{2})\\s(\\d|\\d{2}))$")) {
			
			String[] parsedInput = cmd.split(" ");
			return new SellAssistantsRequestMessage(token, Integer.parseInt(parsedInput[1]),
					Integer.parseInt(parsedInput[2]));
		} else if(cmd.matches("^(PURCHASE\\s(A|PT|PC)\\s(\\d|\\d{2}))$")) {
			
			String[] parsedInput = cmd.split(" ");
			return new PurchaseInsertionRequestMessage(token, parsedInput[1], Integer.parseInt(parsedInput[2]));	
		} else if(cmd.matches("^(DONE)$")) {
			
			return new DoneRequestMessage(token);
		} else {

			return null;
		}


	}

}