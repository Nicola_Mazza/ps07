package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;

/**
 * Request message to sell a Permit Tile while in the Market Phase.
 */
public class SellPermitTileRequestMessage extends ActionRequestMessage {

	private final int indexOfPermitTile;
	private final int price;
	
	/**
	 * Constructor for SellPermitTileRequestMessage.
	 * 
	 * @param token player's token.
	 * @param indexOfPermitTile index of the Permit Tile the player is willing to sell. 
	 * @param price price of the Permit Tile expressed in coins.
	 */
	public SellPermitTileRequestMessage(Token token, int indexOfPermitTile, int price) {
		
		super(token);
		
		this.indexOfPermitTile = indexOfPermitTile;
		this.price = price;
	}
	
	/**
	 * Gets indexOfPermitTile.
	 * 
	 * @return indexOfPermitTile the index of the Permit Tile the player is willing to sell.
	 */
	public int getIndexOfPermitTile() {
		
		return indexOfPermitTile;
	}
	
	/**
	 * Gets price.
	 * 
	 * @return price the price for the Permit Tile the player is willing to sell expressed in coins.
	 */
	public int getPrice() {
		
		return price;
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
