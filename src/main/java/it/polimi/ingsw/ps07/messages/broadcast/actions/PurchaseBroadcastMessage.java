package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the execution of purchase action.
 */
public class PurchaseBroadcastMessage implements BroadcastMessage{

	private final int playerInt;
	private final String typeOfInsertion;
	private final int nOfInsertion;
	private final int playerID;
	private final int price;
	
	/**
	 * Constructs a new PurchaseBroadcastMessage initializing
	 * the number of the player that performed the action, the
	 * type of the purchased insertion and its number, the number
	 * of the player that made the insertion and the price.
	 * 
	 * @param playerInt the number of the player as an int.
	 * @param typeOfInsertion the type of insertion (PT, PC or A).
	 * @param nOfInsertion the index number of the insertion.
	 * @param playerID the playerID.
	 * @param price the price of the insertion posted.
	 */
	public PurchaseBroadcastMessage(int playerInt, String typeOfInsertion, int nOfInsertion, int playerID, int price) {
		
		this.playerInt = playerInt;
		this.typeOfInsertion = typeOfInsertion;
		this.nOfInsertion = nOfInsertion;
		this.playerID = playerID;
		this.price = price;
	}
	
	/**
	 * Gets playerInt.
	 * 
	 * @return playerInt, the number of the player that performed this action.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * @return typeOfInsertion, the object of the insertion
	 */
	public String getTypeOfInsertion() {
		
		return typeOfInsertion;
	}
	
	/**
	 * Gets nOfInsertino.
	 * 
	 * @return nOfInsertion, the number of the purchased insertion.
	 */
	public int getNOfInsertion() {
		
		return nOfInsertion;
	}
	
	/**
	 * Gets playerID.
	 * 
	 * @return playerID, the number of the player that made the insertion.
	 */
	public int getPlayerID() {
		
		return playerID;
	}
	
	/**
	 * Gets price.
	 * 
	 * @return price, the price in coins of the purchased insertion.
	 */
	public int getPrice() {
		
		return price;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
