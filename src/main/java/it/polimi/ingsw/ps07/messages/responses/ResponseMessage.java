package it.polimi.ingsw.ps07.messages.responses;

import java.io.Serializable;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * This interface represents a Response message from the server to the client,
 * subsequent to a client request.
 */
public interface ResponseMessage extends Serializable {

	
	/**
     * Visit this message to display the result.
     *
     * @param visitor the visitor.
     */
	void display(MessageVisitor messageVisitor);

}
