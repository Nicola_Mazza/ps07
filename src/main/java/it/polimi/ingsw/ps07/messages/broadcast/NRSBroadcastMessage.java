package it.polimi.ingsw.ps07.messages.broadcast;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * This is a broadcast message sent when the market phase is 
 * completed and a new round starts.
 */
public class NRSBroadcastMessage implements BroadcastMessage{

	private final int currentPlayerNumber;
	private final int roundNumber;
	
	/**
	 * Constructor for NRSBroadcastMessage.
	 * 
	 * @param currentPlayerNumber, the number of the current player.
	 * @param roundNumber, the number of the current round.
	 */
	public NRSBroadcastMessage(int currentPlayerNumber, int roundNumber) {
		
		this.currentPlayerNumber = currentPlayerNumber;
		this.roundNumber = roundNumber;
	}
	
	/**
	 * Gets currentPlayerNumber.
	 * 
	 * @return currentPlayerNumber, the number of the current player.
	 */
	public int getCurrentPlayerNumber() {
		
		return currentPlayerNumber;
	}
	
	/**
	 * Gets roundNumber.
	 * 
	 * @return roundNumber, the number of the current round.
	 */
	public int getRoundNumber() {
		
		return roundNumber;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
