package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 *  Broadcast message to notify the clients about the execution of a BEWKH action.
 *
 */
public class BEWKHBroadcastMessage implements BroadcastMessage {

	private final int playerInt;
	private final String chosenCity;
	private final int numberOfCardsUsed;
	private final int numberOfJolliesUsed;
	private final int spentCoins;
	private final int usedAssistants;
	
	/**
	 * Constructor for BEWKHBroadcastMessage.
	 * 
	 * @param playerInt the number of the player as an int.
	 * @param chosenCity the city chosen by the player as a String.
	 * @param numberOfCardsUsed the number of Politics Cards used by the player.
	 * @param numberOfJolliesUsed the number of Jolly Cards used by the player.
	 * @param spentCoins the amount of coins spent by the player as an int.
	 * @param usedAssistants the numebr of Assistants used by the player as an int.
	 */
	public BEWKHBroadcastMessage(int playerInt, String chosenCity, int numberOfCardsUsed, int numberOfJolliesUsed,
			int spentCoins, int usedAssistants) {
		
		this.playerInt = playerInt;
		this.chosenCity = chosenCity;
		this.numberOfCardsUsed = numberOfCardsUsed;
		this.numberOfJolliesUsed = numberOfJolliesUsed;
		this.spentCoins = spentCoins;
		this.usedAssistants = usedAssistants;
	}

	
	/**
	 * Returns the index of the player that
	 * performed the action.
	 * 
	 * @return playerInt the number of the player as an int.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Returns the city in which the player has built
	 * an emporium as a string.
	 * 
	 * @return chosenCity the city chosen by the player as a String.
	 */
	public String getChosenCity() {
		
		return chosenCity;
	}
	
	/**
	 * Returns the number of normal cards used by the
	 * player to satisfy the king's balcony
	 * 
	 * @return numberOfCardsUsed the number of Politics Cards used by the player.
	 */
	public int getNumberOfCardsUsed() {
		
		return numberOfCardsUsed;
	}
	
	/**
	 * Returns the number of jolly cards used by the
	 * player to satisfy the king's balcony.
	 * 
	 * @return numberOfJolliesUsed the number of Jolly Cards used by the player.
	 */
	public int getNumberOfJolliesUsed() {
		
		return numberOfJolliesUsed;
	}
	
	/**
	 * returns the number of coins the player had to spent
	 * to satisfy the king's balcony and move him to the
	 * city in which he wanted to build the emporium.
	 * 
	 * @return spentCoins the amount of coins spent by the player as an int.
	 */
	public int getSpentCoins() {
		
		return spentCoins;
	}
	
	/**
	 * Returns the number of assistants the player had to use
	 * to build the emporium.
	 *  
	 * @return usedAssistants the number of Assistants used by the player.
	 */
	public int getUsedAssistants() {
		
		return usedAssistants;
	}
	@Override
	public void display(MessageVisitor message) {

		message.display(this);
	}

}
