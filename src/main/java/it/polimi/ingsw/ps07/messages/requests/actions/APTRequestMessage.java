package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request a AcquirePermitTile (slow) action.
 */
public class APTRequestMessage extends ActionRequestMessage {
	
	private final String balcony;
	private final String permitTile;
	private final String card1;
	private final String card2;
	private final String card3;
	private final String card4;
	
	private int numberOfPoliticsCards = 0;
	private int jollies = 4;

	/**
	 * Constructor for a APTRequestMessage.
	 * 
	 * @param token player's token.
	 * @param balcony balcony chosen by the player as a String.
	 * @param permitTile the index number of the chosen Permit Tile as a String.
	 * @param card1 card1 chosen by the player as a String.
	 * @param card2 card2 chosen by the player as a String.
	 * @param card3 card3 chosen by the player as a String.
	 * @param card4 card4 chosen by the player as a String. 
	 */
	public APTRequestMessage(Token token, String balcony, String permitTile,
			String card1, String card2, String card3, String card4) {
		
		super(token);
		
		// TODO : "4" can be read the file?
		for(int i = 0; i <= 4; i++) {
			
			if(card1.equals("JOLLY")) {
				numberOfPoliticsCards++;
			}
			if(card2.equals("JOLLY")) {
				numberOfPoliticsCards++;
			}
			if(card3.equals("JOLLY")) {
				numberOfPoliticsCards++;
			}
			if(card4.equals("JOLLY")) {
				numberOfPoliticsCards++;
			}
		}
		
		jollies = 4 - numberOfPoliticsCards;
		
		this.balcony = balcony;
		this.permitTile = permitTile;
		this.card1 = card1;
		this.card2 = card2;
		this.card3 = card3;
		this.card4 = card4;
	}


	/**
	 * Returns the balcony chosen by the player as a String.
	 * 
	 * @return balcony balcony chosen by the player as a String.
	 */
	public String getChosenBalconyString() {
		
		return balcony;
	}
	
	/**
	 * Returns the permit tile chosen by the player as a string
	 * 
	 * @return permitTile
	 */
	public String getChosenpermitTileString() {
		
		return permitTile;
	}
	
	/**
	 * Gets card1.
	 * 
	 * @return card1 the first card chosen by the player as a String.
	 */
	public String getCard1() {
		
		return card1;
	}
	
    /**
     * Gets card2.
     * 
     * @return card3 the second card chosen by the player as a String.
     */
    public String getCard2() {
		
		return card2;
	}

    /**
     * Gets card3.
     * 
     * @return card3 the third card chosen by the player as a String.
     */
    public String getCard3() {
	
	    return card3;
    }


    /**
     * Gets card4.
     * 
     * @return card4 the fourth card chosen by the player as a String.
     */
    public String getCard4() {
	
	    return card4;
    }
	
	/**
	 * Returns the number of politics cards the player wants to use.
	 * 
	 * @return politicsCards number of politics cards used by the player.
	 */
	public int getNumberOfPoliticsCards() {
		
		return numberOfPoliticsCards;
	}
	
	/**
	 * Returns the number of jolly cards the player wants to use.
	 * 
	 * @return jollies number of jolly cards used by the player.
	 */
	public int getNumberOfJollies() {
		
		return jollies;
	}
	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
