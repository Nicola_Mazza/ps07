package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request a SendAssistantToElectCouncillor (quick) action.
 */
public class SATECRequestMessage extends ActionRequestMessage {

	private final String councillorColor;
	private final String balcony;
	
	/**
	 * Constructs a Send Assistant To Elect Councillor action.
	 * The token is needed to identify the player while councillorColor and balcony are
	 * player's choices to perform the action.
	 * 
	 * @param token player's token.
	 * @param councillorColor the color of the Councillor chosen by the player as a String.
	 * @balcony the name of the region of the balcony chosen by the player as a String.
	 */
	public SATECRequestMessage(Token token, String councillorColor, String balcony) {
		
		super(token);
		
		this.councillorColor = councillorColor;
		this.balcony = balcony;
	}

	/**
	 * Getter to get the councillor chosen by the player.
	 * 
	 * @return councillorColor the color of the Councillor chosen by the player as a String.
	 */
	public String getColor() {
		
		return councillorColor;
	}
	
	/**
	 * Getter to get the balcony chosen by the player.
	 * 
	 * @return balcony the name of the region of the balcony chosen by the player as a String.
	 */
	public String getBalcony() {
		
		return balcony;
	}
	
	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
