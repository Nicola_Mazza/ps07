package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;

/**
 * Message to request to sell Assistants while in the Market Phase.
 *
 */
public class SellAssistantsRequestMessage extends ActionRequestMessage {

	private final int numberOfAssistants;
	private final int price;
	
	/**
	 * Constructor for SellAssistantRequestMessage.
	 * 
	 * @param token player's token.
	 * @param numberOfAssistants number of Assistants the player is willing to sell.
	 * @param price the price for the assistants expressed in coins.
	 */
	public SellAssistantsRequestMessage(Token token, int numberOfAssistants, int price) {
		
		super(token);
		
		this.numberOfAssistants = numberOfAssistants;
		this.price = price;
	}
	
	/**
	 * Gets numberOfAssistants.
	 * 
	 * @return numberOfAssistants the number of assistants the player is willing to sell.
	 */
	public int getNumberOfAssistants() {
		
		return numberOfAssistants;
	}
	
	/**
	 * Gets price.
	 * 
	 * @return price the price for the Assistants expressed in coins.
	 */
	public int getPrice() {
		
		return price;
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}
	


}
