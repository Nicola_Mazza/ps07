package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class APTResponseMessage extends ActionResponseMessage {

	private final int chosenPermitTileNumber;
	private final String regionName;
	private final int numberOfCardsUsed;
	private final int numberOfJolliesUsed;
	private final int spentCoins;
	
	public APTResponseMessage(boolean flag, int chosenPermitTileNumber, String regionName, int numberOfCardsUsed,
			int numberOfJolliesUsed, int spentCoins) {
		
		super(flag);
		this.chosenPermitTileNumber = chosenPermitTileNumber;
		this.regionName = regionName;
		this.numberOfCardsUsed = numberOfCardsUsed;
		this.numberOfJolliesUsed = numberOfJolliesUsed;
		this.spentCoins = spentCoins;
	}

	/**
	 * Returns the number of the permit tile chosen by the player 
	 * as an int
	 * @return chsoenPermitTileNumber
	 */
	public int getChosenPermitTileNumber() {
		
		return chosenPermitTileNumber;
	}
	
	/**
	 * Returns the name of the region whose balcony has been satisfied
	 * as a string
	 * @return regionName
	 */
	public String getRegionName() {
		
		return regionName;
	}
	
	/**
	 * Returns the number of used cards as an int
	 * @return numberOfCardsUsed
	 */
	public int getNumberOfCardsUsed() {
		
		return numberOfCardsUsed;
	}
	
	/**
	 * Returns the number of used jollies as an int
	 * @return numberOfJolliesUsed
	 */
	public int getNumberOfJolliesUsed() {
		
		return numberOfJolliesUsed;
	}
	
	/**
	 * returns the number of spent coins as an int
	 * @return spentCoins
	 */
	public int getSpentCoins() {
		
		return spentCoins;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
