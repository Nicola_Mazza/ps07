package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;

public abstract class ActionResponseMessage implements ResponseMessage {

	private final boolean flag;
	
	public ActionResponseMessage(boolean flag) {

		this.flag = flag;
	}

}
