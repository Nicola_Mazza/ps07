package it.polimi.ingsw.ps07.messages.responses;

import java.util.List;
import java.util.ArrayList;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a PlayerDataRequestMessage.
 */
public class PlayerDataResponseMessage implements ResponseMessage {

	private final int playerNumber;
	private final int turnPlayer;
	private final ArrayList<String> politicsCards;
	private final ArrayList<String> permitTiles;
	private final int coinsProgression;
	private final int victoryProgression;
	private final int nobilityProgression;
	private final int assistants;
	private final int remainingEmporia;
	
	/**
	 * Constructor for PlayerDataResponseMessage.
	 * 
	 * @param playerNumber the number of the player.
	 * @param turnPlayer the player who is allowed to execute action in this turn.
	 * @param politicsCards the Politics Cards of the player.
	 * @param permitTiles the Permit Tiles of the player.
	 * @param coinsProgression the current Coin progression of the player.
	 * @param victoryProgression the current Victory progression of the player.
	 * @param nobilityProgression the current Nobility progression of the player.
	 * @param assistants the current number of Assistants of the player.
	 * @param remainingEmporia the remaining emporia of the player.
	 */
	public PlayerDataResponseMessage(int playerNumber, int turnPlayer, List<String> politicsCards,
			List<String> permitTiles, int coinsProgression, int victoryProgression, int nobilityProgression,
			int assistants, int remainingEmporia) {
		
		this.playerNumber = playerNumber;
		this.turnPlayer = turnPlayer;
		this.politicsCards = new ArrayList<>(politicsCards);
		this.permitTiles = new ArrayList<>(permitTiles);
		this.coinsProgression = coinsProgression;
		this.victoryProgression = victoryProgression;
		this.nobilityProgression = nobilityProgression;
		this.assistants = assistants;
		this.remainingEmporia = remainingEmporia;
	}
	
	/**
	 * Gets playerNumber.
	 * 
	 * @return playerNumber the number of the player.
	 */
	public int getPlayerNumber() {
		
		return playerNumber;
	}
	


	/**
	 * Gets politicsCards.
	 * 
	 * @return politicsCards a list of PoliticsCard as Strings representing the cards of the player.
	 */
	public List<String> getPoliticsCard() {
		
		return politicsCards;
	}

	/**
	 * Gets turnPlayer.
	 * 
	 * @return turnPlayer the number of the player who is currently playing the turn.
	 */
	public int getTurnPlayer() {
		
		return turnPlayer;
	}

	/**
	 * Gets permitTiles.
	 * 
	 * @return permitTiles a list of PermitTile as Strings representing the Permit Tiles of the player.
	 */
	public List<String> getPermitTile() {
		
		return permitTiles;
	}
	
	
	/**
	 * Gets victoryProgression.
	 * 
	 * @return victoryProgression the current progression of the player on the victory path.
	 */
	public int getVictoryProgression() {

		return victoryProgression;
	}

	/**
	 * Gets coinsProgression.
	 * 
	 * @return coinsProgression the current progression of the player on the coins path.
	 */
	public int getCoinsProgression() {
		
		return coinsProgression;
	}
	
	/**
	 * Gets nobilityProgression.
	 * 
	 * @return nobilityProgression the current progression of the player on the nobility path.
	 */
	public int getNobilityProgression() {
		
		return nobilityProgression;
	}
	

	/**
	 * Gets assistants.
	 * 
	 * @return assistants the current number of Assistants of the player.
	 */
	public int getAssistants() {
		
		return assistants;
	}
	
	/**
	 * Gets remainingEmporia.
	 * 
	 * @return remainingEmporia the number of remaining emporia of the player.
	 */
	public int getRemainingEmporia() {
		
		return remainingEmporia;
	}

	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

	
	
	
}
