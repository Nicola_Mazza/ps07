package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request a BuildEmporiumWithKingsHelp (slow) action.
 */
public class BEWKHRequestMessage extends ActionRequestMessage {
	
	private final String destinationCity;
	private final String card1;
	private final String card2;
	private final String card3;
	private final String card4;


	/**
	 * Constructor for BEWKHRequestMessage.
	 * 
	 * @param token player's token.
	 * @param destinationCity the initial letter of the city chosen by the player.
	 * @param card1 the first card used by the player.
	 * @param card2 the second card used by the player.
	 * @param card3 the third card used by the player.
	 * @param card4 the fourth card used by the player.
	 */
	public BEWKHRequestMessage(Token token, String destinationCity,
			String card1, String card2, String card3, String card4) {
		
		super(token);
		
		this.destinationCity = destinationCity;
		this.card1 = card1;
		this.card2 = card2;
		this.card3 = card3;
		this.card4 = card4;
		
	}
	
	/**
	 * Gets card1.
	 * 
	 * @return card1 the first card chosen by the player as a String.
	 */
	public String getCard1() {
		
		return card1;
	}
	
	/**
	 * Gets card2.
	 * 
	 * @return card2 the second card chosen by the player as a String.
	 */
	public String getCard2() {
		
		return card2;
	}
	
	/**
	 * Gets card3.
	 * 
	 * @return card3 the third card chosen by the player as a String.
	 */
	public String getCard3() {
		
		return card3;
	}
	
	/**
	 * Gets card4.
	 * 
	 * @return card4 the fourth card chosen by the player as a String.
	 */
	public String getCard4() {
		
		return card4;
	}
	
	/**
	 * Gets destinationCity.
	 * 
	 * @return destinationCity the emperor destination chosen by the player.
	 */
	public String getDestinationCity() {
		
		return destinationCity;
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
