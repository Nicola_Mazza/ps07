package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class EAAResponseMessage extends ActionResponseMessage {

	private int numberOfSpentCoins;
	
	public EAAResponseMessage(boolean flag, int numberOfSpentCoins) {
		
		super(flag);
		this.numberOfSpentCoins = numberOfSpentCoins;
	}

	/**
	 * Returns the number of coins the player had to spent to perform the action
	 * @return numberOfCoinsSpent
	 */
	public int getNumberOfSpentCoins() {
		
		return numberOfSpentCoins;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
