package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * A message to request to show the tiles on the map.
 */
public class ShowTilesRequestMessage extends RequestMessage {

	/**
	 * Constructor for ShowTilesRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public ShowTilesRequestMessage(Token token) {
		
		super(token);
	}

}
