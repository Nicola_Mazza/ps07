package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the execution of a SATEC action.
 */
public class SATECBroadcastMessage implements BroadcastMessage {
	
	private int playerInt;
	private String councillorColor;
	private String balconyName;
	private String updatedBalcony;

	/**
	 * Constructor for SATECBroadcastMessage.
	 * 
	 * @param playerInt the player number as an int.
	 * @param councillorColor the Councillor color chosen by the player.
	 * @param balconyName the name of the region the balcony belongs to as a String.
	 * @param updatedBalcony the updated balcony as a String.
	 */
	public SATECBroadcastMessage(int playerInt, String councillorColor, String balconyName, String updatedBalcony) {
		
		this.playerInt = playerInt;
		this.councillorColor = councillorColor;
		this.balconyName = balconyName;
		this.updatedBalcony = updatedBalcony;
	}
	
	/**
	 * Returns the index of the player that performed the action.
	 * 
	 * @return playerInt the number of the player as an int.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Returns the color of the elected councillor as a String.
	 * 
	 * @return councillorColor the color of the Councillor chosen by the player as a String.
	 */
	public String getCouncillorColor() {
		
		return councillorColor;
	}
	
	/**
	 * Returns the balcony that has been changed as a String.
	 * 
	 * @return balconyName the name of the region the balcony belongs to as a String.
	 */
	public String getBalconyName() {
		
		return balconyName;
	}
	
	/**
	 * Returns the updated balcony as a String.
	 * 
	 * @return updatedBalcony the updated balcony after the execution of this action as a String.
	 */
	public String getUpdatedBalcony() {
		
		return updatedBalcony;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
