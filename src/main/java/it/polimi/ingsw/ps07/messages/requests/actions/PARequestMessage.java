package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request a PassAction (pass) action.
 */
public class PARequestMessage extends ActionRequestMessage {

	/**
	 * Creates a new PassRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public PARequestMessage(Token token) {
		
		super(token);
		
	}
	
	@Override
	public Action createAction(ActionFactoryVisitor visitor) {

		return visitor.visit(this);
		
	}

}
