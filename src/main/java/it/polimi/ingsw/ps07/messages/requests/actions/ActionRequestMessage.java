package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionCreator;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;

/**
 * This is the class handling the game-related request messages coming from the player
 * to the server. Subclasses will implement the ActionCreator interface so that
 * the concrete type of the request message does not have be known to create the
 * appropriate action.
 */

public abstract class ActionRequestMessage extends RequestMessage implements ActionCreator {
	
	
	/**
	 * Constructor for the ActionRequestMessage class. A token is required to identify
	 * uniquely the client.
	 */
	public ActionRequestMessage(Token token) {
		
		super(token);
		
	}
	
	@Override
    public abstract Action createAction(ActionFactoryVisitor visitor);

}
