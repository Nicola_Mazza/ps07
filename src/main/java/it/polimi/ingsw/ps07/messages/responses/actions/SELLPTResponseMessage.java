package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class SELLPTResponseMessage extends ActionResponseMessage{

	private final int chosenPermitTile;
	private final int price;
	
	/**
	 * Constructs a new SELLPTResponse message initializing the index
	 * of the permit tile chosen by the player and its price
	 * @param flag
	 * @param chosenPermitTile
	 * @param price
	 */
	public SELLPTResponseMessage(boolean flag, int chosenPermitTile, int price) {
		
		super(flag);
		
		this.chosenPermitTile = chosenPermitTile;
		this.price = price;
	}
	
	/**
	 * @return chosenPermitTile, the index of the tile chosen by the player
	 */
	public int getChosenpermitTile() {
		
		return chosenPermitTile;
	}
	
	/**
	 * @return price, the price of the tile chosen by the player
	 */
	public int getPrice() {
		
		return price;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
