package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * A response message to an invalid request message.
 */
public class InvalidRequestMessage implements ResponseMessage {

	private final String reason;
	
	/**
     * Constructs a new InvalidRequestMessage without a reason.
     */
	public InvalidRequestMessage() {
		
		this("");
	}
	
	/**
     * Constructs a new InvalidRequestMessage containing a reason.
     * 
     * @string the reason why we generate an InvalidRequestMessage as a String.
     */
	public InvalidRequestMessage(String string) {

		reason = string;
	}
	
	/**
     * Gets the reason why the message is invalid.
     * 
     * @return reason the reason why we generated the InvalidRequestMessage as a String.
     */
    public String getReason() {

        return reason;
    }

	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
