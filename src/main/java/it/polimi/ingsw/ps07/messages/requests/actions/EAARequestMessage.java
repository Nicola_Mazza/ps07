package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request an EngageAnAssistant (quick) action.
 */
public class EAARequestMessage extends ActionRequestMessage {

	/**
	 * Constructor for EAARequestMessage.
	 * 
	 * @param token player's token.
	 */
	public EAARequestMessage(Token token) {
		
		super(token);
		
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
