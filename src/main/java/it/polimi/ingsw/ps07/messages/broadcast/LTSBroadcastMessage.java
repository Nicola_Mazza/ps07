package it.polimi.ingsw.ps07.messages.broadcast;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * BroadcastMessage for the start of the last round in which
 * each player but the one that built all his emporia can perform
 * another turn
 */
public class LTSBroadcastMessage implements BroadcastMessage{

	private final int currentPlayer;
	
	/**
	 * Constructs a new LTSBroadcastMessage initializing the number
	 * of the player that built all his emporia
	 * @param currentPlayer
	 */
	public LTSBroadcastMessage(int currentPlayer) {
		
		this.currentPlayer = currentPlayer;
	}
	
	/**
	 * @return currentPlayer, the player that built all his emporia
	 */
	public int getCurrentPlayer() {
		
		return currentPlayer;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
