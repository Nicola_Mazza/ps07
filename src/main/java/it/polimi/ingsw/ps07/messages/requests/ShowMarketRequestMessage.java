package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * Request message to show the Insertions of the Market.
 *
 */
public class ShowMarketRequestMessage extends RequestMessage {

	/**
	 * Constructor for ShowMarketRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public ShowMarketRequestMessage(Token token) {
		super(token);
	}

}
