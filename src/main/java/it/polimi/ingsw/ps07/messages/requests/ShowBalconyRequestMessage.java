package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * The message to request to show the balconies of
 * the region boards.
 */
public class ShowBalconyRequestMessage extends RequestMessage {

	/**
	 * Constructor for the ShowBalconyRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public ShowBalconyRequestMessage(Token token) {
		
		super(token);
	}

}
