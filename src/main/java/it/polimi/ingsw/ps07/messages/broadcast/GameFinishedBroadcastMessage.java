package it.polimi.ingsw.ps07.messages.broadcast;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the end of the game.
 */
public class GameFinishedBroadcastMessage implements BroadcastMessage {

	private final int playerNumber;
	private final int winnerPoints;
	
	/**
	 * Constructs a new GameFinishedBroadcastMessage
	 * @param playerNumber, the number of the player that
	 * won the game
	 * @param winnerPoints , the number of victory points of
	 * the player that won the game
	 */
	public GameFinishedBroadcastMessage(int playerNumber, int winnerPoints) {
		
		this.playerNumber = playerNumber;
		this.winnerPoints = winnerPoints;
	}
	
	/**
	 * @return playerNumber, the number of the player that won
	 * the game
	 */
	public int getPlayerNumber() {
		
		return playerNumber;
	}
	
	/**
	 * @return winnerPoints, the number of victory points
	 * of the player that won the game
	 */
	public int getWinnerPoints() {
		
		return winnerPoints;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
