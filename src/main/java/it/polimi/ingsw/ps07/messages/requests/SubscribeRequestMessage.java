package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * A message to request the subscription to a broker.
 */
public class SubscribeRequestMessage extends RequestMessage {

	/**
	 * Constructs a subscribe request message.
	 * 
	 * @param token player's token.
	 */
	public SubscribeRequestMessage(Token token) {
		
		super(token);
	}

}
