package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;
import it.polimi.ingsw.ps07.messages.requests.RequestMessage;

/**
 * Request message to sell a Politics Card while in the Market Phase.
 */
public class SellPoliticsCardRequestMessage extends ActionRequestMessage {

	private final String politicsCard;
	private final int price;
	
	/**
	 * Constructor for SellPoliticsCardRequestMessage.
	 * 
	 * @param token player's token.
	 * @param politicsCard one of the Politics Card of the player's hand.
	 * @param price price of the insertion expressed in coins.
	 */
	public SellPoliticsCardRequestMessage(Token token, String politicsCard, int price) {
		
		super(token);
		
		this.politicsCard = politicsCard;
		this.price = price;
	}
	
	/**
	 * Gets politicsCard.
	 * 
	 * @return politicsCard the Politics Card the player is willing to sell as a String.
	 */
	public String getPoliticsCard() {
		
		return politicsCard;
	}
	
	/**
	 * Gets price.
	 * 
	 * @return the price of the Politics Card the player is willing to sell expressed in coins.
	 */
	public int getPrice() {
		
		return price;
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
