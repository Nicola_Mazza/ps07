package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * Message to request to show the CLI command list.
 */
public class ShowCommandRequestMessage extends RequestMessage {

	/**
	 * Constructor for ShowCommandRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public ShowCommandRequestMessage(Token token) {
		
		super(token);
	}

}
