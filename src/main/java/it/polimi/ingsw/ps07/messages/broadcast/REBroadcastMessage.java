package it.polimi.ingsw.ps07.messages.broadcast;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the end of the round.
 */
public class REBroadcastMessage implements BroadcastMessage{

	private final int currentPlayer;
	
	/**
	 * Constructs a new REBroadcastMessage initializing its
	 * parameter currentPlayer.
	 * @param currentPlayer the current player as an int.
	 */
	public REBroadcastMessage(int currentPlayer) {
		
		this.currentPlayer = currentPlayer;
	}
	
	/**
	 * Returns the index of the current player.
	 * @return currentPlayer the current player as an int.
	 */
	public int getCurrentPlayer() {
		
		return currentPlayer;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
