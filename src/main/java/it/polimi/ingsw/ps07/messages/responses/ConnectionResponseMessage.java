package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ConnectionRequestMessage.
 */
public class ConnectionResponseMessage implements ResponseMessage {
	
	private final Token token;
	
	/**
	 * Constructor for ConnectionResponseMessage.
	 * 
	 * @param token the player's token.
	 */
	public ConnectionResponseMessage(Token token) {
		this.token = token;
	}
	
	/**
	 * Gets token.
	 * 
	 * @return token the player's token.
	 */
	public Token getToken() {
		return token;
	}

	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
