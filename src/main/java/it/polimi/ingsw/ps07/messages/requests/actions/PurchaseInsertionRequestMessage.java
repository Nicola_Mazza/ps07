package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

public class PurchaseInsertionRequestMessage extends ActionRequestMessage{
	
	private final String typeOfInsertion;
	private final int indexOfInsertion;

	/**
	 * Constructor for PurchaseInsertionRequestMessage
	 * 
	 * @param token player's token
	 * @param typeOfInsertion the type of Insertion (Assistant, PoliticsCard, PermitTile) as a String.
	 * @param indexOfInsertion the index number of the Insertion.
	 */
	public PurchaseInsertionRequestMessage(Token token, String typeOfInsertion, int indexOfInsertion) {
		
		super(token);
		
		this.typeOfInsertion = typeOfInsertion;
		this.indexOfInsertion = indexOfInsertion;
	}
	
	/**
	 * Gets typeOfInsertion.
	 * 
	 * @return typeOfInsertion the type of Insertion chosen by the player as a String.
	 */
	public String getTypeOfInsertion() {
		
		return typeOfInsertion;
	}
	
	/**
	 * Gets indexOfInsertion.
	 * 
	 * @return indexOfInsertion the index number of the Insertion chosen by the player.
	 */
	public int getIndexOfInsertion() {
		
		return indexOfInsertion;
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
