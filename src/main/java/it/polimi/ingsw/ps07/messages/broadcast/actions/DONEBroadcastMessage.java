package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 *  Broadcast message to notify the clients the current player has finished
 *  selling/purchasing from the Market.
 */
public class DONEBroadcastMessage implements BroadcastMessage{

	private final int playerInt;
	private final int turnNumber;
	private final int nextPlayer;
	
	/**
	 * Constructs a new DONEBroadcastMessage initializing the number of the
	 * player that performed the action and the number of the turn
	 * @param playerInt the number of the player as an int.
	 * @param turnNumber the number of the turn.
	 */
	public DONEBroadcastMessage(int playerInt, int nextPlayer, int turnNumber) {
		
		this.playerInt = playerInt;
		this.turnNumber = turnNumber;
		this.nextPlayer = nextPlayer;
	}
	
	/**
	 * Gets playerInt.
	 * 
	 * @return playerInt, the number of the player that performed the action.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Gets nextPlayer.
	 * 
	 * @return nextPlayer, the number of the next player.
	 */
	public int getNextPlayer() {
		
		return nextPlayer;
	}
	
	/**
	 * Gets turnNumber.
	 * 
	 * @return turnNumber, the number of the new turn.
	 */
	public int getTurnNumber() {
		
		return turnNumber;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
