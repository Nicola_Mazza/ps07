package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * A request to change the map, before the game is created.
 */
public class ChangeMapRequestMessage extends RequestMessage {
	
	private final String map;

	/**
	 * Constructor for ChangeMapRequestMessage.
	 * 
	 * @param token player's token.
	 * @param map the filename of the chosen map.
	 */
	public ChangeMapRequestMessage(Token token, String map) {
		
		super(token);
		
		 if (!map.matches("^(MAPA|MAPB)$")) {
	            throw new IllegalArgumentException("Invalid MAP.");
	        }

	     this.map = map;
	}
	
	/**
	 * Gets map.
	 * 
	 * @return map the chosen map.
	 */
	public String getMap() {
		
		return map;
	}

}
