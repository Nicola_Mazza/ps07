package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the execution of a SELLPC action.
 */
public class SELLPCBroadcastMessage implements BroadcastMessage{

	private final int playerInt;
	
	/**
	 * Constructs a new SELLPCBroadcastMessage initializing
	 * the number of the player that performed the action.
	 * 
	 * @param playerInt the number of the player as an int.
	 */
	public SELLPCBroadcastMessage(int playerInt) {
		
		this.playerInt = playerInt;
	}
	
	/**
	 * Gets playerInt.
	 * 
	 * @return playerInt, the number of the player that performed the action.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
