package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request a ChangeBuildingPermitTiles (quick) action.
 */
public class CBPTRequestMessage extends ActionRequestMessage {
	
	private final String region;

	/**
	 * Constructs a new change building permit tiles action initializing the 
	 * chosen region whose permit tiles will be changed
	 * @param token player's token.
	 * @param region the name of the region where the player wants to execute the CBPT action.
	 */
	public CBPTRequestMessage(Token token, String region) {
		
		super(token);

		this.region = region;
	}

	/**
	 * Gets region.
	 * 
	 * @return region the region as a String.
	 */
	public String getRegion(){
		
		return region;
	}
	
	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
