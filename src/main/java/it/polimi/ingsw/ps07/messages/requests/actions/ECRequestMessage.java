package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request an ElectCouncillor (slow) action.
 */
public class ECRequestMessage extends ActionRequestMessage {

	private final String councillorColor;
	private final String balcony;
	
	/**
	 * Constructor for ECRequestMessage.
	 * 
	 * @param token player's token.
	 * @param councillorColor the color of the Councillor chosen by the player as a String.
	 * @param balcony the region of the balcony chosen by the player as a String.
	 */
	public ECRequestMessage(Token token, String councillorColor, String balcony) {
		
		super(token);
		this.councillorColor = councillorColor;
		this.balcony = balcony;
	}

	/**
	 * Gets councillorColor.
	 * 
	 * @return councillorColor the color of the Councillor chosen by the player as a String.
	 */
	public String getColor() {
		
		return councillorColor;
	}
	
	/**
	 * Gets balcony.
	 * 
	 * @return balcony the name of the region of the balcony chosen by the player as a String.
	 */
	public String getBalcony() {
		
		return balcony;
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
