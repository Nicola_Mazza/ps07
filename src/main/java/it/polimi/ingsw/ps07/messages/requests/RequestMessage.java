package it.polimi.ingsw.ps07.messages.requests;

import java.io.Serializable;

import it.polimi.ingsw.ps07.controller.Token;

/**
 *  This class represents a generic message sent to the server by the client.
 *  The client identifies himself through the token, which is unique, given by the server.
 *  If the client is connecting to the server for the first time, a null value
 *  of the token is allowed.
 */
public abstract class RequestMessage implements Serializable {
	
	Token token;
	
	/**
	 * Constructor of the RequestMessage : it sets the token of the request to
	 * the unique one of the client who is sending the request.
	 * 
	 * @param token player's token.
	 */
	public RequestMessage(Token token) {
		
		this.token = token;
	
	}

	/**
	 * Gets the token used by the client to identify himself
	 * when sending a request.
	 * 
	 * @return token the player's token.
	 */
	public Token getToken() {
		
		return token;
		
	}
}
