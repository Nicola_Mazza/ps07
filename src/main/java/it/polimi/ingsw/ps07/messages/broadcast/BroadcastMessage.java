package it.polimi.ingsw.ps07.messages.broadcast;

import java.io.Serializable;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Interface for a generic broadcast message. Every message must implement
 * the Visitor pattern to be displayed properly.
 */
public interface BroadcastMessage extends Serializable {

	/**
	 * Visits this message to display the result.
	 * 
	 * @param message the visitor.
	 */
	void display(MessageVisitor message);
}
