package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * Message to request the player's private data.
 */
public class PlayerDataRequestMessage extends RequestMessage {

	/**
	 * Constructor for PlayerDataRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public PlayerDataRequestMessage(Token token) {
		
		super(token);
	}

}
