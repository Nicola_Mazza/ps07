package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 *  Broadcast message to notify the clients about the execution of an EAA action.
 *
 */
public class EAABroadcastMessage implements BroadcastMessage {

	private int playerInt;
	private int numberOfSpentCoins;
	
	/**
	 * @param playerInt the number of the player as an int.
	 * @param numberOfSpentCoins the amount of coins spent by the player as an int.
	 */
	public EAABroadcastMessage(int playerInt, int numberOfSpentCoins) {
		
		this.playerInt = playerInt;
		this.numberOfSpentCoins = numberOfSpentCoins;
	}
	
	/**
	 * Returns the index of the player that performed the action.
	 * 
	 * @return playerInt the number of the player as an int.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Returns the number of coins the player had to spent to perform the action.
	 * 
	 * @return numberOfSpentCoins the number of coins spent by the player.
	 */
	public int getNumberOfSpentCoins() {
		
		return numberOfSpentCoins;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
