package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class PASAResponseMessage extends ActionResponseMessage {

	private final int numberOfUsedAssistants;
	
	/**
	 * Construct a new perform another slow action response message
	 * @param flag
	 * @param numberOfUsedAssistants
	 */
	public PASAResponseMessage(boolean flag, int numberOfUsedAssistants) {
		super(flag);
		
		this.numberOfUsedAssistants = numberOfUsedAssistants;
	}
	
	/**
	 * Returns the number of assistants returned to the pool as an int
	 * @return numberOfUsedAssistants
	 */
	public int getNumberOfUsedAssistants() {
		
		return numberOfUsedAssistants;
	}

	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
