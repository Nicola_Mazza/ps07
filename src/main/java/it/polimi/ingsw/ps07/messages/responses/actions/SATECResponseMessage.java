package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class SATECResponseMessage extends ActionResponseMessage {

	private String councillorColor;
	private String balconyName;
	private String updatedBalcony;
	
	public SATECResponseMessage(boolean flag, String councillorColor, String balconyName, String updatedBalcony) {
		super(flag);
		
		this.councillorColor = councillorColor;
		this.balconyName = balconyName;
		this.updatedBalcony = updatedBalcony;
	}

	/**
	 * Returns the color of the elected councillor as a string
	 * @return councillorColor
	 */
	public String getCouncillorColor() {
		
		return councillorColor;
	}
	
	/**
	 * Returns the balcony that has been changes as a string
	 * @return balconyName
	 */
	public String getBalconyName() {
		
		return balconyName;
	}
	
	/**
	 * Returns the updated balcony as a string
	 * @return updatedBalcony
	 */
	public String getUpdatedBalcony() {
		
		return updatedBalcony;
	}
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
