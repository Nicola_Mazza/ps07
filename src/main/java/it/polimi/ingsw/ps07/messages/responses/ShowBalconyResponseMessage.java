package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ShowBalconyRequestMessage.
 */
public class ShowBalconyResponseMessage implements ResponseMessage {

	private final String coastBalcony;
	private final String hillsBalcony;
	private final String mountainsBalcony;
	private final String kingsBalcony;
	
	/**
	 * Constructor for ShowBalconyResponseMessage.
	 * 
	 * @param coastBalcony the string representing the coast balcony.
	 * @param hillsBalcony the string representing the hills balcony.
	 * @param mountainsBalcony the string representing the mountains balcony.
	 * @param kingsBalcony the string representing the king's balcony.
	 */
	public ShowBalconyResponseMessage(String coastBalcony, String hillsBalcony, String mountainsBalcony, String kingsBalcony) {
		
		this.coastBalcony = coastBalcony;
		this.hillsBalcony = hillsBalcony;
		this.mountainsBalcony = mountainsBalcony;
		this.kingsBalcony = kingsBalcony;
	}
	
	

	/**
	 * Gets the coastBalcony.
	 * 
	 * @return coastBalcony the coast balcony as a string
	 */
	public String getCoastBalcony() {
		
		return coastBalcony.substring(1, coastBalcony.length() - 1);
	}

	/**
	 * Gets the hillsBalcony.
	 * 
	 * @return hillsBalcony the hills balcony as a string
	 */
	public String getHillsBalcony() {
		
		return hillsBalcony.substring(1, hillsBalcony.length() - 1);
	}

	/**
	 * Gets the mountainsBalcony.
	 * 
	 * @return mountainsBalcony the mountains balcony as a string
	 */
	public String getMountainsBalcony() {
		
		return mountainsBalcony.substring(1, mountainsBalcony.length() - 1);
	}
	
	/**
	 * Gets the kingsBalcony.
	 * 
	 * @return kingsBalcony the king's balcony as a string
	 */
	public String getKingsBalcony() {
		
		return kingsBalcony.substring(1, kingsBalcony.length() - 1);
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
