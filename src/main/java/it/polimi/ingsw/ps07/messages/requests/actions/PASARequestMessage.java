package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request a PerformAnotherSlowAction (quick) action.
 */
public class PASARequestMessage extends ActionRequestMessage {

	/**
	 * Constructor for PASARequestMessage.
	 * 
	 * @param token player's token.
	 */
	public PASARequestMessage(Token token) {
		
		super(token);
		
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
