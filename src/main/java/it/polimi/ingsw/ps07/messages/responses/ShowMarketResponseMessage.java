package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ShowMarketRequestMessage.
 */
public class ShowMarketResponseMessage implements ResponseMessage {

	private final String PTInsertions;
	private final String PCInsertions;
	private final String AInsertions;
	
	/**
	 * Constructor for ShowMarketResponseMessage.
	 * 
	 * @param PTInsertions List of insertions of Permit Tiles as a String.
	 * @param PCInsertions List of insertions of Politics Cards as a String.
	 * @param AInsertions List of insertions of Assistants as a String.
	 */
	public ShowMarketResponseMessage(String PTInsertions, String PCInsertions, String AInsertions) {
		
		this.PTInsertions = PTInsertions;
		this.PCInsertions = PCInsertions;
		this.AInsertions = AInsertions;
	}
	
	/**
	 * Gets PTInsertions.
	 * 
	 * @return PTInsertions List of insertions of Permit Tiles as a String.
	 */
	public String getPTInsertions() {
		
		return PTInsertions;
	}
	
	/**
	 * Gets PCInsertions.
	 * 
	 * @return PCInsertions List of insertions of Politics Cards as a String.
	 */
	public String getPCInsertions() {
		
		return PCInsertions;
	}
	
	/**
	 * Gets AInsertions.
	 * 
	 * @return AInsertions List of insertions of Assistants as a String.
	 */
	public String getAInsertions() {
		
		return AInsertions;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
