package it.polimi.ingsw.ps07.messages.responses;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ShowMapRequestMessage.
 */
public class ShowMapResponseMessage implements ResponseMessage {

	private final String cities;
	private final String chosenMap;
	private final String coastBonus;
	private final String hillsBonus;
	private final String mountainsBonus;
	private final String colorBonuses;
	private String kingsRewardTile;
	
	/**
	 * Constructor for the ShowMapResponseMessage.
	 * 
	 * @param game the running game.
	 */
	public ShowMapResponseMessage(Game game) {
		
		cities = game.getMap().getAllCities().toString();
		chosenMap = game.getChosenMap().getFileName();
		

		try {
			
			kingsRewardTile = game.getMap().getKingsRewardTiles().get(0).toString();
		} catch(IndexOutOfBoundsException e) {
			
			kingsRewardTile = "<<none>>";
		}
		
		colorBonuses = getBonuses(game.getMap().getColorBonusTiles()).toString();
		
		coastBonus = game.getMap().getCoast().getRegionBonusTile().toString();
		hillsBonus = game.getMap().getHills().getRegionBonusTile().toString();
		mountainsBonus = game.getMap().getMountains().getRegionBonusTile().toString();
	}
	
	/**
	 * Gets cities.
	 * 
	 * @return cities the cities on the map as a String.
	 */
	public String getCities() {
		
		// to get rid of one set of parentheses.
		return cities.substring(1, cities.length() - 1);
	}
	
	/**
	 * Gets the map chosen by the player.
	 * 
	 * @return chosenMap the map chosen by the player.
	 */
	public String getChosenMap() {
		
		return chosenMap;
	}
	
	/**
	 * Gets coastBonus.
	 * 
	 * @return coastBonus the RegionBonusTile of Coast.
	 */
	public String getCoastBonus() {
		
		return coastBonus;
	}
	
	/**
	 * Gets hillsBonus.
	 * 
	 * @return hillsBonus the RegionBonusTile of Hills.
	 */
	public String getHillsBonus() {
		
		return hillsBonus;
	}
	
	/**
	 * Gets mountainsBonus.
	 * 
	 * @return mountainsBonus the RegionBonusTile of Mountains.
	 */
	public String getMountainsBonus() {
		
		return mountainsBonus;
	}
	
	/**
	 * Gets colorBonuses.
	 * 
	 * @return colorBonues the ColorBonusTiles as a String.
	 */
	public String getColorBonuses() {
		
		return colorBonuses;
	}
	
	/**
	 * Gets a valid list of ColorBonusTiles.
	 * 
	 * @param bonuses a list of ColorBonusTiles.
	 * @return a valid list of ColorBonusTiles.
	 */
	private List<ColorBonusTile> getBonuses(List<ColorBonusTile> bonuses) {
		
		List<ColorBonusTile> list = new ArrayList<>();
		for(int i = 0; i < bonuses.size(); i++) {
			list.add(bonuses.get(i));
		}
		
		return list;
	}
	
	/**
	 * Gets kingsRewardTile.
	 * 
	 * @return kingsRewardTile the King's reward tile as a String.
	 */
	public String getKingsRewardTile() {
		
		return kingsRewardTile;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
