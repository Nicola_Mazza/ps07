package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * Request message to show the councillors pool.
 */
public class ShowPoolRequestMessage extends RequestMessage {

	/**
	 * Constructor for the ShowPoolRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public ShowPoolRequestMessage(Token token) {
		
		super(token);
	}
}
