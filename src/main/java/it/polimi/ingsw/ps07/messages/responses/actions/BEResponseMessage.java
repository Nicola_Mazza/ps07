package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class BEResponseMessage extends ActionResponseMessage {
	
	private final String chosenCity;

	public BEResponseMessage(boolean flag, String chosenCity) {
		
		super(flag);
		
		this.chosenCity = chosenCity;
	}

	/*
	 * Returns the city chosen by the player as a string
	 * @return chosenCity
	 */
	public String getChosenCity() {
		
		return chosenCity;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
