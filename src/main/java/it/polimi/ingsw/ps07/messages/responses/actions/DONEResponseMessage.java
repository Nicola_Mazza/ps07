package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class DONEResponseMessage extends ActionResponseMessage{

	/**
	 * Constructs a new DONEResponseMessage
	 * @param flag
	 */
	public DONEResponseMessage(boolean flag) {
		super(flag);
	}

	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
