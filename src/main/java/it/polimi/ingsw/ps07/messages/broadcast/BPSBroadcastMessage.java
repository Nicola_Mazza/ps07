package it.polimi.ingsw.ps07.messages.broadcast;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the start of the buying phase of the Market.
 */
public class BPSBroadcastMessage implements BroadcastMessage{

	private final int currentPlayer;
	
	/**
	 * Constructs a new BPSBroadcastMessage initializing
	 * the current player.
	 * 
	 * @param currentPlayer the currentPlayer as an int.
	 */
	public BPSBroadcastMessage(int currentPlayer) {
		
		this.currentPlayer = currentPlayer;
	}
	
	/**
	 * Gets currentPlayer.
	 * 
	 * @return currentPlayer, the current player
	 */
	public int getCurrentPlayer() {
		
		return currentPlayer;
	}
	
	@Override
	public void display(MessageVisitor message) {

		message.display(this);
	}

}
