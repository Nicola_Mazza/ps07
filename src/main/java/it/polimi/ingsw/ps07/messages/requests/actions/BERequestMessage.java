package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * Message to request a BuildEmporium (slow) action.
 */
public class BERequestMessage extends ActionRequestMessage {

	private final int chosenPermitTile;
	private final String chosenCityCapitalLetter;
	
	/**
	 * @param token player's token.
	 * @param chosenPermitTile index of the chosen Permit Tile.
	 * @param chosenCityCapitalLetter city the player decides to build in. If city equals "cityIsUnique", it means the
	 * Permit Tile allows to build only in a city, therefore the player does not have to
	 * decide where he wants to build in. The parameter must be gotten by the permitTile.
	 */
	public BERequestMessage(Token token, int chosenPermitTile, String chosenCityCapitalLetter) {
		
		super(token);
		
		// TODO : get the cityname somewhere somehow
		
		this.chosenCityCapitalLetter = chosenCityCapitalLetter;
		this.chosenPermitTile = chosenPermitTile;	
	}
	
	/**
	 * Returns the index of the chosen permit tile in player's hand.
	 * 
	 * @return chosenNumberOfPermitTile
	 */
	public int getChosenPermitTile() {
		
		return chosenPermitTile;
	}
	
	/**
	 * Returns the first letter of the chosen city.
	 * 
	 * @return chosenCityCapitalLetter
	 */
	public String getChosenCityCapitalLetter() {
		
		return chosenCityCapitalLetter;
	}
		
	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
