package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * The message to request a connection to the server.
 *
 */
public class ConnectionRequest extends RequestMessage {

	/**
	 * Constructor for ConnectionRequest.
	 * 
	 * @param token player's token.
	 */
	public ConnectionRequest(Token token) {
		
		super(token);
		
	}

}
