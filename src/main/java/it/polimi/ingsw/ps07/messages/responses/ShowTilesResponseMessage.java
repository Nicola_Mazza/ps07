package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ShowTilesRequestMessage.
 *
 */
public class ShowTilesResponseMessage implements ResponseMessage {

	private final String coastPermitTiles;
	private final String hillsPermitTiles;
	private final String mountainsPermitTiles;
	
	/**
	 * Constructor for ShowTilesResponseMessage.
	 * 
	 * @param coastPermitTiles the Permit Tiles shown on the Coast region board as a String.
	 * @param hillsPermitTiles the Permit Tiles shown on the Hills region board as a String.
	 * @param mountainsPermitTiles the Permit Tiles shown on the Mountains region board as a String.
	 */
	public ShowTilesResponseMessage(String coastPermitTiles, String hillsPermitTiles, String mountainsPermitTiles) {
		
		this.coastPermitTiles = coastPermitTiles;
		this.hillsPermitTiles = hillsPermitTiles;
		this.mountainsPermitTiles = mountainsPermitTiles;
	}
	
	/**
	 * Gets coastPermitTiles.
	 * 
	 * @return coastPermitTiles the Permit Tiles of the Coast region as a String.
	 */
	public String getCoastPermitTiles() {
		
		return coastPermitTiles;
	}
	
	/**
	 * Gets hillsPermitTiles.
	 * 
	 * @return hillsPermitTiles the Permit Tiles of the Hills region as a String.
	 */
	public String getHillsPermitTiles() {
		
		return hillsPermitTiles;
	}
	
	/**
	 * Gets mountainsPermitTiles.
	 * 
	 * @return mountainsPermitTiles the Permit Tiles of the Mountains region as a String.
	 */
	public String getMountainsPermitTiles() {
		
		return mountainsPermitTiles;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
