package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * 
 *
 */
public class SubscribeResponseMessage implements ResponseMessage {

	private final boolean success;
	
	/**
	 * Constructor for SubscribeResponseMessage.
	 * 
	 * @param success flag
	 */
	public SubscribeResponseMessage(boolean success) {

	     this.success = success;
	}

	/**
	 * Checks the status of success.
	 * 
	 * @return success flag, bool value
	 */
	public boolean isSuccess() {
	      
		 return success;
	 }
	
	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
