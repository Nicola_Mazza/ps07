package it.polimi.ingsw.ps07.messages.broadcast;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 *  Initial message shown to the clients when the game is created.
 */
public class GameStartedBroadcastMessage implements BroadcastMessage {
	
	private final int gameNumber;
	private final int numberOfPlayers;
	private final int playerTurn;
	private final String cities;
	private final String coastBalcony;
	private final String hillsBalcony;
	private final String mountainsBalcony;
	private final String kingsBalcony;
	private final String chosenMap;
	
	private final String coastBonus;
	private final String hillsBonus;
	private final String mountainsBonus;
	
	private final String colorBonuses;
	private String kingsRewardTile;

	
	/**
	 * Constructor for the GameStartedBroadCastMessage.
	 * 
	 * @param gameNumber the number of the game.
	 * @param game the running game.
	 */
	public GameStartedBroadcastMessage(int gameNumber, Game game) {
		
		this.gameNumber = gameNumber;
		numberOfPlayers = game.getNumberOfPlayers();
		playerTurn = 0;
		
		coastBonus = game.getMap().getCoast().getRegionBonusTile().toString();
		hillsBonus = game.getMap().getHills().getRegionBonusTile().toString();
		mountainsBonus = game.getMap().getMountains().getRegionBonusTile().toString();
		
		colorBonuses = getBonuses(game.getMap().getColorBonusTiles()).toString();
		
		try {
			
			kingsRewardTile = game.getMap().getKingsRewardTiles().get(0).toString();
		} catch(IndexOutOfBoundsException e) {
			
			kingsRewardTile = "<<none>>";
		}
		
		
		cities = game.getMap().getAllCities().toString();
		coastBalcony = game.getMap().getCoast().getRegionBalcony().toString();
		hillsBalcony = game.getMap().getHills().getRegionBalcony().toString();
		mountainsBalcony = game.getMap().getMountains().getRegionBalcony().toString();
		kingsBalcony = game.getMap().getKingsBalcony().toString();
		chosenMap = game.getChosenMap().getFileName();
		
	}
	
	/**
	 * Gets gameNumber.
	 * 
	 * @return gameNumer number of the game
	 */
	public int getGameNumber() {
		
		return gameNumber;
	}
	
	/**
	 * Gets numberOfPlayers.
	 * 
	 * @return numberOfPlayers the number of players
	 */
	public int getNumberOfPlayers() {
		
		return numberOfPlayers;
	}
	
	/**
	 * Gets playerTurn.
	 * 
	 * @return player turn player who is currently playing.
	 */
	public int getPlayerTurn() {
		
		return playerTurn;
	}

	/**
	 * Gets cities.
	 * 
	 * @return cities the cities as a string
	 */
	public String getCities() {
		
		// to get rid of one set of parentheses.
		return cities.substring(1, cities.length() - 1);
	}
	
	/**
	 * Gets the coastBalcony.
	 * 
	 * @return coastBalcony the balcony of the coast region as a String
	 */
	public String getCoastBalcony() {
		
		return coastBalcony.substring(1, coastBalcony.length() - 1);
	}
	
	/**
	 * Gets the hillsBalcony.
	 * 
	 * @return hillsBalcony the balcony of the hills region as a String
	 */
	public String getHillsBalcony() {
		
		return hillsBalcony.substring(1, hillsBalcony.length() - 1);
	}
	
	/**
	 * Gets the mountainsBalcony.
	 * 
	 * @return mountainsBalcony the balcony of the mountains region as a String
	 */
	public String getMountainsBalcony() {
		
		return mountainsBalcony.substring(1, mountainsBalcony.length() - 1);
	}
	
	/**
	 * Gets the kingsBalcony.
	 * 
	 * @return kingsBalcony the king's balcony as a String
	 */
	public String getKingsBalcony() {
		
		return kingsBalcony.substring(1, kingsBalcony.length() - 1);
	}
	
	/**
	 * Gets the map chosen by the player.
	 * 
	 * @return chosenMap the map chosen by the player.
	 */
	public String getChosenMap() {
		
		return chosenMap;
	}
	
	/**
	 * Gets coastBonus.
	 * 
	 * @return coastBonus the RegionBonusTile of Coast.
	 */
	public String getCoastBonus() {
		
		return coastBonus;
	}
	
	/**
	 * Gets hillsBonus.
	 * 
	 * @return hillsBonus the RegionBonusTile of Hills.
	 */
	public String getHillsBonus() {
		
		return hillsBonus;
	}
	
	/**
	 * Gets mountainsBonus.
	 * 
	 * @return mountainsBonus the RegionBonusTile of Mountains.
	 */
	public String getMountainsBonus() {
		
		return mountainsBonus;
	}
	
	/**
	 * Gets colorBonuses.
	 * 
	 * @return colorBonues the ColorBonusTiles as a String.
	 */
	public String getColorBonuses() {
		
		return colorBonuses;
	}
	
	/**
	 * Gets kingsRewardTile.
	 * 
	 * @return kingsRewardTile the King's reward tile as a String.
	 */
	public String getKingsRewardTile() {
		
		return kingsRewardTile;
	}
	
	/**
	 * Gets a valid list of ColorBonusTiles.
	 * 
	 * @param bonuses a list of ColorBonusTiles.
	 * @return a valid list of ColorBonusTiles.
	 */
	private List<ColorBonusTile> getBonuses(List<ColorBonusTile> bonuses) {
		
		List<ColorBonusTile> list = new ArrayList<>();
		for(int i = 0; i < bonuses.size(); i++) {
			list.add(bonuses.get(i));
		}
		
		return list;
	}

	@Override
	public void display(MessageVisitor message) {

		message.display(this);
	}

}
