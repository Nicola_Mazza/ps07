package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the execution of a PASA action.
 */
public class PASABroadcastMessage implements BroadcastMessage {

	private final int playerInt;
	private final int numberOfUsedAssistants;
	
	/**
	 * Constructs a PASABroadcastMessage.
	 * 
	 * @param playerInt the number of the player as an int.
	 * @param numberOfUsedAssistants the number of Assistants used by the player.
	 */
	public PASABroadcastMessage(int playerInt, int numberOfUsedAssistants) {
		
		this.playerInt = playerInt;
		this.numberOfUsedAssistants = numberOfUsedAssistants;
	}

	/**
	 * Returns the index of the player that performed the action.
	 * 
	 * @return playerInt the number of the player.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Returns the number of assistants return to the pool by the player.
	 * 
	 * @return numberOfUsedAssistants the number of Assistants used by the player.
	 */
	public int getNumberOfUsedAssistants() {
		
		return numberOfUsedAssistants;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
