package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * A request message to show the Permit Tiles used by the player.
 */
public class ShowUsedTilesRequestMessage extends RequestMessage {

	/**
	 * Constructor for showUsedTilesRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public ShowUsedTilesRequestMessage(Token token) {
		
		super(token);
	}

}
