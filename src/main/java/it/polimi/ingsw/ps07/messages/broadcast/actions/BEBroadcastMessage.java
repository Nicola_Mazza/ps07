package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the execution of a BE action.
 */
public class BEBroadcastMessage implements BroadcastMessage {

	private final int playerInt;
	private final String chosenCity;
	
	/**
	 * Constructor for BEBroadcastMessage.
	 * 
	 * @param playerInt the number of the player as an int.
	 * @param chosenCity the city chosen by the player as a String.
	 */
	public BEBroadcastMessage(int playerInt, String chosenCity){
		
		this.playerInt = playerInt;
		this.chosenCity = chosenCity;
	}
	
	/**
	 * Gets playerInt.
	 * 
	 * @return playerInt the number of the player as an int.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Gets chosenCity.
	 * 
	 * @return chosenCity the city chosen by the player as a String.
	 */
	public String getChosenCity() {
		
		return chosenCity;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
