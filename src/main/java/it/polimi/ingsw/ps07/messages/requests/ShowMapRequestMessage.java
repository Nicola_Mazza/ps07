package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * Message to request to show the map.
 */
public class ShowMapRequestMessage extends RequestMessage {

	/**
	 * Constructor for ShowMapRequestMessage.
	 * 
	 * @param token player's token
	 */
	public ShowMapRequestMessage(Token token) {
		
		super(token);
	}
}
