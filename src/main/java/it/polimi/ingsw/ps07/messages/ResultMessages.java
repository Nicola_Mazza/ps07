package it.polimi.ingsw.ps07.messages;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.messages.responses.ResponseMessage;

/**
 * This class represents both the private response messages and the broadcast ones
 * to a specific request.
 */
public class ResultMessages {
	
	public final ResponseMessage response;
	public final BroadcastMessage broadcast;
	
	/**
	 *  Creates a new ResultMessages, which contains a private ResponseMessage 
	 *  and a public BroadcastMessage to a specific request message.
	 *  
	 *  @param response  the response message.
	 *  @param broadcast the broadcast message.
	 */
	public ResultMessages(ResponseMessage response, BroadcastMessage broadcast) {
		
		this.response = response;
		this.broadcast = broadcast;
		
	}
}
