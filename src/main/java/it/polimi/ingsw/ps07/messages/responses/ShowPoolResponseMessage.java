package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ShowPoolRequestMessage.
 */
public class ShowPoolResponseMessage implements ResponseMessage {

	private final String pool;
	
	/**
	 * Constructor for ShowPoolResponseMessage.
	 * 
	 * @param pool the pool of councillors as a String.
	 */
	public ShowPoolResponseMessage(String pool) {
		
		this.pool = pool;
	}
	
	/**
	 * Gets pool.
	 * 
	 * @return pool the pool of councillors as a String.
	 */
	public String getPool() {
		
		return pool;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
