package it.polimi.ingsw.ps07.messages.broadcast;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * A message to send a broadcast chat message.
 */
public class ChatBroadcastMessage implements BroadcastMessage {

	private final int clientNumber;
	private final String message;
	
	/**
	 * Constructor for ChatBroadcastMessage.
	 * 
	 * @param clientNumber the number of the client requesting to chat.
	 * @param message the content of the message as a String.
	 */
	public ChatBroadcastMessage(int clientNumber, String message) {

		this.clientNumber = clientNumber;
		this.message = message;
	
	}
	
	/**
	 * Gets clientNumber.
	 * 
	 * @return clientNumber the of the client requesting to chat.
	 */
	public int getClientNumber() {
		
		return clientNumber;
	}
	
	/**
	 * Gets message.
	 * 
	 * @return message the content of the message as a String.
	 */
	public String getMessage() {
		
		return message;
	}

	@Override
	public void display(MessageVisitor message) {

		message.display(this);
	}

}
