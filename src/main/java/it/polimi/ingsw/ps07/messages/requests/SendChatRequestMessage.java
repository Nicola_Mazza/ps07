package it.polimi.ingsw.ps07.messages.requests;

import it.polimi.ingsw.ps07.controller.Token;

/**
 * Request to send a chat message.
 */
public class SendChatRequestMessage extends RequestMessage{
	
	private final String message;

	/**
	 * Constructor for SendChatRequestMessage.
	 * 
	 * @param token player's token.
	 * @param message the message to be sent as a String.
	 */
	public SendChatRequestMessage(Token token, String message) {
		
		super(token);
		this.message = message;
	}
	
	/**
	 * Gets message.
	 * 
	 * @return message the message sent as a String.
	 */
	public String getMessage() {
		
		return message;
	}

}
