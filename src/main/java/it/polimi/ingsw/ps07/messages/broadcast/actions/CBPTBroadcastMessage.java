package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 *  Broadcast message to notify the clients about the execution of an CBPT action.
 *
 */
public class CBPTBroadcastMessage implements BroadcastMessage {

	private final int playerInt;
	private final String regionName;
	private final String newRevealedPermitTiles;
	
	
	/**
	 * Creates a new CBPT broadcast message.
	 * @param playerInt the number of the player as an int.
	 * @param regionName the name of the region.
	 * @param newRevealedPermitTiles the new reveled Permit Tiles.
	 */
	public CBPTBroadcastMessage(int playerInt, String regionName, String newRevealedPermitTiles) {
		
		this.playerInt = playerInt;
		this.regionName = regionName;
		this.newRevealedPermitTiles = newRevealedPermitTiles;
	}

	/**
	 * Returns the index of the player that performed the action.
	 * 
	 * @return playerInt the number of the player as an int.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Returns the balcony whose permit tile has been changed
	 * as a string.
	 * 
	 * @return balconyName the name of the region the balcony belongs to as a String.
	 */
	public String getRegionName() {
		
		return regionName;
	}
	
	/**
	 * Returns the new revealed permit tiles as a String.
	 * 
	 * @return newRevealedPermtiTiles the new revelead Permit Tiles as a String.
	 */
	public String getNewRevealedPermitTiles() {
		
		return newRevealedPermitTiles;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}

}
