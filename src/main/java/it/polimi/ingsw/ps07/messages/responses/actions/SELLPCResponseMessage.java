package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class SELLPCResponseMessage extends ActionResponseMessage{

	private final String chosenPoliticsCard;
	private final int price;
	
	/**
	 * Constructs a new SELLPCResponseMessage initializing the
	 * color of the chosen card and its price
	 * @param flag
	 * @param chosenPoliticsCard
	 * @param price
	 */
	public SELLPCResponseMessage(boolean flag, String chosenPoliticsCard, int price) {
		
		super(flag);
		
		this.chosenPoliticsCard = chosenPoliticsCard;
		this.price = price;
	}
	
	/**
	 * @return chosenPoliticsCard, the color of the card chosen 
	 * by the player
	 */
	public String getChosenPoliticsCard() {
		
		return chosenPoliticsCard;
	}
	
	/**
	 * @return price, the price of the insertion
	 */
	public int getPrice() {
		
		return price;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
