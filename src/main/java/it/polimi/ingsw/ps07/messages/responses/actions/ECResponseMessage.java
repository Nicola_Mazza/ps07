package it.polimi.ingsw.ps07.messages.responses.actions;


import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class ECResponseMessage extends ActionResponseMessage {

	private final String updatedBalcony;
	private final String balconyName;
	private final String colorString;
	
	/**
	 * Constructor for ECResponseMessage.
	 * 
	 * @param flag
	 * @param balconyName
	 * @param updatedBalcony
	 */
	public ECResponseMessage(boolean flag, String balconyName, String updatedBalcony, String colorString) {
		
		super(flag);
		this.updatedBalcony = updatedBalcony;
		this.balconyName = balconyName;
		
		// TODO : Control here?
		this.colorString = colorString;
	}

	/**
	 * Returns the updated balcony as a String
	 * 
	 * @return updatedBalcony as a String
	 */
	public String getUpdatedBalcony() {
		
		return updatedBalcony;
	}
	
	/**
	 * Gets the balcony name.
	 * 
	 * @return balconyName as a String
	 */
	public String getBalconyName() {
		
		return balconyName;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

	/**
	 * Gets colorString.
	 * 
	 * @return colorString the color of the chosen councillor as a String
	 */
	public String getColorString() {
		
		return colorString;
	}

}
