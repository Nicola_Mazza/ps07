package it.polimi.ingsw.ps07.messages.requests.actions;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.controller.actions.Action;
import it.polimi.ingsw.ps07.controller.actions.ActionFactoryVisitor;

/**
 * A message to request to stop the Market phase actions.
 */
public class DoneRequestMessage extends ActionRequestMessage {

	/**
	 * Constructor for DoneRequestMessage.
	 * 
	 * @param token player's token.
	 */
	public DoneRequestMessage(Token token) {
		
		super(token);
	}

	@Override
	public Action createAction(ActionFactoryVisitor visitor) {
		
		return visitor.visit(this);
	}

}
