package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the execution of an APT action.
 */
public class APTBroadcastMessage implements BroadcastMessage{

	private final int playerInt;
	private final int chosenPermitTileNumber;
	private final String regionName;
	private final int numberOfCardsUsed;
	private final int numberOfJolliesUsed;
	private final int spentCoins;
	
	/**
	 * Constructor for APTBroadcastMessage.
	 * 
	 * @param playerInt the player number.
	 * @param chosenPermitTileNumber the index of the chosen Permit Tile.
	 * @param regionName the name of the region.
	 * @param numberOfCardsUsed the number of Politics Cards used.
	 * @param numberOfJolliesUsed the number of Jolly Cards used.
	 * @param spentCoins the amount of coins spent as an int.
	 */
	public APTBroadcastMessage(int playerInt, int chosenPermitTileNumber, String regionName, int numberOfCardsUsed,
			int numberOfJolliesUsed, int spentCoins) {
		
		this.playerInt = playerInt;
		this.chosenPermitTileNumber = chosenPermitTileNumber;
		this.regionName = regionName;
		this.numberOfCardsUsed = numberOfCardsUsed;
		this.numberOfJolliesUsed = numberOfJolliesUsed;
		this.spentCoins = spentCoins;
	}
	
	/**
	 * Returns the index of the player that performed the action.
	 * 
	 * @return playerInt the number of the player.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Returns the number of the permit tile chosen by the player 
	 * as an int.
	 * 
	 * @return chsoenPermitTileNumber
	 */
	public int getChosenPermitTileNumber() {
		
		return chosenPermitTileNumber;
	}
	
	/**
	 * Returns the name of the region whose balcony has been satisfied
	 * as a String.
	 * 
	 * @return regionName
	 */
	public String getRegionName() {
		
		return regionName;
	}
	
	/**
	 * Returns the number of used cards as an int.
	 *
	 * @return numberOfCardsUsed the number of Politics Cards used.
	 */
	public int getNumberOfCardsUsed() {
		
		return numberOfCardsUsed;
	}
	
	/**
	 * Returns the number of used jollies as an int.
	 * 
	 * @return numberOfJolliesUsed the number of Jolly Cards used.
	 */
	public int getNumberOfJolliesUsed() {
		
		return numberOfJolliesUsed;
	}
	
	/**
	 * returns the number of spent coins as an int.
	 *
	 * @return spentCoins the amount of coins spent as an int.
	 */
	public int getSpentCoins() {
		
		return spentCoins;
	}

	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
	}
}
