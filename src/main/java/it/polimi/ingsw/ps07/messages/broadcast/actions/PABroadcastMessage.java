package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 *  Broadcast message to notify the clients about the execution of a PA action.
 */
public class PABroadcastMessage implements BroadcastMessage {

	private final int player;
	private final int newTurnNumber;
	private final int newPlayerNumber;
	
	/**
	 * Constructor for a new PassActionBroadcastMessage.
	 * 
	 * @param playerInt the number of the player as an int.
	 * @param turnNumber the number of the current turn.
	 * @param nextPlayerInt the number of the next player.
	 */
	public PABroadcastMessage(int playerInt, int turnNumber, int nextPlayerInt) {

		player = playerInt;
		newTurnNumber = turnNumber;
		newPlayerNumber = nextPlayerInt;
	}

	@Override
	public void display(MessageVisitor message) {

		message.display(this);
	}

	
	/**
	 * Gets the number of the player who is passing the turn.
	 * 
	 * @return player the player who is passing the turn.
	 */
	public int getPlayer() {
		
		return player;
	}
	
	/**
	 * Gets the number of the new player who is playing the turn.
	 * 
	 * @return newPlayerNumber the next player.
	 */
	public int getNewPlayer() {
		
		return newPlayerNumber;
	}

	/**
	 * Gets the new turn number.
	 * 
	 * @return newTurnNumber the new turn number.
	 */
	public int getNewTurn() {
		
		return newTurnNumber;
	}

}
