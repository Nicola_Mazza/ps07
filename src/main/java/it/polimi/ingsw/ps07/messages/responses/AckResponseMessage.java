package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 *  An acknowledgment message.
 */
public class AckResponseMessage implements ResponseMessage {

	private final String message;
	
	/**
	 * Create a new acknowledgment message without message.
	 */
	public AckResponseMessage() {
		
		this("");
	}
	
	/**
	 * Create a new acknowledgment message with a message.
	 * 
	 * @param message the message.
	 */
	public AckResponseMessage(String message) {
		
		this.message = message;
	}
	
	/**
	 * Get the message of the acknowledgment message.
	 * 
	 * @return the message.
	 */
	public String getMessage() {
		
		return message;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
