package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class BEWKHResponseMessage extends ActionResponseMessage {

	private final String chosenCity;
	private final int numberOfCardsUsed;
	private final int numberOfJolliesUsed;
	private final int spentCoins;
	private final int usedAssistants;
	
	public BEWKHResponseMessage(boolean flag, String chosenCity, int numberOfCardsUsed, int numberOfJolliesUsed, int spentCoins, int usedAssistants) {
		
		super(flag);
		this.chosenCity = chosenCity;
		this.numberOfCardsUsed = numberOfCardsUsed;
		this.numberOfJolliesUsed = numberOfJolliesUsed;
		this.spentCoins = spentCoins;
		this.usedAssistants = usedAssistants;
	}

	/**
	 * Returns the city in which the player has built
	 * an emporium as a string
	 * @return chosenCity
	 */
	public String getChosenCity() {
		
		return chosenCity;
	}
	
	/**
	 * Returns the number of normal cards used by the
	 * player to satisfy the king's balcony
	 * @return numberOfCardsUsed
	 */
	public int getNumberOfCardsUsed() {
		
		return numberOfCardsUsed;
	}
	
	/**
	 * Returns the number of jolly cards used by the
	 * player to satisfy the king's balcony
	 * @return numberOfJolliesUsed
	 */
	public int getNumberOfJolliesUsed() {
		
		return numberOfJolliesUsed;
	}
	
	/**
	 * returns the number of coins the player had to spent
	 * to satisfy the king's balcony and move him to the
	 * city in which he wanted to build the emporium
	 * @return spentCoins
	 */
	public int getSpentCoins() {
		
		return spentCoins;
	}
	
	/**
	 * Returns the number of assistants the player had to use
	 * to build the emporium 
	 * @return usedAssistants
	 */
	public int getUsedAssistants() {
		
		return usedAssistants;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
