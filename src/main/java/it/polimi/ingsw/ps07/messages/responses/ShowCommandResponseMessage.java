package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ShowCommandRequestMessage.
 *
 */
public class ShowCommandResponseMessage implements ResponseMessage {

	@Override
	public void display(MessageVisitor messageVisitor) {

		messageVisitor.display(this);
	}

}
