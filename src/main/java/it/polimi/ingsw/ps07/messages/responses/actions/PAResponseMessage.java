package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class PAResponseMessage extends ActionResponseMessage {
	
	private final int newTurn;
	private final int nextPlayer;

	public PAResponseMessage(boolean flag, int turnNumber, int nextPlayerInt) {
		
		super(flag);
		
		newTurn = turnNumber;
		nextPlayer = nextPlayerInt;
	}
	
	public int getNewTurn() {
		
		return newTurn;
	}
	
	public int getNextPlayer() {
		
		return nextPlayer;
	}

	@Override
	public void display(MessageVisitor messageVisitor) {
		// TODO Auto-generated method stub
		
	}

}
