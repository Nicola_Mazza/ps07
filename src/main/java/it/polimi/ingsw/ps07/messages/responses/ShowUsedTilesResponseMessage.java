package it.polimi.ingsw.ps07.messages.responses;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Response message to a ShowUsedTilesRequestMessage.
 */
public class ShowUsedTilesResponseMessage implements ResponseMessage {

	private final String usedTiles;
	
	/**
	 * Constructor for ShowUsedTilesResponseMessage.
	 * 
	 * @param usedTiles the Permit Tiles used by the player as a String.
	 */
	public ShowUsedTilesResponseMessage(String usedTiles) {
	
		this.usedTiles = usedTiles;
	}
	
	/**
	 * Gets usedTiles.
	 * 
	 * @return usedTiles the Permit Tiles used by the player.
	 */
	public String getUsedTiles() {
		
		return usedTiles;
	}

	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
