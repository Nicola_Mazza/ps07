package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class PurchaseResponseMessage extends ActionResponseMessage{

	private final String typeOfInsertion;
	private final int nOfInsertion;
	private final int playerID;
	private final int price;
	
	/**
	 * Constructs a new PurchaseResponseMessage initializing
	 * the type of the purchased insertion and its number, 
	 * the number of the player that made the insertion 
	 * and the price
	 * @param flag
	 * @param typeOfInsertion
	 * @param nOfInsertion
	 * @param playerID
	 * @param price
	 */
	public PurchaseResponseMessage(boolean flag, String typeOfInsertion, int nOfInsertion, int playerID, int price) {
		
		super(flag);
		
		this.typeOfInsertion = typeOfInsertion;
		this.nOfInsertion = nOfInsertion;
		this.playerID = playerID;
		this.price = price;
	}
	
	/**
	 * @return typeOfInsertion, the object of the insertion
	 */
	public String getTypeOfInsertion() {
		
		return typeOfInsertion;
	}
	
	/**
	 * @return nOfInsertion, the number of the purchased insertion
	 */
	public int getNOfInsertion() {
		
		return nOfInsertion;
	}
	
	/**
	 * @return playerID, the number of the player that made 
	 * the insertion
	 */
	public int getPlayerID() {
		
		return playerID;
	}
	
	/**
	 * @return price, the price of the purchased insertion
	 */
	public int getPrice() {
		
		return price;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
