package it.polimi.ingsw.ps07.messages.broadcast.actions;

import it.polimi.ingsw.ps07.messages.broadcast.BroadcastMessage;
import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the clients about the execution of an EC action.
 */
public class ECBroadcastMessage implements BroadcastMessage {

	private final String updatedBalcony;
	private final String balconyName;
	private final String colorString;
	private final int playerInt;
	
	/**
	 * Constructor for ECBroadcastMessage.
	 * 
	 * @param playerInt the number of the player as an int.
	 * @param balconyName the name of the region the balcony belongs to as a String.
	 * @param updatedBalcony the updated balcony as a String.
	 * @param colorString the color of the Councillor chosen by the player as a String.
	 */
	public ECBroadcastMessage(int playerInt, String balconyName, String updatedBalcony, String colorString) {
		
		this.playerInt = playerInt;
		this.updatedBalcony = updatedBalcony;
		this.balconyName = balconyName;
		this.colorString = colorString;
	}
	
	/**
	 * Returns the updated balcony as a string.
	 * 
	 * @return updatedBalcony the updated balcony after the execution of this action as a String.
	 */
	public String getUpdatedBalcony() {
		
		return updatedBalcony;
	}
	
	
	/**
	 * Returns the name of the balcony that has been updated.
	 * 
	 * @return balconyName the name of the region the balcony belongs to as a String.
	 */
	public String getBalconyName() {
		
		return balconyName;
	}
	
	/**
	 * Returns the index of the player that performed the action.
	 * 
	 * @return playerInt the number of the player.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	

	/**
	 * Gets colorString.
	 * 
	 * @return colorString the color as a String.
	 */
	public String getColorString() {
		
		return colorString;
	}
	
	@Override
	public void display(MessageVisitor message) {
		
		message.display(this);
		
	}

}

