package it.polimi.ingsw.ps07.messages.broadcast;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

/**
 * Broadcast message to notify the end of a turn.
 */
public class TEBroadcastMessage implements BroadcastMessage {

	private final int playerInt;
	private final int turnNumber;
	private final int nextPlayerInt;
	
	/**
	 * Constructor for TEBroadcastMessage.
	 * 
	 * @param playerInt current player number.
	 * @param turnNumber current turn number.
	 * @param nextPlayerInt next player number.
	 */
	public TEBroadcastMessage(int playerInt, int turnNumber, int nextPlayerInt) {
		
		this.playerInt = playerInt;
		this.turnNumber = turnNumber;
		this.nextPlayerInt = nextPlayerInt;
	}
	
	/**
	 * Gets the current player number.
	 * 
	 * @return playeInt the current player number.
	 */
	public int getPlayerInt() {
		
		return playerInt;
	}
	
	/**
	 * Gets the next player number.
	 * 
	 * @return nextPlayerInt next player number.
	 */
	public int getNextPlayerInt() {
		
		return nextPlayerInt;
	}
	
	/**
	 * Gets the turn number.
	 * 
	 * @return turnNumber the turn number.
	 */
	public int getTurnNumber() {
		
		return turnNumber;
	}
	
	@Override
	public void display(MessageVisitor message) {

		message.display(this);
	}

}
