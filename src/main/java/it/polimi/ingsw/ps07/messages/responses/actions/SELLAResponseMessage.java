package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class SELLAResponseMessage extends ActionResponseMessage{

	private final int nOfAssistants;
	private final int price;
	
	/**
	 * Constructs a new SELLAResponseMessage initializing
	 * the number of assistants the player wants to sell and
	 * their price
	 * @param flag
	 * @param nOfAssistants
	 * @param price
	 */
	public SELLAResponseMessage(boolean flag, int nOfAssistants, int price) {
		
		super(flag);
		
		this.nOfAssistants = nOfAssistants;
		this.price = price;
	}
	
	/**
	 * @return nOfAssistants, the number of assistants 
	 * the player wants to sell
	 */
	public int getNumberOfAssistants() {
		
		return nOfAssistants;
	}
	
	/**
	 * @return price, the price of the insertion
	 */
	public int getPrice() {
		
		return price;
	}
	
	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
