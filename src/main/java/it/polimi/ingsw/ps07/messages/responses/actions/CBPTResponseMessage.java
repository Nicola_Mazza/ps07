package it.polimi.ingsw.ps07.messages.responses.actions;

import it.polimi.ingsw.ps07.view.gameplayer.MessageVisitor;

public class CBPTResponseMessage extends ActionResponseMessage {

	private final String balconyName;
	private final String newRevealedPermitTiles;
	
	
	/**
	 * Constructs a new CBPTResponseMessage 
	 * @param flag
	 * @param balconyName
	 * @param newRevealedPermitTiles
	 */
	public CBPTResponseMessage(boolean flag, String balconyName, String  newRevealedPermitTiles) {
		super(flag);
		
		this.balconyName = balconyName;
		this.newRevealedPermitTiles = newRevealedPermitTiles;
	}
	
	
	/**
	 * Returns the balcony whose permit tile has been changed
	 * as a string
	 * @return balconyName
	 */
	public String getBalconyName() {
		
		return balconyName;
	}
	
	/**
	 * Returns the new revealed permit tiles as a string
	 * @return newRevealedPermtiTiles
	 */
	public String getNewRevealedPermitTiles() {
		
		return newRevealedPermitTiles;
	}

	@Override
	public void display(MessageVisitor messageVisitor) {
		
		messageVisitor.display(this);
	}

}
