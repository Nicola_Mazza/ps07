package it.polimi.ingsw.ps07.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.polimi.ingsw.ps07.model.map.City;

/**
 * Class to read mostly map-related information from the
 * configuration file. It implements the Singleton pattern.
 */
public class ReaderXML {
	
	//Singleton pattern
	private static ReaderXML instance = new ReaderXML();
	private static final Logger LOG = Logger.getLogger(ReaderXML.class.getName());
	private static final String VALUE = "Value";
	private static final String TILES = "Tiles";
	Document doc;
	
	/**
	 * Constructor for ReaderXML.
	 */
	private ReaderXML() {
		
		try {
			
			File fileXML = new File("docs/ConfigurationFile.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			this.doc = dBuilder.parse(fileXML);	
			this.doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException e) {
			
			LOG.log(Level.WARNING, "ParserConfigurationException occured while creating the ReaderXML." , e);
		} catch (SAXException e) {
			
			LOG.log(Level.WARNING, "SAXException occured while creating the ReaderXML." , e);
		} catch (IOException e) {
			
			LOG.log(Level.WARNING, "IOexception occured while creating the ReaderXML." , e);
		}
		
		
	}
	
	/**
	 * Implementation of the Singleton pattern.
	 * 
	 * @return instance the instance of ReaderXML
	 */
	public static ReaderXML getInstance() {
		
		return instance;
	}
	
	public List<City> readCities(String regionName, int numberOfPlayers, char mapChar) {
		
		//gets the "regionName" tagged node
		NodeList nListRegion = doc.getElementsByTagName(regionName + mapChar);
		
		// creates the node list of cities from the given region
		NodeList nListCity = nListRegion.item(0).getChildNodes();
		List<City> regionCities= new ArrayList<>();
		
		for (int i = 0 ; i < nListCity.getLength(); i++) {
			Node nNodeCity = nListCity.item(i);
			if(nNodeCity.getNodeType() == Node.ELEMENT_NODE) {
				Element eElementCity = (Element) nNodeCity;
				City city = new City(eElementCity.getElementsByTagName("cityName").item(0).getTextContent() ,
						eElementCity.getElementsByTagName("cityColor").item(0).getTextContent(),
						eElementCity.getElementsByTagName("cityColorString").item(0).getTextContent(), numberOfPlayers);
				regionCities.add(city);
			}
		}
		return regionCities;
	}
	
	/**
	 * @param regionName, the name of the region of the cities that
	 * will be read
	 * @return regionCities, an arrayList containing all the 
	 * cities of a region
	 */
	public List<String> readCitiesCapitalLetters(String regionName) {
		NodeList nListRegion = doc.getElementsByTagName(regionName);
		NodeList nListCity = nListRegion.item(0).getChildNodes();
		List<String> regionCities = new ArrayList<>();
		
		for(int i = 0; i < nListCity.getLength(); i++) {
			Node nNodeCity = nListCity.item(i);
			if(nNodeCity.getNodeType() == Node.ELEMENT_NODE) {
				Element eElementCity = (Element) nNodeCity;
				String cityName = eElementCity.getElementsByTagName("cityName").item(0).getTextContent();
				
				//add to region cities only the first char of the city name as a String
				regionCities.add(cityName.substring(0, 1));
			}
		}
		
		return regionCities;
	}
	
	/**
	 * @param mapChar, a char indicating the map
	 * @return the king's starting city as a string
	 */
	public String readKingsStartingCity(char mapChar) {
		NodeList nList = doc.getElementsByTagName("KingsStartingCity" + mapChar);
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getTextContent();
		}
		return null;
	}
	
	/**
	 * @return colors, an arrayList containing all the
	 * politics colors as hex strings
	 */
	public List<String> readPoliticsColors () {
		NodeList nListColors = doc.getElementsByTagName("pColor");
		List<String> colors = new ArrayList<>();
		
		for(int i = 0; i < nListColors.getLength(); i++) {
			Node nNodeColor = nListColors.item(i);
			if(nNodeColor.getNodeType() == Node.ELEMENT_NODE) {
				Element eElementColor = (Element) nNodeColor;
				String string = eElementColor.getAttribute("hex");
				colors.add(string);
			}
		}
		return colors;
	}
	
	/**
	 * @return colors, an arrayList containing all the
	 * politics colors as strings
	 */
	public List<String> readPoliticsColorsString() {
		
		NodeList nList = doc.getElementsByTagName("pColor");
		List<String> colors = new ArrayList<>();
		
		for(int i = 0; i < nList.getLength(); i++) {
			
			Node nNode = nList.item(i);
			
			if(nNode.getNodeType() == Node.ELEMENT_NODE) {
				
				Element eElement = (Element) nNode;
				String string = eElement.getAttribute("StringPColor");
				colors.add(string);
				
			}
		}
		
		return colors;
	}
	
	/**
	 * @param region, the name of the region of the permit tiles
	 * that will be read
	 * @param permitNumber, number of the permit tile that will 
	 * be read
	 * @param cityNumber, the index of the city on the permit tile
	 * @return a city on the permit tile
	 */
	public String readPermitCity(String region, int permitNumber, int cityNumber) {
		NodeList nList = doc.getElementsByTagName(region + TILES);
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("Tile" + permitNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName("city" + cityNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param region, the name of the region 
	 * @param permitNumber, the number of the permit tile that
	 * will be read
	 * @param bonusNumber, the index of the bonus on the permit tile
	 * @return a bonus on the permit tile
	 */
	public String readPermitBonus(String region, int permitNumber, int bonusNumber) {
		NodeList nList = doc.getElementsByTagName(region + TILES);
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("Tile" + permitNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName("bonus" + bonusNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param region, the name of the region
	 * @param permitNumber, the number of the permit tile that
	 * will be read
	 * @param bonusNumber, the index of the bonus of which will be
	 * read a value 
	 * @return the value of a bonus on the permit tile as a string
	 */
	public String readPermitBonusValue(String region, int permitNumber, int bonusNumber) {
		NodeList nList = doc.getElementsByTagName(region + TILES);
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("Tile" + permitNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName(VALUE + bonusNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param tileNumber, the number of the bonus tile that 
	 * will be read
	 * @param bonusNumber, the number of the bonus that will
	 * be read
	 * @return the bonus of the tile as a string
	 */
	public String readBonusTileBonus(int tileNumber, int bonusNumber) {
		NodeList nList = doc.getElementsByTagName("BonusCard" + tileNumber);
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getElementsByTagName("Bonus" + bonusNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param tileNumber, the number of the bonus tile that
	 * will be read
	 * @param bonusNumber, the number of the bonus that will 
	 * be read
	 * @return the value of the bonus of the tile as a string
	 */
	public String readBonusTileBonusValue(int tileNumber, int bonusNumber) {
		NodeList nList = doc.getElementsByTagName("BonusCard" + tileNumber);
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getElementsByTagName(VALUE + bonusNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param cellNumber, the number of the cell of
	 * the nobility board
	 * @param bonusNumber, the number of the bonus of 
	 * a cell
	 * @return the bonus of the cell as a string
	 */
	public String readNobilityBonus(int cellNumber, int bonusNumber) {
		NodeList nList = doc.getElementsByTagName("NobilityBonus");
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("NBonus" + cellNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName("nBonus" + bonusNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param cellNumber, the number of the cell of
	 * the nobility board
	 * @param bonusNumber, the number of the bonus of
	 * a cell
	 * @return the value of the bonus of the cell
	 */
	public String readNobilityBonusValue(int cellNumber, int bonusNumber) {
		NodeList nList = doc.getElementsByTagName("NobilityBonus");
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("NBonus" + cellNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName("nValue" + bonusNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param linkNumber, the number of the link
	 * between cities
	 * @param vertexNumber, the number of vertex
	 * @param mapChar, a char indicating the map
	 * @return the vertex of a link
	 */
	public String readLinkVertex(int linkNumber, int vertexNumber, char mapChar) {
		NodeList nList = doc.getElementsByTagName("link" + linkNumber + mapChar);
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getElementsByTagName("vertex" + vertexNumber).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param tileNumber, the number of a color
	 * bonus tile
	 * @return the color of a bonus tile as an hex string
	 */
	public String readColorBonusTileColor(int tileNumber) {
			NodeList nList = doc.getElementsByTagName("ColorBonusTile");
			Node nNode = nList.item(0);
			Element eElement = (Element) nNode;
			nList = eElement.getElementsByTagName("Tile" + tileNumber);
			nNode = nList.item(0);
			if(nNode.getNodeType() == Node.ELEMENT_NODE) {
				eElement = (Element) nNode;
				return eElement.getElementsByTagName("Color").item(0).getTextContent();
			}
			return null;
	}
	
	/**
	 * @param tileNumber, the number of a color
	 * bonus tile
	 * @return the color of a bonus tile as a string
	 */
	public String readColorBonusTileStringColor(int tileNumber) {
		NodeList nList = doc.getElementsByTagName("ColorBonusTile");
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("Tile" + tileNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName("StringColor").item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param tileNumber, the number of a color
	 * bonus tile
	 * @return the value of a bonus tile as a string
	 */
	public String readColorBonusTileValue(int tileNumber) {
		NodeList nList = doc.getElementsByTagName("ColorBonusTile");
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("Tile" + tileNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName(VALUE).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param tileNumber, the number of a king's 
	 * reward bonus tile
	 * @return the value of the tile as a string
	 */
	public String readKingsRewardTile(int tileNumber) {
		NodeList nList = doc.getElementsByTagName("KingsRewardBonusTile");
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		nList = eElement.getElementsByTagName("Tile" + tileNumber);
		nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			eElement = (Element) nNode;
			return eElement.getElementsByTagName(VALUE).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param colorNumber, the index of a color
	 * @return the color of a councillor as an hex string
	 */
	public String readCouncillorHexColor(int colorNumber) {
		NodeList nList = doc.getElementsByTagName("cColor" + colorNumber);
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getAttribute("Hex");
		}
		return null;
	}
	
	/**
	 * @param colorNumber, the index of a color
	 * @return the color of a councillor as a string
	 */
	public String readCouncillorStringColor(int colorNumber) {
		
		NodeList nList = doc.getElementsByTagName("cColor" + colorNumber);
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getAttribute("StringCColor");
		}
		
		return null;
	}
	
	/**
	 * @param typeOfCard, type of card of which the
	 * number will be read
	 * @return the number of cards of this type to be
	 * instantiated in the game as a string
	 */
	public String readNumberOf(String typeOfCard) {
		NodeList nList = doc.getElementsByTagName("NumberOfCards");
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE){
			Element eElement = (Element) nNode;
			return eElement.getElementsByTagName(typeOfCard).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param constant, a type of constant of the game
	 * @return the value of the constant as a string
	 */
	public String readConstant(String constant) {
		NodeList nList = doc.getElementsByTagName("Constants");
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getElementsByTagName(constant).item(0).getTextContent();
		}
		return null;
	}
	
	/**
	 * @param color, a city color
	 * @param mapChar, a char indicating the map
	 * @return the number of cities of this color
	 */
	public String readNumberOfCitiesOfAColor(String color, char mapChar) {
		
		NodeList nList = doc.getElementsByTagName("Constants");
		Node nNode = nList.item(0);
		if(nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			return eElement.getElementsByTagName("n" + color + mapChar).item(0).getTextContent();
		}
		return null;
	}
}	