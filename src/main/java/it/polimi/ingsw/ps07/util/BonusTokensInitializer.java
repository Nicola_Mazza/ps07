package it.polimi.ingsw.ps07.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.bonus.AssistantBonus;
import it.polimi.ingsw.ps07.model.bonus.Bonus;
import it.polimi.ingsw.ps07.model.bonus.BonusToken;
import it.polimi.ingsw.ps07.model.bonus.CardBonus;
import it.polimi.ingsw.ps07.model.bonus.CoinBonus;
import it.polimi.ingsw.ps07.model.bonus.NobilityBonus;
import it.polimi.ingsw.ps07.model.bonus.VictoryBonus;
import it.polimi.ingsw.ps07.model.map.Emperor;
import it.polimi.ingsw.ps07.model.map.Map;
import it.polimi.ingsw.ps07.model.map.RegionBoard;

/**
 * This class helps initializing the bonus tokens of the cities.
 */
public class BonusTokensInitializer {
	
	private List<BonusToken> bonusTokens;
	
	/**
	 * Constructor for the BonusTokensInitializer.
	 */
	public BonusTokensInitializer() {
		
		bonusTokens = new ArrayList<>();
		List<Bonus> bonuses = null;
		
		for(int i = 1; i <= Map.NUMBER_OF_BONUS_TOKENS; i++) {
			
			bonuses = new ArrayList<>();
			
			for(int j = 1; j <= 2; j++){
				
				Bonus bBonus = null;
				String bonus = ReaderXML.getInstance().readBonusTileBonus(i, j);
				if(!("null").equals(bonus)) {
					int bonusValue = Integer.parseInt(ReaderXML.getInstance().readBonusTileBonusValue(i, j));
					switch(bonus){
					case "NumberOfVictoryPoints" :
						bBonus = new VictoryBonus(bonusValue);
						break;
					case "NumberOfCoins" :
						bBonus = new CoinBonus(bonusValue);
						break;
					case "NumberOfAssistants" :
						bBonus = new AssistantBonus(bonusValue);
						break;
					case "NumberOfCards" :
						bBonus = new CardBonus(bonusValue);
						break;
					case "NobilitySteps" :
						bBonus = new NobilityBonus(bonusValue);	
						break;
					default :
						break;
					}
					bonuses.add(bBonus);
				}
			}
			bonusTokens.add(new BonusToken(bonuses));
		}
		
		//shuffles bonusTokens array list before they are assigned to each city		
		Collections.shuffle(bonusTokens);
	}
	
	/**
	 * Assignment of the city bonuses, one for each city
	 * except for the king's starting city. The control of the king's starting city
	 * is implemented for each region, so that we can support changes to the configuration file.
	 * 
	 * @param coast the regionBoard coast.
	 * @param hills the regionBoard hills.
	 * @param mountains the regionBoard mountains.
	 */
	public void assignTokens(RegionBoard coast, RegionBoard hills, RegionBoard mountains) {
		int k = 0;
		for(int i = 0; i < coast.getRegionCities().size(); i++){
			if(!coast.getRegionCities().get(i).getCityName().equals(Emperor.getInstance().getKingsStartingCity())) {
				
				coast.getRegionCities().get(i).setBonusTile(bonusTokens.get(k));
				k++;
			}
			
		}
		for(int i = 0; i < hills.getRegionCities().size(); i++){
			if(!hills.getRegionCities().get(i).getCityName().equals(Emperor.getInstance().getKingsStartingCity())){
				
				hills.getRegionCities().get(i).setBonusTile(bonusTokens.get(k));
				//index k must be incremented only when a bonusToken is assigned 
				k++;
			}
		}
		for(int i = 0; i < mountains.getRegionCities().size(); i++){
			if(!mountains.getRegionCities().get(i).getCityName().equals(Emperor.getInstance().getKingsStartingCity())) {
				
				mountains.getRegionCities().get(i).setBonusTile(bonusTokens.get(k));
				k++;
			}
			
		}
	}
}
