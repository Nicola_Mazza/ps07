package it.polimi.ingsw.ps07.util;

import java.util.ArrayList;
import java.util.List;

/*
 * Singleton object to read the cities capital letters from the xml file.
 * It's primarily used to check if the city choices of the players
 * are correct
 */
public class CitiesCollection {

	private static CitiesCollection instance = new CitiesCollection();
	
	List<String> citiesCapitalLetters;
	
	/*
	 * Private constructor to make this a Singleton
	 */
	private CitiesCollection() {
		
		//Reads all the cities capital letters
		citiesCapitalLetters = new ArrayList<>();
		citiesCapitalLetters.addAll(ReaderXML.getInstance().readCitiesCapitalLetters("CoastA"));
		citiesCapitalLetters.addAll(ReaderXML.getInstance().readCitiesCapitalLetters("HillsA"));
		citiesCapitalLetters.addAll(ReaderXML.getInstance().readCitiesCapitalLetters("MountainsA"));
		
	}
	
	/*
	 * Getter to get the cities capital letters
	 */
	public List<String> getCitiesCapitalLetters() {
		
		return citiesCapitalLetters;
	}
	
	/*
	 * Method to get the single object of the class
	 */
	public static CitiesCollection getInstance() {
		
		return instance;
	}
}
