package it.polimi.ingsw.ps07.util;

/**
 * This class enumerates the pre-made maps for this game, for each map
 * it is possible to get the name of the file containing the map.
 *
 */
public enum ConfigMap {

	MAPA("docs/maps/mapA"),
	MAPB("docs/maps/mapB");
	
	private final String fileName;

    ConfigMap(String fileName) {

        this.fileName = fileName;
    }
    
    /**
     * Gets the name of the file the map (the file path).
     *
     * @return the file name
     */
    public String getFileName() {

        return fileName;
    }
}
