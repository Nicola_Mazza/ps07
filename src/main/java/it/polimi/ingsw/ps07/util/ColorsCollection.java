package it.polimi.ingsw.ps07.util;

import java.util.List;

/*
 * Singleton object to read the colors from the .xml file.
 * It's primarily used to check if the color choices of the players
 * are correct
 */
public class ColorsCollection {

	private static ColorsCollection instance = new ColorsCollection();
	
	List<String> colors;
	
	/*
	 * Private constructor to make this a Singleton
	 */
	private ColorsCollection() {
		
		//Reads the politics colors (councillor's colors too)
		colors = ReaderXML.getInstance().readPoliticsColorsString();
	}
	
	/*
	 * Getter to get the colors used in the game
	 */
	public List<String> getColors() {
		
		return colors;
	}
	
	/*
	 * Method to get the single object of the class
	 */
	public static ColorsCollection getInstance() {
		
		return instance;
	}
}
