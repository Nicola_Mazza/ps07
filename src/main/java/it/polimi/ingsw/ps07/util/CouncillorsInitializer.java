package it.polimi.ingsw.ps07.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.map.RegionBoard;

/**
 * 
 * Class to initialize all the councillors and
 * assign them to the balconies
 */
public class CouncillorsInitializer {
	
	private static final int COUNCILLORS_PER_COLOR = Integer.parseInt(ReaderXML.getInstance().readNumberOf("Councillors"));
	private static final int NUMBER_OF_COUNCILLORS_COLORS = Integer.parseInt(ReaderXML.getInstance().readConstant("cColors"));
	private static final int COUNCILLORS_PER_BALCONY = Integer.parseInt(ReaderXML.getInstance().readConstant("BalconyLength"));
	private List<Councillor> councillors;
	
	/**
	 * Initializes all the councillors and
	 * shuffle the list
	 */
	public CouncillorsInitializer() {
		
		councillors = new ArrayList<>();
		
		//Creates all the councillors reading the configuration file
		for(int i = 1; i <= NUMBER_OF_COUNCILLORS_COLORS; i++) {
			
			String hexColor = ReaderXML.getInstance().readCouncillorHexColor(i);
			String stringColor = ReaderXML.getInstance().readCouncillorStringColor(i);
			
			for(int j = 0; j < COUNCILLORS_PER_COLOR; j++) {
				councillors.add(new Councillor(hexColor, stringColor));
			}
		}
		
		//Shuffle the arrayList of councillors
		Collections.shuffle(councillors);
		
	}
	
	/**
	 * @param coast, the coast region
	 * @param hills, the hills region
	 * @param mountains, the mountains region
	 * @param kingsBalcony, the balcony of the king
	 * @return the remaining councillors that have not been
	 * assigned (the councillors pool)
	 */
	public List<Councillor> assignCouncillors(RegionBoard coast, RegionBoard hills, RegionBoard mountains,
			List<Councillor> kingsBalcony) {
		
		//assign COUNCILLORS_PER_BALCONY councillors to every balcony
		for(int i = 0; i < COUNCILLORS_PER_BALCONY; i++) {
			coast.getRegionBalcony().add(councillors.remove(0));
			hills.getRegionBalcony().add(councillors.remove(0));
			mountains.getRegionBalcony().add(councillors.remove(0));
			kingsBalcony.add(councillors.remove(0));
		}
		
		//returns the remaining councillors
		return councillors;
	}
}
