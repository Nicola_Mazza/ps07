package it.polimi.ingsw.ps07.util.exception;

/**
 * This exception is thrown when an invalid RequestMessage is
 * received.
 */
public class InvalidRequestMessageException extends RuntimeException {

	
	private static final long serialVersionUID = -5489096742751815909L;

	/**
	 * Constructs an InvalidRequestMessageException with no message details.
	 */
	public InvalidRequestMessageException() {
		
		super();
	}
	
	/**
	 * Constructs an InvalidRequestMessageException with the message details,
	 * specified in the string parameter.
	 */
	public InvalidRequestMessageException(String string) {
		
		super(string);
	}
}
