package it.polimi.ingsw.ps07.util;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.ps07.model.bonus.AssistantBonus;
import it.polimi.ingsw.ps07.model.bonus.Bonus;
import it.polimi.ingsw.ps07.model.bonus.BonusPermitTile;
import it.polimi.ingsw.ps07.model.bonus.BonusTokenBonus;
import it.polimi.ingsw.ps07.model.bonus.CardBonus;
import it.polimi.ingsw.ps07.model.bonus.CoinBonus;
import it.polimi.ingsw.ps07.model.bonus.PermitTileBonus;
import it.polimi.ingsw.ps07.model.bonus.SlowActionBonus;
import it.polimi.ingsw.ps07.model.bonus.VictoryBonus;
import it.polimi.ingsw.ps07.model.map.NobilityBonusCell;
import it.polimi.ingsw.ps07.model.map.Route;

/**
 * Class to initialize the nobility cells
 *
 */
public class NobilityBonusCellInitializer {
	
	private List<Bonus> nBonuses;
	private boolean bonusCreated;
	
	/**
	 * @param numberOfCell, the number of the cell that is being initialized
	 * @return the new cell initialized
	 */
	public NobilityBonusCell createNobilityBonusCell(int numberOfCell) {
		
		nBonuses = new ArrayList<>();
		bonusCreated = false;
		
		for (int maxNumber = 1; maxNumber <= Route.NUMBER_OF_NOBILITY_BONUS; maxNumber++){
			
			//reading of bonus name and bonus value of a nobility cell
			String bonusName = ReaderXML.getInstance().readNobilityBonus(numberOfCell,maxNumber); //reading the bonus from the .xml file
			int bonusValue = Integer.parseInt(ReaderXML.getInstance().readNobilityBonusValue(numberOfCell,maxNumber));
			
			//switch on bonus name only if the bonus exists, in other words, if what
			//has been read is not null
			if(!"null".equals(bonusName)) {
				bonusCreated = true;
				switchOnBonusName(bonusName, bonusValue);
			}
		}
		//if a bonus or 2 have been created, creates a nobility cell passing those bonus as a parameter
		if(bonusCreated) {
			return new NobilityBonusCell(nBonuses, numberOfCell);
		}else {
			return null;
		}
	}
	
	/**
	 * Creates the bonuses in line with what it's read from the file
	 * @param bonusName, the bonus name read from the file
	 * @param bonusValue, the value of the bonus read from the file
	 */
	private void switchOnBonusName(String bonusName, int bonusValue) {
		
		switch (bonusName) {
		case "NumberOfCoins":
			nBonuses.add(new CoinBonus(bonusValue));
			break;
		case "NumberOfVictoryPoints":
			nBonuses.add(new VictoryBonus(bonusValue));
			break;
		case "NumberOfAssistants":
			nBonuses.add(new AssistantBonus(bonusValue));
			break;
		case "NumberOfCards":
			nBonuses.add(new CardBonus(bonusValue));
			break;
		case "SlowAction":
			nBonuses.add(new SlowActionBonus(bonusValue));
			break;
		case "BonusRewardToken":
			nBonuses.add(new BonusTokenBonus(bonusValue));
			break;
		case "GetAPermitTile":
			nBonuses.add(new BonusPermitTile(bonusValue));
			break;
		case "GetAPermitTileBonus":
			nBonuses.add(new PermitTileBonus(bonusValue));
			break;
		default :
			break;
		}
	}
}
