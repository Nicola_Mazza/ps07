package it.polimi.ingsw.ps07;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.PlayersHand;

public class PlayerTest {

	private static Player player;
	private static PlayersHand playersHand;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@BeforeClass
	public static void initClass() {
		
		player = new Player("test");
		player.setPlayerNumber(5);
		playersHand = player.getPlayersHand();
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(player);
	}
	
	@Test
	public void shouldCreatePlayersHand() {
		
		assertNotNull(playersHand);
	}
	
	@Test
	public void shouldGetPlayerID() {
		
		assertEquals("test", player.getPlayerID());
	}
	
	@Test
	public void shouldGetPlayerNumber() {
		
		assertEquals(5, player.getPlayerNumber());
	}
	
	
}
