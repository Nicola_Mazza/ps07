package it.polimi.ingsw.ps07;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;
import it.polimi.ingsw.ps07.model.market.Insertion;
import it.polimi.ingsw.ps07.model.market.Market;

public class MarketTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Market market;
	
	private static List<Insertion> permitTileInsertions;
	private static List<Insertion> politicsCardInsertions;
	private static List<Insertion> assistantsInsertions;
	
	private static PermitTile permitTile;
	private static PoliticsCard politicsCard;
	
	
	@BeforeClass
	public static void initClass() {
		
		market = new Market();
		
		market.addAssistantsInsertion(4, 0, 10);
		market.addPermitTileInsertion(permitTile, 0, 11);
		market.addPoliticsCardInsertion(politicsCard, 0, 12);
		
		permitTileInsertions = market.getInsertionsArray("PT");
		politicsCardInsertions = market.getInsertionsArray("PC");
		assistantsInsertions = market.getInsertionsArray("A");
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(market);
	}
	
	@Test
	public void shouldGetInsertionsArray() {
		
		assertEquals(4, Integer.parseInt(assistantsInsertions.get(0).getInsertionObject().toString()));
		assertEquals(0, assistantsInsertions.get(0).getPlayerID());
		assertEquals(10, assistantsInsertions.get(0).getPrice());
		
		assertEquals(permitTile, permitTileInsertions.get(0).getInsertionObject());
		assertEquals(0, permitTileInsertions.get(0).getPlayerID());
		assertEquals(11, permitTileInsertions.get(0).getPrice());
		
		assertEquals(politicsCard, politicsCardInsertions.get(0).getInsertionObject());
		assertEquals(0, politicsCardInsertions.get(0).getPlayerID());
		assertEquals(12, politicsCardInsertions.get(0).getPrice());
	}
}
