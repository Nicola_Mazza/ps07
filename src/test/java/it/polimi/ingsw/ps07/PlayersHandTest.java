package it.polimi.ingsw.ps07;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.PlayersHand;
import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;
import it.polimi.ingsw.ps07.model.cards.KingsRewardTile;
import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.cards.PoliticsCard;

public class PlayersHandTest {

	private static PlayersHand playersHand;
	private static List<KingsRewardTile> kingsRewardTiles;
	private static List<ColorBonusTile> colorBonusTiles;
	private static List<PoliticsCard> politicsCards;
	private static List<PermitTile> permitTiles;
	private static List<PermitTile> usedPermitTiles;
	private static PoliticsCard politicsCard;
	private static PermitTile permitTile;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@BeforeClass
	public static void initClass() {
		
		playersHand = new PlayersHand("test");
		
		politicsCard = new PoliticsCard();
		permitTile = new PermitTile("Coast", 1);
		
		playersHand.setAssistantCounter(4);
		playersHand.increaseAssistantCounter(3);
		playersHand.decreaseAssistantCounter(2);
		
		playersHand.setEmporiumCounter(PlayersHand.EMPORIUM_INIT);
		playersHand.decreaseEmporiumCounter();
		
		playersHand.setCoins(PlayersHand.COINS_INIT);
		playersHand.acquireCoins(3);
		playersHand.spendCoins(4);
		
		playersHand.setVictoryPoints(10);
		playersHand.increaseVictoryPoints(2);

		playersHand.progressOntoNobilityPath(4);
		
		playersHand.addPermitTile(permitTile);
		playersHand.addPoliticsCard(politicsCard);
		
		kingsRewardTiles = playersHand.getKingsRewardTiles();
		colorBonusTiles = playersHand.getColorBonusTiles();
		politicsCards = playersHand.getPoliticsCards();
		permitTiles = playersHand.getPermitTiles();
		usedPermitTiles = playersHand.getUsedPermitTiles();
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(playersHand);
	}
	
	@Test
	public void shouldGetAssistantCounter() {
		
		assertEquals(5, playersHand.getAssistantCounter());
	}
	
	@Test
	public void shouldGetEmporiumCounter() {
		
		assertEquals(PlayersHand.EMPORIUM_INIT - 1, playersHand.getEmporiumCounter());
	}
	
	@Test
	public void shouldGetCoins() {
		
		assertEquals(PlayersHand.COINS_INIT - 1, playersHand.getCoins());
	}
	
	@Test
	public void shouldGetVictoryPoints() {
		
		assertEquals(12, playersHand.getVictoryPoints());
	}
	
	@Test
	public void shouldGetNobilityProgression() {
		
		assertEquals(4, playersHand.getNobilityProgression());
	}
	
	@Test
	public void shouldCreateKingsRewardTiles() {
		
		assertNotNull(kingsRewardTiles);
	}
	
	@Test
	public void shouldCreateColorBonusTiles() {
		
		assertNotNull(colorBonusTiles);
	}
	
	@Test
	public void shouldCreatePoliticsCards() {
		
		assertNotNull(politicsCards);
		assertEquals(1, politicsCards.size());
	}
	
	@Test
	public void shouldCreatePermitTiles() {
		
		assertNotNull(permitTiles);
		assertEquals(1, permitTiles.size());
	}
	
	@Test
	public void shouldCreateUsedPermitTiles() {
		
		assertNotNull(usedPermitTiles);
	}
}
