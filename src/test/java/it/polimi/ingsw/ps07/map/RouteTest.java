package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.map.Route;

public class RouteTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Route route;
	
	@BeforeClass
	public static void initClass() {
		
		route = new Route();
		
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(route);
		
		// in the configuration file we have a bonus at the index 3.
		assertNotNull(route.getNobilityCell(3));
	}
}
