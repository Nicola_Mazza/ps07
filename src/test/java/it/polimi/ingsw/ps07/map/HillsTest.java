package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.map.Hills;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class HillsTest {

	public ExpectedException thrown = ExpectedException.none();
	
	private static Hills hills;
	
	@BeforeClass
	public static void initClass() {
		
		hills = new Hills(3, ConfigMap.MAPA.toString());
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(hills);
	}
}
