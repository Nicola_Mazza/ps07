package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.map.Emporium;

public class EmporiumTest {

	
	public ExpectedException thrown = ExpectedException.none();
	private static Emporium emporium;
	
	@BeforeClass
	public static void initGame() {
		
		emporium = new Emporium();
		emporium.setBuilt();
		emporium.setPlayerID(2);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(emporium);
	}
	
	@Test
	public void shouldBeBuilt() {
		
		assertTrue(emporium.isBuilt());
	}
	
	@Test
	public void shouldGetPlayerID() {
		
		assertEquals(2, emporium.getPlayerID());
	}
	
	@Test
	public void testToString() {
		
		assertFalse(emporium.toString().contains("@"));
	}
}
