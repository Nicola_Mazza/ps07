package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.map.Coast;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class CoastTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Coast coast;
	
	@BeforeClass
	public static void initClass() {
	
		coast = new Coast(2, ConfigMap.MAPA.toString());
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(coast);
	}
}
