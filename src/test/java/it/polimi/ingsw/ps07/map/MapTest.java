package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.map.CitiesGraph;
import it.polimi.ingsw.ps07.model.map.Map;
import it.polimi.ingsw.ps07.model.map.RegionBoard;
import it.polimi.ingsw.ps07.util.ConfigMap;
import it.polimi.ingsw.ps07.util.ReaderXML;


public class MapTest {

	
	public ExpectedException thrown = ExpectedException.none();
	private static Map map;
	private static List<Councillor> kingsBalcony;
	private static List<Councillor> councillorsPool;
	private static CitiesGraph citiesGraph;
	private static RegionBoard coast;
	private static RegionBoard hills;
	private static RegionBoard mountains;
	
	
	@BeforeClass
	public static void initClass() {
		
		map = new Map(3, ConfigMap.MAPA);
		
		coast = map.getCoast();
		hills = map.getHills();
		mountains = map.getMountains();
		kingsBalcony = map.getKingsBalcony();
		councillorsPool = map.getCouncillorsPool();
		citiesGraph = map.getCitiesGraph();
	}
	
	@Test
    public void shouldBeCreated() throws Exception {

        assertNotNull(map);
    }
	
	@Test
	public void shouldCreateKingsBalcony() {
		
		assertNotNull(kingsBalcony);
		assertTrue(kingsBalcony.size() == Integer.parseInt(ReaderXML.getInstance().readConstant("BalconyLength")));
	}
	
	@Test
	public void shouldCreateCouncillorsPool() {
		
		assertNotNull(councillorsPool);
	}
	
	@Test
	public void shouldCreateCoast() {
		
		assertNotNull(coast);
		assertTrue(map.getAllCities().containsAll(coast.getRegionCities()));
	}
	
	@Test
	public void shouldCreateHills() {
		
		assertNotNull(hills);
		assertTrue(map.getAllCities().containsAll(hills.getRegionCities()));
	}
	
	@Test
	public void shouldCreateMountains() {
		
		assertNotNull(mountains);
		assertTrue(map.getAllCities().containsAll(mountains.getRegionCities()));
	}
	
	@Test
	public void shouldCreateCitiesGraph() {
		
		assertNotNull(citiesGraph);
	}
		
}
