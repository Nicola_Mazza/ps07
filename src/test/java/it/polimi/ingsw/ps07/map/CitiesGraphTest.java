package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.graph.SimpleGraph;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.map.CitiesGraph;
import it.polimi.ingsw.ps07.model.map.City;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class CitiesGraphTest {

	public ExpectedException thrown = ExpectedException.none();
	private static CitiesGraph citiesGraph;
	
	private static List<City> list1;
	private static List<City> list2;
	private static List<City> list3;
	

	private static Game game;
	private static List<Token> tokens;
	
	private static Token token1, token2;
	
	private static City city1;
	private static City city2;
	private static List<City> citiesToNavigate;
	private static List<City> adiacentCities;
	
	private static Player player;

	
	@BeforeClass
	public static void initClass() throws Exception {
		
		token1 = new Token("1");
		token2 = new Token("2");
		
		tokens = new ArrayList<>();
		tokens.add(token1);
		tokens.add(token2);
		
		game = new Game(tokens, ConfigMap.MAPA);
		
		citiesToNavigate = new ArrayList<>();
		
		citiesGraph = game.getMap().getCitiesGraph();
		
		player = game.getPlayers().get(0);
		
		city1 = game.getMap().getCoast().getRegionCities().get(0);
		city2 = game.getMap().getCoast().getRegionCities().get(1);
		
		city1.buildEmporium(Integer.parseInt(player.getPlayerID()));
		city2.buildEmporium(Integer.parseInt(player.getPlayerID()));
		
		adiacentCities = new ArrayList<>();
		adiacentCities.add(city1);
		adiacentCities.add(city2);
			
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(citiesGraph);
	}
	
	@Test
	public void shouldGetGraph() {
		
		assertTrue(citiesGraph.getGraph() instanceof SimpleGraph);
	}
	
}
