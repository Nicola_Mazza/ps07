package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.Councillor;
import it.polimi.ingsw.ps07.model.cards.RegionBonusTile;
import it.polimi.ingsw.ps07.model.map.City;
import it.polimi.ingsw.ps07.model.map.Coast;
import it.polimi.ingsw.ps07.model.map.RegionBoard;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class RegionBoardTest {

	public ExpectedException thrown = ExpectedException.none();
	private static RegionBoard regionBoard;
	private static List<City> regionCities;
	private static List<Councillor> balcony;
	private static RegionBonusTile rBonus;
	
	@BeforeClass
	public static void initClass() {
		
		regionBoard = new Coast(2, ConfigMap.MAPA.toString());
		
		regionCities = new ArrayList<>();
		balcony = new ArrayList<>();
		
		regionCities = regionBoard.getRegionCities();
		balcony = regionBoard.getRegionBalcony();
		rBonus = regionBoard.getRegionBonusTile();
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(regionBoard);
	}
	
	@Test
	public void shouldGetRegionCities() {
		
		assertNotNull(regionCities);
	}
	
	@Test
	public void shouldGetRegionBalcony() {
		
		assertNotNull(balcony);
	}
	
	@Test
	public void shouldGetRegionBonusTile() {
		
		assertNotNull(rBonus);
	}
}
