package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import it.polimi.ingsw.ps07.model.map.City;
import it.polimi.ingsw.ps07.util.ReaderXML;

public class CityTest {

	private static City city;	
	private static String cityName;
	private static String cityColor;
	private static String cityColorString;
	
	@BeforeClass
	public static void initClass() {
		
		cityName = ReaderXML.getInstance().readCities("Coast", 3, 'A').get(0).getCityName();
		cityColor = "#00AEEF";
		cityColorString = ReaderXML.getInstance().readCities("Coast", 3, 'A').get(0).getColor();
		
		city = new City(cityName, cityColor, cityColorString, 3);
	}
	
	@Test
    public void shouldBeCreated() throws Exception {

        assertNotNull(city);

    }

    @Test
    public void shouldGetName() {

        assertEquals(cityName, city.getCityName());
    }
    
    @Test
    public void shouldGetColorString() {
    	
    	assertEquals(cityColorString, city.getColor());
    }

}