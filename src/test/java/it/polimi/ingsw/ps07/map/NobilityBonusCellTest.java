package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.bonus.Bonus;
import it.polimi.ingsw.ps07.model.map.NobilityBonusCell;

public class NobilityBonusCellTest {

	public ExpectedException thrown = ExpectedException.none();
	private static NobilityBonusCell nCell;
	private static List<Bonus> nobilityBonuses;
	private static int nobilityIndex;
	
	@BeforeClass
	public static void initClass() {
		
		nobilityBonuses = new ArrayList<>();
		nCell = new NobilityBonusCell(nobilityBonuses, 2);
		nobilityIndex = nCell.getNobilityIndex();
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(nCell);
	}
	
	@Test
	public void shouldGetNobilityIndex() {
		
		assertEquals(2, nobilityIndex);
	}
	
	@Test
	public void shouldGetNobilityBonus() {
		
		assertEquals(nobilityBonuses, nCell.getNobilityBonus());
	}
}
