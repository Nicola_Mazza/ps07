package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.map.Emperor;
import it.polimi.ingsw.ps07.model.map.Map;

public class EmperorTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Emperor emperor;
	
	@BeforeClass
	public static void initClass() {
		
		emperor = Emperor.getInstance();
		emperor.moveEmperor("Lyram");
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(emperor);
	}
	
	@Test
	public void shouldGetKingsStartingCity() {
		
		assertEquals(Map.KINGS_STARTING_CITY, emperor.getKingsStartingCity());
	}
	
	@Test
	public void shouldGetCurrentCity() {
		
		assertEquals("Lyram", emperor.getCurrentCity());
	}
}
