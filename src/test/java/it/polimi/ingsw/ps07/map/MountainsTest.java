package it.polimi.ingsw.ps07.map;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.map.Mountains;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class MountainsTest {

	public ExpectedException thrown = ExpectedException.none();
	
	private static Mountains mountains;
	
	@BeforeClass
	public static void initClass() {
		
		mountains = new Mountains(5, ConfigMap.MAPB.toString());
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(mountains);
	}
}
