package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.PoliticsCard;

public class PoliticsCardTest {

	public ExpectedException thrown = ExpectedException.none();
	private static PoliticsCard pCard;
	
	@BeforeClass
	public static void initClass() {
	
		pCard = new PoliticsCard("#00AEEF", "CYAN");
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(pCard);
	}
	
	@Test
	public void testIsJolly() {
		
		assertTrue(!pCard.isJolly());
	}
	
	@Test
	public void getColor() {
		
		assertEquals(Color.decode("#00AEEF"), pCard.getColor());
	}
	
	@Test
	public void getStringColor() {
		
		assertEquals("CYAN", pCard.getStringColor());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!pCard.toString().contains("@"));
	}
}
