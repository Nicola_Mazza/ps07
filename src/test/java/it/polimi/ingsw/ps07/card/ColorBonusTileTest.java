package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.ColorBonusTile;

public class ColorBonusTileTest {
	
	public ExpectedException thrown = ExpectedException.none();
	private static ColorBonusTile cbBonus;

	
	@BeforeClass
	public static void initClass() {
		
		cbBonus = new ColorBonusTile("#00AEEF", "CYAN", 5);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(cbBonus);
	}
	
	@Test
	public void shouldGetStringColor() {
		
		assertEquals("CYAN", cbBonus.getStringColor());
	}
	
	@Test
	public void shouldGetColor() {
		
		assertEquals(Color.decode("#00AEEF"), cbBonus.getColor());
	}
	
	@Test
	public void shouldGetBonusValue() {
		
		assertEquals(5, cbBonus.getBonusValue());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!cbBonus.toString().contains("@"));
	}
}
