package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.PermitTile;

public class PermitTileTest {

	public ExpectedException thrown = ExpectedException.none();
	private static PermitTile permitTile;
	
	@BeforeClass
	public static void initClass() {
		
		permitTile = new PermitTile("Coast", 1);
		
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(permitTile);
	}
	
	@Test
	public void shouldGetIndex() {
		
		assertEquals(1, permitTile.getIndex());
	}
	
	@Test
	public void shouldGetRegionName() {
		
		assertEquals("Coast", permitTile.getRegionName());
	}
	
	@Test
	public void shouldCreateTileCities() {
		
		assertNotNull(permitTile.getTileCities());
	}
	
	@Test
	public void shouldCreateTileBonuses() {
		
		assertNotNull(permitTile.getTileBonuses());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!permitTile.toString().contains("@"));
	}
}
