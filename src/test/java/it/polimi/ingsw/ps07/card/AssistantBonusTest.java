package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.bonus.AssistantBonus;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class AssistantBonusTest {

	public ExpectedException thrown = ExpectedException.none();
	private static AssistantBonus aBonus;
	private static Game game;
	private static Token token1, token2;
	
	private static List<Token> tokens;
	
	@BeforeClass
	public static void initClass() throws Exception {
		
		aBonus = new AssistantBonus(5);
		tokens = new ArrayList<>();
		
		token1 = new Token("0");
		token2 = new Token("1");
		
		tokens.add(token1);
		tokens.add(token2);
		game = new Game(tokens, ConfigMap.MAPA);
		aBonus.assignBonus(game);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(aBonus);
	}
	
	@Test
	public void shouldAssignBonus() {
		
		// the first players starts with 1 assistant, the second one with 2 and so on.
		assertEquals(6, game.getCurrentPlayer().getPlayersHand().getAssistantCounter());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!aBonus.toString().contains("@"));
	}
}
