package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.Councillor;

public class CouncillorTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Councillor councillor;
	
	@BeforeClass
	public static void initClass() {
		
		councillor = new Councillor("#00AEEF", "CYAN");
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(councillor);
	}
	
	@Test
	public void shouldGetColor() {
		
		assertEquals(Color.decode("#00AEEF"), councillor.getColor());
	}
	
	@Test
	public void shouldGetStringColor() {
		
		assertEquals("CYAN", councillor.getStringColor());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!councillor.toString().contains("@"));
	}
}
