package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.KingsRewardTile;

public class KingsRewardTileTest {

	public ExpectedException thrown = ExpectedException.none();
	private static KingsRewardTile kingsRewardTile;
	
	@BeforeClass
	public static void initGame() {
		
		kingsRewardTile = new KingsRewardTile(99);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(kingsRewardTile);
	}
	
	@Test
	public void shouldGetValue() {
		
		assertEquals(99, kingsRewardTile.getValue());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!kingsRewardTile.toString().contains("@"));
	}
}
