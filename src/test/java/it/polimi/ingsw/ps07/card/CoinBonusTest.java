package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.PlayersHand;
import it.polimi.ingsw.ps07.model.bonus.CoinBonus;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class CoinBonusTest {
	
	public ExpectedException thrown = ExpectedException.none();
	private static CoinBonus cBonus;
	private static Game game;
	private static Token token1, token2;
	
	private static List<Token> tokens;
	
	@BeforeClass
	public static void initClass() throws Exception {
		
		cBonus = new CoinBonus(5);
		tokens = new ArrayList<>();
		
		token1 = new Token("0");
		token2 = new Token("1");
		
		tokens.add(token1);
		tokens.add(token2);
		game = new Game(tokens, ConfigMap.MAPA);
		cBonus.assignBonus(game);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(cBonus);
	}
	
	@Test
	public void shouldAssignBonus() {
		
		// the first players starts with 1 assistant, the second one with 2 and so on.
		assertEquals(PlayersHand.COINS_INIT + 5, game.getCurrentPlayer().getPlayersHand().getCoins());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!cBonus.toString().contains("@"));
	}
}
