package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.RegionBonusTile;

public class RegionBonusTileTest {

	public ExpectedException thrown = ExpectedException.none();
	private static RegionBonusTile rbTile;
	
	@BeforeClass
	public static void initClass() {
		
		rbTile = new RegionBonusTile();
		rbTile.setAssigned();
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(rbTile);
	}
	
	@Test
	public void testIsAssigned() {
		
		assertTrue(rbTile.isAssigned());
	}
	
	@Test
	public void shouldGetBonusValue() {
		
		assertEquals(5, rbTile.getBonusValue());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!rbTile.toString().contains("@"));
	}
}
