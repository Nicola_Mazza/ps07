package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.PlayersHand;
import it.polimi.ingsw.ps07.model.bonus.CardBonus;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class CardBonusTest {

	public ExpectedException thrown = ExpectedException.none();
	private static CardBonus cBonus;
	private static Game game;
	private static Token token1, token2;
	
	private static List<Token> tokens;
	
	@BeforeClass
	public static void initClass() throws Exception {
		
		cBonus = new CardBonus(6);
		tokens = new ArrayList<>();
		
		token1 = new Token("0");
		token2 = new Token("1");
		
		tokens.add(token1);
		tokens.add(token2);
		game = new Game(tokens, ConfigMap.MAPA);
		cBonus.assignBonus(game);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(cBonus);
	}
	
	@Test
	public void shouldAssignBonus() {
		
		// +1 is the card drawn at the beginning of each turn
		assertEquals(PlayersHand.STARTING_NUMBER_OF_POLITICS_CARDS + 1 + 6,
				game.getCurrentPlayer().getPlayersHand().getPoliticsCards().size());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!cBonus.toString().contains("@"));
	}
}
