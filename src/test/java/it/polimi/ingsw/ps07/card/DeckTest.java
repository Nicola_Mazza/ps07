package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.Deck;

public class DeckTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Deck deck;
	
	@BeforeClass
	public static void classInit() {
	
		deck = new Deck();
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(deck);
	}
	
	@Test
	public void shouldCreatePoliticsDeck() {
		
		assertNotNull(deck.getPoliticsDeck());
	}
	
	@Test
	public void shouldDrawAPoliticsCard() {
		
		assertNotNull(deck.drawAPoliticsCard());
	}
	
	@Test
	public void shouldCreatePermitTileDeckCoast() {
		
		assertNotNull(deck.getPermitTileDeckCoast());
	}
	
	@Test
	public void shouldCreatePermitTileDeckHills() {
		
		assertNotNull(deck.getPermitTileDeckHills());
	}
	
	@Test
	public void shouldCreatePermitTileDeckMountains() {
		
		assertNotNull(deck.getPermitTileDeckMountains());
	}
	
}
