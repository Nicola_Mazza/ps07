package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.bonus.VictoryBonus;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class VictoryBonusTest {

	public ExpectedException thrown = ExpectedException.none();
	private static VictoryBonus vBonus;
	
	private static Game game;
	private static Token token1, token2;
	
	private static List<Token> tokens;
	
	@BeforeClass
	public static void initClass() throws Exception {
		
		vBonus = new VictoryBonus(59);
		tokens = new ArrayList<>();
		
		token1 = new Token("0");
		token2 = new Token("1");
		
		tokens.add(token1);
		tokens.add(token2);
		game = new Game(tokens, ConfigMap.MAPA);
		vBonus.assignBonus(game);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(vBonus);
	}
	
	@Test
	public void shouldAssignBonus() {
		
		assertEquals(59, game.getCurrentPlayer().getPlayersHand().getVictoryPoints());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!vBonus.toString().contains("@"));
	}
}
