package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.Assistant;

public class AssistantTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Assistant assistant;
	
	@BeforeClass
	public static void initClass() {
		
		assistant = new Assistant(42);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(assistant);
	}
	
	@Test
	public void shouldGetNumberOfAssistants() {
		
		assertEquals(42, assistant.getNumberOfAssistants());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!assistant.toString().contains("@"));
	}
}
