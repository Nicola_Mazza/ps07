package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.bonus.NobilityBonus;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class NobilityBonusTest {

	public ExpectedException thrown = ExpectedException.none();
	private static NobilityBonus nBonus;
	
	private static Game game;
	private static Token token1, token2;
	
	private static List<Token> tokens;
	
	@BeforeClass
	public static void initClass() throws Exception {
		
		nBonus = new NobilityBonus(144);

		tokens = new ArrayList<>();	
		token1 = new Token("0");
		token2 = new Token("1");
		
		tokens.add(token1);
		tokens.add(token2);
		game = new Game(tokens, ConfigMap.MAPA);
		nBonus.assignBonus(game);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(nBonus);
	}
	
	@Test
	public void testIsNobilityBonus() {
		
		assertTrue(nBonus.isNobilityBonus());
	}
	
	@Test
	public void shouldAssignBonus() {
		
		assertEquals(144, game.getCurrentPlayer().getPlayersHand().getNobilityProgression());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!nBonus.toString().contains("@"));
	}
}
