package it.polimi.ingsw.ps07.card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.bonus.AssistantBonus;
import it.polimi.ingsw.ps07.model.bonus.Bonus;
import it.polimi.ingsw.ps07.model.bonus.BonusToken;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class BonusTokenTest {

	public ExpectedException thrown = ExpectedException.none();
	
	private static BonusToken btBonus;
	private static Bonus bonus;
	private static List<Bonus> bonuses;
	
	@BeforeClass
	public static void initClass() throws Exception {
		
		bonus = new AssistantBonus(5);	
		bonuses = new ArrayList<>();
		bonuses.add(bonus);
		btBonus = new BonusToken(bonuses);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(btBonus);
	}
	
	@Test
	public void shouldGetBonuses() {
		
		assertEquals(bonus.getBonusValue(), bonuses.get(0).getBonusValue());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!btBonus.toString().contains("@"));
	}
}
