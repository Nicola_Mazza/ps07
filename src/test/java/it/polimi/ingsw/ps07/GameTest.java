package it.polimi.ingsw.ps07;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.xml.sax.SAXException;

import it.polimi.ingsw.ps07.controller.Token;
import it.polimi.ingsw.ps07.model.Game;
import it.polimi.ingsw.ps07.model.Player;
import it.polimi.ingsw.ps07.model.cards.Deck;
import it.polimi.ingsw.ps07.model.map.Map;
import it.polimi.ingsw.ps07.model.market.Market;
import it.polimi.ingsw.ps07.util.ConfigMap;

public class GameTest {

	private static Game game;
	private static Map map;
	private static Deck deck;
	private static Market market;
	private static List<Player> players;
	private static Player player;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@BeforeClass
	public static void initClass() throws ParserConfigurationException, SAXException, IOException {
		
		List<Token> tokens = new ArrayList<>();
		tokens.add(new Token("test"));
		
		game = new Game(tokens, ConfigMap.MAPA);
		
		map = game.getMap();
		deck = game.getDeck();
		market = game.getMarket();
		players = game.getPlayers();
		
		game.setCurrentPlayer(player);
		game.setGameID(1);
		game.setTurnNumber(2);
		game.increaseRoundNumber();
		
	}
	
	@Test
	public void shouldBeCreated() throws Exception {
		
		assertNotNull(game);
	}
	
	@Test
	public void shouldCreateMap() {
		
		assertNotNull(map);
	}
	
	@Test
	public void shouldCreateDeck() {
		
		assertNotNull(deck);
	}
	
	@Test
	public void shouldCreateMarket() {
		
		assertNotNull(market);
	}
	
	@Test
	public void shouldCreatePlayers() {
		
		assertNotNull(players);
	}
	
	@Test
	public void shouldGetGameID() {
		
		assertEquals(1, game.getGameID());
	}
	
	@Test
	public void shouldGetTurnNumber() {
		
		assertEquals(2, game.getTurnNumber());
	}
	
	@Test
	public void shouldGetRoundNumber() {
		
		assertEquals(2, game.getRoundNumber());
	}
	
	@Test
	public void shouldGetNumberOfPlayers() {
		
		assertEquals(1, game.getNumberOfPlayers());
		assertEquals(players.size(), game.getNumberOfPlayers());
	}
	
	@Test
	public void shouldGetChosenMap() {
		
		assertEquals("MAPA", game.getChosenMap().toString());
	}
}
