package it.polimi.ingsw.ps07;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.market.AssistantsInsertion;
import it.polimi.ingsw.ps07.model.market.Insertion;

public class InsertionTest {

	public ExpectedException thrown = ExpectedException.none();
	private static Insertion insertion;
	
	@BeforeClass
	public static void initClass() {
		insertion = new AssistantsInsertion(4, 0, 3);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(insertion);
	}
	
	@Test
	public void shouldGetPlayedID() {
		
		assertEquals(0, insertion.getPlayerID());
	}
	
	@Test
	public void shouldGetPrice() {
		
		assertEquals(3, insertion.getPrice());
	}
}
