package it.polimi.ingsw.ps07;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.PoliticsCard;
import it.polimi.ingsw.ps07.model.market.PoliticsCardInsertion;

public class PoliticsCardInsertionTest {


	public ExpectedException thrown = ExpectedException.none();
	private static PoliticsCardInsertion pInsertion;
	private static PoliticsCard politicsCard;
	
	@BeforeClass
	public static void initClass() {
		
		politicsCard = new PoliticsCard();
		pInsertion = new PoliticsCardInsertion(politicsCard, 0, 3);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(pInsertion);
	}
	
	@Test
	public void shouldGetInsertionObject() {
		
		assertEquals(politicsCard, pInsertion.getInsertionObject());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!pInsertion.toString().contains("@"));
	}
}
