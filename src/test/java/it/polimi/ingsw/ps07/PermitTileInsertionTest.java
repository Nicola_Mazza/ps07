package it.polimi.ingsw.ps07;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.cards.PermitTile;
import it.polimi.ingsw.ps07.model.market.PermitTileInsertion;

public class PermitTileInsertionTest {


	public ExpectedException thrown = ExpectedException.none();
	
	private static PermitTile permitTile;
	private static PermitTileInsertion pInsertion;
	
	@BeforeClass
	public static void initClass() {
		permitTile = new PermitTile("Coast", 1);
		pInsertion = new PermitTileInsertion(permitTile, 0, 3);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(pInsertion);
	}
	
	@Test
	public void shouldGetInsertionObject() {
		
		assertEquals(permitTile, pInsertion.getInsertionObject());
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!pInsertion.toString().contains("@"));
	}
}
