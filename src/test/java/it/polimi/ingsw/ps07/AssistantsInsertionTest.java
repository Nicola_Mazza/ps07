package it.polimi.ingsw.ps07;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import it.polimi.ingsw.ps07.model.market.AssistantsInsertion;

public class AssistantsInsertionTest {

	public ExpectedException thrown = ExpectedException.none();
	private static AssistantsInsertion aInsertion;
	
	@BeforeClass
	public static void initClass() {
		
		aInsertion = new AssistantsInsertion(4, 0, 3);
	}
	
	@Test
	public void shouldBeCreated() {
		
		assertNotNull(aInsertion);
	}
	
	@Test
	public void shouldGetInsertionObject() {
		
		assertEquals(4, Integer.parseInt(aInsertion.getInsertionObject().toString()));
	}
	
	@Test
	public void testToString() {
		
		assertTrue(!aInsertion.toString().contains("@"));
	}
	
}
